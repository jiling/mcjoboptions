#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process          = "bbH H->mumu"
evgenConfig.description      = 'POWHEG+Herwig7 bbH production with H7UE tune'
evgenConfig.keywords         = [ "SM", "Higgs", "SMHiggs", "2muon", "mH125"  ]
evgenConfig.generators       += [ 'Powheg', 'Herwig7'  ]
evgenConfig.contact          = [ 'xinmeng.ye@cern.ch'  ]
evgenConfig.tune             = "H7UE"
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob    = 10000

#--------------------------------------------------------------
# Herwig showering
#--------------------------------------------------------------
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC21_40_pdfas")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# only consider H->ZZ decays
Herwig7Config.add_commands("""
# force H->mumu decays
do /Herwig/Particles/h0:SelectDecayModes h0->mu-,mu+;
# print out Higgs decays modes and branching ratios to the terminal/log.generate
do /Herwig/Particles/h0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

