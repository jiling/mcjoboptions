#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "ggH FxFx, point-like coupling in infinite top mass limit, H->yy"
evgenConfig.keywords = ["SM","Higgs"]
evgenConfig.contact = ["ana.cueto@cern.ch"]
evgenConfig.generators = ["MadGraph","Pythia8"]
evgenConfig.nEventsPerJob = 10000



# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

gridpack_mode=True


if not is_gen_from_gridpack():
    process = """
    import model HC_NLO_X0_UFO-heft
    define p = g u c b d s u~ c~ d~ s~ b~
    define j = g u c b d s u~ c~ d~ s~ b~
    generate p p > x0 / t a [QCD] @0
    add process p p > x0 j / t a [QCD] @1
    add process p p > x0 j j / t a [QCD] @2
    output -f
    """

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION


#Fetch default LO run_card.dat and set parameters
settings = {'parton_shower':'PYTHIA8',
            'req_acc':0.001,
            'ickkw':3,
            'ptj'  :10,
            'maxjetflavor': 5,
            'mll_sf'        : 10.0,
            #'reweight_scale': 'True',
            'muR_ref_fixed' : 125.0,
            'muF1_ref_fixed': 125.0,
            'muF2_ref_fixed': 125.0,
            'QES_ref_fixed' : 125.0,
            'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
#modify_config_card(process_dir=process_dir,settings={'cluster_type':'condor','cluster_queue':'workday'})

masses={'25': '1.250000e+02'}
frblock= {               'kAza'  : 0.0,
                            'kAgg'  : 0.0,
                            'kAaa'  : 0.0,
                            'cosa'  : 1.0,
                            'kHdwR' : 0.0,
                            'kHaa'  : 1.0,
                            'kAll'  : 0.0,
                            'kHll'  : 0.0,
                            'kAzz'  : 0.0,
                            'kSM'   : 1.0,
                            'kHdwI' : 0.0,
                            'kHdz'  : 0.0,
                            'kAww'  : 0.0,
                            'kHgg'  : 1.0,
                            'kHda'  : 0.0,
                            'kHza'  : 0.0,
                            'kHww'  : 0.0,
                            'kHzz'  : 0.0,
                            'Lambda': 1000.0
}

params={}
params['MASS']=masses
params['frblock']=frblock
modify_param_card(process_dir=process_dir,params=params)

#input_events=process_dir+'/Events/GridRun_'+str(runArgs.randomSeed)+'/events.lhe.gz'
madspin_card=process_dir+'/Cards/madspin_card.dat'
#if os.access(madspin_card,os.R_OK):
#    os.unlink(madspin_card)
fMadSpinCard = open(madspin_card,'w')
#fMadSpinCard.write('import '+input_events+'\n')
#fMadSpinCard.write('set ms_dir '+process_dir+'/MadSpin\n')
fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
fMadSpinCard.write('''set Nevents_for_max_weight 250 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event (default: 400)
set spinmode none
decay x0 > a a
launch''')
fMadSpinCard.close()


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

############################
# Shower JOs will go here
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
# Matching settings
genSeq.Pythia8.Commands += [
    "JetMatching:setMad           = off",
    "JetMatching:merge            = on",
    "JetMatching:scheme           = 1",
    "JetMatching:nQmatch          = 5",     # Should match maxjetflavour in 'mg5'.
    "JetMatching:jetAlgorithm     = 2",
    "JetMatching:slowJetPower     = 1",
    "JetMatching:clFact           = 1.0",
    "JetMatching:eTjetMin         = 40.0",  # Should match qcut.
    "JetMatching:coneRadius       = 1.0",   # Default.
    "JetMatching:etaJetMax        = 4.5",
    "JetMatching:exclusive        = 1",     # Exclusive - all PS jets must match HS jets.
    "JetMatching:nJetMax          = 2",     # *** Must match highest Born-level jet multiplicity. ***
    "JetMatching:nJet             = 0",
    "JetMatching:jetAllow         = 1",
    "JetMatching:doShowerKt       = off",
    "JetMatching:qCut             = 40.0",  # Merging scale for FxFx.
    "JetMatching:doFxFx           = on",
    "JetMatching:qCutME           = 10.0"   # Should match ptj.
]
