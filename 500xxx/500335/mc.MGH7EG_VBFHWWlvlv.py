######################################################################
# fix check_poles issue with helper function
######################################################################

from tempfile import mkstemp
from shutil import move
from os import remove, close

def replace(file_path, pattern, subst):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    remove(file_path)
    move(abs_path, file_path)

def replaceNextLine(file_path, pattern, strNextLine):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            found = False
            for line in old_file:
                if found:
                    new_file.write(strNextLine+"\n")
                    found = False
                else:
                    new_file.write(line)
                    if pattern in line:
                        found = True
    close(fh)
    remove(file_path)
    move(abs_path, file_path)

######################################################################
from MadGraphControl.MadGraphUtils import *
#Some setup variables.
mode=0

from os import environ,path
environ['ATHENA_PROC_NUMBER'] = '1'

#Need extra events to avoid Pythia8 problems.
safefactor=2.5
nevents=500*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor
######################################################################
# Configure event generation.
######################################################################
runName='run_01'
#Set input file for Pythia8.
runArgs.inputGeneratorFile = runName+'._00001.events.tar.gz'
#Make process card.
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model HC_NLO_X0_UFO-no_b_mass
define p = g d d~ u u~ s s~ c c~ b b~
define j = g d d~ u u~ s s~ c c~ b b~
generate p p > x0 j j $$ w+ w- z / a [QCD]
output -f""")
fcard.close()
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#Define process directory.
process_dir = new_process()

# fix check_poles issue
replace(os.path.join(process_dir,"bin/internal/amcatnlo_run_interface.py"), "tests.append('check_poles')", "pass #tests.append('check_poles')")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#IRPoleCheckThreshold", "-1")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#PrecisionVirtualAtRunTime", "-1")

#Fetch default run_card.dat and set parameters
ptj = 10
maxjetflavor = 5
#Fetch default run_card.dat and set parameters
extras = {'lhe_version'   :'3.0',
          'reweight_scale':'True',
          'pdlabel'       :"lhapdf",
          'lhaid'         :260000,
	      'ptj'           :str(ptj),
          'maxjetflavor'  :str(maxjetflavor),
	      'jetalgo'       :'-1',
	      'jetradius'     :'0.4'
}
#Set couplings here.
parameters={'frblock': {
        'Lambda': 1000.0,
        'cosa'  : 1.0,
        'kSM'   : 1.0,
        'kHtt'  : 0.0,
        'kAtt'  : 0.0,
        'kHll'  : 0.0,
        'kAll'  : 0.0,
        'kHaa'  : 0.0,
        'kAaa'  : 0.0,
        'kHza'  : 0.0,
        'kAza'  : 0.0,
        'kHzz'  : 0.0,
        'kAzz'  : 0.0,
        'kHww'  : 0.0,
        'kAww'  : 0.0,
        'kHda'  : 0.0,
        'kHdz'  : 0.0,
        'kHdwR' : 0.0,
        'kHdwI' : 0.0
        }}

#Create parame card
build_param_card(param_card_old=path.join(process_dir,'Cards/param_card.dat'),
                param_card_new='param_card_new.dat',
                masses={'25': '1.250000e+02'},params=parameters)
#Create run card.
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
               run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,
               beamEnergy=beamEnergy,extras=extras)

#Create MadSpin decay card.
madspin_card_loc='madspin_card.dat'

#NOTE: Can't set random seed in due to crashes when using "set spinmode none".
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
 set spinmode none
#
# specify the decay for the final state particles
define l- = e- mu- ta-
define l+ = e+ mu+ ta+
decay x0 > w+ w- > l+ vl l- vl~
# running the actual code
launch""")
mscard.close()

print_cards()

#Run the event generation!
generate(run_card_loc='run_card.dat',
	param_card_loc='param_card_new.dat',
	mode=mode,
	proc_dir=process_dir,
	run_name=runName,
	madspin_card_loc=madspin_card_loc)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runArgs.inputGeneratorFile,lhe_version=3)

#--------------------------------------------------------------
# Herwig 7 showering setup
#--------------------------------------------------------------
include("Herwig7_i/Herwig7_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
include("Herwig7_i/Herwig71_EvtGen.py")
# run Herwig7
Herwig7Config.run()
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'MG+Herwig7 VBF Higgs production with H7UE tune'
evgenConfig.keywords = [ "SM", "Higgs", "VBF", "WW", "mH125" ]
evgenConfig.generators += [ 'MadGraph', 'Herwig7' ]
evgenConfig.nEventsPerJob = 1000 
evgenConfig.contact = [ 'jiayi.chen@cern.ch' ]
evgenConfig.tune = "H7.1-Default"
#--------------------------------------------------------------
# Dilepton filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("Multi1TLeptonFilter")
filtSeq += MultiLeptonFilter("Multi2LLeptonFilter")

Multi1TLeptonFilter = filtSeq.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 15000.
Multi1TLeptonFilter.Etacut = 5.0
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = filtSeq.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 5000.
Multi2LLeptonFilter.Etacut = 5.0
Multi2LLeptonFilter.NLeptons = 2

filtSeq.Expression = "(Multi1TLeptonFilter) and (Multi2LLeptonFilter)"

