from MadGraphControl.MadGraphUtils import *
import subprocess
#from os.path import join as pathjoin 

fcard = open('proc_card_mg5.dat','w')

safefactor=1.1
nevents=runArgs.maxEvents*safefactor
runName='run_01'

#==================================================================================

print "Generate VBF+VHhad events."
processCommand="""
generate p p > h j j  QCD=0 /a  NP=0, h > e+ ve mu- vm~ NP=1 
add process p p > h j j QCD=0 /a NP=0, h > mu+ vm e- ve~ NP=1 
"""




fcard.write("""
import model SMEFTsim_A_U35_MwScheme_UFO-massless_HWW"""
+processCommand+"""
output -f""")
fcard.close()


#==================================================================================



# General settings
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#==================================================================================

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'3.0', 
           'cut_decays':'F', 
           'pdlabel':'nn23lo1',
#           'ickkw'       : ickkw,
           'ptj'         : 20.,
           'ptb'         : 20.,
           'use_syst'    : "False" }
    

#==================================================================================
process_dir = new_process()

#==================================================================================

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)


#==================================================================================
#Building param_card setting to 0 c_is that are not of interest

parameters={'frblock':
                {'cHDD'  : 0.0, 
                 'cHW'   : 0.0, 
                 'cHB'   : 0.0, 
                 'cHWB'  : 0.0, 
                 'cHl3'  : 0.5, 
                 'cHe'   : 0.0, 
                 }
            }


build_param_card(param_card_old=process_dir+'/Cards/param_card.dat',param_card_new='param_card_new.dat', params=parameters)

print_cards()

#==================================================================================


rcard = open('reweight_card.dat','w')

reweightCommand="""

launch --rwgt_name=cHl3_m_5p0
set frblock 46 -5.0

launch --rwgt_name=cHl3_m_3p0
set frblock 46 -3.0

launch --rwgt_name=cHl3_m_2p0
set frblock 46 -2.0

launch --rwgt_name=cHl3_m_1p5
set frblock 46 -1.5

launch --rwgt_name=cHl3_m_1p0
set frblock 46 -1.0

launch --rwgt_name=cHl3_m_0p75
set frblock 46 -0.75

launch --rwgt_name=cHl3_m_0p5
set frblock 46 -0.5

launch --rwgt_name=cHl3_m_0p25
set frblock 46 -0.25

launch --rwgt_name=cHl3_m_0p1
set frblock 46 -0.1

launch --rwgt_name=cHl3_p_0p1
set frblock 46 0.1

launch --rwgt_name=cHl3_p_0p25
set frblock 46 0.25

launch --rwgt_name=cHl3_p_0p5
set frblock 46 0.5

launch --rwgt_name=cHl3_p_0p75
set frblock 46 0.75

launch --rwgt_name=cHl3_p_1p0
set frblock 46 1.0

launch --rwgt_name=cHl3_p_1p5
set frblock 46 1.5

launch --rwgt_name=cHl3_p_2p0
set frblock 46 2.0

launch --rwgt_name=cHl3_p_3p0
set frblock 46 3.0

launch --rwgt_name=cHl3_p_5p0
set frblock 46 5.0

"""

rcard.write(reweightCommand)
rcard.close()

subprocess.call('cp reweight_card.dat ' + process_dir+'/Cards/', shell=True)

generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat', proc_dir=process_dir,run_name=runName,nevents=nevents,random_seed=runArgs.randomSeed, reweight_card_loc='reweight_card.dat')

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)  

#==================================================================================
# Shower 
evgenConfig.description = 'MadGraphSMEFT_VBF'
evgenConfig.contact     = [ 'ana.cueto@cern.ch' ]
evgenConfig.nEventsPerJob = 10000 
evgenConfig.keywords+=['Higgs']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'



include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")



