include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtils import *

evt_multiplier=2.5

# default to "all" if nothing specified
if 'all' in phys_short:
    higgs_decay_process = 'all'
elif 'bb' in phys_short:
    higgs_decay_process = 'bb'
else:
    from AthenaCommon import Logging
    locallog = Logging.logging.getLogger('monoShh')
    locallog.warning("Higgs decay process not set, defaulting to 'all'")
    higgs_decay_process = 'all'

# Get other mass parameters from the phys short
mzp = int(phys_short.split('_zp')[1].split('_')[0])
mdm = int(phys_short.split('_dm')[1].split('_')[0])
mhs = float(phys_short.split('_dh')[1].split('_')[0].replace("p","."))

print("Z\' mass: ", mzp)
print("DM mass: ", mdm)
print("dark-Higgs mass: ", mhs)
print("Higgs decay processes: ", higgs_decay_process)

# Form full process string and set up directory
process = """
import model DarkHiggs2MDM
generate p p > zp > n1 n1 hs QED<=2, (hs > h h) @0
add process p p > zp > n1 n1 hs j QED<=2, (hs > h h) @1
"""

# determine ktdurham cut from dark Higgs mass
# (ktdurham cut sets scale at which event description is split between parton shower and matrix element) 
try:
    ktdurham = int(mhs / 4)
    assert ktdurham > 40
except AssertionError:
    ktdurham = 40

# Set couplings
param_blocks['frblock'] = { '1':gq , '2':gx , '3':th }
# Set decays
decays['54'] = "AUTO"
decays['55'] = "AUTO"
# Set masses
masses['54'] = mhs
masses['55'] = mzp
masses['1000022'] = mdm

# Debug the SM higgs mass/branching ratio in the default param_card
masses['5'] = 0.00
masses['25'] = 125.00

if higgs_decay_process == 'bb':
    higgs_decay = {'25':'''DECAY   25     4.06911399E-03   # higgs decays
    #          BR         NDA          ID1       ID2       ID3       ID4
            0.00000000E+00    2          15       -15   # BR(H1 -> tau- tau+)
            1.00000000E+00    2           5        -5   # BR(H1 -> b bbar)
            0.00000000E+00    2          24       -24   # BR(H1 -> W+ W-)
            0.00000000E+00    2          23        23   # BR(H1 -> Z Z)
            0.00000000E+00    2          23        22   # BR(H1 -> Z gamma)  
            0.00000000E+00    2          22        22   # BR(H1 -> gamma gamma)   
            0.00000000E+00    2          13       -13   # BR(H1 -> mu mu)         
            0.00000000E+00    2           4        -4   # BR(H1 -> c cbar)       
            0.00000000E+00    2           3        -3   # BR(H1 -> s sbar)     
            0.00000000E+00    2          21        21   # BR(H1 -> g g)
    #
      '''}
    decays.update(higgs_decay)

elif higgs_decay_process == 'all':
    higgs_decay = {'25':'''DECAY   25     4.06911399E-03   # higgs decays
    #          BR         NDA      ID1       ID2
            5.82000000E-01    2           5        -5   # BR(H1 -> b bbar)
            2.13700000E-01    2          24       -24   # BR(H1 -> W+ W-)
            8.18700000E-02    2          21        21   # BR(H1 -> g g)
            6.27200000E-02    2          15       -15   # BR(H1 -> tau- tau+)
            2.89100000E-02    2           4        -4   # BR(H1 -> c cbar)   
            2.61900000E-02    2          23        23   # BR(H1 -> Z Z)
            2.27000000E-03    2          22        22   # BR(H1 -> gamma gamma)   
            2.17600000E-04    2          13       -13   # BR(H1 -> mu mu)         
            1.53300000E-03    2          23        22   # BR(H1 -> Z gamma)  
            2.46000000E-04    2           3        -3   # BR(H1 -> s sbar)     
    #
           '''}
    decays.update(higgs_decay)



njets=1

evgenConfig.contact = ["Tim Lukas Bruckler <tim.lukas.bruckler@cern.ch>"]
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "Dark Higgs (hh) Dark Matter from 2MDM UFO"
evgenConfig.keywords += ["exotic","BSM"]
#if (higgs_decay_process == "all"):
#    evgenConfig.process = "generate p p > zp > n1 n1 hs, (hs > h h > all)"
#elif (higgs_decay_process == "bb"):
#    evgenConfig.process = "generate p p > zp > n1 n1 hs, (hs > h h > b b~ b b~)"



include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )  


# Pythia settings: make the dark matter invisible
# syntax: particle data = name antiname spin=2s+1 3xcharge colour mass width (left out, so set to 0: mMin mMax tau0)

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess",
                                "SLHA:allowUserOverride = on",
                                "1000022:all = chi chi 2 0 0 %d 0.0 0.0 0.0 0.0" %(mdm),
                                "1000022:isVisible = false",
                                "25:onMode = on"]

    genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]

