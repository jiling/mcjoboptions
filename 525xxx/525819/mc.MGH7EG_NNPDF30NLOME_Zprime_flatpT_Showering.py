#--------------------------------------------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------------------------------------------
evgenConfig.description = 'MadGraph configuration to produce a flat-pT Zprime sample, showered with Herwig7'
evgenConfig.keywords = ['Zprime','jets']
evgenConfig.generators = ["MadGraph", "Herwig7", "EvtGen"]
evgenConfig.contact = ['yi.yu@cern.ch']
evgenConfig.tune = "H7.2-Default"
evgenConfig.inputFilesPerJob=1
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# from Herwig7_i.Herwig7_iConf import Herwig7 
# from Herwig7_i.Herwig72ConfigLHEF import Hw72ConfigLHEF
# genSeq += Herwig7()
# Herwig7Config = Hw72ConfigLHEF(genSeq, runArgs)

Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename = runArgs.inputGeneratorFile, me_pdf_order = "LO")

# add EvtGen
include("Herwig7_i/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()
