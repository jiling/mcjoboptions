import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment

# Some includes that are necessary to interface MadGraph with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_EvtGen.py")

evgenConfig.description     = "ttHH"
evgenConfig.keywords        = ['SM', 'top', 'ttVV']
evgenConfig.contact         = ["metsai@cern.ch"]
evgenConfig.generators      = ['aMcAtNlo', 'Pythia8']
evgenConfig.nEventsPerJob   = 50000

# Number of events to produce
safety = 5.0 # safety factor to account for filter efficiency
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*safety)
else: nevents = int(evgenConfig.nEventsPerJob*safety)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define w = w+ w-
generate p p > t t~ h h 
output -f"""

extras = {
           'nevents':int(nevents),
	}

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,
               settings=extras, runArgs=runArgs)

generate(process_dir=process_dir, runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs,lhe_version=3,saveProcDir=True)
 
### Set lepton filters
if not hasattr(filtSeq, "DecaysFinalStateFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
   HHbbbbFilter = DecaysFinalStateFilter("HHbbbbFilter")
   filtSeq += HHbbbbFilter
if not hasattr(filtSeq, "MultiLeptonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
   lepfilter = MultiLeptonFilter("lepfilter")
   filtSeq += lepfilter
if not hasattr(filtSeq, "LeptonPairFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import LeptonPairFilter
   lepPairfilter = LeptonPairFilter("lepPairfilter")
   filtSeq += lepPairfilter


filtSeq.HHbbbbFilter.PDGAllowedParents = [25]
filtSeq.HHbbbbFilter.MinNbQuarks = 4


filtSeq.lepfilter.Ptcut = 7000.0 #MeV
filtSeq.lepfilter.Etacut = 2.8
filtSeq.lepfilter.NLeptons = 2 #minimum


# no requirement on the OS pairs
filtSeq.lepPairfilter.NSFOS_Max = -1 
filtSeq.lepPairfilter.NSFOS_Min = -1
filtSeq.lepPairfilter.NOFOS_Max = -1
filtSeq.lepPairfilter.NOFOS_Min = -1


filtSeq.lepPairfilter.NSFSS_Min = 0 # at least 0 SFSS pairs with NPairSum_Min which will give at least 1 SS pair
filtSeq.lepPairfilter.NOFSS_Min = 0 # at least 0 OSSS pairs with NPairSum_Min which will give at least 1 SS pair
filtSeq.lepPairfilter.NSFSS_Max = -1 # no requirement on max of SS pairs
filtSeq.lepPairfilter.NOFSS_Max = -1 # no requirement on max of SS pairs

# Count number of SS pair 
filtSeq.lepPairfilter.UseSFSSInSum = True
filtSeq.lepPairfilter.UseOFSSInSum = True
filtSeq.lepPairfilter.UseSFOSInSum = False # no requirement on the OS pairs
filtSeq.lepPairfilter.UseOFOSInSum = False # no requirement on the OS pairs

# At least >=1 SS pair
filtSeq.lepPairfilter.NPairSum_Min = 1 # at least 1 SS pairs
filtSeq.lepPairfilter.NPairSum_Max = -1 # at least 1 SS pairs


# Require the event have leptons from resonant decays but not heavy flavor decays (>20 GeV cut on the resonance...)
# However, it will find the first parent particle 
filtSeq.lepPairfilter.OnlyMassiveParents = False 

filtSeq.lepPairfilter.Ptcut = 7000.0 #MeV
filtSeq.lepPairfilter.Etacut = 2.8

filtSeq.lepPairfilter.NLeptons_Min = 2
filtSeq.lepPairfilter.NLeptons_Max = -1 # No max of leptons 


filtSeq.Expression = "((not HHbbbbFilter) and lepfilter and lepPairfilter)"

