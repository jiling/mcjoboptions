from MadGraphControl.MadGraphUtils import *
import math, subprocess, os, shutil
#include('MadGraphControl/setupEFT.py')

mgmodels='/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/'
if os.path.exists('mgmodels_local'):
    shutil.rmtree('mgmodels_local')

os.mkdir( "mgmodels_local" )
sys.path.append(os.getcwd() + '/mgmodels_local')
os.environ['PYTHONPATH'] = os.environ['PYTHONPATH'] + ':' + os.getcwd() + '/mgmodels_local'

nevents = runArgs.maxEvents*2 if runArgs.maxEvents>0 else 2*evgenConfig.nEventsPerJob

def string_to_value( s_value ) :
    s_value = s_value.replace( 'm', '-' )
    s_value = s_value.replace( 'p', '.' )
    return float( s_value )

def str_to_coupling( str_coupling ) :
    c_count = str_coupling.count( 'c' )
    t_count = str_coupling.count( 't' )

    cHW   = 0.
    cHB   = 0.
    cHWB  = 0.
    tcHW  = 0.
    tcHB  = 0.
    tcHWB = 0.

    if c_count == 1 :
        if "tcHB" in str_coupling :
            s_value = str_coupling[ ( str_coupling.find("tcHB") + 4 ) : ] 
            tcHB = string_to_value( s_value )
        elif "tcHWB" in str_coupling :
            s_value = str_coupling[ ( str_coupling.find("tcHWB") + 5 ) : ] 
            tcHWB = string_to_value( s_value )
        elif "tcHW" in str_coupling :
            s_value = str_coupling[ ( str_coupling.find("tcHW") + 4 ) : ] 
            tcHW = string_to_value( s_value )
        elif "cHB" in str_coupling :
            s_value = str_coupling[ ( str_coupling.find("cHB") + 3 ) : ] 
            cHB = string_to_value( s_value )
        elif "cHWB" in str_coupling :
            s_value = str_coupling[ ( str_coupling.find("cHWB") + 4 ) : ] 
            cHWB = string_to_value( s_value )
        elif "cHW" in str_coupling :
            s_value = str_coupling[ ( str_coupling.find("cHW") + 3 ) : ] 
            cHW = string_to_value( s_value )

    elif c_count == 2 :
        if t_count == 0 :
            c_last = str_coupling.rfind( 'c' )
            if not "cHWB" in str_coupling :
                s_value1 = str_coupling[ ( str_coupling.find("cHW") + 3 ) : c_last ]
                s_value2 = str_coupling[ ( str_coupling.find("cHB") + 3 ) : ]
                cHW = string_to_value( s_value1 )
                cHB = string_to_value( s_value2 )
            elif not "cHB" in str_coupling :
                s_value1 = str_coupling[ ( str_coupling.find("cHW") + 3 ) : c_last ]
                s_value2 = str_coupling[ ( str_coupling.find("cHWB") + 4 ) : ]
                cHW = string_to_value( s_value1 )
                cHWB = string_to_value( s_value2 )
            elif not "cHW" in str_coupling :
                s_value1 = str_coupling[ ( str_coupling.find("cHB") + 3 ) : c_last ]
                s_value2 = str_coupling[ ( str_coupling.find("cHWB") + 4 ) : ]
                cHB = string_to_value( s_value1 )
                cHWB = string_to_value( s_value2 )
        elif t_count == 2 :
            t_last = str_coupling.rfind( 't' )
            if not "tcHWB" in str_coupling :
                s_value1 = str_coupling[ ( str_coupling.find("tcHW") + 4 ) : t_last ]
                s_value2 = str_coupling[ ( str_coupling.find("tcHB") + 4 ) : ]
                tcHW = string_to_value( s_value1 )
                tcHB = string_to_value( s_value2 )
            elif not "tcHB" in str_coupling :
                s_value1 = str_coupling[ ( str_coupling.find("tcHW") + 4 ) : t_last ]
                s_value2 = str_coupling[ ( str_coupling.find("tcHWB") + 5 ) : ]
                tcHW = string_to_value( s_value1 )
                tcHWB = string_to_value( s_value2 )
            elif not "tcHW" in str_coupling :
                s_value1 = str_coupling[ ( str_coupling.find("tcHB") + 4 ) : t_last ]
                s_value2 = str_coupling[ ( str_coupling.find("tcHWB") + 5 ) : ]
                tcHB = string_to_value( s_value1 )
                tcHWB = string_to_value( s_value2 )
        else :
            raise RuntimeError("Coupling name invalid.")

    elif str_coupling == "SM" :
        pass

    else :
        raise RuntimeError("Coupling name invalid.")

    couplings = {
            '28': '{:.6e}'.format(cHW),
            '29': '{:.6e}'.format(tcHW),
            '30': '{:.6e}'.format(cHB),
            '31': '{:.6e}'.format(tcHB),
            '32': '{:.6e}'.format(cHWB),
            '33': '{:.6e}'.format(tcHWB),
            #'83': '8.038700e+01',
            }
    return couplings

#---------------------------------------------------------------------------------------------------
#General Settings
#---------------------------------------------------------------------------------------------------
minevents=5000
#nevents=5500

#mode=0
# gridpack_dir='madevent/'
# gridpack_mode=True
gridpack_dir=None
gridpack_mode=False
safefactor=2.0
splitConfig = jofile.rstrip('.py').split('_')
#modelExtension="masslessEFT_" + splitConfig[-1]
str_coupling = splitConfig[-1]


#---------------------------------------------------------------------------------------------------
# generating process cards
#---------------------------------------------------------------------------------------------------

fcard = open('proc_card_mg5.dat','w')


#---------------------------------------------------------------------------------------------------
# creating restriction param card
#---------------------------------------------------------------------------------------------------


masses = {
        '15': '1.777000e+00',
        '25': '1.250000e+02',
        '24': '80.387000',
        '9000003': '80.387000',
        '9000004': '80.387000',
        '251': '80.387000',
        'DECAY  25': '4.995000e-03',
        }
#decays = { 'DECAY  25': '4.995000e-03' }

frblock = str_to_coupling( str_coupling )

yukawa = { '15': '1.777000e+00' }

runName='run_01'


model="SMEFTsim_A_U35_MwScheme_UFO"
restricted_model = "mgmodels_local/" + model + "_loc"
shutil.copytree(mgmodels+model,restricted_model)

new_frblock = {}
for p in frblock:
    if frblock[p] != '0.000000e+00':
        new_frblock[p] = frblock[p]

#modify_param_card(param_card_input="MadGraph_param_card_EFTAWU.dat",output_location=restricted_model+'/'+'restrict_masslessEFT_'+str_coupling+'.dat',params={'MASS': masses, 'FRBLOCK': new_frblock,  'YUKAWA': yukawa})
modify_param_card(param_card_input=restricted_model+"/restrict_SMlimit_massless_SMEFT_H4l.dat",output_location=restricted_model+'/'+'restrict_masslessEFT_'+str_coupling+'.dat',params={'FRBLOCK': new_frblock})

restrict_card = restricted_model+'/'+'restrict_masslessEFT_'+str_coupling+'.dat'
with open(restrict_card, 'r') as f:
    lines = f.readlines()
with open(restrict_card, 'w') as f:
    for line in lines:
        if line.strip("\n").rstrip() != "DECAY  25 4.070000e-03":
            f.write(line)

model_loc = "./" + restricted_model
model_extension = "masslessEFT_"+str_coupling

my_process = """
import model """+model+"""_loc-"""+model_extension+"""
set gauge unitary

define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define jb = g u c d s b u~ c~ d~ s~ b~
generate p p > h  QED=1 NP=1, h > l+ l- l+ l- NP=1 @0
add process p p > h jb QED=1 NP=1, h > l+ l- l+ l- NP=1 @1
add process p p > h jb jb  QED=1 NP=1, h > l+ l- l+ l- NP=1 @2

output -f
"""

process_dir = new_process( my_process )

#---------------------------------------------------------------------------------------------------
# require beam energy to be set as argument
#---------------------------------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



#ktdurham cut
ktdurham = 40

#---------------------------------------------------------------------------------------------------
# creating run_card.dat for ggF
#---------------------------------------------------------------------------------------------------

extras = {'lhe_version'  : '3.0',
          'pdlabel'      : "'nn23lo1'",
          'lhaid'        : 230000,
          #'parton_shower'  :'PYTHIA8',
          'event_norm'   : 'sum',
          'cut_decays'   : 'T',
          'pta'          : 0.,
          'ptl'          : 0.,
          'etal'         : -1.0,
          'drjj'         : 0.0,
          'draa'         : 0.0,
          'etaj'         : -1.0,
          'draj'         : 0.0,
          'drjl'         : 0.0,
          'dral'         : 0.0,
          'etaa'         : -1.0,
          'drll'         : 0.05,
          'ktdurham'     : ktdurham,
          'auto_ptj_mjj' : "False",
          'ickkw'        : 0,
          'xqcut'        : 0.0,
          'dparameter'   : 0.4,
          'ptj'          : 10.,
          'ptj1min'      : 0,
          'ptj1max'      : -1.0,
          'mmjj'         : 3.,
          'mmjjmax'      : -1.0,
          'nevents'      : int(nevents),
          }
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
print_cards()


generate(process_dir=process_dir,runArgs=runArgs)

runName='run_01'

#---------------------------------------------------------------------------------------------------
# Multi-core capability
#---------------------------------------------------------------------------------------------------
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts
arrange_output(process_dir = process_dir, runArgs = runArgs, lhe_version=3, saveProcDir=True)




evgenConfig.description = "ggF 125 GeV Higgs CKKW-L merged jets production in the SMEFT model decaying to zz4l."
evgenConfig.keywords = ['BSM', 'Higgs', 'mH125', 'gluonFusionHiggs','jets']
evgenConfig.contact = ['Jiawei Wang <jiawei.wang@cern.ch>']


#---------------------------------------------------------------------------------------------------
# CKKW-L jet matching
#---------------------------------------------------------------------------------------------------

PYTHIA8_nJetMax=2
PYTHIA8_Dparameter=float(extras['dparameter'])
#PYTHIA8_Process=my_process
#PYTHIA8_Process="guess"
PYTHIA8_Process="pp>h"
PYTHIA8_TMS=float(extras['ktdurham'])
PYTHIA8_nQuarksMerge=4

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
genSeq.Pythia8.Commands+=["Merging:mayRemoveDecayProducts=on"]
# modification of merging to allow pythia to guess the hard process with "guess" syntax
#if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    #genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
#else:
    #genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'
