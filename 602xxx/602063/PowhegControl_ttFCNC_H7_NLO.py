#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO, top->qX decays'
evgenConfig.keywords    = [ 'top', 'ttbar', 'FCNC' ]
evgenConfig.contact     = [ 'oliver.thielmann@cern.ch']
evgenConfig.inputFilesPerJob=1
evgenConfig.tune = "H7.1-Default"
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]


#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

command = """
set /Herwig/EventHandlers/LHEReader:PDFA /Herwig/Partons/HardNLOPDF
set /Herwig/EventHandlers/LHEReader:PDFB /Herwig/Partons/HardNLOPDF

set /Herwig/Generators/EventGenerator:DebugLevel 0
set /Herwig/Generators/EventGenerator:PrintEvent 0
set /Herwig/Generators/EventGenerator:UseStdout No
set /Herwig/Generators/EventGenerator:NumberOfEvents 1000000000
set /Herwig/Generators/EventGenerator:MaxErrors 500
"""
Herwig7Config.add_commands(command)

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

include('GeneratorFilters/MultiLeptonFilter.py')
filtSeq.MultiLeptonFilter.Ptcut = 5000.
filtSeq.MultiLeptonFilter.Etacut = 4.5
filtSeq.MultiLeptonFilter.NLeptons = 2