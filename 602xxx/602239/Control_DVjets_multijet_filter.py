### multijet filtering
include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)

### add multijet filters ###
# cuts used at analysis level for the DV+jets search
mjFilters = ['4j137', '5j101', '6j83', '7j55']

fAlgNames = []
for mjf in mjFilters:
    fName = "mjf_%s" % mjf
    fAlgNames.append(fName)
    nJets = mjf.split('j')[0]
    pTmin = mjf.split('j')[1]
    from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
    currentFilter = QCDTruthMultiJetFilter(fName)
    currentFilter.Njet = int(nJets)
    currentFilter.NjetMinPt = float(pTmin)*0.8*GeV # cut 20% below our analysis level cuts
    currentFilter.MaxEta = 2.8
    currentFilter.TruthJetContainer = "AntiKt4TruthJets"
    currentFilter.DoShape = False
    filtSeq += currentFilter

# configure the expression for the filtering
filtSeq.Expression = '(' + ' or '.join(fAlgNames) + ')'
print("filtSeq.Expression set: %s" % filtSeq.Expression)
