# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+HERWIG7 QCD Wjj production (Wp->munu)"
evgenConfig.keywords = ["SM", "W", "2jet", "QCD"]
evgenConfig.contact = ["philippe.calfayan@cern.ch"]
evgenConfig.generators  = ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.nEventsPerJob = 2000
evgenConfig.tune     = "H7.1-Default"

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg Wjj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_Wjj_Common.py")

PowhegConfig.bornsuppfact = 30
PowhegConfig.mjjminsuppfact = 200
PowhegConfig.ptborncut = 20
PowhegConfig.doublefsr = 1

PowhegConfig.decay_mode = "w+ > mu+ vm"

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------                 
# Herwig7 showering                                                             
#--------------------------------------------------------------                 
# initialize Herwig7 generator configuration for showering of LHE files         
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7                                                             
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=False)
Herwig7Config.tune_commands()

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

