# Provide config information
evgenConfig.generators    += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.tune           = "H7.2-Default"
evgenConfig.description    = "PowhegBox+Herwig7+EvtGen tW production (antitop), DR scheme, dynamic scale, dilepton, hdamp equal 1.5*top mass, H7.2-Default tune"
evgenConfig.keywords       = ['SM', 'top', 'singleTop', 'Wt', '2lepton']
evgenConfig.contact        = ['tetiana.moskalets@cern.ch', 'qibin.liu@cern.ch']
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 2

# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")
# add EvtGen
include("Herwig7_i/Herwig7_EvtGen.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)

# run Herwig7
Herwig7Config.run()
