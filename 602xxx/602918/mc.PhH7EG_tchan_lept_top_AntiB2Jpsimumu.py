# based on 600018 and 602235(Herwig7.2)
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7+EvtGen single-top-quark t-channel (2->3) production (top),MadSpin, H7.2-Default tune, ME NNPDF3.04f NLO, Anti-B->Jpsi->mumu filter'
evgenConfig.keywords    = [ 'SM', 'top', 'lepton', 'Jpsi' ]
evgenConfig.contact     = [ 'serena.palazzo@cern.ch, asada@hepl.phys.nagoya-u.ac.jp, derue@lpnhe.in2p3.fr' ]
evgenConfig.nEventsPerJob    = 10000
evgenConfig.inputFilesPerJob = 100
evgenConfig.generators += ['Powheg','Herwig7','EvtGen']
#--------------------------------------------------------------
# Herwig7 (H71UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118_nf_4")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
evgenConfig.tune        = "H7.2-Default"

# add EvtGen
include("Herwig7_i/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# Special decay of anti-B->Jpsi->mumu
#--------------------------------------------------------------
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles += ['AntiB2Jpsimumu.DEC']
genSeq.EvtInclusiveDecay.userDecayFile = 'AntiB2Jpsimumu.DEC'

# apply a J/psi to muons filter
include('GeneratorFilters/TTbarWithJpsimumuFilter.py')
filtSeq.TTbarWithJpsimumuFilter.JpsipTMinCut = 5000.
