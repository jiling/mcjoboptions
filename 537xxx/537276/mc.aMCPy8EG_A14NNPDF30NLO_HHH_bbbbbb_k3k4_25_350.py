from MadGraphControl.MadGraphUtils import *
import math
import os
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

# a safe margin for the number of generated events
nevents=int(runArgs.maxEvents*1.5) 
mode=0 
gridpack_mode=True
generate_gridpack=False

if not generate_gridpack:
   nevents = runArgs.maxEvents*1.5
else: 
   nevents = runArgs.maxEvents

# create the process string to be copied to proc_card_mg5.dat
if not is_gen_from_gridpack():
    process="""
    import model loop_sm_c3d4
    # Define multiparticle labels
    # Specify process(es) to run

    generate     g g > h h h [QCD]
    output -f
    """
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

#print 'process string: ',process

#---------------------------------------------------------------------------------------------------
# Set masses in param_card.dat
#---------------------------------------------------------------------------------------------------



params = {}

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

decays ={'4':'DECAY 25 4.070000e-03 # Wh1',  
         '35':'DECAY 35 1.000000e-03 # Wh2',  
         '36':'DECAY 36 40.000000 # Wh3'}
k3 ={'4':24}
k4 ={'6':349}
params['tripcoup'] = k3
params['quartcoup'] = k4

#Fetch default LO run_card.dat and set parameters (extras)
extras = { 'lhe_version':'3.0', 
           'cut_decays':'F'} 
extras['nevents'] = nevents

# set up process
#process_dir = new_process(process) 

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=params)
    
print_cards()

# Reweighting setup
# No reweighting for these points

# and the generation
generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)


evgenConfig.contact = ['nkyriaco@umich.edu']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]

evgenConfig.description = 'SM tri-Higgs production (Kappa-Scan Official JOs) (Reweighting Feature)'
evgenConfig.contact = [ 'nkyriaco@umich.edu' ]

if not generate_gridpack:
   genSeq.Pythia8.Commands += [ 
	'25:onMode = off',
	'25:onIfMatch = 5 -5'] #to specify the decay pathway

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
if not hasattr( filtSeq, "MultiBjetFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiBjetFilter
    filtSeq += MultiBjetFilter()
    pass
MultiBjetFilter = filtSeq.MultiBjetFilter
MultiBjetFilter.NJetsMin = 4
MultiBjetFilter.NBJetsMin = 4
MultiBjetFilter.JetPtMin = 25*GeV
