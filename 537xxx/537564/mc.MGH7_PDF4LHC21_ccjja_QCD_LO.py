import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

evgenConfig.description     = "Non-resonant production of two c-jets two additional jets and a photon"
evgenConfig.keywords        = ['bottom', 'photon', 'QCD']
evgenConfig.contact         = ["hava.rhian.schwartz@cern.ch"]
evgenConfig.generators      = ['MadGraph', 'Herwig7']
evgenConfig.nEventsPerJob   = 2000

gridpack_mode = False
gridpack_dir = 'madevent/'
minMjj       = 500

if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*4)
else: nevents = int(evgenConfig.nEventsPerJob*4)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Process type:
#---------------------------------------------------------------------------

process="""
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > c c~ j j a $ z h
output -f"""

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------

fcard = open('proc_card_mg5.dat','w')
fcard.write(process)
fcard.close()

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf'      : 93300, # PDF4LHC21_40_pdfas 
    'pdf_variations'   : [93300], 
     # NNPDF31 hessian, CT14, MMHT14, CT18, CT18Z, CT18A, HERAPDF2.0, ABMP16 5FS, NNPDF30 hessian, MSHT20, NNPDF40_nnlo_as_01180_hessian, PPDF4LHC15_nlo_mc
    'alternative_pdfs' : [304400,13000,25300,14000,14100,14200,61200,42560,303200,27400,331500,90500],
    'scale_variations' :[0.5,1,2], 
    }

extras = { 'nevents'      : nevents,
           'lhe_version'  : '3.0',
           'cut_decays'   : 'F',
           'maxjetflavor' : '5', 
           'ptj'          : '15.0',
           'ptb'          : '15.0',
           'pta'          : '20.0',
           'etaj'         : '-1.0',
           'etaa'         : '3.0',
           'drjj'         : '0.2',
           'drbb'         : '0.2',
           'drbj'         : '0.2',
           'drab'         : '0.2',
           'draj'         : '0.2'
           }

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,
               settings=extras)

print_cards()

generate(process_dir=process_dir,
         grid_pack=gridpack_mode, runArgs=runArgs)

outputDS = arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3)

#### Shower
include("Herwig7_i/Herwig72_LHEF.py")
include("Herwig7_i/Herwig71_EvtGen.py")
evgenConfig.tune = "H7.1-Default"


Herwig7Config.me_pdf_commands(order="LO", name="PDF4LHC21_40_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename="tmp_LHE_events.events", me_pdf_order="LO")

Herwig7Config.run()

# Filter
include('GeneratorFilters/FindJets.py')
CreateJets(prefiltSeq, 0.4, "WZ") 

#https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/GeneratorFilters/share/common/VBFForwardJetsFilter.py
from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
filtSeq += VBFForwardJetsFilter()
filtSeq.VBFForwardJetsFilter.JetMinPt = 0.
filtSeq.VBFForwardJetsFilter.Jet1MinPt = 0.
filtSeq.VBFForwardJetsFilter.Jet2MinPt = 0.
filtSeq.VBFForwardJetsFilter.TruthJetContainer = "AntiKt4WZTruthJets"
filtSeq.VBFForwardJetsFilter.MassJJ = minMjj*1000.
filtSeq.VBFForwardJetsFilter.DeltaEtaJJ = 0.1

