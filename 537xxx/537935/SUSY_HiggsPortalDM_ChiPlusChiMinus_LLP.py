from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
import os

def get_lifetime(s:str) -> float:

    #string should be "0pxns" which gives a lifetime of 0.x ns
    s = s.strip("ns")
    s = s.replace("p",".")
    return float(s)

JOName = get_physics_short()
evgenLog.info("Job Option name: " + str(JOName))

jobConfigParts = JOName.split("_")

runName = "run_01"
#Our MG5 command
process = """import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/DarkChargino
generate p p > x1+ x1-
#add process p p > x1+ x1- j
#add process p p > x1+ x1- j j
output -f"""

#param card stuff
mc1 = float(jobConfigParts[3])
delta_m = float(jobConfigParts[4])
mn1 = mc1 - delta_m
mn2 = mc1 - 0.16
tau = get_lifetime(jobConfigParts[5])

masses = {"1000022":mn1, "1000023":mn2,"1000024":mc1}

#set all the BSM particles stable 
decays = {"1000022":"""DECAY 1000022 0.00000000E+00""",
          "1000023":"""DECAY 1000023 0.00000000E+00""",
          "1000024":"""DECAY 1000024 0.00000000E+00"""}

#other parameters
#need to change ms, mt to be consistent with our particle masses for this to work properly
frblock = {"ms":mn1, "mt":mn2, "rr":1e-8}

#run card stuff
safety = 1.1 # safety factor to account for filter efficiency
nevents = runArgs.maxEvents * safety
settings = {"nevents":nevents, "iseed":runArgs.randomSeed,"ptl":-1, "etal":-1} # no cuts
#modify param and run cards
process_dir = new_process(process)
modify_param_card(process_dir=process_dir, params={"MASS":masses, "DECAY":decays, "FRBLOCK":frblock})
modify_run_card(process_dir=process_dir, settings=settings)

# Do the event generation
generate(process_dir=process_dir, runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs)

#Shower with Pythia     
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
runArgs.inputGeneratorFile = outputDS
evgenConfig.description = 'MadGraph_DarkChargino'                                                                                                                                
evgenConfig.contact = ["prathik.reddy.boyella@cern.ch"]

evgenConfig.keywords += ['SUSY','BSM']
evgenConfig.specialConfig = 'AMSBC1Mass=%s*GeV;AMSBN1Mass=%s*GeV;AMSBC1Lifetime=%s*ns;AMSBC1ToEleBR=%s;AMSBC1ToMuBR=%s;preInclude=SimulationJobOptions/preInclude.AMSB.py' % (mc1, mn1, tau, 0.5, 0.5)

#testSeq.TestHepMC.MaxVtxDisp = 1850
#testSeq.TestHepMC.MaxTransVtxDisp = 1850

#this tells TestHepMC to leave these for Geant4
bonus_file = open('pdg_extras.dat', 'w')
bonus_file.write( '1000024 Chargino1 %s (MeV/c) fermion Chargino 1\n' % (f"{mc1 * 1000}") )
bonus_file.write( '1000022 Neutralino1 %s (MeV/c) fermion Neutralino 0\n' % (f"{mn1 * 1000}") )
bonus_file.write( '-1000024 Chargino1 %s (MeV/c) fermion Chargino -1\n' % (f"{mc1 * 1000}") )
bonus_file.close()
testSeq.TestHepMC.G4ExtraWhiteFile = 'pdg_extras.dat'
os.system("get_files %s" % testSeq.TestHepMC.G4ExtraWhiteFile)
