from MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment import *
import re
import os
import subprocess

from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

evgenConfig.nEventsPerJob = 5000
nevents = runArgs.maxEvents*5.0 if runArgs.maxEvents>0 else 1.5*evgenConfig.nEventsPerJob

job_option_name = get_physics_short()
print(job_option_name)

# Get LQ mass from jo file
matches = re.search("M([0-9]+).*", job_option_name)
if matches is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    lqmass = float(matches.group(1))

# Merging settings
process_name = "pp>LEPTONS,NEUTRINOS"
maxjetflavor=5
ickkw=0
xqcut=0.0
nJetMax=2
ktdurham=15.0
dparameter=0.4

# dynamical_scale_choice
dynamical_scale_choice=3
# -1 : MadGraph5_aMC@NLO default (different for LO/NLO/ ickkw mode) same as previous version.
#  0 : Tag reserved for user define dynamical scale (need to be added in setscales.f).
#  1 : Total transverse energy of the event.
#  2 : sum of the transverse mass
#  3 : sum of the transverse mass divide by 2
#  4 : \sqrt(s), partonic energy (only for LO run)

#
ptcut=15.0

# minimum missing Et (sum of neutrino's momenta) (if not, set to 0.)
misset=150.

# maximum missing Et (sum of neutrino's momenta) (if not, set to -1.)
missetmax=-1.

# Kappa : for Yangs-Mills kappa=0 ; Minimal coupling kappa=1
lkappa=0.0

# Kappa tilde
lkappatilde=0.0

# gU : overall coupling strength
lgu=1.

# betaL32
betaL32=0.0

# betaR33
betaR33=0.0

# betaL23
betaL23=0.2

# betaL33
betaL33=1.0

#
process_def="""
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model ./vector_LQ_UFO
define p = g u c d s b u~ c~ d~ s~ b~
define j = p
generate p p > ta- vt~ {npOrder}
add process p p > ta+ vt {npOrder}
add process p p > ta- vt~ j {npOrder}
add process p p > ta+ vt j {npOrder}
add process p p > ta- vt~ j j {npOrder}
add process p p > ta+ vt j j {npOrder}
output -f
"""

#
process_def_run=process_def

#
process_def_SM       = process_def_run.replace("{npOrder}", "NP==0")
process_def_allNonSM = process_def_run.replace("{npOrder}", "NP^2>0")
process_def_interfer = process_def_run.replace("{npOrder}", "NP^2==2")
process_def_BSM      = process_def_run.replace("{npOrder}", "NP==2")
process_def_all      = process_def_run.replace("{npOrder}", "")

#
process_ME_type = ""

# Get beam energy
beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

if "inf" in job_option_name :
    process_dir = new_process(process_def_interfer)
    process_ME_type = "only interference"
elif "bsm" in job_option_name :
    process_dir = new_process(process_def_BSM)
    process_ME_type = "only BSM (no interference)"
elif "bi" in job_option_name :
    process_dir = new_process(process_def_allNonSM)
    process_ME_type = "BSM+interference"
elif "sm" in job_option_name : # must be after "bsm"
    process_dir = new_process(process_def_SM)
    process_ME_type = "SM"
else:
    process_dir = new_process(process_def_all)
    process_ME_type = "SM+BSM+interference"

#
extras = {
    'dynamical_scale_choice': int(dynamical_scale_choice),
    'lhe_version': '3.0',
    'sde_strategy': 1,
    'ihtmin': 0.0,
    'misset': misset,
    'missetmax': missetmax,
    'cut_decays': 'F',
    'nevents': int(nevents),
    'ickkw': int(ickkw),
    'xqcut': xqcut,
    'asrwgtflavor': int(maxjetflavor),
    'maxjetflavor': int(maxjetflavor),
    'ktdurham': ktdurham,
    'dparameter': dparameter,
    'ptj': ptcut,
    'ptb': ptcut,
    'etab': '5.0',
    'etaa': '5.0',
    'etal': '5.0',
    'drjj': '0.0',
    'drll': '0.0',
    'draa': '0.0',
    'draj': '0.0',
    'drjl': '0.0',
    'dral': '0.0',
}

#----------------------------------------------------------------------------------------
# Setting model parameters
#----------------------------------------------------------------------------------------
#---Define model parameter: - for LQ production no couplings to Z_prime and heavy gluon
#                           - beta23 and beta33 taken from job_option_name (encoded cf table)
vLQ_par={
    'gU'         : '{:e}'.format(lgu),
    'betaL33'    : '{:e}'.format(betaL33),
    'betaRd33'   : '{:e}'.format(betaR33),
    'betaL23'    : '{:e}'.format(betaL23),
    'betaL32'    : '{:e}'.format(betaL32),
    'kappaU'     : '{:e}'.format(lkappa),
    'kappautilde': '{:e}'.format(lkappatilde),
}

vLQ_Mass={
    '9000005' : '{:e}'.format(1e9),
    '9000006' : '{:e}'.format(1e9),
    '9000007' : '{:e}'.format(lqmass),
}

vLQ_Width={
    '9000007' : 'Auto',
}

zP_par={
    'gZp'      : '{:e}'.format(0.0),
    'zetaq33'  : '{:e}'.format(0.0),
    'zetal33'  : '{:e}'.format(0.0),
    'zetaRu33' : '{:e}'.format(0.0),
    'zetaRd33' : '{:e}'.format(0.0),
    'zetaRe33' : '{:e}'.format(0.0),
    'zetaqll'  : '{:e}'.format(0.0),
    'zetal22'  : '{:e}'.format(0.0),
    'zetal23'  : '{:e}'.format(0.0),
    'zetaRull' : '{:e}'.format(0.0),
    'zetaRdll' : '{:e}'.format(0.0),
    'zetaRe22' : '{:e}'.format(0.0),
}

gP_par={
    'gGp'       : '{:e}'.format(0.0),
    'kappaq33'  : '{:e}'.format(0.0),
    'kappaRu33' : '{:e}'.format(0.0),
    'kappaRd33' : '{:e}'.format(0.0),
    'kappaqll'  : '{:e}'.format(0.0),
    'kappaRull' : '{:e}'.format(0.0),
    'kappaRdll' : '{:e}'.format(0.0),
    'kappaG1'   : '{:e}'.format(0.0),
    'kappaG2'   : '{:e}'.format(0.0),
}
 
params = {}

params['NPLQCOUP'] = vLQ_par
params['MASS'] = vLQ_Mass
params['DECAY'] = vLQ_Width
params['NPGPCOUP'] = gP_par
params['NPZPCOUP'] = zP_par

modify_param_card(process_dir=process_dir, params=params)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

pdgfile = open("pdgid_extras.txt" , "w+")
pdgfile.write("""9000007
-9000007
9000006
-9000006
9000005
-9000005
""")
pdgfile.close()

#BLOCK NPLQCOUP #
#      1 1.000000e+00 # gu
#      2 1.000000e+00 # betal33
#      3 0.000000e+00 # betard33
#      4 2.000000e-01 # betal23
#      5 0.000000e+00 # betal32
#      6 0.000000e+00 # kappau
#      7 0.000000e+00 # kappautilde

reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write("""
use_eventid True
""")
scan_Ms=[1500, 2000, 2500, 3000, 4000, 5000]
scan_gUs=[0.5, 1.0, 1.5, 2.5, 3.5]
scan_b23s=[0.0, 0.2, 0.6, 1.0, 1.4]
for scan_M in scan_Ms:
    for scan_gU in scan_gUs:
        for scan_b23 in scan_b23s:
            reweight_card_f.write("""
            launch --rwgt_name=MU{:s}_gU{:s}_23L{:s}
                set mass 9000007 {:e}
                set NPLQCOUP 1 {:e}
                set NPLQCOUP 4 {:e}
            """.format(str(scan_M),str(scan_gU).replace(".", "_"),str(scan_b23).replace(".", "_" ),scan_M,scan_gU,scan_b23))
#
reweight_card_f.write("""
launch --rwgt_name=33Rdp1_0
    set NPLQCOUP 3 1.
launch --rwgt_name=33Rdm1_0
    set NPLQCOUP 3 -1.
""")
#
reweight_card_f.write("""
launch --rwgt_name=kappa1_0
    set NPLQCOUP 6 1.0
    set NPLQCOUP 7 0.0
launch --rwgt_name=twokappa1_0
    set NPLQCOUP 6 1.0
    set NPLQCOUP 7 1.0
""")
#
reweight_card_f.write("""
launch --rwgt_name=MU10000
    set mass 9000007 10000.
launch --rwgt_name=MU15000
    set mass 9000007 15000.
launch --rwgt_name=MU20000
    set mass 9000007 20000.
""")
reweight_card_f.close()

#
print_cards()

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs, lhe_version=3, saveProcDir=True)

#----------------------------------------------------------------------------------------
# EVGEN configuration
#----------------------------------------------------------------------------------------
evgenConfig.description = 'Vector LQ production of U1 {0:s}: tau(had)nu+0,1,2jets, mLQ={1:d}, misset={2:d}GeV, missetmax={3:d}GeV'.format(process_ME_type,int(lqmass),int(misset),int(missetmax))
evgenConfig.keywords += ['BSM', 'exotic', 'leptoquark', 'tau']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.contact = ['junichi.tanaka@cern.ch']

check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# CKKWL setting
PYTHIA8_nJetMax=nJetMax
PYTHIA8_TMS=ktdurham
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process=process_name
PYTHIA8_nQuarksMerge=maxjetflavor
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on"]

genSeq.Pythia8.Commands += [ '15:onMode = on', # decay of taus
                             '15:offIfAny = 11 13',
                             'TauDecays:externalMode = 0',
                             'TauDecays:mode = 5' ]
