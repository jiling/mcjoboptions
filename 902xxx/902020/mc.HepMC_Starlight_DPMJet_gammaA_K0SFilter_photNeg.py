##############################################################
f = open("LAMBDA0_KS0_USER.DEC", "w")
f.write("Decay Lambda0\n")
f.write("1.0000    p+         pi-    PHSP;\n")
f.write("Enddecay\n")
f.write("Decay anti-Lambda0\n")
f.write("1.0000    anti-p-    pi+    PHSP;\n")
f.write("Enddecay\n")
f.write("Decay K_S0\n")
f.write("1.0000    pi+        pi-    PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################

evgenConfig.description = "Photonuclear events generated with STARlight using DPMJET with V0 Filter"
evgenConfig.keywords = ["QCD","coherent","minBias"]
evgenConfig.tune = "none"
evgenConfig.contact  = ["sruthy.jyothi.das@cern.ch"]
evgenConfig.nEventsPerJob = 5000

from TruthIO.TruthIOConf import HepMCReadFromFile
genSeq += HepMCReadFromFile()
genSeq.HepMCReadFromFile.InputFile="events.hepmc"
evgenConfig.generators += ["HepMCAscii"]
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]

from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
genSeq += EvtInclusiveDecay()
genSeq.EvtInclusiveDecay.OutputLevel = 3
genSeq.EvtInclusiveDecay.pdtFile = "inclusive.pdt"
genSeq.EvtInclusiveDecay.decayFile = "inclusive.dec"
genSeq.HepMCReadFromFile.OutputLevel = VERBOSE
genSeq.EvtInclusiveDecay.userDecayFile = "LAMBDA0_KS0_USER.DEC"
genSeq.EvtInclusiveDecay.allowAllKnownDecays = False
genSeq.EvtInclusiveDecay.whiteList = [
    111,   130,   310,
    -3112, -3122, -3212, -3222, -3312, -3322,
    3112,  3122,  3212,  3222,  3312,  3322,
    -4112, -4122, -4132, -4212, -4222, -4232, -4312, -4322, -4332, -4412, -4422, -4432,
    4112,  4122,  4132,  4212,  4222,  4232,  4312,  4322,  4332,  4412,  4422,  4432,
    -411,  -421,  -431,
    411,   421,   431,
]

del testSeq.TestHepMC

