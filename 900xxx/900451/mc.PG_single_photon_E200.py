evgenConfig.description = "Single photons in FCAL acceptance at fixed energy"
evgenConfig.keywords = ["singleParticle"] 
evgenConfig.contact  = ["tommaso.lari@cern.ch"]
evgenConfig.nEventsPerJob = 10000

include("ParticleGun/ParticleGun_Common.py")

import ParticleGun as PG
topSeq += PG.ParticleGun()
topSeq.ParticleGun.sampler.pid = 22
topSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=200000., eta=[-5.0,-3.0, 3.0,5.0])
