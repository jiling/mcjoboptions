import re
import os
import math
import subprocess

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()

nevents=runArgs.maxEvents
LHE_EventMultiplier = 2
if LHE_EventMultiplier > 0 :
    nevents=runArgs.maxEvents*LHE_EventMultiplier


if full_or_int_or_np == 1 :
    my_process = """
    import model 4fermitop_ttnunu_5F --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > vt vt~ t t~ NP^2>0
    add process p p > vt vt~ t t~ j NP^2>0
    output -f"""
elif full_or_int_or_np == 2 :
    my_process = """
    import model 4fermitop_ttnunu_5F --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > vt vt~ t t~ NP^2==2
    add process p p > vt vt~  t t~ j NP^2==2
    output -f"""
elif full_or_int_or_np == 3 :
    my_process = """
    import model 4fermitop_ttnunu_5F --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > vt vt~  t t~ NP^2==4
    add process p p > vt vt~ t t~ j NP^2==4
    output -f"""


if "met50" in phys_short:
    include('GeneratorFilters/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 50000.

    filtSeq.Expression = "MissingEtFilter"

beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(my_process)
extras = {
#          'pdlabel': "'lhapdf'",
#          'lhaid'      : '263000',
          'ickkw'      : '0',
          'ptj'   : '20.0',
          'xqcut'   : '0.0',
          'nevents'   : nevents,
          'maxjetflavor': '5',
          'pdgs_for_merging_cut': '1, 2, 3, 4, 5, 21',
          'asrwgtflavor': 5
}


couplings = {'lambdas': str(Lambda),
          'fvll'   : str(fvll),
          'fvlr'   : str(fvlr)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

params={}
params['DIM6']=couplings
modify_param_card(process_dir=process_dir,params=params)

print_cards()

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir = process_dir, runArgs = runArgs, lhe_version=3, saveProcDir=True)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Pythia8 setup for matching if necessary
genSeq.Pythia8.Commands += [ "Merging:Process = guess" ]
genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']

#_nQuarksMerge = 5
#njets=max([l.count('j') for l in my_process.split('\n')])
#njets_min=min([l.count('j') for l in my_process.split('\n') if 'generate ' in l or 'add process' in l])
#if njets>0 and njets!=njets_min and hasattr(genSeq,'Pythia8'):
#    genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on",
#                                "Merging:nJetMax = "+str(njets),
#                                "Merging:doKTMerging = on",
#                                "Merging:TMS = 200",
#                                "Merging:ktType = 1",
#                                "Merging:Dparameter = 0.4",
#                                "Merging:nQuarksMerge = {0:d}".format(_nQuarksMerge)]

evgenConfig.description = 'Production of ttnunu EFT'
evgenConfig.keywords += ['BSM', 'exotic']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> ttnunu'
evgenConfig.tune = 'A14 NNPDF23LO'
evgenConfig.contact = ["Yoav Afik <yafik@cern.ch>","Daniele Zanzi <dzanzi@cern.ch>"]

