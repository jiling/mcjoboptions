# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 16.03.2021,  01:32
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.68716564E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.20871874E+03  # scale for input parameters
    1   -9.48745239E+02  # M_1
    2    8.61685664E+02  # M_2
    3    2.30677110E+03  # M_3
   11    6.51780308E+03  # A_t
   12    1.12938105E+03  # A_b
   13   -9.33195272E+02  # A_tau
   23    5.86628436E+02  # mu
   25    2.57709271E+01  # tan(beta)
   26    2.24399440E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.84988243E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.71187293E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.38371804E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.20871874E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.20871874E+03  # (SUSY scale)
  1  1     8.39068196E-06   # Y_u(Q)^DRbar
  2  2     4.26246643E-03   # Y_c(Q)^DRbar
  3  3     1.01366178E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.20871874E+03  # (SUSY scale)
  1  1     4.34517778E-04   # Y_d(Q)^DRbar
  2  2     8.25583777E-03   # Y_s(Q)^DRbar
  3  3     4.30905477E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.20871874E+03  # (SUSY scale)
  1  1     7.58265296E-05   # Y_e(Q)^DRbar
  2  2     1.56785214E-02   # Y_mu(Q)^DRbar
  3  3     2.63686938E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.20871874E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     6.51780307E+03   # A_t(Q)^DRbar
Block Ad Q=  3.20871874E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.12938106E+03   # A_b(Q)^DRbar
Block Ae Q=  3.20871874E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -9.33195276E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.20871874E+03  # soft SUSY breaking masses at Q
   1   -9.48745239E+02  # M_1
   2    8.61685664E+02  # M_2
   3    2.30677110E+03  # M_3
  21    4.61072798E+06  # M^2_(H,d)
  22   -1.77136691E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.84988243E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.71187293E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.38371804E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28884201E+02  # h0
        35     2.24372399E+03  # H0
        36     2.24399440E+03  # A0
        37     2.24534457E+03  # H+
   1000001     1.01123124E+04  # ~d_L
   2000001     1.00877030E+04  # ~d_R
   1000002     1.01119558E+04  # ~u_L
   2000002     1.00907601E+04  # ~u_R
   1000003     1.01123153E+04  # ~s_L
   2000003     1.00877073E+04  # ~s_R
   1000004     1.01119587E+04  # ~c_L
   2000004     1.00907613E+04  # ~c_R
   1000005     2.48151715E+03  # ~b_1
   2000005     3.83333867E+03  # ~b_2
   1000006     2.67535880E+03  # ~t_1
   2000006     3.84840940E+03  # ~t_2
   1000011     1.00208867E+04  # ~e_L-
   2000011     1.00087726E+04  # ~e_R-
   1000012     1.00201283E+04  # ~nu_eL
   1000013     1.00208988E+04  # ~mu_L-
   2000013     1.00087946E+04  # ~mu_R-
   1000014     1.00201399E+04  # ~nu_muL
   1000015     1.00149673E+04  # ~tau_1-
   2000015     1.00243991E+04  # ~tau_2-
   1000016     1.00234155E+04  # ~nu_tauL
   1000021     2.72336196E+03  # ~g
   1000022     5.86018086E+02  # ~chi_10
   1000023     5.94051776E+02  # ~chi_20
   1000025     9.41906376E+02  # ~chi_30
   1000035     9.59721859E+02  # ~chi_40
   1000024     5.86976574E+02  # ~chi_1+
   1000037     9.41575368E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.73862383E-02   # alpha
Block Hmix Q=  3.20871874E+03  # Higgs mixing parameters
   1    5.86628436E+02  # mu
   2    2.57709271E+01  # tan[beta](Q)
   3    2.43131980E+02  # v(Q)
   4    5.03551087E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -1.13968272E-01   # Re[R_st(1,1)]
   1  2     9.93484390E-01   # Re[R_st(1,2)]
   2  1    -9.93484390E-01   # Re[R_st(2,1)]
   2  2    -1.13968272E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     3.36923888E-03   # Re[R_sb(1,1)]
   1  2     9.99994324E-01   # Re[R_sb(1,2)]
   2  1    -9.99994324E-01   # Re[R_sb(2,1)]
   2  2     3.36923888E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.55270278E-01   # Re[R_sta(1,1)]
   1  2     9.87872027E-01   # Re[R_sta(1,2)]
   2  1    -9.87872027E-01   # Re[R_sta(2,1)]
   2  2     1.55270278E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -2.07891449E-02   # Re[N(1,1)]
   1  2    -1.68571052E-01   # Re[N(1,2)]
   1  3     7.01792609E-01   # Re[N(1,3)]
   1  4    -6.91837225E-01   # Re[N(1,4)]
   2  1     8.33641614E-02   # Re[N(2,1)]
   2  2     3.56707207E-02   # Re[N(2,2)]
   2  3     7.04721505E-01   # Re[N(2,3)]
   2  4     7.03665842E-01   # Re[N(2,4)]
   3  1    -3.51901594E-03   # Re[N(3,1)]
   3  2     9.85039219E-01   # Re[N(3,2)]
   3  3     9.44439727E-02   # Re[N(3,3)]
   3  4    -1.44103055E-01   # Re[N(3,4)]
   4  1     9.96296063E-01   # Re[N(4,1)]
   4  2    -3.02293579E-03   # Re[N(4,2)]
   4  3    -4.39894333E-02   # Re[N(4,3)]
   4  4    -7.38237566E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.33932503E-01   # Re[U(1,1)]
   1  2     9.90990456E-01   # Re[U(1,2)]
   2  1     9.90990456E-01   # Re[U(2,1)]
   2  2     1.33932503E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -2.04427570E-01   # Re[V(1,1)]
   1  2     9.78881693E-01   # Re[V(1,2)]
   2  1     9.78881693E-01   # Re[V(2,1)]
   2  2     2.04427570E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.93740761E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     4.37207981E-04    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.02878975E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.92521588E-01    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.42388284E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.93218625E-03    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.97873312E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.94154031E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     8.51730457E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.10370170E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.97724987E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.94713886E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.20291389E-04    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.50287124E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.90573395E-01    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     9.64971375E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.42437122E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.00970787E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.14772301E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.94056560E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     8.51446014E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.10332462E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.97520791E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.88503727E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.71903446E-02    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     9.16206151E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.54195754E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     6.08718694E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.85629167E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.12992222E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.54273993E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     4.75196750E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.58581078E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.70335015E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     8.57201170E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.88904250E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.47678042E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42392996E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.62746502E-03    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.96375139E-01    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     8.71204980E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.57105349E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.83142197E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42441822E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.62485942E-03    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.96273902E-01    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     8.70907393E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.60372656E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.82949076E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.56208851E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.95516794E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.70254099E-01    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     7.94421638E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.10013512E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.33313021E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.89949585E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.95639216E-03    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.91983560E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.19251675E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.46689627E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.18712918E-02    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.73711310E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.93600638E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.04867740E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.38097528E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.89977062E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.95611654E-03    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.91944611E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.19269182E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.47502999E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.18703458E-02    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.73710828E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.94020879E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.04865717E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.38079935E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.72009053E+01   # ~b_1
#    BR                NDA      ID1      ID2
     2.34429397E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.35695787E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     3.22070920E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     5.85185212E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.61754492E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     6.38109354E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     2.74029600E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.58263874E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.44689933E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     5.32571601E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.85745254E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.61559948E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.18436226E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.70841917E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     2.43296846E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.54528390E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.00541502E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     7.06821677E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.20028645E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.10756387E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.68690259E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.19239641E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.60154792E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.37681871E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.17383367E-02    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.62261746E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     4.51033657E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.02318440E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.38071039E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.06828997E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.22564496E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.10753456E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.68680334E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.19257116E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.60363332E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.39872190E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.17373411E-02    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.62260748E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     4.52652084E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.02316584E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.38053441E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.02332421E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.25934004E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.39805144E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.57321818E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.23684473E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.44809221E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.13380028E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.99165447E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.23254311E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.23004901E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     4.96698528E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.11075974E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     5.89622923E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     8.86280282E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.56156503E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     2.88026077E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.13564323E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.82361003E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     9.32977802E-13   # chi^+_1
#    BR                NDA      ID1      ID2
     7.21096653E-01    2     1000022       211   # BR(chi^+_1 -> chi^0_1 pi^+)
#    BR                NDA      ID1      ID2       ID3
     1.43603671E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.35299676E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
DECAY   1000037     6.97713318E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.50859067E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.81840855E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.45205192E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.20162158E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.00247539E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     9.25652292E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.42848035E-07   # chi^0_2
#    BR                NDA      ID1      ID2
     3.55659252E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     3.82798915E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     2.73477051E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     4.90823998E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     4.90026210E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.10764836E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.10481461E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     5.26991773E-03    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     6.56372774E-02    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     7.00050931E-02    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     7.00050931E-02    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     5.98812175E-02    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     5.98812175E-02    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     2.33350005E-02    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     2.33350005E-02    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     2.33091332E-02    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     2.33091332E-02    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     1.72677085E-02    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     1.72677085E-02    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     8.38011373E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.12448727E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.12448727E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     9.48686482E-03    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.28223298E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.72615307E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.63077325E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.36501219E-04    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     8.36501219E-04    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     2.61095230E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.14548758E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.14548758E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.05970282E-01    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.28497935E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.91173345E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.58436493E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.22310308E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.22310308E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.86943360E+00   # ~g
#    BR                NDA      ID1      ID2
     4.04512875E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.04512875E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     5.39095853E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     6.21108123E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
#    BR                NDA      ID1      ID2       ID3
     4.20598461E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     4.60629187E-04    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     4.51869004E-02    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     4.28443579E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     3.89301090E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     8.72062315E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     5.61300813E-03    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     4.08227516E-02    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     4.08227516E-02    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     4.71530257E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     4.71530257E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.13832240E-03   # Gamma(h0)
     2.50753792E-03   2        22        22   # BR(h0 -> photon photon)
     1.95800905E-03   2        22        23   # BR(h0 -> photon Z)
     4.14451610E-02   2        23        23   # BR(h0 -> Z Z)
     3.18497360E-01   2       -24        24   # BR(h0 -> W W)
     7.42940170E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.35283410E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.93623645E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.58328402E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.22284314E-07   2        -2         2   # BR(h0 -> Up up)
     2.37348421E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.01058324E-07   2        -1         1   # BR(h0 -> Down down)
     1.81221053E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.81354761E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     3.52837933E+01   # Gamma(HH)
     1.01567812E-07   2        22        22   # BR(HH -> photon photon)
     2.72040796E-07   2        22        23   # BR(HH -> photon Z)
     2.66890176E-06   2        23        23   # BR(HH -> Z Z)
     1.20072909E-06   2       -24        24   # BR(HH -> W W)
     5.07985831E-06   2        21        21   # BR(HH -> gluon gluon)
     6.44575259E-09   2       -11        11   # BR(HH -> Electron electron)
     2.86970051E-04   2       -13        13   # BR(HH -> Muon muon)
     8.28766091E-02   2       -15        15   # BR(HH -> Tau tau)
     1.88318802E-13   2        -2         2   # BR(HH -> Up up)
     3.65296552E-08   2        -4         4   # BR(HH -> Charm charm)
     2.49793065E-03   2        -6         6   # BR(HH -> Top top)
     4.85351626E-07   2        -1         1   # BR(HH -> Down down)
     1.75538626E-04   2        -3         3   # BR(HH -> Strange strange)
     4.02160700E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.71415401E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.47809797E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.47809797E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.87139471E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     5.32707611E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.47315876E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.68152045E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.27213476E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     5.18685317E-06   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     9.86974266E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.52115852E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.45627947E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.58899378E-03   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     4.49690505E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.86437511E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     3.47265108E+01   # Gamma(A0)
     2.24737592E-07   2        22        22   # BR(A0 -> photon photon)
     4.10496685E-07   2        22        23   # BR(A0 -> photon Z)
     1.95913798E-05   2        21        21   # BR(A0 -> gluon gluon)
     6.33939972E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.82234242E-04   2       -13        13   # BR(A0 -> Muon muon)
     8.15091373E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.85983667E-13   2        -2         2   # BR(A0 -> Up up)
     3.60724806E-08   2        -4         4   # BR(A0 -> Charm charm)
     2.53111529E-03   2        -6         6   # BR(A0 -> Top top)
     4.77333571E-07   2        -1         1   # BR(A0 -> Down down)
     1.72639056E-04   2        -3         3   # BR(A0 -> Strange strange)
     3.95521223E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.61984260E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.42483352E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.42483352E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     9.31294316E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     8.50461266E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.89039151E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     9.27828724E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.49014968E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     6.24670867E-06   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     5.05456527E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.44571964E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     4.70558183E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     5.60241610E-04   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.26762008E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     3.77653133E-06   2        23        25   # BR(A0 -> Z h0)
     1.47417553E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     3.69488643E+01   # Gamma(Hp)
     7.25657091E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.10241070E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     8.77537681E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.77014214E-07   2        -1         2   # BR(Hp -> Down up)
     8.02380924E-06   2        -3         2   # BR(Hp -> Strange up)
     4.45438129E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.42448616E-08   2        -1         4   # BR(Hp -> Down charm)
     1.71935403E-04   2        -3         4   # BR(Hp -> Strange charm)
     6.23772641E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.22384070E-07   2        -1         6   # BR(Hp -> Down top)
     5.10101549E-06   2        -3         6   # BR(Hp -> Strange top)
     4.23166624E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.83754257E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.52749833E-01   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.09447011E-02   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.39900810E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.46984491E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     7.82312258E-08   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     3.49483193E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.03980101E-03   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     3.55982890E-06   2        24        25   # BR(Hp -> W h0)
     4.11688642E-13   2        24        35   # BR(Hp -> W HH)
     1.65274038E-13   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.29254559E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    6.64211429E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    6.64140684E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00010652E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.39918331E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.50570508E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.29254559E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    6.64211429E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    6.64140684E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99998046E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.95361876E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99998046E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.95361876E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26289307E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.52821962E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.04025677E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.95361876E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99998046E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.44165795E-04   # BR(b -> s gamma)
    2    1.58752669E-06   # BR(b -> s mu+ mu-)
    3    3.52423529E-05   # BR(b -> s nu nu)
    4    2.44841737E-15   # BR(Bd -> e+ e-)
    5    1.04593778E-10   # BR(Bd -> mu+ mu-)
    6    2.19054827E-08   # BR(Bd -> tau+ tau-)
    7    8.19759568E-14   # BR(Bs -> e+ e-)
    8    3.50201596E-09   # BR(Bs -> mu+ mu-)
    9    7.43147005E-07   # BR(Bs -> tau+ tau-)
   10    9.58694123E-05   # BR(B_u -> tau nu)
   11    9.90294326E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42461548E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93782702E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15952985E-03   # epsilon_K
   17    2.28168153E-15   # Delta(M_K)
   18    2.47967592E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28954933E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.34285453E-16   # Delta(g-2)_electron/2
   21    1.00164669E-11   # Delta(g-2)_muon/2
   22    2.83561376E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    5.96717082E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.14604223E-01   # C7
     0305 4322   00   2    -2.85930903E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.18327735E-01   # C8
     0305 6321   00   2    -3.22842029E-04   # C8'
 03051111 4133   00   0     1.61883136E+00   # C9 e+e-
 03051111 4133   00   2     1.61910597E+00   # C9 e+e-
 03051111 4233   00   2     1.55903559E-04   # C9' e+e-
 03051111 4137   00   0    -4.44152346E+00   # C10 e+e-
 03051111 4137   00   2    -4.43888370E+00   # C10 e+e-
 03051111 4237   00   2    -1.16540431E-03   # C10' e+e-
 03051313 4133   00   0     1.61883136E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61910586E+00   # C9 mu+mu-
 03051313 4233   00   2     1.55903368E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44152346E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43888380E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.16540420E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50477983E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.52449316E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50477983E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.52449353E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50477983E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.52459749E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85792288E-07   # C7
     0305 4422   00   2     1.16043469E-05   # C7
     0305 4322   00   2    -1.36699276E-06   # C7'
     0305 6421   00   0     3.30456609E-07   # C8
     0305 6421   00   2    -1.56836329E-05   # C8
     0305 6321   00   2    -1.03001395E-06   # C8'
 03051111 4133   00   2    -2.30140533E-07   # C9 e+e-
 03051111 4233   00   2     3.29702881E-06   # C9' e+e-
 03051111 4137   00   2     5.67776601E-06   # C10 e+e-
 03051111 4237   00   2    -2.46531816E-05   # C10' e+e-
 03051313 4133   00   2    -2.30141854E-07   # C9 mu+mu-
 03051313 4233   00   2     3.29702843E-06   # C9' mu+mu-
 03051313 4137   00   2     5.67776766E-06   # C10 mu+mu-
 03051313 4237   00   2    -2.46531828E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.20227354E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     5.34036290E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.20227347E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     5.34036290E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.20225311E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     5.34036289E-06   # C11' nu_3 nu_3
