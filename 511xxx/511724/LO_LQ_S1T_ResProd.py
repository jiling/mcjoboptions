import os

from MadGraphControl.MadGraphUtils import *

# read out the LQ mass and generation for evgen description
# top-level JOs have the following format: mc.MGPy8EG_LO_LQ_S1T_ResProd_lam11_2000_1p0.py
job_option_name = get_physics_short()
JO_name_list = job_option_name.split('_')
leptoquark_mass = float( JO_name_list[-2] )

if "lam11" in job_option_name:
    generation = 1
elif "lam22" in job_option_name:
    generation = 2
elif "lam33" in job_option_name:
    generation = 3
else:
    raise RuntimeError("Cannot determine LQ generation from job option name: {:s}.".format(job_option_name))

# Going straight to the commands necessary for Pythia8 as the LHEs will be provided
input_files = runArgs.inputGeneratorFile.replace(".tar.gz", "").split(",")
for input_file in input_files:
    remap_lhe_pdgids(input_file+'.events',pdgid_map={9000005:42})

evgenConfig.description = 'Scalar LO resonant production of S1~ with intra-generation couplings only, generation: {0:d}, mLQ={1:d}'.format(
    int(generation), int(leptoquark_mass))
evgenConfig.keywords += ['BSM', 'exotic', 'leptoquark', 'scalar']
evgenConfig.process = 'pp -> S1~ -> lepton + jet'
evgenConfig.contact = ["Daniel Buchin <daniel.buchin@cern.ch>, Michael Holzbock <michael.holzbock@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# This Pythia command is necessary as the initial state leptons from the proton (which pythia can't handle) have been replaced by photons -> need to circumvent the charge conservation check
genSeq.Pythia8.Commands += ['Check:event = off']
