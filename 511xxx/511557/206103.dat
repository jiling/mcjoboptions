# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  14:04
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.80722286E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.31046313E+03  # scale for input parameters
    1    7.03524536E+02  # M_1
    2   -1.21964179E+03  # M_2
    3    1.61161020E+03  # M_3
   11    7.48036623E+03  # A_t
   12   -9.74886906E+02  # A_b
   13    1.12660463E+03  # A_tau
   23   -8.76258883E+01  # mu
   25    4.72996747E+01  # tan(beta)
   26    3.92341810E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.98510062E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.88345887E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.89254319E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.31046313E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.31046313E+03  # (SUSY scale)
  1  1     8.38624573E-06   # Y_u(Q)^DRbar
  2  2     4.26021283E-03   # Y_c(Q)^DRbar
  3  3     1.01312584E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.31046313E+03  # (SUSY scale)
  1  1     7.97087475E-04   # Y_d(Q)^DRbar
  2  2     1.51446620E-02   # Y_s(Q)^DRbar
  3  3     7.90461004E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.31046313E+03  # (SUSY scale)
  1  1     1.39097593E-04   # Y_e(Q)^DRbar
  2  2     2.87609707E-02   # Y_mu(Q)^DRbar
  3  3     4.83712213E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.31046313E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     7.48036626E+03   # A_t(Q)^DRbar
Block Ad Q=  3.31046313E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -9.74886913E+02   # A_b(Q)^DRbar
Block Ae Q=  3.31046313E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.12660464E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.31046313E+03  # soft SUSY breaking masses at Q
   1    7.03524536E+02  # M_1
   2   -1.21964179E+03  # M_2
   3    1.61161020E+03  # M_3
  21    1.53915520E+07  # M^2_(H,d)
  22    1.62870924E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.98510062E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.88345887E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.89254319E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.29848710E+02  # h0
        35     3.92193739E+03  # H0
        36     3.92341810E+03  # A0
        37     3.92371892E+03  # H+
   1000001     1.01191092E+04  # ~d_L
   2000001     1.00945951E+04  # ~d_R
   1000002     1.01187531E+04  # ~u_L
   2000002     1.00976578E+04  # ~u_R
   1000003     1.01191160E+04  # ~s_L
   2000003     1.00946075E+04  # ~s_R
   1000004     1.01187600E+04  # ~c_L
   2000004     1.00976589E+04  # ~c_R
   1000005     2.95532696E+03  # ~b_1
   2000005     3.91042130E+03  # ~b_2
   1000006     2.92617890E+03  # ~t_1
   2000006     3.74521399E+03  # ~t_2
   1000011     1.00206818E+04  # ~e_L-
   2000011     1.00089402E+04  # ~e_R-
   1000012     1.00199205E+04  # ~nu_eL
   1000013     1.00207143E+04  # ~mu_L-
   2000013     1.00090036E+04  # ~mu_R-
   1000014     1.00199530E+04  # ~nu_muL
   1000015     1.00270351E+04  # ~tau_1-
   2000015     1.00300672E+04  # ~tau_2-
   1000016     1.00292400E+04  # ~nu_tauL
   1000021     1.98297699E+03  # ~g
   1000022     1.01829255E+02  # ~chi_10
   1000023     1.04276473E+02  # ~chi_20
   1000025     7.12161731E+02  # ~chi_30
   1000035     1.31180886E+03  # ~chi_40
   1000024     1.03069034E+02  # ~chi_1+
   1000037     1.31121789E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.08334555E-02   # alpha
Block Hmix Q=  3.31046313E+03  # Higgs mixing parameters
   1   -8.76258883E+01  # mu
   2    4.72996747E+01  # tan[beta](Q)
   3    2.43075084E+02  # v(Q)
   4    1.53932096E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.83518304E-01   # Re[R_st(1,1)]
   1  2     1.80808590E-01   # Re[R_st(1,2)]
   2  1    -1.80808590E-01   # Re[R_st(2,1)]
   2  2    -9.83518304E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99999811E-01   # Re[R_sb(1,1)]
   1  2     6.14233001E-04   # Re[R_sb(1,2)]
   2  1    -6.14233001E-04   # Re[R_sb(2,1)]
   2  2    -9.99999811E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.51793378E-01   # Re[R_sta(1,1)]
   1  2     9.88412247E-01   # Re[R_sta(1,2)]
   2  1    -9.88412247E-01   # Re[R_sta(2,1)]
   2  2    -1.51793378E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -3.89108351E-02   # Re[N(1,1)]
   1  2    -4.79422654E-02   # Re[N(1,2)]
   1  3    -7.11229595E-01   # Re[N(1,3)]
   1  4     7.00242779E-01   # Re[N(1,4)]
   2  1    -5.05535073E-02   # Re[N(2,1)]
   2  2    -4.01087333E-02   # Re[N(2,2)]
   2  3     7.02891025E-01   # Re[N(2,3)]
   2  4     7.08364200E-01   # Re[N(2,4)]
   3  1     9.97962125E-01   # Re[N(3,1)]
   3  2    -2.52711599E-03   # Re[N(3,2)]
   3  3     7.86699875E-03   # Re[N(3,3)]
   3  4     6.32718076E-02   # Re[N(3,4)]
   4  1    -1.37383561E-03   # Re[N(4,1)]
   4  2     9.98041303E-01   # Re[N(4,2)]
   4  3    -5.89756010E-03   # Re[N(4,3)]
   4  4     6.22646688E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -8.35169435E-03   # Re[U(1,1)]
   1  2    -9.99965124E-01   # Re[U(1,2)]
   2  1    -9.99965124E-01   # Re[U(2,1)]
   2  2     8.35169435E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     8.81756082E-02   # Re[V(1,1)]
   1  2     9.96104945E-01   # Re[V(1,2)]
   2  1     9.96104945E-01   # Re[V(2,1)]
   2  2    -8.81756082E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.97853685E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.52916086E-03    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.58110454E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.95887816E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.40295789E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.48474958E-03    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.41582981E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     8.76193509E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.02064311E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.07371911E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.01150381E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     3.18129642E-03    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     4.18756502E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.89343204E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.28577333E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.40461140E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.07675130E-03    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.99406512E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     8.75165668E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01709806E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.06659015E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.44224527E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.66042904E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.59887178E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     3.38062132E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     6.24908669E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.17192244E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.25664549E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.86094835E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.26573505E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.25657633E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     7.10692026E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.23125208E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     4.94696339E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.48627488E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.40300660E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     2.31753883E-04    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.92865003E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02918998E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     4.88703193E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02621766E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.40465999E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     2.31481843E-04    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.91816987E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02563490E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.05483822E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01914605E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.87114102E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.73933857E-04    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     6.70117463E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.27358708E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.53094663E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.52320460E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.42072201E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.48857125E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.92480170E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.69138565E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.64829599E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     4.93679442E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.90713780E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.49769330E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.42165106E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.48764696E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.92357344E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.69188921E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.13043661E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.64820486E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     4.93651212E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.90657412E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.49720760E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.92433431E+02   # ~b_1
#    BR                NDA      ID1      ID2
     9.64557743E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     9.41458106E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.95805202E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.34368566E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.08267993E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     8.93349660E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     3.66400548E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     2.69263703E+02   # ~b_2
#    BR                NDA      ID1      ID2
     9.12035640E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.90843467E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     7.55882089E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.79551902E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     6.32566409E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.59078180E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.92922675E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.70586805E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.69128445E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.54258126E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.23569447E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.55711126E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     4.93182829E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     7.96825424E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.83054100E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.49744543E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.59085533E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.92920059E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.70577485E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.69178776E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.56310111E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.25671238E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.55703858E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     4.93154747E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     8.49738955E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.82997928E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.49695974E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     2.16713031E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.35189561E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.37831966E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     4.61069134E-03    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.43961577E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.77386664E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     6.93107487E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.31385960E-01    2     1000021         4   # BR(~t_1 -> ~g c)
     2.88017239E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     2.17354888E-02    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     4.60707785E+02   # ~t_2
#    BR                NDA      ID1      ID2
     8.03601166E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     8.25189894E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.60963627E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.67272774E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.59002824E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.36970307E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.37588960E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.34119112E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     7.11532170E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.11117987E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     6.74558636E-12   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     6.04229885E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     2.01426559E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.94343556E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
DECAY   1000037     1.18764581E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.44851899E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.50368241E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.83785235E-04    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.46584669E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.37808653E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.33796635E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     6.18704353E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     6.43192886E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.19501284E-09   # chi^0_2
#    BR                NDA      ID1      ID2
     8.27563579E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     2.90252477E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     3.72150459E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     3.65696758E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     8.39872721E-03    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     8.17053175E-03    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     4.97693005E-02    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     9.93699941E-04    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     9.93699941E-04    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     3.31254361E-04    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     3.31254361E-04    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     3.18991972E-04    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     3.18991972E-04    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
DECAY   1000025     1.96028757E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.38834075E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.38834075E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.48694579E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     9.19338163E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.42313648E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.34018074E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.07391232E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     5.09250787E-04    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     2.17380404E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     2.17380404E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.23584086E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.39948764E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.39948764E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     9.49698567E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.42273451E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     3.57081461E-04    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.34316289E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.27124162E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     3.33536079E-04    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.35772636E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     3.45652502E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     6.91305523E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.91305523E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     5.88889369E-02   # ~g
#    BR                NDA      ID1      ID2
     4.14083374E-03    2     1000022        21   # BR(~g -> chi^0_1 g)
     5.07740024E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
#    BR                NDA      ID1      ID2       ID3
     1.51946899E-01    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     9.08163956E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.54700248E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     8.85525152E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     5.05755067E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.42473642E-03    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.01002648E-03    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.39702676E-03    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.45988170E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.45988170E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.33449919E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.33449919E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.20593869E-03   # Gamma(h0)
     2.55530694E-03   2        22        22   # BR(h0 -> photon photon)
     2.08478648E-03   2        22        23   # BR(h0 -> photon Z)
     4.55931454E-02   2        23        23   # BR(h0 -> Z Z)
     3.44862729E-01   2       -24        24   # BR(h0 -> W W)
     7.46832258E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.11291848E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.82952083E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.27565211E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.16184122E-07   2        -2         2   # BR(h0 -> Up up)
     2.25521699E-02   2        -4         4   # BR(h0 -> Charm charm)
     4.72829342E-07   2        -1         1   # BR(h0 -> Down down)
     1.71011444E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.54557559E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.41541828E+02   # Gamma(HH)
     5.46673536E-10   2        22        22   # BR(HH -> photon photon)
     2.02939015E-09   2        22        23   # BR(HH -> photon Z)
     7.99237304E-08   2        23        23   # BR(HH -> Z Z)
     1.72744344E-08   2       -24        24   # BR(HH -> W W)
     5.53596377E-06   2        21        21   # BR(HH -> gluon gluon)
     8.27639466E-09   2       -11        11   # BR(HH -> Electron electron)
     3.68535238E-04   2       -13        13   # BR(HH -> Muon muon)
     1.06441871E-01   2       -15        15   # BR(HH -> Tau tau)
     2.68322315E-14   2        -2         2   # BR(HH -> Up up)
     5.20552967E-09   2        -4         4   # BR(HH -> Charm charm)
     3.92698728E-04   2        -6         6   # BR(HH -> Top top)
     5.90871383E-07   2        -1         1   # BR(HH -> Down down)
     2.13728406E-04   2        -3         3   # BR(HH -> Strange strange)
     5.84875333E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.96381423E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     9.14921260E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     9.14921260E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     4.72893688E-06   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     9.74256104E-05   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.06634441E-05   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.49941177E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     4.40733597E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.21349964E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.55774178E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     4.79476438E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     3.14962403E-06   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.00326021E-05   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.38419016E-06   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     4.68343881E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.34074225E+02   # Gamma(A0)
     2.51600737E-08   2        22        22   # BR(A0 -> photon photon)
     2.70943796E-08   2        22        23   # BR(A0 -> photon Z)
     9.44497098E-06   2        21        21   # BR(A0 -> gluon gluon)
     8.06970275E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.59332662E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.03784047E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.49683024E-14   2        -2         2   # BR(A0 -> Up up)
     4.84382983E-09   2        -4         4   # BR(A0 -> Charm charm)
     3.68465307E-04   2        -6         6   # BR(A0 -> Top top)
     5.76091404E-07   2        -1         1   # BR(A0 -> Down down)
     2.08381658E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.70258661E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.09400538E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     9.66224345E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     9.66224345E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.06458689E-05   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.11866193E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     8.30347207E-06   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.69139624E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     5.20617306E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.12606705E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.53766949E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     4.51289577E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.69150658E-06   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     6.50812973E-06   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.03562252E-05   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.74309526E-07   2        23        25   # BR(A0 -> Z h0)
     5.83286014E-14   2        23        35   # BR(A0 -> Z HH)
     3.35794534E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.62535905E+02   # Gamma(Hp)
     9.22566653E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.94426059E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.11566151E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.73010643E-07   2        -1         2   # BR(Hp -> Down up)
     9.70175284E-06   2        -3         2   # BR(Hp -> Strange up)
     6.55350017E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.70790329E-08   2        -1         4   # BR(Hp -> Down charm)
     2.06521450E-04   2        -3         4   # BR(Hp -> Strange charm)
     9.17724640E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.62205140E-08   2        -1         6   # BR(Hp -> Down top)
     8.79239256E-07   2        -3         6   # BR(Hp -> Strange top)
     6.18993615E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.88260806E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     8.17235121E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     5.88483606E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     7.91088718E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.61271476E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.40495619E-05   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     7.99533226E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.84898877E-11   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.43871499E-07   2        24        25   # BR(Hp -> W h0)
     1.50321728E-13   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.71337529E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.23728789E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.23725923E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00001281E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    4.34164051E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    4.46975473E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.71337529E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.23728789E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.23725923E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999907E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    9.31408348E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999907E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    9.31408348E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25935909E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    9.64146051E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.30370201E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    9.31408348E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999907E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    2.93382786E-04   # BR(b -> s gamma)
    2    1.59628128E-06   # BR(b -> s mu+ mu-)
    3    3.52774069E-05   # BR(b -> s nu nu)
    4    7.97695498E-15   # BR(Bd -> e+ e-)
    5    3.40739407E-10   # BR(Bd -> mu+ mu-)
    6    6.97738592E-08   # BR(Bd -> tau+ tau-)
    7    2.77685145E-13   # BR(Bs -> e+ e-)
    8    1.18617468E-08   # BR(Bs -> mu+ mu-)
    9    2.46037653E-06   # BR(Bs -> tau+ tau-)
   10    9.60215805E-05   # BR(B_u -> tau nu)
   11    9.91866165E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41863553E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93889639E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15738463E-03   # epsilon_K
   17    2.28165973E-15   # Delta(M_K)
   18    2.48131184E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29370846E-11   # BR(K^+ -> pi^+ nu nu)
   20    4.44808358E-16   # Delta(g-2)_electron/2
   21    1.90171431E-11   # Delta(g-2)_muon/2
   22    5.39401435E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.22888938E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.56755789E-01   # C7
     0305 4322   00   2     8.20163678E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -7.61144961E-02   # C8
     0305 6321   00   2     1.79432011E-05   # C8'
 03051111 4133   00   0     1.62073658E+00   # C9 e+e-
 03051111 4133   00   2     1.62138838E+00   # C9 e+e-
 03051111 4233   00   2     7.57341856E-04   # C9' e+e-
 03051111 4137   00   0    -4.44342868E+00   # C10 e+e-
 03051111 4137   00   2    -4.44441831E+00   # C10 e+e-
 03051111 4237   00   2    -5.65319590E-03   # C10' e+e-
 03051313 4133   00   0     1.62073658E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62138814E+00   # C9 mu+mu-
 03051313 4233   00   2     7.57341356E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44342868E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44441855E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.65319649E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50556640E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.22407071E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50556640E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.22407076E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50556640E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.22408472E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85826758E-07   # C7
     0305 4422   00   2     9.73778683E-06   # C7
     0305 4322   00   2     2.97477964E-06   # C7'
     0305 6421   00   0     3.30486135E-07   # C8
     0305 6421   00   2     6.71444153E-05   # C8
     0305 6321   00   2     2.30099338E-06   # C8'
 03051111 4133   00   2     6.47333344E-07   # C9 e+e-
 03051111 4233   00   2     1.41875014E-05   # C9' e+e-
 03051111 4137   00   2     1.53112391E-06   # C10 e+e-
 03051111 4237   00   2    -1.05905588E-04   # C10' e+e-
 03051313 4133   00   2     6.47326283E-07   # C9 mu+mu-
 03051313 4233   00   2     1.41874966E-05   # C9' mu+mu-
 03051313 4137   00   2     1.53113027E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.05905602E-04   # C10' mu+mu-
 03051212 4137   00   2    -2.96103961E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.29314416E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.96103670E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.29314416E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.96022615E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.29314415E-05   # C11' nu_3 nu_3
