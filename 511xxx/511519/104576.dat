# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  15:07
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.08544199E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.45704504E+03  # scale for input parameters
    1    9.57808374E+02  # M_1
    2    1.77929297E+03  # M_2
    3    4.11269216E+03  # M_3
   11    2.05175552E+03  # A_t
   12   -1.00303047E+03  # A_b
   13    1.24753559E+03  # A_tau
   23    1.31231455E+02  # mu
   25    3.97288441E+01  # tan(beta)
   26    2.65359333E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.01905725E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.72120564E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.22599420E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.45704504E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.45704504E+03  # (SUSY scale)
  1  1     8.38702772E-06   # Y_u(Q)^DRbar
  2  2     4.26061008E-03   # Y_c(Q)^DRbar
  3  3     1.01322031E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.45704504E+03  # (SUSY scale)
  1  1     6.69567330E-04   # Y_d(Q)^DRbar
  2  2     1.27217793E-02   # Y_s(Q)^DRbar
  3  3     6.64000980E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.45704504E+03  # (SUSY scale)
  1  1     1.16844395E-04   # Y_e(Q)^DRbar
  2  2     2.41597152E-02   # Y_mu(Q)^DRbar
  3  3     4.06326664E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.45704504E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.05175554E+03   # A_t(Q)^DRbar
Block Ad Q=  4.45704504E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.00303049E+03   # A_b(Q)^DRbar
Block Ae Q=  4.45704504E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.24753558E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.45704504E+03  # soft SUSY breaking masses at Q
   1    9.57808374E+02  # M_1
   2    1.77929297E+03  # M_2
   3    4.11269216E+03  # M_3
  21    7.11468316E+06  # M^2_(H,d)
  22    3.74267370E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.01905725E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.72120564E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.22599420E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22855670E+02  # h0
        35     2.65312492E+03  # H0
        36     2.65359333E+03  # A0
        37     2.65346129E+03  # H+
   1000001     1.01076922E+04  # ~d_L
   2000001     1.00837968E+04  # ~d_R
   1000002     1.01073272E+04  # ~u_L
   2000002     1.00864909E+04  # ~u_R
   1000003     1.01076929E+04  # ~s_L
   2000003     1.00837980E+04  # ~s_R
   1000004     1.01073280E+04  # ~c_L
   2000004     1.00864911E+04  # ~c_R
   1000005     3.37602955E+03  # ~b_1
   2000005     4.14574816E+03  # ~b_2
   1000006     4.14659857E+03  # ~t_1
   2000006     4.79073393E+03  # ~t_2
   1000011     1.00204194E+04  # ~e_L-
   2000011     1.00092481E+04  # ~e_R-
   1000012     1.00196511E+04  # ~nu_eL
   1000013     1.00204227E+04  # ~mu_L-
   2000013     1.00092543E+04  # ~mu_R-
   1000014     1.00196543E+04  # ~nu_muL
   1000015     1.00110635E+04  # ~tau_1-
   2000015     1.00213712E+04  # ~tau_2-
   1000016     1.00205926E+04  # ~nu_tauL
   1000021     4.61409040E+03  # ~g
   1000022     1.50723290E+02  # ~chi_10
   1000023     1.56134499E+02  # ~chi_20
   1000025     9.63659133E+02  # ~chi_30
   1000035     1.88946912E+03  # ~chi_40
   1000024     1.53547030E+02  # ~chi_1+
   1000037     1.88943445E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.45549642E-02   # alpha
Block Hmix Q=  4.45704504E+03  # Higgs mixing parameters
   1    1.31231455E+02  # mu
   2    3.97288441E+01  # tan[beta](Q)
   3    2.42847003E+02  # v(Q)
   4    7.04155756E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99196205E-01   # Re[R_st(1,1)]
   1  2     4.00867022E-02   # Re[R_st(1,2)]
   2  1    -4.00867022E-02   # Re[R_st(2,1)]
   2  2    -9.99196205E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     2.09846563E-03   # Re[R_sb(1,1)]
   1  2     9.99997798E-01   # Re[R_sb(1,2)]
   2  1    -9.99997798E-01   # Re[R_sb(2,1)]
   2  2     2.09846563E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     3.16676715E-02   # Re[R_sta(1,1)]
   1  2     9.99498454E-01   # Re[R_sta(1,2)]
   2  1    -9.99498454E-01   # Re[R_sta(2,1)]
   2  2     3.16676715E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -3.92174526E-02   # Re[N(1,1)]
   1  2     3.29585352E-02   # Re[N(1,2)]
   1  3    -7.12999074E-01   # Re[N(1,3)]
   1  4     6.99291103E-01   # Re[N(1,4)]
   2  1    -2.75411096E-02   # Re[N(2,1)]
   2  2     2.74246324E-02   # Re[N(2,2)]
   2  3     7.01098308E-01   # Re[N(2,3)]
   2  4     7.12004592E-01   # Re[N(2,4)]
   3  1     9.98848760E-01   # Re[N(3,1)]
   3  2     4.20306669E-03   # Re[N(3,2)]
   3  3    -8.65366253E-03   # Re[N(3,3)]
   3  4     4.69957696E-02   # Re[N(3,4)]
   4  1     2.15237173E-03   # Re[N(4,1)]
   4  2    -9.99071548E-01   # Re[N(4,2)]
   4  3    -4.31241747E-03   # Re[N(4,3)]
   4  4     4.28113492E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.09668972E-03   # Re[U(1,1)]
   1  2     9.99981415E-01   # Re[U(1,2)]
   2  1     9.99981415E-01   # Re[U(2,1)]
   2  2     6.09668972E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.05254677E-02   # Re[V(1,1)]
   1  2     9.98166653E-01   # Re[V(1,2)]
   2  1     9.98166653E-01   # Re[V(2,1)]
   2  2     6.05254677E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.93668556E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.56617405E-03    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.72390964E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.97656991E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.35439147E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.24548937E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01642266E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.05779097E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.95992306E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     2.74956119E-03    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.92004247E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.92983728E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.34217842E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.35555499E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     4.83070309E-04    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.74274016E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.23756306E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01383485E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.05259364E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.15165106E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.46559089E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.39990114E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.27345214E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.78251832E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.85271565E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     5.55766881E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.68298652E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.88697847E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     9.65549060E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     7.47085008E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.42592054E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     4.87182776E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.35442568E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.45870162E-04    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     5.77870475E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     8.95778899E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02999805E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.38455055E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.03514014E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.35558910E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.45058684E-04    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     5.77374711E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     8.95010405E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02739871E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.24034750E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02996308E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.68355007E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.61029539E-04    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     4.64943839E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     7.20729508E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.43791365E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.97319896E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.85589815E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.03065508E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.09620556E-02    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.89011851E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.25658152E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.13466480E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.10578420E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.61529503E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.32445706E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.99102454E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.03130355E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.09606489E-02    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.88884621E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.25694319E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.39816842E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.10566647E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.61491326E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.32438081E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.99056376E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.04139876E+01   # ~b_1
#    BR                NDA      ID1      ID2
     2.48290136E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.39882628E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.62874836E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.85534211E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     1.55270479E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.18903103E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.14934038E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.19057251E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     7.36942238E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     5.39927887E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.49269532E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
DECAY   2000002     5.19927917E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.24378807E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.57462486E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.25640204E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.31455519E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     6.60506774E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.20883673E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.31963749E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.99066135E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.19935187E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.24373031E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.57449078E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.25676351E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.31443393E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     6.60468774E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.72696518E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.31956139E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.99020050E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.66296809E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.46907140E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.56072174E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     4.64735183E-03    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     6.99333875E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.17755149E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.38115937E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     6.64841932E-02    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.10182321E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.27266732E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.35376680E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     4.73177055E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.93357785E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.63919953E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     3.88454272E-04    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.92137451E-03    2     1000021         6   # BR(~t_2 -> ~g t)
     8.84812508E-03    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     4.37055139E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.03967472E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.71196038E-10   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     4.81373198E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     1.78647913E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.60457758E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.59343245E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     2.01778854E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.71380998E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.36893908E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.45752266E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.37948374E-03    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.41926045E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.41537339E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.28033028E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.93241620E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     9.13831118E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     9.28416248E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.82694203E-08   # chi^0_2
#    BR                NDA      ID1      ID2
     5.69330193E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     6.41589410E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     2.97535996E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     8.22645974E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     8.19696979E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.85647835E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.84601685E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.92409929E-03    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.10011431E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     5.66357052E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     5.66357052E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     1.71442607E-03    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     1.71442607E-03    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     1.88788067E-03    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     1.88788067E-03    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     1.87229277E-03    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     1.87229277E-03    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     1.43074486E-04    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     1.43074486E-04    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     2.71595733E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.39071194E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.39071194E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     7.47824028E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.63138583E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.40384756E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.33283759E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.40703206E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.39548287E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     3.72600054E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     3.72600054E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.77720044E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.34006985E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.34006985E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     9.15142435E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.42173883E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.06560391E-04    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.37257211E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.29743447E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.90728853E-03    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.67394066E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     5.78868953E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     8.73397224E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     8.73397224E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     4.13357127E+01   # ~g
#    BR                NDA      ID1      ID2
     1.71721078E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.71721078E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     5.89658465E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     5.89658465E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.56664342E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.56664342E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     6.13477289E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     6.13477289E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
     1.18848829E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
#    BR                NDA      ID1      ID2       ID3
     2.62515080E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     2.71949732E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     5.78775249E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.70434692E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.70434692E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.32885428E-03   # Gamma(h0)
     2.54516423E-03   2        22        22   # BR(h0 -> photon photon)
     1.43308208E-03   2        22        23   # BR(h0 -> photon Z)
     2.45197271E-02   2        23        23   # BR(h0 -> Z Z)
     2.11476907E-01   2       -24        24   # BR(h0 -> W W)
     8.04177854E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.22610632E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.32465343E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.70252050E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.48537784E-07   2        -2         2   # BR(h0 -> Up up)
     2.88262754E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.06779523E-07   2        -1         1   # BR(h0 -> Down down)
     2.19457601E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.83303170E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     6.25185353E+01   # Gamma(HH)
     3.52076374E-09   2        22        22   # BR(HH -> photon photon)
     9.13360373E-09   2        22        23   # BR(HH -> photon Z)
     4.41496133E-07   2        23        23   # BR(HH -> Z Z)
     1.57218621E-07   2       -24        24   # BR(HH -> W W)
     1.01369604E-05   2        21        21   # BR(HH -> gluon gluon)
     9.83925391E-09   2       -11        11   # BR(HH -> Electron electron)
     4.38074822E-04   2       -13        13   # BR(HH -> Muon muon)
     1.26518848E-01   2       -15        15   # BR(HH -> Tau tau)
     5.92808433E-14   2        -2         2   # BR(HH -> Up up)
     1.14996402E-08   2        -4         4   # BR(HH -> Charm charm)
     8.44272666E-04   2        -6         6   # BR(HH -> Top top)
     7.30198170E-07   2        -1         1   # BR(HH -> Down down)
     2.64114858E-04   2        -3         3   # BR(HH -> Strange strange)
     6.78240727E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.40321173E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     5.14808640E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     5.14808640E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     4.97679791E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.27033238E-05   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.70808164E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.20142676E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.39640196E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.96745006E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     2.96942757E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.22826419E-06   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     2.41050738E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     6.01714534E+01   # Gamma(A0)
     5.17389842E-08   2        22        22   # BR(A0 -> photon photon)
     4.28047921E-08   2        22        23   # BR(A0 -> photon Z)
     1.92523059E-05   2        21        21   # BR(A0 -> gluon gluon)
     9.74581807E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.33912863E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.25317099E-01   2       -15        15   # BR(A0 -> Tau tau)
     5.63315792E-14   2        -2         2   # BR(A0 -> Up up)
     1.09273160E-08   2        -4         4   # BR(A0 -> Charm charm)
     8.16617160E-04   2        -6         6   # BR(A0 -> Top top)
     7.23242382E-07   2        -1         1   # BR(A0 -> Down down)
     2.61599786E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.71789922E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.48669552E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     5.35144105E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     5.35144105E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     5.74552170E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     4.73100947E-06   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.11658215E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.19727812E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.22639055E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.70189798E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.17800906E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     4.95704147E-06   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     6.89939971E-07   2        23        25   # BR(A0 -> Z h0)
     7.96526079E-41   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     6.85560527E+01   # Gamma(Hp)
     1.06833072E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.56744747E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.29193362E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.90773713E-07   2        -1         2   # BR(Hp -> Down up)
     1.16436562E-05   2        -3         2   # BR(Hp -> Strange up)
     7.33434042E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.27669391E-08   2        -1         4   # BR(Hp -> Down charm)
     2.48958609E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.02706998E-03   2        -5         4   # BR(Hp -> Bottom charm)
     6.03944053E-08   2        -1         6   # BR(Hp -> Down top)
     1.68418902E-06   2        -3         6   # BR(Hp -> Strange top)
     6.93056575E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.17873177E-05   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.81668796E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     6.78252649E-05   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     4.61438522E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     3.46586656E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     4.69462170E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     6.05949305E-07   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.52090534E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.57842896E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.57838105E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00003035E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.03207021E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.33560570E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.52090534E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.57842896E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.57838105E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999627E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.72528576E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999627E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.72528576E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27037621E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    8.03336750E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.24960799E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.72528576E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999627E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.27882713E-04   # BR(b -> s gamma)
    2    1.58942019E-06   # BR(b -> s mu+ mu-)
    3    3.52535185E-05   # BR(b -> s nu nu)
    4    2.52667771E-15   # BR(Bd -> e+ e-)
    5    1.07936865E-10   # BR(Bd -> mu+ mu-)
    6    2.25990890E-08   # BR(Bd -> tau+ tau-)
    7    8.44470601E-14   # BR(Bs -> e+ e-)
    8    3.60757873E-09   # BR(Bs -> mu+ mu-)
    9    7.65376008E-07   # BR(Bs -> tau+ tau-)
   10    9.54579999E-05   # BR(B_u -> tau nu)
   11    9.86044593E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41867835E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93541373E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15691240E-03   # epsilon_K
   17    2.28165559E-15   # Delta(M_K)
   18    2.48000914E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29054046E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.93544902E-16   # Delta(g-2)_electron/2
   21    1.68253275E-11   # Delta(g-2)_muon/2
   22    4.76302092E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.88597888E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.96424978E-01   # C7
     0305 4322   00   2    -1.54835532E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.05849301E-01   # C8
     0305 6321   00   2    -2.15146203E-04   # C8'
 03051111 4133   00   0     1.63085957E+00   # C9 e+e-
 03051111 4133   00   2     1.63109775E+00   # C9 e+e-
 03051111 4233   00   2     3.50643168E-04   # C9' e+e-
 03051111 4137   00   0    -4.45355167E+00   # C10 e+e-
 03051111 4137   00   2    -4.45206659E+00   # C10 e+e-
 03051111 4237   00   2    -2.57908585E-03   # C10' e+e-
 03051313 4133   00   0     1.63085957E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63109764E+00   # C9 mu+mu-
 03051313 4233   00   2     3.50642563E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45355167E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45206670E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.57908567E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50503029E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     5.57172585E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50503029E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     5.57172693E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50503029E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     5.57203129E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85817668E-07   # C7
     0305 4422   00   2     9.54173914E-06   # C7
     0305 4322   00   2     1.73947297E-07   # C7'
     0305 6421   00   0     3.30478348E-07   # C8
     0305 6421   00   2    -3.72262253E-06   # C8
     0305 6321   00   2    -8.53459335E-08   # C8'
 03051111 4133   00   2     2.08211572E-07   # C9 e+e-
 03051111 4233   00   2     7.19084515E-06   # C9' e+e-
 03051111 4137   00   2    -9.18363799E-08   # C10 e+e-
 03051111 4237   00   2    -5.28955948E-05   # C10' e+e-
 03051313 4133   00   2     2.08209278E-07   # C9 mu+mu-
 03051313 4233   00   2     7.19084329E-06   # C9' mu+mu-
 03051313 4137   00   2    -9.18339421E-08   # C10 mu+mu-
 03051313 4237   00   2    -5.28956004E-05   # C10' mu+mu-
 03051212 4137   00   2     3.76974745E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.14272968E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     3.76975593E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.14272968E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     3.77212864E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.14272968E-05   # C11' nu_3 nu_3
