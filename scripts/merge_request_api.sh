#!/usr/bin/env bash

# For coloured output
. scripts/helpers.sh

##############
# Project info
##############
PROJECT_ID=62692
PROJECT_URL=https://gitlab.cern.ch/api/v4/projects
TARGET_BRANCH=master

##############
# Functions for API
##############
LISTMR=`curl --silent --header "PRIVATE-TOKEN: $CI_UPLOAD_TOKEN" \
        "$PROJECT_URL/$PROJECT_ID/merge_requests/?author_username=mcgensvc&state=opened&labels=auto&source_branch=$CI_COMMIT_REF_ID"`
COUNTMR=`echo ${LISTMR} | grep -o "\"source_branch\":\"${CI_COMMIT_REF_NAME}\"" | wc -l`
  
# Function to get information for a single MR
get_mr_info() {
    local iid=$1
    curl --silent --header "PRIVATE-TOKEN: $CI_UPLOAD_TOKEN" \
            "$PROJECT_URL/$PROJECT_ID/merge_requests/$iid/"
}

# Function to clean up old MRs
cleanup_MR() {
  ALLMR=`curl --silent --header "PRIVATE-TOKEN: $CI_UPLOAD_TOKEN" \
        "$PROJECT_URL/$PROJECT_ID/merge_requests/?state=opened"`
  COUNT=`echo ${ALLMR} | grep -o created_at | wc -l`
  
  # Array to hold old MR iid
  OLDIID=()
  
  # Loop over all open MR and find ones that have been created more than 2 months ago
  for i in `seq 0 $((COUNT-1))` ; do
      # Get the iid of the MR
      iid=`echo $ALLMR | jq -c -r ".[$i].iid"`
      # Calculate days that the MR has been open
      created=`gdate -d $(get_mr_info ${iid} | jq -c -r ".created_at") +%j`
      now=`gdate +%j`
      # Remove leading 0's
      created=${created#0}
      now=${now#0}
      # Calculate difference
      daysOpen=$((now-created))
      # Keep track of old MRs
      if (( daysOpen > 30 )) || (( daysOpen < 0 )) ; then
          printWarning "MR: $iid - days open = $daysOpen"
          OLDIID+=($iid)
      else
          printGood "MR: $iid - days open = $daysOpen"
      fi
  done
    
  # Loop over all old MRs
  for i in "${OLDIID[@]}"; do
      # Get the pipeline status
      ci_status=`get_mr_info $i | jq -c -r ".head_pipeline" | jq -c -r ".status"`
      
      printInfo "MR: $iid - pipeline status = $ci_status"

      # if pipeline has failed
      if [[ $ci_status != "success" ]]; then
          labels=`get_mr_info $i | jq -c -r ".labels"`
          # If todelete label exists
          if [[ $labels == *"todelete"* ]] ; then
              printInfo -f "\tMarked todelete. Closing and deleting the branch"
              # Close the MR
              curl --silent --request PUT \
                   --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
                   --header "Content-Type: application/json" \
                   --data "{\"state_event\":\"close\"}" \
                   "${PROJECT_URL}/${PROJECT_ID}/merge_requests/$i";
              # Delete the branch - to be turned on in the future
              #branch=`get_mr_info $i | jq -c -r ".source_branch"`
              #git push origin –delete $branch
          # If todelete label does not exist
          else
              printInfo -f "\tAdding todelete label"
              # Add todelete label
              curl --silent --request PUT \
                   --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
                   --header "Content-Type: application/json" \
                   --data "{\"add_labels\":[\"todelete\"]}" \
                   "${PROJECT_URL}/${PROJECT_ID}/merge_requests/$i";
              # Comment that the MR will be closed in 1 week
              BODY="{
                     \"id\": ${PROJECT_ID},
                     \"merge_request_iid\": \"${CI_MERGE_REQUEST_IID}\",
                     \"body\": \"This MR has been open for more than 30 days. The pipeline has failed and no action has been taken. It is marked to be closed in 1 week.\"
                    }";
              curl --silent --request POST -o /dev/null \
                   --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
                   --header "Content-Type: application/json" \
                   --data "${BODY}" \
                   "${PROJECT_URL}/${PROJECT_ID}/merge_requests/$i/discussions";
          fi
      # if pipeline has succeeded
      else
          # Get username of MR author
          username=`get_mr_info $iid | jq -c -r ".author" | jq -c -r ".username"`
          
          printInfo -f "\tAlerting $username to take action"
          
          # comment that the user should take action
          BODY="{
                 \"id\": ${PROJECT_ID},
                 \"merge_request_iid\": \"${CI_MERGE_REQUEST_IID}\",
                 \"body\": \"This MR has been open for more than 30 days. @$username please take action!\"
                }";
          curl --silent --request POST -o /dev/null \
               --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
               --header "Content-Type: application/json" \
               --data "${BODY}" \
               "${PROJECT_URL}/${PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/discussions";
      fi
  done
}

# Function to open new MR
open_new_MR() {
    # Check how many open MRs there are for the source branch
    printInfo "There are: $COUNTMR open MR for $CI_COMMIT_REF_NAME"
    
    # The curl request for opening a MR
    BODY="{
        \"id\": ${PROJECT_ID},
        \"source_branch\": \"${CI_COMMIT_REF_NAME}\",
        \"target_branch\": \"${TARGET_BRANCH}\",
        \"remove_source_branch\": true,
        \"title\": \"Automatic merge: ${CI_COMMIT_REF_NAME}\",
        \"labels\":[\"auto\",\"jobOptions\"]
    }";
    
    
    if [ ${COUNTMR} -eq "0" ]; then
        curl --request POST \
            --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
            --header "Content-Type: application/json" \
            --data "${BODY}" \
            "${PROJECT_URL}/${PROJECT_ID}/merge_requests";
        printGood "\tOpened a new merge request";
    else
        printError "No new MR created - the following open MR exist for the branch $CI_COMMIT_REF_NAME"
        for i in `seq 0 $((COUNTMR-1))` ; do
            echo $LISTMR | jq -c -r ".[$i].iid"
            exit 1
        done
    fi
}

# Function to approve and merge existing MR
list_approve_merge_MR() {
    # Make sure there is only 1 open MR for this branch
    if [ ${COUNTMR} -ne "1" ]; then
        printError "There are $COUNTMR open MR for $CI_COMMIT_REF_NAME - this indicates an error somewhere upstream"
        exit 1
    else
        iid=`echo $LISTMR | jq -c -r ".[0].iid"`
    fi
    
    # Get number of approvals left
    approvals_left=`curl --silent --header "PRIVATE-TOKEN: $CI_UPLOAD_TOKEN" \
                    "$PROJECT_URL/$PROJECT_ID/merge_requests/$iid/approvals" | jq -c -r ".approvals_left"`
    printInfo "MR: $iid - approvals left: $approvals_left"
    
    # approve MR
    if $approve; then
        printInfo "Approving $iid"
        if [ ${approvals_left} -gt "0" ] ; then
            curl --silent --request POST \
                 --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
                 "${PROJECT_URL}/${PROJECT_ID}/merge_requests/$iid/approve";
        fi
    fi
    
    # merge MR
    if $merge ; then
        BODY="{
            \"id\": ${PROJECT_ID},
            \"squash\": true,
            \"should_remove_source_branch\": true,
            \"force_remove_source_branch\": true,
            \"merge_request_iid\": \"${iid}\",
            \"merge_when_pipeline_succeeds\": true
        }";
    
        printInfo "Toggling merge_when_pipeline_succeeds for $iid"
        curl --silent --request PUT \
             --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
             --header "Content-Type: application/json" \
             --data "${BODY}" \
             "${PROJECT_URL}/${PROJECT_ID}/merge_requests/$iid/merge";
    fi
}

##############
# Main script
##############
merge=false
approve=false
list=false
open=false
cleanup=false
for opt in $@ ; do
    case $opt in
      -a|--approve)
        approve=true
        list=true ;;
      -m|--merge)
        merge=true
        list=true ;;
      -l|--list)
        list=true ;;
      -o|--open)
        open=true ;;
      -c|--cleanup)
        cleanup=true ;;
      *)
        ;;
    esac
done

export PATH=$PWD:$PATH

if $list; then
  list_approve_merge_MR
elif $open ; then
  open_new_MR
elif $cleanup ; then
  cleanup_MR
fi

