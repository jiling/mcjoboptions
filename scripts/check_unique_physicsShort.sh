#!/bin/bash

# For coloured output
. scripts/helpers.sh

# Make sure that if a command fails the script will continue
set +e

unique=true

# Get the files modified in the last commit
modified=($(git diff-tree --name-only -r origin/master..HEAD --diff-filter=AM | grep -E "mc.*py"))

if [[ "${#modified[@]}" > 0 ]] ; then
  printInfo "jO files modified in the latest commit: ${#modified[@]}"
else
  printGood "No jO files modified since last commit"
  exit 0
fi

# Loop over modified files
for file in "${modified[@]}" ; do
  # Get physics short
  physicsShort=$(echo $file | awk 'BEGIN {FS="/"} ; END {print $NF}' | sed -E "s/mc\.//" | sed "s/\.py//")
  printInfo "Checking $file : physicsShort = $physicsShort"
  # Find how many files have same physics short
  nfiles=$(find . -name "mc.$physicsShort.py" | wc -l)
  if (( $nfiles > 1 )) ; then
    unique=false
    printError "Duplicate physicsShort found:"
    find . -name "mc.$physicsShort.py"
  fi
done

if [ "$unique" = true ] ; then
  printGood "Result: SUCCESS"
  exit 0
else
  printError "Result: FAILURE"
  exit 1
fi
