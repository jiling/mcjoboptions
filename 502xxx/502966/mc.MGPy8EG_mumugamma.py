#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'MadGraph+Pythia8 samples for mumugamma plus jets production at LO'
evgenConfig.keywords = ['Z','photon','muon','jets']
evgenConfig.contact = ['shuqi.li@cern.ch']
evgenConfig.generators = ['MadGraph', 'Pythia8']


# -------------------------------------------------------------- 
# Generate events
# -------------------------------------------------------------- 
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *


# General settings
nevents = runArgs.maxEvents*1.2 if runArgs.maxEvents>0 else 1.2*evgenConfig.nEventsPerJob


# Madgraph run card and shower settings

# MG Particle cuts
mllcut=10
# Shower/Merging settings
maxjetflavor=5
nJetMax=3
ickkw=0
ktdurham=30
dparameter=0.4


gridpack_mode=True


if not is_gen_from_gridpack():
    process="""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu+ mu- a @0
    add process p p > mu+ mu- a j @1
    add process p p > mu+ mu- a j j @2
    add process p p > mu+ mu- a j j j @3
    output -f
    """

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION


settings = { 'lhe_version'   : '3.0',
	     'cut_decays'    : 'F',
	     'maxjetflavor'  : int(maxjetflavor),
	     'asrwgtflavor'  : int(maxjetflavor),
	     'ickkw'         : 0,
	     'xqcut'         : 0,
	     'ptj'           : 10.0,  #jet
             'ptjmax'        : -1,
	     'etaj'          : -1,
             'mmjj'          : 0,
	     'ptl'           : 0,   #lepton
	     'ptlmax'        : -1.0,
	     'etal'          : -1.0,
	     'etalmin'       : 0.0,
	     'mmll'          : mllcut,
	     'drll'          : 0.0,
             'ptamax'        : -1, #photon
             'etaa'          : -1,
             'dral'          : 0.1,
	     'ptgmin'        : 7,
             'epsgamma'      : 0.1,
             'R0gamma'       : 0.1,
             'xn'            : 2,
             'isoEM'         : 'True',
             'ktdurham'      : ktdurham,
	     'dparameter'    : dparameter,
	     'nevents'       : int(nevents)
	   }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  


# --------------------------------------------------------------
# Shower settings 
# --------------------------------------------------------------
process="pp>mu+mu-a"

PYTHIA8_nJetMax=nJetMax
PYTHIA8_TMS=settings['ktdurham']
PYTHIA8_Dparameter=settings['dparameter']
PYTHIA8_Process=process
PYTHIA8_nQuarksMerge=settings['maxjetflavor']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")

