evgenConfig.keywords    = ['Higgs'] 
evgenConfig.contact     = ['ana.cueto@cern.ch'] 
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen'] 
evgenConfig.description = """ggH+0,1,2 jet at LO in SMEFT"""

from MadGraphControl.MadGraphUtils import *
from os.path import join as pathjoin 
import os
import sys

safe_factor=2.
nevents=safe_factor*runArgs.maxEvents


#==================================================================================

# Merging parameters in MadGraph. removed xqcut
ickkw=0

ptj=20.0
ptb=20.0

ptl=0.0
etal=10

drll=0.05
drjj=0.1
drbb=0.1
drbj=0.1

pdfwgt='T'

# Additional merging parameters in Pythia
ktdurham=30
dparameter=0.4
nJetMax=2 
physProcess='pp>h'

maxjetflavor=5  # should be 5 if b included;

mbottom=0
ymb=0
#==================================================================================


process = ("""
import model SMEFTsim_U35_MwScheme_UFO-massless
set gauge unitary
define p = p b b~
define j = p
generate p p > h QED=1 NP^2==1 @0
add process p p > h j QED=1 NP^2==1 @1
add process p p > h j j QED=1 NP^2==1 @2
output -f
""")

#==================================================================================
# General settings
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#==================================================================================

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0', 
             'cut_decays':'F', 
             'pdlabel':'lhapdf',
             'lhaid':'90400',
             'ickkw'       : ickkw,
             'drll'        : drll,
             'drjj'        : drjj,
             'drbb'        : drbb,
             'drbj'        : drbj,
             'ptj'         : ptj,
             'ptb'         : ptb,
             'ptl'         : ptl,
             'etal'        : etal,
             'pdfwgt'      : pdfwgt,
             'maxjetflavor': maxjetflavor,
             'dparameter'  : dparameter,
             'ktdurham'    : ktdurham,
             'use_syst'    : "False",
             'nevents'     : int(nevents),
             'xqcut'       : 0.0,
             'beamEnergy'  : beamEnergy,
}

#==================================================================================
process_dir = new_process(process)

#==================================================================================

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

#==================================================================================

#param_card = open('param_card.dat','w')
#param_card_default = open('/afs/cern.ch/user/s/smonig/afsWorkspace/Analysis/EFT/lapp-eftinterpretation/MadGraph/ParamCards/param_card_SMEFTsim_A_U35_MwScheme_UFO_v3-massless_default.dat','r')

smeft={
    'cG':0,
    'cW':0,
    'cH':0,
    'cHbox':0,
    'cHDD':0,
    'cHG':1.0,
    'cHW':0,
    'cHB':0,
    'cHWB':0,
    'ceHRe':0,
    'cuHRe':0,
    'cdHRe':0,
    'ceWRe':0,
    'ceBRe':0,
    'cuGRe':0,
    'cuWRe':0,
    'cuBRe':0,
    'cdGRe':0,
    'cdWRe':0,
    'cdBRe':0,
    'cHl1':0,
    'cHl3':0,
    'cHe':0,
    'cHq1':0,
    'cHq3':0,
    'cHu':0,
    'cHd':0,
    'cHudRe':0,
    'cll':0,
    'cll1':0,
    'cqq1':0,
    'cqq11':0,
    'cqq3':0,
    'cqq31':0,
    'clq1':0,
    'clq3':0,
    'cee':0,
    'cuu':0,
    'cuu1':0,
    'cdd':0,
    'cdd1':0,
    'ceu':0,
    'ced':0,
    'cud1':0,
    'cud8':0,
    'cle':0,
    'clu':0,
    'cld':0,
    'cqe':0,
    'cqu1':0,
    'cqu8':0,
    'cqd1':0,
    'cqd8':0,
    'cledqRe':0,
    'cquqd1Re':0,
    'cquqd11Re': 0,
    'cquqd8Re':0,
    'cquqd81Re': 0,
    'clequ1Re':0,
    'clequ3Re':0
}


yukawa={'ymb' : ymb}
mass={'MB' : mbottom}

params={}
#params['yukawa']=yukawa
params['mass']=mass
params['smeft']=smeft

modify_param_card(process_dir=process_dir,params=params)

#param_card.close()
#param_card_default.close()

#==================================================================================

print_cards()


#==================================================================================
rcard = open('reweight_card.dat','w')

params=["cHG"]
block=[6]
vals=[-1.0,-0.5,-0.1,-0.01,-0.001,-0.0001,0.0001,0.001,0.01,0.1,0.5,1.0]

reweightCommand=""


for par in params:
    for val in vals:
        sign = "p"
        if val < 0.0:
            sign="m"
        valabs = str(abs(val)).replace(".","p")

        launch="launch --rwgt_name="+str(par)+"_"+sign+"_"+str(valabs)+"\n"
        reweightCommand=reweightCommand+launch
        idx=0
        for par2 in params:
            paramstring=""
            if(par2==par):
                paramstring="set smeft "+str(block[idx])+" "+str(val)+"\n"
            else:
                paramstring="set smeft "+str(block[idx])+" 0.0"+"\n"
            reweightCommand=reweightCommand+paramstring
            idx=idx+1

#This is SM                                                                                                                                                               
SMprocess="change process p p > h QED=1 NP=0 @0\n"
SMprocess+="change process p p > h j QED=1 NP=0 @1 --add \n"
SMprocess+="change process p p > h j j QED=1 NP=0 @2 --add\n"
launch="launch --rwgt_name=SM"+"\n"
reweightCommand=reweightCommand+"\n"+SMprocess+launch


rcard.write(reweightCommand)
rcard.close()

subprocess.call('cp reweight_card.dat ' + process_dir+'/Cards/', shell=True)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

#==================================================================================

# Shower 

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

PYTHIA8_nJetMax=nJetMax
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process=physProcess
PYTHIA8_TMS=ktdurham
PYTHIA8_nQuarksMerge=maxjetflavor
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
genSeq.Pythia8.Commands+=["Merging:mayRemoveDecayProducts=on"]
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 22 22' ]
