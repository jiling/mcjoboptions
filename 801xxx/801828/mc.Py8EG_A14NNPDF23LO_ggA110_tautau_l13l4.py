include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
                            "HiggsBSM:gg2A3 = on",
                            "36:m0 = 110.000",
                            "36:mMin = 15",
                            "36:onMode = off",
                            "36:onIfAny = 15",
                            "15:onMode = off",
                            "15:onIfAny = 11 13",
                            "-15:onMode = off",
                            "-15:onIfAny = 11 13"]

evgenConfig.process     = "gg->A(110), A(110)->tautau"
evgenConfig.description = "Pythia 8 A->tau(lep)tau(lep) production with NNPDF23LO tune"
evgenConfig.keywords    = ["BSM", "Higgs", "2tau"]
evgenConfig.contact     = ["Tom Kresse <tom.kresse@cern.ch>"]

# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lep13lep4filter = TauFilter("lep13lep4filter")
  filtSeq += lep13lep4filter

filtSeq.lep13lep4filter.UseNewOptions = True
filtSeq.lep13lep4filter.Ntaus = 2
filtSeq.lep13lep4filter.Nleptaus = 2
filtSeq.lep13lep4filter.Nhadtaus = 0
filtSeq.lep13lep4filter.EtaMaxlep = 2.7
filtSeq.lep13lep4filter.Ptcutlep = 4000.0 #MeV
filtSeq.lep13lep4filter.Ptcutlep_lead = 13000.0 #MeV
