evgenConfig.description = "pp->J/psi(1S)(mu6mu2.5 vetoing mu6mu4) production with Photos"
evgenConfig.process = "J/Psi(1S) -> 2mu"
evgenConfig.keywords = ["charmonium","2muon"]
evgenConfig.contact  = ["sabidi@cern.ch" "rles@cern.ch"]
evgenConfig.nEventsPerJob = 500

include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include('Pythia8B_i/Pythia8B_Photospp.py')
include("Pythia8B_i/Pythia8B_Charmonium_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 3.']
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [6.0,2.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [1,2]

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
MultiLeptonFilter = MultiLeptonFilter("MultiLeptonFilter")
filtSeq += MultiLeptonFilter
filtSeq.MultiLeptonFilter.Ptcut = 4000.
filtSeq.MultiLeptonFilter.Etacut = 2.5
filtSeq.MultiLeptonFilter.NLeptons = 2

filtSeq.Expression = "(not MultiLeptonFilter)"
