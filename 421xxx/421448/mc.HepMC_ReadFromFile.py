evgenConfig.description = "Photonuclear events generated with STARlight using DPMJET"
evgenConfig.keywords = ["QCD","coherent","minBias"]
evgenConfig.tune = "none"

from TruthIO.TruthIOConf import HepMCReadFromFile
genSeq += HepMCReadFromFile()
genSeq.HepMCReadFromFile.InputFile="events.hepmc"
evgenConfig.generators += ["HepMCAscii"]

del testSeq.TestHepMC
rel = os.popen("echo $AtlasVersion").read()
if (rel[:2] == 21) :
    del testSeq.TestLHE


