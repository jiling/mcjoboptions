# based on 421414, filter moved to xAOD format
##############################################################
# B0d -> J/psi (mu3.5mu3.5) K0*(K+ pi-) and Bplus -> J/psi Kplus
##############################################################
# JOs absed on 421414, only filter format changed to xAOD

evgenConfig.description = "Exclusive B0d -> J/psi (mu3.5mu3.5) K0*(K+ pi-) and Bplus -> J/psi Kplus with a tau cut"
evgenConfig.keywords    = ["exclusive", "B0", "2muon", "Jpsi", "Bplus"]
evgenConfig.nEventsPerJob = 5000
evgenConfig.contact = ["adam.edward.barton@cern.ch"]

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py")
include("Pythia8B_i/Pythia8B_exclusiveB_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11.']

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 9.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 2.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True


# define pseudo-J/psi for exclusive decay
genSeq.Pythia8B.Commands += ['999443:all = myJ/psi antimyJ/psi 3 0 0 3.09692 0.00009 3.09602 3.09782 0']  # name antiName spinType chargeType colType m0 mWidth mMin mMax tau0
genSeq.Pythia8B.Commands += ['999313:all = myK*0 antimyK*0 3 0 0 0.89594 0.04870 0.65000 1.20000 0']  # name antiName spinType chargeType colType m0 mWidth mMin mMax tau0

# force its decay to mu+ mu-
genSeq.Pythia8B.Commands += ['999443:onMode = off'] 
genSeq.Pythia8B.Commands += ['999443:oneChannel = on 1. 0 -13 13'] # onMode bRatio meMode products

genSeq.Pythia8B.Commands += ['999313:onMode = off'] 
genSeq.Pythia8B.Commands += ['999313:oneChannel = on 1. 0 321 -211'] # onMode bRatio meMode products

genSeq.Pythia8B.Commands += ['521:m0 = 5.27929'] # PDG2014 mass
genSeq.Pythia8B.Commands += ['521:addChannel = 2 1. 0 999443 321'] # onMode bRatio meMode products
genSeq.Pythia8B.Commands += ['521:onPosIfMatch = 999443 321']

genSeq.Pythia8B.Commands += ['511:addChannel = 2 1. 0 999443 999313'] # onMode bRatio meMode products
genSeq.Pythia8B.Commands += ['511:onPosIfMatch = 999443 999313']

genSeq.Pythia8B.Commands += ['511:tau0 = 2.2935']
genSeq.Pythia8B.Commands += ['521:tau0 = 2.4555']

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True


genSeq.Pythia8B.NHadronizationLoops = 10

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [2]



#genSeq.Pythia8B.UserSelection = "BD_BPLUS_TAUCUT"
#genSeq.Pythia8B.UserSelectionVariables = [ 8.0, 999999.]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)

# add two instances of xAODHeavyFlavorHadronFilter for b and c filtering
include("GeneratorFilters/xAODBHadronFilter_Common.py")
filtSeq.xAODHeavyFlavorBHadronFilter.BottomEtaMax = 2.9
filtSeq.xAODHeavyFlavorBHadronFilter.BottomPtMin = 5*GeV
filtSeq.xAODHeavyFlavorBHadronFilter.RequireTruthJet = True
filtSeq.xAODHeavyFlavorBHadronFilter.JetPtMin = 10*GeV
filtSeq.xAODHeavyFlavorBHadronFilter.JetEtaMax = 2.9
filtSeq.xAODHeavyFlavorBHadronFilter.TruthContainerName = "AntiKt4TruthJets"
filtSeq.xAODHeavyFlavorBHadronFilter.DeltaRFromTruth = 0.4

include("GeneratorFilters/xAODCHadronFilter_Common.py")
filtSeq.xAODHeavyFlavorCHadronFilter.CharmPtMin=4*GeV
filtSeq.xAODHeavyFlavorCHadronFilter.CharmEtaMax=3.0

filtSeq.Expression = "( xAODHeavyFlavorBHadronFilter) and (not xAODHeavyFlavorCHadronFilter)"

