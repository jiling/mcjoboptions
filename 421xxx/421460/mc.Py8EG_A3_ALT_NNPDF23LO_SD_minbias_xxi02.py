#Originally adapted from: MC15.361204.Pythia8_A2_MSTW2008LO_SD_minbias.py
# Pythia8 minimum bias SD with A3
# using 421447 only moving filter to xAOD
evgenConfig.description = "SD Minbias with Pythia8 A3"
evgenConfig.contact        = ['ewelina@mail.desy.de']
evgenConfig.keywords = ["minBias", "diffraction","SD"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.nEventsPerJob = 1000

include("Pythia8_i/Pythia8_A3_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:singleDiffractive = on"]

from AthenaCommon.SystemOfUnits import GeV
include("GeneratorFilters/xAODForwardProtonFilter_Common.py")

filtSeq.xAODForwardProtonFilter.xi_min = 0.00
filtSeq.xAODForwardProtonFilter.xi_max = 0.20
filtSeq.xAODForwardProtonFilter.beam_energy = 6500.*GeV
filtSeq.xAODForwardProtonFilter.pt_min = 0.5*GeV
filtSeq.xAODForwardProtonFilter.pt_max = 1.5*GeV

#include ("GeneratorFilters/FindJets.py")
#CreateJets(prefiltSeq, 0.4)
#AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)
        
#from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
#if "QCDTruthMultiJetFilter" not in filtSeq:
#    filtSeq += QCDTruthMultiJetFilter()

#filtSeq.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
#filtSeq.QCDTruthMultiJetFilter.NjetMinPt = 12.*GeV
#filtSeq.QCDTruthMultiJetFilter.Njet = 2
#filtSeq.QCDTruthMultiJetFilter.DoShape = False
#filtSeq.QCDTruthMultiJetFilter.MaxEta = 4.

