# taken from PowhegControl/share/example/processes/ZZ

#--------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include("PowhegControl/PowhegControl_ZZ_Common.py")
PowhegConfig.nEvents     *= 5. # compensate filter efficiency 
PowhegConfig.generate()
#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 ZZ production with A14 NNPDF2.3 tune"
evgenConfig.keywords    = [ "SM", "diboson", "ZZ" ]
evgenConfig.contact     = [ "james.robinson@cern.ch" ]

#---------------------------------------------------------------------------------------------------
# xAOD Filter for 2 electrons 
#---------------------------------------------------------------------------------------------------
include ("GeneratorFilters/xAODMultiElectronFilter_Common.py")
filtSeq.xAODMultiElectronFilter.Ptcut = 13000.0
filtSeq.xAODMultiElectronFilter.Etacut = 3.0
filtSeq.xAODMultiElectronFilter.NElectrons = 2


