include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/PDF4LHC21.py")

evgenConfig.description = "Electroweak same-sign llvvjj+0,1j@LO  with a t-channel requirement to suppress triboson final states."
evgenConfig.keywords = ["SM", "diboson", "triboson", "2lepton", "jets", "VBS"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch"]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
ME_GENERATORS:
- Comix

SCALES: VBF

MEPS:
  CORE_SCALE: VAR{PPerp(p[6])*PPerp(p[7])}

COMIX_DEFAULT_GAUGE: 0

# improve integration
PSI:
  ITMIN: 25000
  NOPT: 15

# Vector boson production process
PROCESSES:
- 93 93 -> -12 11 -12 11 93 93 93{1}:
    Order: {QCD: 0, EW: 6}
    CKKW: 20
    Min_N_TChannels: 1
- 93 93 -> -12 11 -14 13 93 93 93{1}:
    Order: {QCD: 0, EW: 6}
    CKKW: 20
    Min_N_TChannels: 1
- 93 93 -> -14 13 -14 13 93 93 93{1}:
    Order: {QCD: 0, EW: 6}
    CKKW: 20
    Min_N_TChannels: 1
- 93 93 -> 12 -11 12 -11 93 93 93{1}:
    Order: {QCD: 0, EW: 6}
    CKKW: 20
    Min_N_TChannels: 1
- 93 93 -> 12 -11 14 -13 93 93 93{1}:
    Order: {QCD: 0, EW: 6}
    CKKW: 20
    Min_N_TChannels: 1
- 93 93 -> 14 -13 14 -13 93 93 93{1}:
    Order: {QCD: 0, EW: 6}
    CKKW: 20
    Min_N_TChannels: 1

# Selection criteria
SELECTORS:
- FastjetSelector:
    Expression: Mass(p[6]+p[7])>100
    Algorithm: antikt
    N: 2
    PTMin: 15.0
    EtaMax: 5.5
    DR: 0.2
"""

genSeq.Sherpa_i.OpenLoopsLibs = []
genSeq.Sherpa_i.ExtraFiles = []
genSeq.Sherpa_i.NCores = 32
