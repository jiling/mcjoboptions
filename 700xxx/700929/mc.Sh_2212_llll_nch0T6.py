evgenConfig.description = "4 charged leptons with 0,1j@NLO + 2,3j@LO and mll>4GeV, pTl1>5 GeV, pTl2>5 GeV with nch filter taking input from existing unfiltered input file."
evgenConfig.keywords =  ["SM", "diboson", "4lepton", "jets", "NLO" ]
evgenConfig.contact  = [ "oldrich.kepka@cern.ch" ]
evgenConfig.nEventsPerJob = 1000
evgenConfig.inputFilesPerJob = 15

if runArgs.trfSubstepName == 'generate' :
   print ("ERROR: These JO require an input file.  Please use the --afterburn option")
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   #--------------------------------------------------------------
   include('GeneratorFilters/MultiLeptonFilter.py')
   ### Default cut params
   filtSeq.MultiLeptonFilter.Ptcut = 3500.
   filtSeq.MultiLeptonFilter.Etacut = 2.7
   filtSeq.MultiLeptonFilter.NLeptons = 2

   include('GeneratorFilters/ChargedTrackWeightFilter.py')
   filtSeq.ChargedTracksWeightFilter.NchMin = 0
   filtSeq.ChargedTracksWeightFilter.NchMax = 6
   filtSeq.ChargedTracksWeightFilter.Ptcut = 500
   filtSeq.ChargedTracksWeightFilter.Etacut= 2.5
   filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 4, 8, 15, 25 ]
   filtSeq.ChargedTracksWeightFilter.SplineY =[ 0.009, 0.009, 0.015, 0.024, 0.028, 0.028 ]

   postSeq.CountHepMC.CorrectRunNumber = True

