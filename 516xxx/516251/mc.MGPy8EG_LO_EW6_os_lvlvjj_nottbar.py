from MadGraphControl.MadGraphUtils import *

import os

evgenConfig.generators += ["MadGraph", "EvtGen"]
evgenConfig.keywords = ['SM', 'diboson', 'VBS', 'WW', 'oppositeSign', 'electroweak', '2lepton', '2jet']
evgenConfig.contact = ['Karolos Potamianos <karolos.potamianos@cern.ch>']

useCluster = False
physShort = get_physics_short()

evgenConfig.nEventsPerJob = 10000

safetyFactor = 1.1
nevents = int(runArgs.maxEvents*safetyFactor if runArgs.maxEvents>0 else safetyFactor*evgenConfig.nEventsPerJob)

required_accuracy = 0.01


gridpack_mode=True

runName = 'lvlvjj_os_nottbar_EW6'
evgenConfig.description = 'MadGraph_' + runName
process = """
import model sm-no_b_mass
define l+ = e+ mu+ ta+
define vl = ve vm vt
define l- = e- mu- ta-
define vl~ = ve~ vm~ vt~
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate p p > l+ vl l- vl~ j j $ t t~ QED==6 QCD=0 @0
output -f"""

#Fetch default NLO run_card.dat and set parameters
extras = { 'lhe_version'  :'3.0',
           'pdlabel'      :"'lhapdf'",
           'lhaid'        : 260000,
           'bwcutoff'     :'15',
           'nevents'      :nevents,
           'dynamical_scale_choice': 0,
           'maxjetflavor': 5, 
           'ptl': 4.0, 
           'ptj': 15.0, 
           'etal': 3.0, 
           'etaj': 5.5, 
           'drll': 0.2, 
           'systematics_program': 'systematics',  
           'systematics_arguments': "['--mur=0.5,1,2', '--muf=0.5,1,2', '--dyn=-1,1,2,3,4', '--weight_info=MUR%(mur).1f_MUF%(muf).1f_PDF%(pdf)i_DYNSCALE%(dyn)i', '--pdf=errorset,NNPDF30_nlo_as_0119@0,NNPDF30_nlo_as_0117@0,CT14nlo@0,MMHT2014nlo68clas118@0,PDF4LHC15_nlo_30_pdfas']"}

process_dir = new_process(process)

if not is_gen_from_gridpack(): # When generating the gridpack
  # Todo: replace in line to be safer
  os.system("cp collect_events.f "+process_dir+"/SubProcesses")


if not is_gen_from_gridpack(): # When generating the gridpack
  os.system("cp setscales_lo.f  "+process_dir+"/SubProcesses/setscales.f")

lo_extras = { 'asrwgtflavor': 5, 
                'auto_ptj_mjj': False, 
                'cut_decays': True, 
                'ptb': 15.0, 
                'etab': 5.5, 
                'dral': 0.1, 
                'drbb': 0.2, 
                'drbj': 0.2, 
                'drbl': 0.2, 
                'drjj': 0.2,
                'drjl': 0.2, 
                'mmll': 0.0, 
                'gridpack': '.true.',
                'use_syst': True, 
            }
extras.update(lo_extras)

if useCluster: modify_config_card(process_dir=process_dir,settings={'cluster_type':'condor'})
modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=extras)
#modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs,required_accuracy=required_accuracy)
arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3,saveProcDir=True)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
# fix for VBS processes (requires version>=8.230)
genSeq.Pythia8.Commands += [ "SpaceShower:pTmaxMatch=1",
                             "SpaceShower:pTmaxFudge=1",
                             "SpaceShower:MEcorrections=off",
                             "TimeShower:pTmaxMatch=1",
                             "TimeShower:pTmaxFudge=1",
                             "TimeShower:MEcorrections=off",
                             "TimeShower:globalRecoil=on",
                             "TimeShower:limitPTmaxGlobal=on",
                             "TimeShower:nMaxGlobalRecoil=1",
                             "TimeShower:globalRecoilMode=2",
                             "TimeShower:nMaxGlobalBranch=1",
                             "TimeShower:weightGluonToQuark=1",
                             "Check:epTolErr=1e-2"]

  
genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]


include("Pythia8_i/Pythia8_MadGraph.py")
# include("Pythia8_i/Pythia8_ShowerWeights.py")

  
