# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.05.2022,  06:40
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.36477975E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.03326719E+03  # scale for input parameters
    1    3.46565419E+02  # M_1
    2   -5.51498690E+02  # M_2
    3    4.35758242E+03  # M_3
   11    2.48694374E+03  # A_t
   12   -1.30822307E+03  # A_b
   13   -1.22144855E+03  # A_tau
   23   -7.07350697E+02  # mu
   25    3.25552463E+01  # tan(beta)
   26    1.92808391E+03  # m_A, pole mass
   31    3.36036053E+02  # M_L11
   32    3.36036053E+02  # M_L22
   33    1.89599622E+03  # M_L33
   34    1.00698354E+03  # M_E11
   35    1.00698354E+03  # M_E22
   36    1.08926704E+03  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.42927251E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.48366214E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.29859418E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.03326719E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.03326719E+03  # (SUSY scale)
  1  1     8.38832668E-06   # Y_u(Q)^DRbar
  2  2     4.26126995E-03   # Y_c(Q)^DRbar
  3  3     1.01337724E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.03326719E+03  # (SUSY scale)
  1  1     5.48752571E-04   # Y_d(Q)^DRbar
  2  2     1.04262989E-02   # Y_s(Q)^DRbar
  3  3     5.44190597E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.03326719E+03  # (SUSY scale)
  1  1     9.57613365E-05   # Y_e(Q)^DRbar
  2  2     1.98004072E-02   # Y_mu(Q)^DRbar
  3  3     3.33010276E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.03326719E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.48694366E+03   # A_t(Q)^DRbar
Block Ad Q=  4.03326719E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.30822278E+03   # A_b(Q)^DRbar
Block Ae Q=  4.03326719E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.22144853E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.03326719E+03  # soft SUSY breaking masses at Q
   1    3.46565419E+02  # M_1
   2   -5.51498690E+02  # M_2
   3    4.35758242E+03  # M_3
  21    3.38272619E+06  # M^2_(H,d)
  22   -1.37245712E+05  # M^2_(H,u)
  31    3.36036053E+02  # M_(L,11)
  32    3.36036053E+02  # M_(L,22)
  33    1.89599622E+03  # M_(L,33)
  34    1.00698354E+03  # M_(E,11)
  35    1.00698354E+03  # M_(E,22)
  36    1.08926704E+03  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.42927251E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.48366214E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.29859418E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23023157E+02  # h0
        35     1.92764557E+03  # H0
        36     1.92808391E+03  # A0
        37     1.92949314E+03  # H+
   1000001     1.01007480E+04  # ~d_L
   2000001     1.00759526E+04  # ~d_R
   1000002     1.01003867E+04  # ~u_L
   2000002     1.00790937E+04  # ~u_R
   1000003     1.01007501E+04  # ~s_L
   2000003     1.00759556E+04  # ~s_R
   1000004     1.01003887E+04  # ~c_L
   2000004     1.00790943E+04  # ~c_R
   1000005     3.44292946E+03  # ~b_1
   2000005     4.53703755E+03  # ~b_2
   1000006     3.58313603E+03  # ~t_1
   2000006     4.53994605E+03  # ~t_2
   1000011     3.53910859E+02  # ~e_L-
   2000011     1.01272740E+03  # ~e_R-
   1000012     3.44413290E+02  # ~nu_eL
   1000013     3.53850119E+02  # ~mu_L-
   2000013     1.01269177E+03  # ~mu_R-
   1000014     3.44369865E+02  # ~nu_muL
   1000015     1.07808491E+03  # ~tau_1-
   2000015     1.89549387E+03  # ~tau_2-
   1000016     1.89304102E+03  # ~nu_tauL
   1000021     4.83600279E+03  # ~g
   1000022     3.40127390E+02  # ~chi_10
   1000023     5.64797866E+02  # ~chi_20
   1000025     7.12004147E+02  # ~chi_30
   1000035     7.32317939E+02  # ~chi_40
   1000024     5.65005105E+02  # ~chi_1+
   1000037     7.33434206E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.99150089E-02   # alpha
Block Hmix Q=  4.03326719E+03  # Higgs mixing parameters
   1   -7.07350697E+02  # mu
   2    3.25552463E+01  # tan[beta](Q)
   3    2.42946972E+02  # v(Q)
   4    3.71750756E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -4.18737074E-02   # Re[R_st(1,1)]
   1  2     9.99122912E-01   # Re[R_st(1,2)]
   2  1    -9.99122912E-01   # Re[R_st(2,1)]
   2  2    -4.18737074E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -5.99818949E-03   # Re[R_sb(1,1)]
   1  2     9.99982011E-01   # Re[R_sb(1,2)]
   2  1    -9.99982011E-01   # Re[R_sb(2,1)]
   2  2    -5.99818949E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.62572483E-02   # Re[R_sta(1,1)]
   1  2     9.99867842E-01   # Re[R_sta(1,2)]
   2  1    -9.99867842E-01   # Re[R_sta(2,1)]
   2  2    -1.62572483E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.96210381E-01   # Re[N(1,1)]
   1  2     2.93470886E-03   # Re[N(1,2)]
   1  3    -7.91147301E-02   # Re[N(1,3)]
   1  4    -3.60155975E-02   # Re[N(1,4)]
   2  1     1.15891801E-02   # Re[N(2,1)]
   2  2     9.31044162E-01   # Re[N(2,2)]
   2  3     2.84409461E-01   # Re[N(2,3)]
   2  4    -2.28328092E-01   # Re[N(2,4)]
   3  1     8.13908786E-02   # Re[N(3,1)]
   3  2    -4.20519983E-02   # Re[N(3,2)]
   3  3     7.01633374E-01   # Re[N(3,3)]
   3  4     7.06624202E-01   # Re[N(3,4)]
   4  1     2.83917608E-02   # Re[N(4,1)]
   4  2    -3.62463495E-01   # Re[N(4,2)]
   4  3     6.48508078E-01   # Re[N(4,3)]
   4  4    -6.68768567E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.15107006E-01   # Re[U(1,1)]
   1  2    -4.03211071E-01   # Re[U(1,2)]
   2  1    -4.03211071E-01   # Re[U(2,1)]
   2  2     9.15107006E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.45782270E-01   # Re[V(1,1)]
   1  2     3.24801321E-01   # Re[V(1,2)]
   2  1     3.24801321E-01   # Re[V(2,1)]
   2  2    -9.45782270E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     2.60224355E-03   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99948047E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     3.98495164E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.97520870E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.16338312E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     2.34312463E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000013     2.58001407E-03   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.99948081E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     3.99058898E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.96069062E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.57255350E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.40868666E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.23256634E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.52640540E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     3.74726448E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
     4.14372569E-04    2     1000013        25   # BR(~mu^-_R -> ~mu^-_L h^0)
DECAY   1000015     5.94915165E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     7.35297749E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.97233970E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     6.40573738E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     4.76682553E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.94139243E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     9.38393004E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     2.60495151E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.66196331E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.42766657E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.86268979E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     7.80572807E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     4.42021430E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     7.80782761E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     7.13074935E-03    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     6.69907552E-03    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     2.59768550E-04   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
DECAY   1000014     2.54595233E-04   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
DECAY   1000016     2.61033012E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.37347501E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.26587031E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.72127121E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.29152729E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.95307341E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     1.45543836E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
     1.41904986E-02    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
DECAY   2000001     4.76725803E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.16839929E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.88227381E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.07521783E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.22239479E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.27607329E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.31716086E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     9.78091051E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.21799799E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     2.35449622E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.79659485E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.76769145E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.16832190E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.88138189E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.07547209E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.22252624E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.27610039E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.49182480E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     9.79542224E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.21795344E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     2.35493111E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.79627210E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.89883620E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.13970757E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     4.12100546E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.34677020E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.98285271E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     8.23135943E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     3.92116985E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.77967758E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.24311236E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.05251259E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     7.07704629E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     7.46383117E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.32008377E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.78921896E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.29967087E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     4.84910298E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     6.84962684E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.93891974E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.51256967E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.98894318E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.54532977E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.07503649E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.37442919E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.33192513E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     9.25000470E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.30101539E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.52780569E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.79621692E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.93899215E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.51250453E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.02535615E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.54519046E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.07529030E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.37433823E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.33169318E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     9.25227138E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.30101919E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.53071366E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.79589409E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.42600231E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.44988156E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.94967484E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.37486219E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.05446848E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     6.00114000E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.13059245E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.80155831E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.70459388E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.15951320E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.42622847E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.34320974E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.06994013E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.48920289E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     9.65573212E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.63557689E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.91646215E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     3.30958163E+00   # chi^+_1
#    BR                NDA      ID1      ID2
     2.32338452E-01    2    -1000011        12   # BR(chi^+_1 -> ~e^+_L nu_e)
     2.32441445E-01    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     2.65437050E-01    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     2.65562958E-01    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
     4.21923413E-03    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     3.82255063E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     8.08059047E-02    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     8.08227412E-02    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     5.41330646E-02    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     5.45182051E-02    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     1.06850336E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.52926163E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.64448717E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.04991973E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.95897579E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.32237214E+00   # chi^0_2
#    BR                NDA      ID1      ID2
     1.21226730E-01    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     1.21226730E-01    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     1.21298406E-01    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     1.21298406E-01    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     1.26265021E-01    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     1.26265021E-01    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     1.26302719E-01    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     1.26302719E-01    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     5.39501035E-03    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     4.41842332E-03    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.95472165E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.62929013E-04    2     1000013       -13   # BR(chi^0_3 -> ~mu^-_L mu^+)
     2.62929013E-04    2    -1000013        13   # BR(chi^0_3 -> ~mu^+_L mu^-)
     2.27653205E-03    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     2.27653205E-03    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     2.27688275E-03    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     2.27688275E-03    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     1.69195651E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     1.69195651E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.85395373E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.79824814E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.06978148E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.46478869E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     1.47881506E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     4.75833819E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.39982556E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     2.39982556E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     2.41530045E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     2.41530045E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     2.92966939E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     2.92966939E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     2.93008901E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     2.93008901E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     2.70219698E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.70219698E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     8.20822713E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.49571216E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     7.52756642E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     8.61907756E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000021     6.77623429E+01   # ~g
#    BR                NDA      ID1      ID2
     2.16185809E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.16185809E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.15867393E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.15867393E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     2.56878169E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.56878169E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.51600656E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.51600656E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
     1.02069138E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.40556675E-03   # Gamma(h0)
     2.48898254E-03   2        22        22   # BR(h0 -> photon photon)
     1.42186177E-03   2        22        23   # BR(h0 -> photon Z)
     2.44945563E-02   2        23        23   # BR(h0 -> Z Z)
     2.10543682E-01   2       -24        24   # BR(h0 -> W W)
     7.89013348E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.25031361E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.33541702E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.72273367E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.47815678E-07   2        -2         2   # BR(h0 -> Up up)
     2.86858394E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.08197800E-07   2        -1         1   # BR(h0 -> Down down)
     2.19970932E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.85782132E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.77288550E+01   # Gamma(HH)
     7.74344628E-08   2        22        22   # BR(HH -> photon photon)
     2.41628336E-07   2        22        23   # BR(HH -> photon Z)
     1.50251578E-06   2        23        23   # BR(HH -> Z Z)
     8.39828808E-07   2       -24        24   # BR(HH -> W W)
     1.26602398E-05   2        21        21   # BR(HH -> gluon gluon)
     8.22540566E-09   2       -11        11   # BR(HH -> Electron electron)
     3.66185127E-04   2       -13        13   # BR(HH -> Muon muon)
     8.91663958E-02   2       -15        15   # BR(HH -> Tau tau)
     9.26165874E-14   2        -2         2   # BR(HH -> Up up)
     1.79646970E-08   2        -4         4   # BR(HH -> Charm charm)
     1.43437942E-03   2        -6         6   # BR(HH -> Top top)
     5.27383774E-07   2        -1         1   # BR(HH -> Down down)
     1.90765478E-04   2        -3         3   # BR(HH -> Strange strange)
     5.79145940E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.15088592E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     8.59178674E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     8.59178674E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     5.59885561E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     4.72594301E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     6.17080368E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.34400698E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.26968257E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.04519536E-02   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     5.40063129E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.92098528E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     3.98006357E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     9.64539839E-03   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     4.23706765E-03   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     6.02586875E-06   2        25        25   # BR(HH -> h0 h0)
     2.42921732E-07   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     3.07008778E-13   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     3.07008778E-13   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     2.40853917E-07   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     1.31252014E-08   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     1.31252014E-08   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
DECAY        36     4.64180084E+01   # Gamma(A0)
     2.02593455E-07   2        22        22   # BR(A0 -> photon photon)
     3.88132876E-07   2        22        23   # BR(A0 -> photon Z)
     2.64011115E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.11309229E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.61184633E-04   2       -13        13   # BR(A0 -> Muon muon)
     8.79503252E-02   2       -15        15   # BR(A0 -> Tau tau)
     8.80666803E-14   2        -2         2   # BR(A0 -> Up up)
     1.70803431E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.41367458E-03   2        -6         6   # BR(A0 -> Top top)
     5.20174328E-07   2        -1         1   # BR(A0 -> Down down)
     1.88157859E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.71241377E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.49184258E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     7.68336066E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     7.68336066E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.58119092E-02   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     5.27474053E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.28182161E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.68777715E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.07617312E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.70084166E-02   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     2.87107034E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     3.88985852E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     7.99688002E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     4.08162372E-03   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.14702816E-02   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     2.07161212E-06   2        23        25   # BR(A0 -> Z h0)
     2.98515116E-39   2        25        25   # BR(A0 -> h0 h0)
     3.10155404E-13   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     3.10155404E-13   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     1.32601390E-08   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     1.32601390E-08   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
DECAY        37     4.94149288E+01   # Gamma(Hp)
     7.31070084E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.12555293E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     8.84083225E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.08281221E-07   2        -1         2   # BR(Hp -> Down up)
     8.53542627E-06   2        -3         2   # BR(Hp -> Strange up)
     6.28887479E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.44651907E-08   2        -1         4   # BR(Hp -> Down charm)
     1.83207229E-04   2        -3         4   # BR(Hp -> Strange charm)
     8.80667054E-04   2        -5         4   # BR(Hp -> Bottom charm)
     9.23342208E-08   2        -1         6   # BR(Hp -> Down top)
     2.28045094E-06   2        -3         6   # BR(Hp -> Strange top)
     5.92615084E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.09331560E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.26322831E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.98954208E-06   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     9.87045130E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     8.34127579E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     9.78818229E-03   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     9.19131181E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.92615705E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.95325425E-06   2        24        25   # BR(Hp -> W h0)
     5.92692779E-13   2        24        35   # BR(Hp -> W HH)
     1.53049418E-13   2        24        36   # BR(Hp -> W A0)
     9.17402412E-07   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     6.02412938E-13   2  -2000011   1000012   # BR(Hp -> Selectron2 snu_e1)
     9.14605634E-07   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     2.57540347E-08   2  -2000013   1000014   # BR(Hp -> Smuon2 snu_mu1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.49074418E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.05989499E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.05984406E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00004805E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    8.95484961E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    9.43535031E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.49074418E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.05989499E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.05984406E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999372E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    6.27820048E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999372E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    6.27820048E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26908002E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    6.20957780E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.17960579E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    6.27820048E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999372E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.32551249E-04   # BR(b -> s gamma)
    2    1.58854911E-06   # BR(b -> s mu+ mu-)
    3    3.52463040E-05   # BR(b -> s nu nu)
    4    2.56184846E-15   # BR(Bd -> e+ e-)
    5    1.09439183E-10   # BR(Bd -> mu+ mu-)
    6    2.29036598E-08   # BR(Bd -> tau+ tau-)
    7    8.70296037E-14   # BR(Bs -> e+ e-)
    8    3.71789861E-09   # BR(Bs -> mu+ mu-)
    9    7.88337981E-07   # BR(Bs -> tau+ tau-)
   10    9.51097433E-05   # BR(B_u -> tau nu)
   11    9.82447236E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41942066E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93553314E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15721869E-03   # epsilon_K
   17    2.28165918E-15   # Delta(M_K)
   18    2.47945745E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28922812E-11   # BR(K^+ -> pi^+ nu nu)
   20   -7.46917082E-15   # Delta(g-2)_electron/2
   21   -3.19992944E-10   # Delta(g-2)_muon/2
   22    3.45934330E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    1.00461500E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.01073193E-01   # C7
     0305 4322   00   2    -2.60176615E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.11439658E-01   # C8
     0305 6321   00   2    -3.30570293E-04   # C8'
 03051111 4133   00   0     1.62706632E+00   # C9 e+e-
 03051111 4133   00   2     1.62725139E+00   # C9 e+e-
 03051111 4233   00   2     1.74480725E-04   # C9' e+e-
 03051111 4137   00   0    -4.44975842E+00   # C10 e+e-
 03051111 4137   00   2    -4.44746406E+00   # C10 e+e-
 03051111 4237   00   2    -1.29029408E-03   # C10' e+e-
 03051313 4133   00   0     1.62706632E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62724990E+00   # C9 mu+mu-
 03051313 4233   00   2     1.74479797E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44975842E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44746555E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.29029398E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50486543E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.78979391E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50486543E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.78979549E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50486485E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.79024519E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85830905E-07   # C7
     0305 4422   00   2     3.62512253E-06   # C7
     0305 4322   00   2     1.80213898E-07   # C7'
     0305 6421   00   0     3.30489686E-07   # C8
     0305 6421   00   2     4.28188435E-06   # C8
     0305 6321   00   2     1.27773066E-07   # C8'
 03051111 4133   00   2    -3.23529045E-07   # C9 e+e-
 03051111 4233   00   2     4.15701119E-06   # C9' e+e-
 03051111 4137   00   2     7.73205959E-07   # C10 e+e-
 03051111 4237   00   2    -3.07470968E-05   # C10' e+e-
 03051313 4133   00   2    -3.23533720E-07   # C9 mu+mu-
 03051313 4233   00   2     4.15700764E-06   # C9' mu+mu-
 03051313 4137   00   2     7.73210231E-07   # C10 mu+mu-
 03051313 4237   00   2    -3.07471080E-05   # C10' mu+mu-
 03051212 4137   00   2     3.60312765E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     6.64798927E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     3.60316208E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     6.64798928E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     2.55801841E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     6.64800003E-06   # C11' nu_3 nu_3
