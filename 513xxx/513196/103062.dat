# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:34
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.04007946E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.09431382E+03  # scale for input parameters
    1   -1.81290143E+02  # M_1
    2   -1.78815117E+02  # M_2
    3    4.85737920E+03  # M_3
   11    2.56024866E+03  # A_t
   12    3.64105532E+02  # A_b
   13    1.58571872E+02  # A_tau
   23   -1.47483062E+03  # mu
   25    3.97083212E+01  # tan(beta)
   26    4.03808861E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.14612563E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.83308066E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.88608471E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.09431382E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.09431382E+03  # (SUSY scale)
  1  1     8.38703046E-06   # Y_u(Q)^DRbar
  2  2     4.26061147E-03   # Y_c(Q)^DRbar
  3  3     1.01322065E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.09431382E+03  # (SUSY scale)
  1  1     6.69221668E-04   # Y_d(Q)^DRbar
  2  2     1.27152117E-02   # Y_s(Q)^DRbar
  3  3     6.63658191E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.09431382E+03  # (SUSY scale)
  1  1     1.16784075E-04   # Y_e(Q)^DRbar
  2  2     2.41472428E-02   # Y_mu(Q)^DRbar
  3  3     4.06116899E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.09431382E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.56024856E+03   # A_t(Q)^DRbar
Block Ad Q=  4.09431382E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     3.64105405E+02   # A_b(Q)^DRbar
Block Ae Q=  4.09431382E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.58571870E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.09431382E+03  # soft SUSY breaking masses at Q
   1   -1.81290143E+02  # M_1
   2   -1.78815117E+02  # M_2
   3    4.85737920E+03  # M_3
  21    1.43708624E+07  # M^2_(H,d)
  22   -1.82031430E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.14612563E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.83308066E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.88608471E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23325001E+02  # h0
        35     4.03557762E+03  # H0
        36     4.03808861E+03  # A0
        37     4.03746255E+03  # H+
   1000001     1.00987109E+04  # ~d_L
   2000001     1.00740350E+04  # ~d_R
   1000002     1.00983420E+04  # ~u_L
   2000002     1.00766482E+04  # ~u_R
   1000003     1.00987138E+04  # ~s_L
   2000003     1.00740380E+04  # ~s_R
   1000004     1.00983442E+04  # ~c_L
   2000004     1.00766488E+04  # ~c_R
   1000005     3.01823203E+03  # ~b_1
   2000005     4.26093198E+03  # ~b_2
   1000006     3.92861869E+03  # ~t_1
   2000006     4.26699739E+03  # ~t_2
   1000011     1.00212731E+04  # ~e_L-
   2000011     1.00093429E+04  # ~e_R-
   1000012     1.00205013E+04  # ~nu_eL
   1000013     1.00212905E+04  # ~mu_L-
   2000013     1.00093532E+04  # ~mu_R-
   1000014     1.00205107E+04  # ~nu_muL
   1000015     1.00121894E+04  # ~tau_1-
   2000015     1.00262635E+04  # ~tau_2-
   1000016     1.00231467E+04  # ~nu_tauL
   1000021     5.31817307E+03  # ~g
   1000022     1.82082403E+02  # ~chi_10
   1000023     1.99141551E+02  # ~chi_20
   1000025     1.49585959E+03  # ~chi_30
   1000035     1.49680700E+03  # ~chi_40
   1000024     1.99321385E+02  # ~chi_1+
   1000037     1.49771374E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.47858936E-02   # alpha
Block Hmix Q=  4.09431382E+03  # Higgs mixing parameters
   1   -1.47483062E+03  # mu
   2    3.97083212E+01  # tan[beta](Q)
   3    2.42932962E+02  # v(Q)
   4    1.63061596E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -1.24119059E-01   # Re[R_st(1,1)]
   1  2     9.92267332E-01   # Re[R_st(1,2)]
   2  1    -9.92267332E-01   # Re[R_st(2,1)]
   2  2    -1.24119059E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.77772847E-02   # Re[R_sb(1,1)]
   1  2     9.99841972E-01   # Re[R_sb(1,2)]
   2  1    -9.99841972E-01   # Re[R_sb(2,1)]
   2  2    -1.77772847E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -4.10851951E-01   # Re[R_sta(1,1)]
   1  2     9.11702076E-01   # Re[R_sta(1,2)]
   2  1    -9.11702076E-01   # Re[R_sta(2,1)]
   2  2    -4.10851951E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99176689E-01   # Re[N(1,1)]
   1  2    -2.46286735E-02   # Re[N(1,2)]
   1  3    -3.18876786E-02   # Re[N(1,3)]
   1  4     4.74846107E-03   # Re[N(1,4)]
   2  1    -2.63532018E-02   # Re[N(2,1)]
   2  2    -9.98181812E-01   # Re[N(2,2)]
   2  3    -5.35637594E-02   # Re[N(2,3)]
   2  4     8.33680075E-03   # Re[N(2,4)]
   3  1     1.83709253E-02   # Re[N(3,1)]
   3  2    -3.24646853E-02   # Re[N(3,2)]
   3  3     7.06024836E-01   # Re[N(3,3)]
   3  4     7.07203991E-01   # Re[N(3,4)]
   4  1    -2.47782435E-02   # Re[N(4,1)]
   4  2     4.44133140E-02   # Re[N(4,2)]
   4  3    -7.05438184E-01   # Re[N(4,3)]
   4  4     7.06944457E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.97045792E-01   # Re[U(1,1)]
   1  2    -7.68094253E-02   # Re[U(1,2)]
   2  1    -7.68094253E-02   # Re[U(2,1)]
   2  2     9.97045792E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.99928374E-01   # Re[V(1,1)]
   1  2     1.19685806E-02   # Re[V(1,2)]
   2  1     1.19685806E-02   # Re[V(2,1)]
   2  2    -9.99928374E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02579281E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.98395434E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     6.94429992E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     3.22836791E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     5.87247560E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44776470E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     7.89289701E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.11938776E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.49430737E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.83069393E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.05263652E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.43610206E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04799717E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.94007207E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.97969962E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.41729285E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.67865577E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     2.18531488E-03    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44887861E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     7.88692408E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.11701796E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.31623855E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     6.64502326E-04    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.04799370E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.43346685E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.23936301E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.56529154E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     7.45612187E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.19878259E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.17398566E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.45427911E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.86204891E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.65437839E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.05856927E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.18533310E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     9.98045101E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.01536475E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     4.22009883E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.22588937E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44780703E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.49293860E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.94795100E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.20538078E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     9.67800730E-04    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.08703736E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44891971E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.48565756E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.94568994E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.20138871E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     9.67058513E-04    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.08241591E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     8.45642418E-04    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.76263073E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.79946487E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.42205737E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     4.27687872E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     7.95170739E-04    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.01214845E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     1.77361911E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.19344231E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.33812154E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.86596864E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.50927567E+02   # ~d_L
#    BR                NDA      ID1      ID2
     3.30811631E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     7.95724818E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.84060256E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.60286286E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     9.10574964E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.55639839E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.19406362E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.33793154E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.86451455E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.50962386E+02   # ~s_L
#    BR                NDA      ID1      ID2
     3.30796795E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     7.95676446E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.26669075E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     2.12033673E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.60276197E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     9.16854569E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.55592633E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.16855787E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.39125765E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.63889813E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.36428844E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.35717837E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     7.34929129E-03    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     4.62952552E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.56539489E+02   # ~b_2
#    BR                NDA      ID1      ID2
     5.03601952E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.18066550E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     9.16365505E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     9.16020333E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.35698076E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.28369186E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.59296232E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     6.51503806E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     7.14692332E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.36484139E+02   # ~u_R
#    BR                NDA      ID1      ID2
     5.14363969E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.48480914E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.50908147E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.89481185E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     8.10829371E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.23486446E-04    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.61214226E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     7.55596498E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.36491132E+02   # ~c_R
#    BR                NDA      ID1      ID2
     5.14355742E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.48465793E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.50942852E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.89469298E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     8.10778447E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.26641415E-04    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.61204452E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     7.55549263E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.26538810E+02   # ~t_1
#    BR                NDA      ID1      ID2
     6.78415974E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     8.10165329E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.77555301E-03    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.34602942E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.30250625E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     5.52253166E-03    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.58045321E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.07914834E-04    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.56875068E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.53377215E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.17188667E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.10009488E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.12659107E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.34997899E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.86828956E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.29962802E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     8.06323060E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.37226010E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.51299561E-08   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.38136947E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.29346521E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.12710640E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.12696474E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.07109390E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.10781699E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     8.55558226E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.03439902E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.93465070E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.95207372E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.11104503E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.30153871E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     8.89778347E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.67493596E-11   # chi^0_2
#    BR                NDA      ID1      ID2
     3.07840472E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     9.10191336E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     8.46935917E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.17017631E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.16977164E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     6.39646085E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.48136677E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.48007276E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.13811743E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.47491830E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.20845963E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.70176186E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.70176186E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     6.04734592E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.71164276E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     4.94947315E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.56405607E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.44614438E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     4.47860130E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     8.08282348E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     8.08282348E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.10848750E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.95225418E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.95225418E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.61455003E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     9.95205842E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.93952237E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.87695397E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.69562286E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     5.23282169E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     9.91888375E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     9.91888375E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.45074464E+02   # ~g
#    BR                NDA      ID1      ID2
     6.63456323E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     6.63456323E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.15263400E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.15263400E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     6.50466260E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     6.50466260E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     2.49350240E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.49350240E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     6.95521904E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     6.95521904E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.35908907E-03   # Gamma(h0)
     2.56170357E-03   2        22        22   # BR(h0 -> photon photon)
     1.48617179E-03   2        22        23   # BR(h0 -> photon Z)
     2.58220745E-02   2        23        23   # BR(h0 -> Z Z)
     2.20601415E-01   2       -24        24   # BR(h0 -> W W)
     8.05805014E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.14152251E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.28703190E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.59411715E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.46557067E-07   2        -2         2   # BR(h0 -> Up up)
     2.84423493E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.96634738E-07   2        -1         1   # BR(h0 -> Down down)
     2.15788602E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.74119373E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.54158616E+02   # Gamma(HH)
     1.21001160E-09   2        22        22   # BR(HH -> photon photon)
     6.49562913E-09   2        22        23   # BR(HH -> photon Z)
     9.93086490E-08   2        23        23   # BR(HH -> Z Z)
     2.07613785E-08   2       -24        24   # BR(HH -> W W)
     5.31031393E-06   2        21        21   # BR(HH -> gluon gluon)
     5.82344771E-09   2       -11        11   # BR(HH -> Electron electron)
     2.59311519E-04   2       -13        13   # BR(HH -> Muon muon)
     7.48957895E-02   2       -15        15   # BR(HH -> Tau tau)
     4.35748698E-14   2        -2         2   # BR(HH -> Up up)
     8.45377434E-09   2        -4         4   # BR(HH -> Charm charm)
     7.83185719E-04   2        -6         6   # BR(HH -> Top top)
     4.48037147E-07   2        -1         1   # BR(HH -> Down down)
     1.62071952E-04   2        -3         3   # BR(HH -> Strange strange)
     6.60549741E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.28446348E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     7.95183115E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     7.95183115E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     5.91911010E-06   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     8.14753035E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.30545720E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.83873216E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     4.04602901E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.63090082E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     6.84683138E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     5.71759913E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     6.56834135E-05   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.71124889E-04   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.06305388E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     2.30952672E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.44529563E+02   # Gamma(A0)
     2.73707277E-08   2        22        22   # BR(A0 -> photon photon)
     4.69553661E-08   2        22        23   # BR(A0 -> photon Z)
     9.60586938E-06   2        21        21   # BR(A0 -> gluon gluon)
     5.68212114E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.53019683E-04   2       -13        13   # BR(A0 -> Muon muon)
     7.30786169E-02   2       -15        15   # BR(A0 -> Tau tau)
     4.06111483E-14   2        -2         2   # BR(A0 -> Up up)
     7.87871558E-09   2        -4         4   # BR(A0 -> Charm charm)
     7.38861598E-04   2        -6         6   # BR(A0 -> Top top)
     4.37143634E-07   2        -1         1   # BR(A0 -> Down down)
     1.58131603E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.44590410E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.40161087E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     8.48756493E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     8.48756493E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.72980752E-05   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     8.89085689E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.51640002E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.33961411E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     5.14360583E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.78055820E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     6.13249088E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     7.27821268E-03   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.36894734E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.81422242E-04   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     2.70644634E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     2.25927972E-07   2        23        25   # BR(A0 -> Z h0)
     7.58768465E-13   2        23        35   # BR(A0 -> Z HH)
     8.58269895E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.65917566E+02   # Gamma(Hp)
     6.56835501E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     2.80817692E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     7.94312365E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.36270329E-07   2        -1         2   # BR(Hp -> Down up)
     7.38935106E-06   2        -3         2   # BR(Hp -> Strange up)
     7.14000774E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.07812397E-08   2        -1         4   # BR(Hp -> Down charm)
     1.57251834E-04   2        -3         4   # BR(Hp -> Strange charm)
     9.99856712E-04   2        -5         4   # BR(Hp -> Bottom charm)
     3.72903011E-08   2        -1         6   # BR(Hp -> Down top)
     1.04734473E-06   2        -3         6   # BR(Hp -> Strange top)
     6.74461004E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.18302467E-05   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.34204306E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     6.80453730E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     3.22509570E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     7.40466700E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.47791443E-04   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     7.39905623E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.04863597E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.96779724E-07   2        24        25   # BR(Hp -> W h0)
     1.95254257E-13   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.69077760E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.57678169E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.57675077E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00001961E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.14604272E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.34215640E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.69077760E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.57678169E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.57675077E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999846E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.53995652E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999846E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.53995652E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26783768E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    5.93238241E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.15722094E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.53995652E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999846E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.23388074E-04   # BR(b -> s gamma)
    2    1.58975274E-06   # BR(b -> s mu+ mu-)
    3    3.52500231E-05   # BR(b -> s nu nu)
    4    2.42301239E-15   # BR(Bd -> e+ e-)
    5    1.03508165E-10   # BR(Bd -> mu+ mu-)
    6    2.16583776E-08   # BR(Bd -> tau+ tau-)
    7    8.25177985E-14   # BR(Bs -> e+ e-)
    8    3.52515049E-09   # BR(Bs -> mu+ mu-)
    9    7.47289707E-07   # BR(Bs -> tau+ tau-)
   10    9.62840915E-05   # BR(B_u -> tau nu)
   11    9.94577803E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41813241E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93568664E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15672033E-03   # epsilon_K
   17    2.28165266E-15   # Delta(M_K)
   18    2.47975956E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28994144E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.72837149E-16   # Delta(g-2)_electron/2
   21    7.38941349E-12   # Delta(g-2)_muon/2
   22    2.09657897E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.81162952E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.92046872E-01   # C7
     0305 4322   00   2    -7.59899585E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.99856209E-02   # C8
     0305 6321   00   2    -1.00849741E-04   # C8'
 03051111 4133   00   0     1.62788106E+00   # C9 e+e-
 03051111 4133   00   2     1.62805483E+00   # C9 e+e-
 03051111 4233   00   2     3.10078351E-04   # C9' e+e-
 03051111 4137   00   0    -4.45057316E+00   # C10 e+e-
 03051111 4137   00   2    -4.44873105E+00   # C10 e+e-
 03051111 4237   00   2    -2.29075101E-03   # C10' e+e-
 03051313 4133   00   0     1.62788106E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62805470E+00   # C9 mu+mu-
 03051313 4233   00   2     3.10078160E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45057316E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44873118E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.29075115E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50495322E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     4.95213124E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50495322E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     4.95213148E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50495323E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     4.95219799E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85842024E-07   # C7
     0305 4422   00   2     1.94817132E-06   # C7
     0305 4322   00   2     1.11703302E-07   # C7'
     0305 6421   00   0     3.30499210E-07   # C8
     0305 6421   00   2     4.71529147E-06   # C8
     0305 6321   00   2     1.27450720E-07   # C8'
 03051111 4133   00   2     6.68195787E-07   # C9 e+e-
 03051111 4233   00   2     6.00626631E-06   # C9' e+e-
 03051111 4137   00   2     9.95840648E-08   # C10 e+e-
 03051111 4237   00   2    -4.43741680E-05   # C10' e+e-
 03051313 4133   00   2     6.68192570E-07   # C9 mu+mu-
 03051313 4233   00   2     6.00626486E-06   # C9' mu+mu-
 03051313 4137   00   2     9.95874682E-08   # C10 mu+mu-
 03051313 4237   00   2    -4.43741724E-05   # C10' mu+mu-
 03051212 4137   00   2     2.28069859E-09   # C11 nu_1 nu_1
 03051212 4237   00   2     9.59278116E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     2.28087097E-09   # C11 nu_2 nu_2
 03051414 4237   00   2     9.59278116E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     2.32883827E-09   # C11 nu_3 nu_3
 03051616 4237   00   2     9.59278114E-06   # C11' nu_3 nu_3
