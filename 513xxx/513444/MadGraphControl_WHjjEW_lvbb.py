import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()

# General settings
evgenConfig.nEventsPerJob = 10000
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
isPos = phys_short.find("Pos") != -1

mass_par={
    '6' : '1.725000e+02',
    '15' : '1.776820e+00',
    '24' : '8.039900e+01'
}
sminputs_par={
    '1' : '1.323489e+02'
}
yukawa_par={
    '6' : '1.725000e+02'
}
decay_par={
    '6' : '1.320000e+00',
    '25' : '6.382339e-03'
}
wolfenstein_par={
    '1' : '2.25000e-01',
    '2' : '8.260000e-01',
    '3' : '1.590000e-01',
    '4' : '3.480000e-01'
}

if isPos:
    kappa_par={
        '1' : '1.0',
        '2' : '1.0'
    }
else:
    kappa_par={
        '1' : '-1.0',
        '2' : '1.0'
    }

gridpack_mode=True

if not is_gen_from_gridpack():
    process = """
    import model SM_kWkZ_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate p p > w+ h j j QCD=0, h > b b~, w+ > l+ vl
    add process p p > w+ h j j QCD=0, h > b b~, w+ > ta+ vt
    add process p p > w- h j j QCD=0, h > b b~, w- > l- vl~
    add process p p > w- h j j QCD=0, h > b b~, w- > ta- vt~    
    output -f
    """
    
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'3.0',
             'cut_decays':'F',
             'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

params = {}

params['kappablock']=kappa_par
params['mass']=mass_par
params['sminputs']=sminputs_par
params['yukawa']=yukawa_par
params['decay']=decay_par
modify_param_card(process_dir=process_dir, params=params)

# Reweighting setup
reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
if isPos:
    reweight_card_f.write("""launch --rwgt_info=kZ099kW105
    set kappablock 1 0.99
    set kappablock 2 1.05
    launch --rwgt_info=kZ105kW099
    set kappablock 1 1.05
    set kappablock 2 0.99
    launch --rwgt_info=kZ094kW111
    set kappablock 1 0.94
    set kappablock 2 1.11
    launch --rwgt_info=kZ105kW111
    set kappablock 1 1.05
    set kappablock 2 1.11
    launch --rwgt_info=kZ093kW099
    set kappablock 1 0.93
    set kappablock 2 0.99
    launch --rwgt_info=kZ108kW095
    set kappablock 1 1.08
    set kappablock 2 0.95
    launch --rwgt_info=kZ091kW113
    set kappablock 1 0.91
    set kappablock 2 1.13
    launch --rwgt_info=kZ093kW105
    set kappablock 1 0.93
    set kappablock 2 1.05
    launch --rwgt_info=kZ105kW105
    set kappablock 1 1.05
    set kappablock 2 1.05
    launch --rwgt_info=kZ099kW111
    set kappablock 1 0.99
    set kappablock 2 1.11
    launch --rwgt_info=kZ099kW093
    set kappablock 1 0.99
    set kappablock 2 0.93
    launch --rwgt_info=kZ087kW093
    set kappablock 1 0.87
    set kappablock 2 0.93
    launch --rwgt_info=kZ087kW105
    set kappablock 1 0.87
    set kappablock 2 1.05
    launch --rwgt_info=kZ111kW105
    set kappablock 1 1.11
    set kappablock 2 1.05
    launch --rwgt_info=kZ099kW117
    set kappablock 1 0.99
    set kappablock 2 1.17
    launch --rwgt_info=kZ111kW117
    set kappablock 1 1.11
    set kappablock 2 1.17""")
else:
    reweight_card_f.write("""launch --rwgt_info=kZ099kW105
    set kappablock 1 -0.99
    set kappablock 2 1.05
    launch --rwgt_info=kZ105kW099
    set kappablock 1 -1.05
    set kappablock 2 0.99
    launch --rwgt_info=kZ094kW111
    set kappablock 1 -0.94
    set kappablock 2 1.11
    launch --rwgt_info=kZ105kW111
    set kappablock 1 -1.05
    set kappablock 2 1.11
    launch --rwgt_info=kZ093kW099
    set kappablock 1 -0.93
    set kappablock 2 0.99
    launch --rwgt_info=kZ108kW095
    set kappablock 1 -1.08
    set kappablock 2 0.95
    launch --rwgt_info=kZ091kW113
    set kappablock 1 -0.91
    set kappablock 2 1.13
    launch --rwgt_info=kZ093kW105
    set kappablock 1 -0.93
    set kappablock 2 1.05
    launch --rwgt_info=kZ105kW105
    set kappablock 1 -1.05
    set kappablock 2 1.05
    launch --rwgt_info=kZ099kW111
    set kappablock 1 -0.99
    set kappablock 2 1.11
    launch --rwgt_info=kZ099kW093
    set kappablock 1 -0.99
    set kappablock 2 0.93
    launch --rwgt_info=kZ087kW093
    set kappablock 1 -0.87
    set kappablock 2 0.93
    launch --rwgt_info=kZ087kW105
    set kappablock 1 -0.87
    set kappablock 2 1.05
    launch --rwgt_info=kZ111kW105
    set kappablock 1 -1.11
    set kappablock 2 1.05
    launch --rwgt_info=kZ099kW117
    set kappablock 1 -0.99
    set kappablock 2 1.17
    launch --rwgt_info=kZ111kW117
    set kappablock 1 -1.11
    set kappablock 2 1.17""")
reweight_card_f.close()

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_VBFWH'
evgenConfig.keywords += ['higgs','jets']
evgenConfig.contact = ['john.stakely.keller@cern.ch']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
