# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  22:05
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.38762188E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.55549666E+03  # scale for input parameters
    1    5.91378696E+01  # M_1
    2    5.13326186E+02  # M_2
    3    3.76259217E+03  # M_3
   11    5.67181986E+03  # A_t
   12    1.37935896E+03  # A_b
   13    1.46560740E+03  # A_tau
   23   -1.22410792E+02  # mu
   25    4.29120352E+01  # tan(beta)
   26    2.89872288E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.43617120E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.61599999E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.82242202E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.55549666E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.55549666E+03  # (SUSY scale)
  1  1     8.38664840E-06   # Y_u(Q)^DRbar
  2  2     4.26041739E-03   # Y_c(Q)^DRbar
  3  3     1.01317449E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.55549666E+03  # (SUSY scale)
  1  1     7.23182313E-04   # Y_d(Q)^DRbar
  2  2     1.37404640E-02   # Y_s(Q)^DRbar
  3  3     7.17170242E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.55549666E+03  # (SUSY scale)
  1  1     1.26200602E-04   # Y_e(Q)^DRbar
  2  2     2.60942819E-02   # Y_mu(Q)^DRbar
  3  3     4.38862895E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.55549666E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     5.67181671E+03   # A_t(Q)^DRbar
Block Ad Q=  3.55549666E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.37935962E+03   # A_b(Q)^DRbar
Block Ae Q=  3.55549666E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.46560777E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.55549666E+03  # soft SUSY breaking masses at Q
   1    5.91378696E+01  # M_1
   2    5.13326186E+02  # M_2
   3    3.76259217E+03  # M_3
  21    8.38348517E+06  # M^2_(H,d)
  22    2.64536465E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.43617120E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.61599999E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.82242202E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28415248E+02  # h0
        35     2.89756608E+03  # H0
        36     2.89872288E+03  # A0
        37     2.89832652E+03  # H+
   1000001     1.00997662E+04  # ~d_L
   2000001     1.00749989E+04  # ~d_R
   1000002     1.00993998E+04  # ~u_L
   2000002     1.00777485E+04  # ~u_R
   1000003     1.00997711E+04  # ~s_L
   2000003     1.00750076E+04  # ~s_R
   1000004     1.00994048E+04  # ~c_L
   2000004     1.00777495E+04  # ~c_R
   1000005     3.50652131E+03  # ~b_1
   2000005     4.89024617E+03  # ~b_2
   1000006     3.44271516E+03  # ~t_1
   2000006     3.67197283E+03  # ~t_2
   1000011     1.00212478E+04  # ~e_L-
   2000011     1.00092499E+04  # ~e_R-
   1000012     1.00204788E+04  # ~nu_eL
   1000013     1.00212709E+04  # ~mu_L-
   2000013     1.00092946E+04  # ~mu_R-
   1000014     1.00205018E+04  # ~nu_muL
   1000015     1.00221085E+04  # ~tau_1-
   2000015     1.00279328E+04  # ~tau_2-
   1000016     1.00271088E+04  # ~nu_tauL
   1000021     4.23222341E+03  # ~g
   1000022     5.39846501E+01  # ~chi_10
   1000023     1.45164798E+02  # ~chi_20
   1000025     1.51168370E+02  # ~chi_30
   1000035     5.72658951E+02  # ~chi_40
   1000024     1.39173851E+02  # ~chi_1+
   1000037     5.72763920E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.28469776E-02   # alpha
Block Hmix Q=  3.55549666E+03  # Higgs mixing parameters
   1   -1.22410792E+02  # mu
   2    4.29120352E+01  # tan[beta](Q)
   3    2.43044396E+02  # v(Q)
   4    8.40259434E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -8.56899666E-01   # Re[R_st(1,1)]
   1  2     5.15483233E-01   # Re[R_st(1,2)]
   2  1    -5.15483233E-01   # Re[R_st(2,1)]
   2  2    -8.56899666E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99999596E-01   # Re[R_sb(1,1)]
   1  2     8.98772647E-04   # Re[R_sb(1,2)]
   2  1    -8.98772647E-04   # Re[R_sb(2,1)]
   2  2    -9.99999596E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.00201169E-01   # Re[R_sta(1,1)]
   1  2     9.94967198E-01   # Re[R_sta(1,2)]
   2  1    -9.94967198E-01   # Re[R_sta(2,1)]
   2  2    -1.00201169E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.27876041E-01   # Re[N(1,1)]
   1  2    -1.84186539E-02   # Re[N(1,2)]
   1  3    -3.50149120E-01   # Re[N(1,3)]
   1  4    -1.26895230E-01   # Re[N(1,4)]
   2  1     3.39194109E-01   # Re[N(2,1)]
   2  2     1.26275313E-01   # Re[N(2,2)]
   2  3     6.50065869E-01   # Re[N(2,3)]
   2  4     6.68143897E-01   # Re[N(2,4)]
   3  1     1.54352594E-01   # Re[N(3,1)]
   3  2    -8.36121071E-02   # Re[N(3,2)]
   3  3     6.73607777E-01   # Re[N(3,3)]
   3  4    -7.17939312E-01   # Re[N(3,4)]
   4  1     1.29879102E-02   # Re[N(4,1)]
   4  2    -9.88293638E-01   # Re[N(4,2)]
   4  3     3.25963659E-02   # Re[N(4,3)]
   4  4     1.48473826E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.63482256E-02   # Re[U(1,1)]
   1  2    -9.98925344E-01   # Re[U(1,2)]
   2  1     9.98925344E-01   # Re[U(2,1)]
   2  2    -4.63482256E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -2.10518743E-01   # Re[V(1,1)]
   1  2     9.77589821E-01   # Re[V(1,2)]
   2  1     9.77589821E-01   # Re[V(2,1)]
   2  2     2.10518743E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02872759E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     8.60998858E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.15016965E-01    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.38165121E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.67601850E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44061029E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     6.97404858E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.89354018E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.92947583E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.31494360E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.07061169E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.05585634E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     8.56711508E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.15533105E-01    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.49051050E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.69534037E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     2.67502418E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.44197059E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     6.97902691E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.93058319E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     4.27383293E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.92672903E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.31370616E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06489907E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.27685080E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.85352059E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.69973646E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.38513523E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.63022908E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.01198633E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.33190964E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.82005275E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.48368747E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.13794533E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.00859637E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.31378354E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.92412440E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.78938189E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44065834E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.09126027E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     9.23325081E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     8.43951837E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.01257519E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.71253251E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.81341710E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44201852E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.08364677E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     9.22456276E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     8.43157719E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.00974058E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.80387227E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.80796718E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.82567507E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.38911888E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     7.29087476E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     6.66411815E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.37884651E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.31332420E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.59498534E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.44979097E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.88552890E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.18706126E-03    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     2.45874539E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.89679613E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.75738819E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.21866065E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.85418251E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     8.11695151E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.41417744E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     2.82531525E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.30446883E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.01813037E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.45055412E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.89278078E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.21614190E-03    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.77241792E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.89542632E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.75780832E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.22538687E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.09047378E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     8.37034350E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.41378771E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     2.87669483E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.30439076E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.01763909E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.51154555E+02   # ~b_1
#    BR                NDA      ID1      ID2
     3.25822516E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.00399235E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.08528705E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     9.51508944E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.49464412E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.13874502E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.26856407E+02   # ~b_2
#    BR                NDA      ID1      ID2
     6.67918287E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.68881614E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.79201558E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     4.26018115E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.91949741E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.52042795E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.91715200E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     5.62130933E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.44669620E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.60430803E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.53411573E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.59968608E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.75722414E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.41783565E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.29156238E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     2.07139952E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     6.35433644E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.82892585E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.24932540E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.01778632E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.62138277E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.44666178E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.60713785E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.56733988E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.59956189E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.75764405E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.41783512E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     2.29382864E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     2.09907418E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     6.35395662E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.88440523E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.24924958E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.01729499E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.49075649E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.71013318E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     8.32686964E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     9.20395731E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.20583628E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.19473723E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.05818363E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.42407175E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.93690213E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     8.20861853E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.68404918E+02   # ~t_2
#    BR                NDA      ID1      ID2
     5.01409133E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.95084154E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.49351793E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.32860366E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.25847136E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     9.71668002E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.42405849E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.66755661E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.16145685E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.23475572E-02   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99998282E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     4.64207507E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.30828762E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.56820902E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.65049201E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.50208026E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.23849902E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.08716020E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.21583623E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.32927689E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.04269907E-02   # chi^0_2
#    BR                NDA      ID1      ID2
#    BR                NDA      ID1      ID2       ID3
     1.18811233E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.18690191E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.52339611E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.52338934E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.51033489E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.43785138E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.43782350E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.42995826E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.03721842E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     5.72690520E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     9.99893992E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     5.25068647E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.32387753E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.32387753E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.61452989E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.31124531E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.68769194E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.64516925E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.38846365E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.61026554E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.12546027E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.12546027E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.84138427E+01   # ~g
#    BR                NDA      ID1      ID2
     4.86601056E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     4.86601056E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.21821959E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.21821959E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     6.76884058E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     6.76884058E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.60296858E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.60296858E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     2.15961238E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.99921839E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     2.04617956E-04    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     5.12280903E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     5.43178609E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     5.90341197E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     5.90341197E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     5.34429511E-03   # Gamma(h0)
     1.90400226E-03   2        22        22   # BR(h0 -> photon photon)
     1.45422670E-03   2        22        23   # BR(h0 -> photon Z)
     3.03773987E-02   2        23        23   # BR(h0 -> Z Z)
     2.35336442E-01   2       -24        24   # BR(h0 -> W W)
     5.69987437E-02   2        21        21   # BR(h0 -> gluon gluon)
     3.29536529E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.46584940E-04   2       -13        13   # BR(h0 -> Muon muon)
     4.22684907E-02   2       -15        15   # BR(h0 -> Tau tau)
     9.30681013E-08   2        -2         2   # BR(h0 -> Up up)
     1.80641354E-02   2        -4         4   # BR(h0 -> Charm charm)
     3.79603055E-07   2        -1         1   # BR(h0 -> Down down)
     1.37293653E-04   2        -3         3   # BR(h0 -> Strange strange)
     3.65033774E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     2.48278432E-01   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.01062148E+02   # Gamma(HH)
     4.05536664E-08   2        22        22   # BR(HH -> photon photon)
     8.08833568E-08   2        22        23   # BR(HH -> photon Z)
     1.74933455E-07   2        23        23   # BR(HH -> Z Z)
     5.53056383E-08   2       -24        24   # BR(HH -> W W)
     7.55835702E-06   2        21        21   # BR(HH -> gluon gluon)
     7.46759687E-09   2       -11        11   # BR(HH -> Electron electron)
     3.32490434E-04   2       -13        13   # BR(HH -> Muon muon)
     9.60267252E-02   2       -15        15   # BR(HH -> Tau tau)
     3.52337143E-14   2        -2         2   # BR(HH -> Up up)
     6.83488974E-09   2        -4         4   # BR(HH -> Charm charm)
     5.17829089E-04   2        -6         6   # BR(HH -> Top top)
     5.54793047E-07   2        -1         1   # BR(HH -> Down down)
     2.00674020E-04   2        -3         3   # BR(HH -> Strange strange)
     5.40927843E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.20297349E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.03321148E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.03321148E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     5.10733771E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.00275225E-02   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     9.99992198E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     7.74697411E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.61691214E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     6.49782494E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.33803199E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     4.41270985E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     3.16518716E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     4.73172537E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.61312946E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     9.87901568E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     9.66234275E+01   # Gamma(A0)
     4.40620960E-10   2        22        22   # BR(A0 -> photon photon)
     2.09185066E-08   2        22        23   # BR(A0 -> photon Z)
     1.33844820E-05   2        21        21   # BR(A0 -> gluon gluon)
     7.27122822E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.23746036E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.35014127E-02   2       -15        15   # BR(A0 -> Tau tau)
     3.28018377E-14   2        -2         2   # BR(A0 -> Up up)
     6.36290553E-09   2        -4         4   # BR(A0 -> Charm charm)
     4.89854026E-04   2        -6         6   # BR(A0 -> Top top)
     5.40177082E-07   2        -1         1   # BR(A0 -> Down down)
     1.95386717E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.26688130E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.24380249E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.08262692E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.08262692E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     3.94746568E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.01252541E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     9.47822293E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     9.25529195E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.68221197E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     6.22955147E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.35675553E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     4.33281015E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     3.68728702E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     5.25490636E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     2.04013308E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     2.90590042E-07   2        23        25   # BR(A0 -> Z h0)
     2.35534389E-14   2        23        35   # BR(A0 -> Z HH)
     1.47334966E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.10779318E+02   # Gamma(Hp)
     8.32928411E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.56102910E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.00726147E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.36375040E-07   2        -1         2   # BR(Hp -> Down up)
     9.05058061E-06   2        -3         2   # BR(Hp -> Strange up)
     6.01099974E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.53601917E-08   2        -1         4   # BR(Hp -> Down charm)
     1.93315231E-04   2        -3         4   # BR(Hp -> Strange charm)
     8.41755015E-04   2        -5         4   # BR(Hp -> Bottom charm)
     3.51116335E-08   2        -1         6   # BR(Hp -> Down top)
     1.05151886E-06   2        -3         6   # BR(Hp -> Strange top)
     5.67995285E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.47890926E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.99668128E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     9.94692482E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     8.10422279E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.51704308E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     8.81718061E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     9.57018160E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     3.81958130E-08   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     2.53521434E-07   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.61559031E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.84148121E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.84144277E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00002088E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    5.22176985E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    5.43052447E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.61559031E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.84148121E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.84144277E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999795E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.04567286E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999795E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.04567286E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26425531E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    9.24238664E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.29219909E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.04567286E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999795E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.17892585E-04   # BR(b -> s gamma)
    2    1.59021203E-06   # BR(b -> s mu+ mu-)
    3    3.52439740E-05   # BR(b -> s nu nu)
    4    3.78872273E-15   # BR(Bd -> e+ e-)
    5    1.61845379E-10   # BR(Bd -> mu+ mu-)
    6    3.36174605E-08   # BR(Bd -> tau+ tau-)
    7    1.29484076E-13   # BR(Bs -> e+ e-)
    8    5.53139636E-09   # BR(Bs -> mu+ mu-)
    9    1.16393089E-06   # BR(Bs -> tau+ tau-)
   10    9.55772941E-05   # BR(B_u -> tau nu)
   11    9.87276857E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41933460E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93606623E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15727590E-03   # epsilon_K
   17    2.28165918E-15   # Delta(M_K)
   18    2.47952155E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28931028E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.60805681E-16   # Delta(g-2)_electron/2
   21   -1.54257430E-11   # Delta(g-2)_muon/2
   22   -4.37715468E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.14176715E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.84874023E-01   # C7
     0305 4322   00   2    -8.85599644E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.84666861E-02   # C8
     0305 6321   00   2    -1.49953795E-04   # C8'
 03051111 4133   00   0     1.62305888E+00   # C9 e+e-
 03051111 4133   00   2     1.62330189E+00   # C9 e+e-
 03051111 4233   00   2     4.42254459E-04   # C9' e+e-
 03051111 4137   00   0    -4.44575098E+00   # C10 e+e-
 03051111 4137   00   2    -4.44335199E+00   # C10 e+e-
 03051111 4237   00   2    -3.28995362E-03   # C10' e+e-
 03051313 4133   00   0     1.62305888E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62330172E+00   # C9 mu+mu-
 03051313 4233   00   2     4.42253827E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44575098E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44335215E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.28995359E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50483268E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     7.11992192E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50483268E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     7.11992296E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50483268E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     7.12021529E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85826573E-07   # C7
     0305 4422   00   2    -1.17601880E-05   # C7
     0305 4322   00   2     9.17251790E-07   # C7'
     0305 6421   00   0     3.30485976E-07   # C8
     0305 6421   00   2     5.20485496E-06   # C8
     0305 6321   00   2     5.39834323E-07   # C8'
 03051111 4133   00   2     5.08439028E-07   # C9 e+e-
 03051111 4233   00   2     8.81265487E-06   # C9' e+e-
 03051111 4137   00   2     1.76885068E-06   # C10 e+e-
 03051111 4237   00   2    -6.55618202E-05   # C10' e+e-
 03051313 4133   00   2     5.08434492E-07   # C9 mu+mu-
 03051313 4233   00   2     8.81265218E-06   # C9' mu+mu-
 03051313 4137   00   2     1.76885566E-06   # C10 mu+mu-
 03051313 4237   00   2    -6.55618283E-05   # C10' mu+mu-
 03051212 4137   00   2    -3.51789911E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.41885002E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -3.51789671E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.41885002E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -3.51722260E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.41885002E-05   # C11' nu_3 nu_3
