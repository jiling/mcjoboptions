from MadGraphControl.MadGraphUtils import *
    
#Need extra events to avoid Pythia8 problems.
safefactor=6.
nevents = runArgs.maxEvents*safefactor if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*safefactor

######################################################################
# Configure event generation.
######################################################################

gridpack_mode=False

if not is_gen_from_gridpack():
    process = """
    import model HC_NLO_X0_UFO-heft
    define p = g d d~ u u~ s s~ c c~ b b~                                   
    define j = g d d~ u u~ s s~ c c~ b b~                                 
    generate    p p > x0     / t a [QCD] @0
    add process p p > x0 j   / t a [QCD] @1
    add process p p > x0 j j / t a [QCD] @2
    output -f
    """

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#Define process directory.

#Fetch default run_card.dat and set parameters
settings = {'lhe_version'   :'3.0',
          'parton_shower' :'PYTHIA8',
          'reweight_scale':'True',
          'ickkw'         :'3',
          'jetradius'     :'1.0',
          'ptj'           :'8',
          'maxjetflavor'  :'5',
          'pdlabel'       : "'lhapdf'",
          'lhaid'         : '90900',
          'reweight_PDF'  : 'True',
          'PDF_set_min'   : '90901',
          'PDF_set_max'   : '90930',
          'muR_ref_fixed' : '125.0',
          'muF1_ref_fixed': '125.0',
          'muF2_ref_fixed': '125.0',
          'QES_ref_fixed' : '125.0',
          'nevents'      :int(nevents)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

masses={'25': '1.250000e+02'}
#Set couplings here.
parameters={'frblock': {
        'kAza':   0.0, 
        'kAgg':   0.0, 
        'kAaa':   0.0, 
        'cosa':   1.0, 
        'kHdwR':  0.0, 
        'kHaa':   0.0, 
        'kAll':   0.0, 
        'kHll':   0.0, 
        'kAzz':   0.0, 
        'kSM':    1.0, 
        'kHdwI':  0.0, 
        'kHdz':   0.0, 
        'kAww':   0.0, 
        'kHgg':   1.0, 
        'kHda':   0.0, 
        'kHza':   0.0, 
        'kHww':   0.0, 
        'kHzz':   0.0, 
        'Lambda': 1000.0
        }}
parameters['MASS']=masses
modify_param_card(process_dir=process_dir,params=parameters)
#Create parame card

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

##arrange output
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

######################################################################
# End of event generation, start configuring parton shower here.
######################################################################


#### Shower 
check_reset_proc_number(opts)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

testSeq.TestHepMC.MaxVtxDisp = 100000000

PYTHIA8_nJetMax=3
PYTHIA8_qCut=20.

include("Pythia8_i/Pythia8_FxFx_A14mod.py")

genSeq.Pythia8.Commands += [
    "JetMatching:qCutME           = 8.0",                #same as Ptj
    "25:onMode                    = off",                # decay of Higgs
    "25:oneChannel                = 1 0.5 100 15 -13",
    "25:addChannel                = 1 0.5 100 13 -15" ]


# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lfvfilter = TauFilter("lfvfilter")
  filtSeq += lfvfilter

filtSeq.lfvfilter.UseNewOptions = True
filtSeq.lfvfilter.Ntaus = 1
filtSeq.lfvfilter.Nleptaus = 0
filtSeq.lfvfilter.Nhadtaus = 0
filtSeq.lfvfilter.EtaMaxlep = 2.6
filtSeq.lfvfilter.EtaMaxhad = 2.6
filtSeq.lfvfilter.Ptcutlep = 10000.0 #MeV
filtSeq.lfvfilter.Ptcutlep_lead = 10000.0 #MeV
filtSeq.lfvfilter.Ptcuthad = 20000.0 #MeV
filtSeq.lfvfilter.Ptcuthad_lead = 20000.0 #MeV

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "ggF+012jets (FxFx merging) 125 GeV Higgs production in the Higgs Characterization model decaying to tau mu."
evgenConfig.keywords = ['BSM','Higgs','mH125','BSMHiggs','dijet','resonance']
evgenConfig.contact = ['Kieran Amos <kieran.robert.amos@cern.ch>']
evgenConfig.nEventsPerJob = 10000
