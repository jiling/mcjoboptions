# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 08.09.2021,  06:50
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.38846215E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.26478856E+03  # scale for input parameters
    1    6.39477072E+01  # M_1
    2   -1.11439616E+03  # M_2
    3    3.58146364E+03  # M_3
   11   -5.23968504E+03  # A_t
   12   -8.43068971E+02  # A_b
   13   -1.43041715E+03  # A_tau
   23    2.72924383E+02  # mu
   25    5.35412290E+01  # tan(beta)
   26    2.04124357E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.36152069E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.34662312E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.67593158E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.26478856E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.26478856E+03  # (SUSY scale)
  1  1     8.38583440E-06   # Y_u(Q)^DRbar
  2  2     4.26000387E-03   # Y_c(Q)^DRbar
  3  3     1.01307615E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.26478856E+03  # (SUSY scale)
  1  1     9.02225017E-04   # Y_d(Q)^DRbar
  2  2     1.71422753E-02   # Y_s(Q)^DRbar
  3  3     8.94724501E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.26478856E+03  # (SUSY scale)
  1  1     1.57444863E-04   # Y_e(Q)^DRbar
  2  2     3.25546042E-02   # Y_mu(Q)^DRbar
  3  3     5.47514888E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.26478856E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -5.23968494E+03   # A_t(Q)^DRbar
Block Ad Q=  3.26478856E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -8.43068998E+02   # A_b(Q)^DRbar
Block Ae Q=  3.26478856E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.43041715E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.26478856E+03  # soft SUSY breaking masses at Q
   1    6.39477072E+01  # M_1
   2   -1.11439616E+03  # M_2
   3    3.58146364E+03  # M_3
  21    3.85842259E+06  # M^2_(H,d)
  22    4.53645154E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.36152069E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.34662312E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.67593158E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.26593031E+02  # h0
        35     2.04022215E+03  # H0
        36     2.04124357E+03  # A0
        37     2.04243416E+03  # H+
   1000001     1.00965676E+04  # ~d_L
   2000001     1.00725300E+04  # ~d_R
   1000002     1.00962032E+04  # ~u_L
   2000002     1.00756019E+04  # ~u_R
   1000003     1.00965779E+04  # ~s_L
   2000003     1.00725489E+04  # ~s_R
   1000004     1.00962135E+04  # ~c_L
   2000004     1.00756032E+04  # ~c_R
   1000005     2.77581158E+03  # ~b_1
   2000005     4.40065892E+03  # ~b_2
   1000006     2.41848903E+03  # ~t_1
   2000006     4.40723288E+03  # ~t_2
   1000011     1.00207212E+04  # ~e_L-
   2000011     1.00089273E+04  # ~e_R-
   1000012     1.00199590E+04  # ~nu_eL
   1000013     1.00207723E+04  # ~mu_L-
   2000013     1.00090256E+04  # ~mu_R-
   1000014     1.00200095E+04  # ~nu_muL
   1000015     1.00345392E+04  # ~tau_1-
   2000015     1.00380415E+04  # ~tau_2-
   1000016     1.00345123E+04  # ~nu_tauL
   1000021     4.03072257E+03  # ~g
   1000022     6.24230710E+01  # ~chi_10
   1000023     2.78976107E+02  # ~chi_20
   1000025     2.86330327E+02  # ~chi_30
   1000035     1.20429469E+03  # ~chi_40
   1000024     2.78406897E+02  # ~chi_1+
   1000037     1.20354106E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.86464003E-02   # alpha
Block Hmix Q=  3.26478856E+03  # Higgs mixing parameters
   1    2.72924383E+02  # mu
   2    5.35412290E+01  # tan[beta](Q)
   3    2.43151542E+02  # v(Q)
   4    4.16667531E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     5.42779647E-02   # Re[R_st(1,1)]
   1  2     9.98525865E-01   # Re[R_st(1,2)]
   2  1    -9.98525865E-01   # Re[R_st(2,1)]
   2  2     5.42779647E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     2.92539561E-03   # Re[R_sb(1,1)]
   1  2     9.99995721E-01   # Re[R_sb(1,2)]
   2  1    -9.99995721E-01   # Re[R_sb(2,1)]
   2  2     2.92539561E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     8.90733578E-01   # Re[R_sta(1,1)]
   1  2     4.54525788E-01   # Re[R_sta(1,2)]
   2  1    -4.54525788E-01   # Re[R_sta(2,1)]
   2  2     8.90733578E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.85070860E-01   # Re[N(1,1)]
   1  2    -2.85890636E-03   # Re[N(1,2)]
   1  3    -1.67318006E-01   # Re[N(1,3)]
   1  4     4.03969363E-02   # Re[N(1,4)]
   2  1     9.04895989E-02   # Re[N(2,1)]
   2  2     6.00203301E-02   # Re[N(2,2)]
   2  3    -7.03379058E-01   # Re[N(2,3)]
   2  4    -7.02472129E-01   # Re[N(2,4)]
   3  1     1.46425803E-01   # Re[N(3,1)]
   3  2    -3.89345561E-02   # Re[N(3,2)]
   3  3    -6.90681552E-01   # Re[N(3,3)]
   3  4     7.07108604E-01   # Re[N(3,4)]
   4  1     2.55296964E-03   # Re[N(4,1)]
   4  2    -9.97433450E-01   # Re[N(4,2)]
   4  3    -1.48855216E-02   # Re[N(4,3)]
   4  4    -6.99886892E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.10901487E-02   # Re[U(1,1)]
   1  2     9.99777578E-01   # Re[U(1,2)]
   2  1    -9.99777578E-01   # Re[U(2,1)]
   2  2    -2.10901487E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.90953861E-02   # Re[V(1,1)]
   1  2     9.95077939E-01   # Re[V(1,2)]
   2  1     9.95077939E-01   # Re[V(2,1)]
   2  2    -9.90953861E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02850040E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.70408742E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     8.17670601E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.14081226E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.41124575E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.74879110E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.66420726E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     4.81670367E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01191048E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.77599096E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06897564E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.07068935E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.62460734E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.01645241E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.32122688E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.15350054E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.41336279E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.73991565E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.39714588E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.19288282E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00741849E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.77184706E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.05991781E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.94697679E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.29621103E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.56132738E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.30536431E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.70444220E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     6.99002050E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.43365302E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     1.76361120E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     2.23478124E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.68204649E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.85679982E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     5.33679346E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.61798890E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.07470421E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.41128736E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.55981984E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     4.28242524E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02809593E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.12804812E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01139176E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.41340414E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.54704332E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     4.27603329E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02357704E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     7.61060862E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.00242726E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     2.01095433E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.01600643E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.00978461E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.12838457E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.01305898E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.22655886E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
DECAY   2000001     5.66434753E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.63280150E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.12649286E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.90072889E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.94142639E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.90615154E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.23348201E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.70245365E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.19506054E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.24375363E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.11317420E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.66553881E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.63369542E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.32493179E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.61969660E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     1.03730907E-04    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     9.89867981E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.94206201E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.90835247E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.65222341E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     3.10604758E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.19450186E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.24364160E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.11244589E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.78360820E+01   # ~b_1
#    BR                NDA      ID1      ID2
     3.13118327E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.44154318E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.35264060E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.89093454E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.17097334E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     2.75807990E+02   # ~b_2
#    BR                NDA      ID1      ID2
     9.13265978E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.24836619E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.20554493E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     5.99093566E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.19006131E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.23333249E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.26656516E-02    2     1000021         5   # BR(~b_2 -> ~g b)
     2.10038723E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.95412729E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.27704453E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.83610960E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.74085730E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.15210538E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     8.25283235E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.61450689E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.94126448E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.03453575E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.70525460E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.18368451E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.25568328E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.23206984E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.11284856E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.83618321E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.74081146E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.18277128E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     8.28385144E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.61438728E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.94189974E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.03436015E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.73079556E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.18312729E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.34019350E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.23195878E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.11212025E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.00380251E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.27315259E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.33319896E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.35851246E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.44884861E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.70607731E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     5.03087071E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.89932959E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.38639657E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.52127923E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.54791121E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     5.65410316E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.43714313E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.11370494E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.68612663E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     3.74987183E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.00369616E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.51462851E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.72045485E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99948766E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.03653876E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     3.25317459E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.49125499E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.43302421E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.45733593E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.41793693E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.52464479E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     5.75596737E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     5.33431674E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.22201704E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     6.68476393E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.31491421E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.39203835E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     4.10361527E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.89631647E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.16426562E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.23098137E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.23098137E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.43415477E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.53279151E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.54503619E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.57126101E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.51381226E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.62893148E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.24787461E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.30489685E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     5.98692912E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.98692912E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     8.16599046E+01   # ~g
#    BR                NDA      ID1      ID2
     7.23682684E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     7.23682684E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.93898648E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.93898648E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.01915844E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.01915844E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     7.73182332E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     6.18492582E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     7.79115446E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     5.97145956E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     2.52807785E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.80792534E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     1.41055247E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.41055247E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     3.20984467E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     3.20984467E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.84349819E-03   # Gamma(h0)
     2.49465197E-03   2        22        22   # BR(h0 -> photon photon)
     1.73735885E-03   2        22        23   # BR(h0 -> photon Z)
     3.39720931E-02   2        23        23   # BR(h0 -> Z Z)
     2.72085940E-01   2       -24        24   # BR(h0 -> W W)
     7.59785608E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.73078727E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.10434690E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.06776879E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.33276106E-07   2        -2         2   # BR(h0 -> Up up)
     2.58663465E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.46308543E-07   2        -1         1   # BR(h0 -> Down down)
     1.97587421E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.25731548E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.04710665E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     8.21565513E+01   # Gamma(HH)
     4.31977141E-08   2        22        22   # BR(HH -> photon photon)
     5.45248096E-08   2        22        23   # BR(HH -> photon Z)
     2.00901000E-07   2        23        23   # BR(HH -> Z Z)
     1.03509709E-07   2       -24        24   # BR(HH -> W W)
     1.80822627E-05   2        21        21   # BR(HH -> gluon gluon)
     1.00084017E-08   2       -11        11   # BR(HH -> Electron electron)
     4.45569263E-04   2       -13        13   # BR(HH -> Muon muon)
     1.28677888E-01   2       -15        15   # BR(HH -> Tau tau)
     2.37369388E-14   2        -2         2   # BR(HH -> Up up)
     4.60414282E-09   2        -4         4   # BR(HH -> Charm charm)
     3.06527511E-04   2        -6         6   # BR(HH -> Top top)
     7.59876229E-07   2        -1         1   # BR(HH -> Down down)
     2.74866003E-04   2        -3         3   # BR(HH -> Strange strange)
     7.04175040E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.11212067E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     4.51046558E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     4.51046558E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.47868164E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.42084681E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.09485496E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.21040919E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.17412831E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     5.13786590E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.84854773E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.16328839E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     2.57382108E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     8.04954109E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     7.78041009E+01   # Gamma(A0)
     8.81289558E-09   2        22        22   # BR(A0 -> photon photon)
     4.67743649E-08   2        22        23   # BR(A0 -> photon Z)
     2.68611018E-05   2        21        21   # BR(A0 -> gluon gluon)
     9.89507970E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.40522984E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.27220981E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.23052216E-14   2        -2         2   # BR(A0 -> Up up)
     4.32599124E-09   2        -4         4   # BR(A0 -> Charm charm)
     2.97258417E-04   2        -6         6   # BR(A0 -> Top top)
     7.51221709E-07   2        -1         1   # BR(A0 -> Down down)
     2.71735165E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.96170136E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.36194849E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     4.76128820E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     4.76128820E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.59683544E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.37208574E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.27397746E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.20853324E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.35204144E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     4.54746373E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.80954679E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.43436358E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.86995855E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     2.87781450E-07   2        23        25   # BR(A0 -> Z h0)
     1.56958170E-14   2        23        35   # BR(A0 -> Z HH)
     3.59989414E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     9.08407933E+01   # Gamma(Hp)
     1.07958660E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.61556989E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.30554457E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.08925136E-07   2        -1         2   # BR(Hp -> Down up)
     1.19134097E-05   2        -3         2   # BR(Hp -> Strange up)
     7.62306839E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.31478595E-08   2        -1         4   # BR(Hp -> Down charm)
     2.55513768E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.06750188E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.06741629E-08   2        -1         6   # BR(Hp -> Down top)
     8.24458403E-07   2        -3         6   # BR(Hp -> Strange top)
     7.17698651E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.54951450E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.13720564E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     6.73036674E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     4.12475148E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.24262804E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     3.93327192E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     4.10428900E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.47251936E-07   2        24        25   # BR(Hp -> W h0)
     7.93083910E-13   2        24        35   # BR(Hp -> W HH)
     3.58413490E-14   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.96937235E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.86666627E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.86666320E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000107E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.47769223E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.48837631E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.96937235E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.86666627E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.86666320E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999999E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.19325351E-10        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999999E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.19325351E-10        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25984998E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.08532377E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.64516577E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.19325351E-10        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999999E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.10975450E-04   # BR(b -> s gamma)
    2    1.59124135E-06   # BR(b -> s mu+ mu-)
    3    3.52440697E-05   # BR(b -> s nu nu)
    4    4.46663369E-15   # BR(Bd -> e+ e-)
    5    1.90792593E-10   # BR(Bd -> mu+ mu-)
    6    3.89726600E-08   # BR(Bd -> tau+ tau-)
    7    1.48419798E-13   # BR(Bs -> e+ e-)
    8    6.33995458E-09   # BR(Bs -> mu+ mu-)
    9    1.31379541E-06   # BR(Bs -> tau+ tau-)
   10    9.34645436E-05   # BR(B_u -> tau nu)
   11    9.65452953E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42018572E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93388844E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15776330E-03   # epsilon_K
   17    2.28166725E-15   # Delta(M_K)
   18    2.47983743E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28989184E-11   # BR(K^+ -> pi^+ nu nu)
   20   -5.16204344E-16   # Delta(g-2)_electron/2
   21   -2.20695583E-11   # Delta(g-2)_muon/2
   22   -6.25857761E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.99569502E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.75498591E-01   # C7
     0305 4322   00   2    -7.40901386E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.73298866E-02   # C8
     0305 6321   00   2    -2.37293976E-04   # C8'
 03051111 4133   00   0     1.62054503E+00   # C9 e+e-
 03051111 4133   00   2     1.62077393E+00   # C9 e+e-
 03051111 4233   00   2     4.64831842E-04   # C9' e+e-
 03051111 4137   00   0    -4.44323713E+00   # C10 e+e-
 03051111 4137   00   2    -4.44085881E+00   # C10 e+e-
 03051111 4237   00   2    -3.47013414E-03   # C10' e+e-
 03051313 4133   00   0     1.62054503E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62077355E+00   # C9 mu+mu-
 03051313 4233   00   2     4.64827860E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44323713E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44085919E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.47013120E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50483628E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     7.51410257E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50483628E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     7.51411063E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50483628E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     7.51638185E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85838115E-07   # C7
     0305 4422   00   2    -4.52841123E-06   # C7
     0305 4322   00   2     2.98176483E-06   # C7'
     0305 6421   00   0     3.30495863E-07   # C8
     0305 6421   00   2    -1.31731252E-05   # C8
     0305 6321   00   2     1.17331761E-06   # C8'
 03051111 4133   00   2    -4.77032799E-07   # C9 e+e-
 03051111 4233   00   2     1.06625944E-05   # C9' e+e-
 03051111 4137   00   2     5.66506022E-06   # C10 e+e-
 03051111 4237   00   2    -7.96128799E-05   # C10' e+e-
 03051313 4133   00   2    -4.77036319E-07   # C9 mu+mu-
 03051313 4233   00   2     1.06625898E-05   # C9' mu+mu-
 03051313 4137   00   2     5.66506547E-06   # C10 mu+mu-
 03051313 4237   00   2    -7.96128938E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.20601364E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     1.72390891E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.20601344E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     1.72390891E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.20595809E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     1.72390891E-05   # C11' nu_3 nu_3
