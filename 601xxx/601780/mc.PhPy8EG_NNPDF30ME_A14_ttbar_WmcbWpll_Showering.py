#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO, with tWmcb filter'
evgenConfig.keywords    =['SM', 'top', 'ttbar']
evgenConfig.contact     = [ 'yi.yu@cern.ch']
evgenConfig.process = "p p > t t~, decay t > w+ b, w+ > l+ vl, decay t~ > w- b~, w- > c~ b"
evgenConfig.inputFilesPerJob=11
evgenConfig.nEventsPerJob = 20000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
# include('Pythia8_i/Pythia8_LHEF.py')
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'POWHEG:pThard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:nFinal = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]
