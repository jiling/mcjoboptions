import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
#include('MadGraphControl/setupEFT.py')

#############################################################################################
### For simple processes only this part and the metadata at the bottom need to be changed

# create dictionary of processes this JO can create
definitions="""import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/nTGC_UFO
define p = g u c d s b u~ c~ d~ s~ b~
define j = p
define l+ = e+ mu+ 
define l- = e- mu-
"""
processes={'mm':'generate p p > mu+ mu- mu+ mu- NP8^2==1\n', # name must not contain special characters, including _
           'ee': 'generate p p > e+ e- e+ e- NP8^2==1\n',
           'em':'generate p p > e+ e- mu+ mu- NP8^2==1\n'
}

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

# define cuts and other run card settings
settings={'dynamical_scale_choice':'-1', # make sure to explicitly set a scale, otherwise it might end up being different for interference terms
        # see here for the scales: https://answers.launchpad.net/mg5amcnlo/+faq/2014
        'nevents': int(nevents),
        'event_norm':'average',
        'drll':'0', #we can allow small dr since we require a min mass
        'mmll':'3', #min invariant mass *same flavour* lepton pair
        'ptl' :'3', #min charged lepton pt
        'etal':'3', #max charged lepton abs eta
        'ptl1min':'15',
        'ptl2min':'10',
}
#############################################################################################

for p in processes:
    processes[p]=definitions+processes[p]+'output -f'

process_dir = new_process(processes['em'])

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
param_dict = {
    'aNTGC':{'f4Z' : 1.},
}
modify_param_card(process_dir=process_dir,params=param_dict)

generate(runArgs=runArgs,process_dir=process_dir)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# turn off mpi for faster generation and smaller files in 'noMPI' is part of name
if 'noMPI' in get_physics_short():
    genSeq.Pythia8.Commands += [' PartonLevel:MPI = off']


#############################################################################################
# add meta data
evgenConfig.description = '4l production with aTGC'
evgenConfig.keywords+=['ZZ','4lepton']
evgenConfig.contact = ['robert.hankache@cern.ch']
evgenConfig.nEventsPerJob = 5000
#############################################################################################
