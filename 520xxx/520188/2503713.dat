# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 08.09.2021,  17:10
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.34020562E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.75557868E+03  # scale for input parameters
    1   -4.41742043E+01  # M_1
    2   -1.89213824E+03  # M_2
    3    3.60162257E+03  # M_3
   11   -6.35288991E+03  # A_t
   12    3.71619575E+02  # A_b
   13    9.32921325E+02  # A_tau
   23    3.64083882E+02  # mu
   25    5.29511963E+01  # tan(beta)
   26    2.78634152E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.56241910E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.92173924E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.71897038E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.75557868E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.75557868E+03  # (SUSY scale)
  1  1     8.38586717E-06   # Y_u(Q)^DRbar
  2  2     4.26002052E-03   # Y_c(Q)^DRbar
  3  3     1.01308011E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.75557868E+03  # (SUSY scale)
  1  1     8.92285843E-04   # Y_d(Q)^DRbar
  2  2     1.69534310E-02   # Y_s(Q)^DRbar
  3  3     8.84867954E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.75557868E+03  # (SUSY scale)
  1  1     1.55710405E-04   # Y_e(Q)^DRbar
  2  2     3.21959732E-02   # Y_mu(Q)^DRbar
  3  3     5.41483304E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.75557868E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.35288980E+03   # A_t(Q)^DRbar
Block Ad Q=  3.75557868E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     3.71619586E+02   # A_b(Q)^DRbar
Block Ae Q=  3.75557868E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     9.32921328E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.75557868E+03  # soft SUSY breaking masses at Q
   1   -4.41742043E+01  # M_1
   2   -1.89213824E+03  # M_2
   3    3.60162257E+03  # M_3
  21    7.63275523E+06  # M^2_(H,d)
  22    1.25753164E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.56241910E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.92173924E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.71897038E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.27073780E+02  # h0
        35     2.78542979E+03  # H0
        36     2.78634152E+03  # A0
        37     2.78868769E+03  # H+
   1000001     1.01025460E+04  # ~d_L
   2000001     1.00793051E+04  # ~d_R
   1000002     1.01021853E+04  # ~u_L
   2000002     1.00821442E+04  # ~u_R
   1000003     1.01025516E+04  # ~s_L
   2000003     1.00793151E+04  # ~s_R
   1000004     1.01021908E+04  # ~c_L
   2000004     1.00821450E+04  # ~c_R
   1000005     2.83092194E+03  # ~b_1
   2000005     3.62962679E+03  # ~b_2
   1000006     3.58682437E+03  # ~t_1
   2000006     3.93227260E+03  # ~t_2
   1000011     1.00198920E+04  # ~e_L-
   2000011     1.00091889E+04  # ~e_R-
   1000012     1.00191327E+04  # ~nu_eL
   1000013     1.00199200E+04  # ~mu_L-
   2000013     1.00092406E+04  # ~mu_R-
   1000014     1.00191597E+04  # ~nu_muL
   1000015     1.00235880E+04  # ~tau_1-
   2000015     1.00282588E+04  # ~tau_2-
   1000016     1.00268383E+04  # ~nu_tauL
   1000021     4.06606413E+03  # ~g
   1000022     4.40583090E+01  # ~chi_10
   1000023     3.75434015E+02  # ~chi_20
   1000025     3.78026603E+02  # ~chi_30
   1000035     2.00483368E+03  # ~chi_40
   1000024     3.74200592E+02  # ~chi_1+
   1000037     2.00480131E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.87738673E-02   # alpha
Block Hmix Q=  3.75557868E+03  # Higgs mixing parameters
   1    3.64083882E+02  # mu
   2    5.29511963E+01  # tan[beta](Q)
   3    2.42987834E+02  # v(Q)
   4    7.76369907E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.40100942E-01   # Re[R_st(1,1)]
   1  2     3.40896198E-01   # Re[R_st(1,2)]
   2  1    -3.40896198E-01   # Re[R_st(2,1)]
   2  2     9.40100942E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     7.83903115E-03   # Re[R_sb(1,1)]
   1  2     9.99969274E-01   # Re[R_sb(1,2)]
   2  1    -9.99969274E-01   # Re[R_sb(2,1)]
   2  2     7.83903115E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     3.76926208E-01   # Re[R_sta(1,1)]
   1  2     9.26243291E-01   # Re[R_sta(1,2)]
   2  1    -9.26243291E-01   # Re[R_sta(2,1)]
   2  2     3.76926208E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.92768174E-01   # Re[N(1,1)]
   1  2     3.44511188E-04   # Re[N(1,2)]
   1  3    -1.19454856E-01   # Re[N(1,3)]
   1  4    -1.19067705E-02   # Re[N(1,4)]
   2  1     9.29962283E-02   # Re[N(2,1)]
   2  2     3.39305477E-02   # Re[N(2,2)]
   2  3    -7.02532451E-01   # Re[N(2,3)]
   2  4    -7.04732981E-01   # Re[N(2,4)]
   3  1    -7.59085942E-02   # Re[N(3,1)]
   3  2     2.43069403E-02   # Re[N(3,2)]
   3  3     7.01521210E-01   # Re[N(3,3)]
   3  4    -7.08177273E-01   # Re[N(3,4)]
   4  1     9.69132144E-04   # Re[N(4,1)]
   4  2    -9.99128506E-01   # Re[N(4,2)]
   4  3    -6.83258476E-03   # Re[N(4,3)]
   4  4    -4.11655763E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.66800450E-03   # Re[U(1,1)]
   1  2     9.99953264E-01   # Re[U(1,2)]
   2  1    -9.99953264E-01   # Re[U(2,1)]
   2  2    -9.66800450E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     5.82424086E-02   # Re[V(1,1)]
   1  2     9.98302470E-01   # Re[V(1,2)]
   2  1     9.98302470E-01   # Re[V(2,1)]
   2  2    -5.82424086E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02885540E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.85628143E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     8.62470771E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     5.74618805E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.34522410E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.20931657E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.28550422E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01283692E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.04190231E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.07004732E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.77683484E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.05583379E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     7.69734687E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.05944745E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.34728893E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.19741659E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.03681838E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     8.38769783E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00822997E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.03266225E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.70714533E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.77821599E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.68710099E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.60110713E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.24515603E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.95828029E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     6.50779993E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.89239421E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.60192992E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.55925942E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.60469236E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.85114980E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     4.12711935E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.71199349E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.34525874E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.23233902E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.37508311E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.01874350E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.21784236E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02128245E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.34732325E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.21821709E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.37297978E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01412734E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.74353053E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01207619E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.92969387E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.44114473E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     9.59361172E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.10636273E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.03768899E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.20167445E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.63344825E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.84458792E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.90011299E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.84518095E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.03806859E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.98609279E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.19878494E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.18097945E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.63460445E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.84403302E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.36473284E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.07580246E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     1.01993928E-04    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     9.89809898E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.84579810E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.03908668E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.34080196E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.98555739E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.19867781E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.18024997E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.63810411E+01   # ~b_1
#    BR                NDA      ID1      ID2
     2.55728001E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.43283374E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.42270307E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.88870911E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     1.51306495E+02   # ~b_2
#    BR                NDA      ID1      ID2
     8.44533439E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.80565550E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.80217630E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     5.09336198E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.75572194E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.03178938E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.89118564E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     6.97614614E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.80510364E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.82246933E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.34495546E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.22856271E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.61217921E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.84501578E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.02221687E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.65145467E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.98185411E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     4.39734356E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.19480930E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.18065891E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.80517671E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.82242146E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.37596190E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     2.25989852E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.61205904E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.84563246E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.02203647E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.67769884E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.98131936E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.23604801E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.19470249E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.17992941E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.47819958E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.22921309E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     8.25192855E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.39979563E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.33730253E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.66865405E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.71629509E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     9.47834647E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     6.07858631E-04    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     3.00317570E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.00486216E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.91084423E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.94517671E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.03128531E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.10292368E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.90974221E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     6.24405865E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.63371996E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     6.96430610E-02    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     4.50623648E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     4.80553550E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.46591314E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.98789448E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.21050091E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.77409225E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.42218153E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.41284169E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.41121118E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.42039549E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.30788031E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.62952923E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     1.00670675E-02    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     9.69309116E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.10833358E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.81038167E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.18933146E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.74993494E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.61469260E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.38523381E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.94359774E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.21304794E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.21304794E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     7.44632429E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.46167999E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.46703791E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.58415558E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.57778282E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.62315841E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     5.77079376E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.56598288E-04    3     1000025         5        -5   # BR(chi^0_4 -> chi^0_3 b b_bar)
     9.91924046E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     9.91924046E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     4.49446243E+01   # ~g
#    BR                NDA      ID1      ID2
     2.43665683E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     2.43665683E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     4.68041380E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     4.68041380E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.55455349E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.55455349E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     5.52651689E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     5.52651689E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
     1.08612117E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
#    BR                NDA      ID1      ID2       ID3
     1.63351899E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     8.63402420E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     9.12214483E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     8.14113582E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     8.14113582E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.82193076E-03   # Gamma(h0)
     2.55076576E-03   2        22        22   # BR(h0 -> photon photon)
     1.82122013E-03   2        22        23   # BR(h0 -> photon Z)
     3.62092736E-02   2        23        23   # BR(h0 -> Z Z)
     2.87395763E-01   2       -24        24   # BR(h0 -> W W)
     7.72486750E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.57596502E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.03548126E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.86925978E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.29443941E-07   2        -2         2   # BR(h0 -> Up up)
     2.51236948E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.28067724E-07   2        -1         1   # BR(h0 -> Down down)
     1.90989882E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.07708092E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     2.85471736E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     9.15862763E+01   # Gamma(HH)
     2.21269811E-08   2        22        22   # BR(HH -> photon photon)
     2.01839669E-08   2        22        23   # BR(HH -> photon Z)
     1.24919277E-07   2        23        23   # BR(HH -> Z Z)
     4.16389069E-08   2       -24        24   # BR(HH -> W W)
     1.21627966E-05   2        21        21   # BR(HH -> gluon gluon)
     1.14194526E-08   2       -11        11   # BR(HH -> Electron electron)
     5.08438067E-04   2       -13        13   # BR(HH -> Muon muon)
     1.46841317E-01   2       -15        15   # BR(HH -> Tau tau)
     2.83330485E-14   2        -2         2   # BR(HH -> Up up)
     5.49581858E-09   2        -4         4   # BR(HH -> Charm charm)
     3.53760772E-04   2        -6         6   # BR(HH -> Top top)
     8.29594729E-07   2        -1         1   # BR(HH -> Down down)
     3.00084689E-04   2        -3         3   # BR(HH -> Strange strange)
     7.23224755E-01   2        -5         5   # BR(HH -> Bottom bottom)
     8.66756565E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     3.10876204E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     3.10876204E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     9.98951019E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.72249742E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.54732067E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     4.51385168E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     2.03445736E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.82366708E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.06255017E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     4.60619886E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     2.00785861E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     4.92751292E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     8.63543381E+01   # Gamma(A0)
     9.20888798E-09   2        22        22   # BR(A0 -> photon photon)
     2.33664249E-08   2        22        23   # BR(A0 -> photon Z)
     1.90191634E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.13150947E-08   2       -11        11   # BR(A0 -> Electron electron)
     5.03789759E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.45499115E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.63271370E-14   2        -2         2   # BR(A0 -> Up up)
     5.10635604E-09   2        -4         4   # BR(A0 -> Charm charm)
     3.32978833E-04   2        -6         6   # BR(A0 -> Top top)
     8.21976343E-07   2        -1         1   # BR(A0 -> Down down)
     2.97328124E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.16586388E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     9.74062019E-04   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     3.30016901E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     3.30016901E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.05281205E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.70892316E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.75913788E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     5.23022701E-04   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.14968936E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.59249181E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.13987699E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     5.66721112E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.11801799E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     2.05084716E-07   2        23        25   # BR(A0 -> Z h0)
     2.23357569E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.08440935E+02   # Gamma(Hp)
     1.21278342E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.18502792E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.46662058E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.59576364E-07   2        -1         2   # BR(Hp -> Down up)
     1.28118851E-05   2        -3         2   # BR(Hp -> Strange up)
     7.86291948E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.56570780E-08   2        -1         4   # BR(Hp -> Down charm)
     2.73770834E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.10108999E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.34099067E-08   2        -1         6   # BR(Hp -> Down top)
     9.14996211E-07   2        -3         6   # BR(Hp -> Strange top)
     7.42546485E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.88793121E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     8.02001885E-04   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     5.56375760E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.62109785E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.81132151E-06   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     2.59915972E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.64334323E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.63847760E-07   2        24        25   # BR(Hp -> W h0)
     4.60601568E-12   2        24        35   # BR(Hp -> W HH)
     8.92181574E-13   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.88468609E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.80384072E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.80382919E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000411E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.52542378E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.56655107E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.88468609E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.80384072E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.80382919E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999988E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.19252052E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999988E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.19252052E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26216190E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.62232147E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.51948405E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.19252052E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999988E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.09988013E-04   # BR(b -> s gamma)
    2    1.59194849E-06   # BR(b -> s mu+ mu-)
    3    3.52551725E-05   # BR(b -> s nu nu)
    4    3.08656210E-15   # BR(Bd -> e+ e-)
    5    1.31851126E-10   # BR(Bd -> mu+ mu-)
    6    2.74126548E-08   # BR(Bd -> tau+ tau-)
    7    1.02702064E-13   # BR(Bs -> e+ e-)
    8    4.38732574E-09   # BR(Bs -> mu+ mu-)
    9    9.24605094E-07   # BR(Bs -> tau+ tau-)
   10    9.50700491E-05   # BR(B_u -> tau nu)
   11    9.82037210E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41800935E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93462414E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15681318E-03   # epsilon_K
   17    2.28165540E-15   # Delta(M_K)
   18    2.48063251E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29181964E-11   # BR(K^+ -> pi^+ nu nu)
   20   -4.98008364E-16   # Delta(g-2)_electron/2
   21   -2.12915305E-11   # Delta(g-2)_muon/2
   22   -6.03084133E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.82676341E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.74959881E-01   # C7
     0305 4322   00   2    -2.21840009E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.45625206E-02   # C8
     0305 6321   00   2    -1.54563855E-04   # C8'
 03051111 4133   00   0     1.62547405E+00   # C9 e+e-
 03051111 4133   00   2     1.62569822E+00   # C9 e+e-
 03051111 4233   00   2     7.01781335E-04   # C9' e+e-
 03051111 4137   00   0    -4.44816615E+00   # C10 e+e-
 03051111 4137   00   2    -4.44694882E+00   # C10 e+e-
 03051111 4237   00   2    -5.20287134E-03   # C10' e+e-
 03051313 4133   00   0     1.62547405E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62569795E+00   # C9 mu+mu-
 03051313 4233   00   2     7.01779841E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44816615E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44694909E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.20287108E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50508801E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.12536297E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50508801E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.12536323E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50508801E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.12543537E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85842647E-07   # C7
     0305 4422   00   2    -4.66823633E-06   # C7
     0305 4322   00   2     2.26271771E-06   # C7'
     0305 6421   00   0     3.30499745E-07   # C8
     0305 6421   00   2    -2.14944951E-05   # C8
     0305 6321   00   2     6.55594058E-07   # C8'
 03051111 4133   00   2    -3.08885514E-07   # C9 e+e-
 03051111 4233   00   2     1.39661908E-05   # C9' e+e-
 03051111 4137   00   2     4.28398579E-06   # C10 e+e-
 03051111 4237   00   2    -1.03548042E-04   # C10' e+e-
 03051313 4133   00   2    -3.08889450E-07   # C9 mu+mu-
 03051313 4233   00   2     1.39661852E-05   # C9' mu+mu-
 03051313 4137   00   2     4.28399119E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.03548059E-04   # C10' mu+mu-
 03051212 4137   00   2    -9.05191573E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.23970833E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -9.05191403E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.23970833E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -9.05144631E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.23970833E-05   # C11' nu_3 nu_3
