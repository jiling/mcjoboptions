# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:16
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.66096789E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.65194214E+03  # scale for input parameters
    1    4.33371262E+01  # M_1
    2    9.12401006E+02  # M_2
    3    2.71208922E+03  # M_3
   11    4.55624288E+02  # A_t
   12    2.37176362E+02  # A_b
   13   -1.23744503E+03  # A_tau
   23   -2.41778308E+02  # mu
   25    2.55567929E+01  # tan(beta)
   26    1.67725628E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.60131697E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.88239702E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.93932712E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.65194214E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.65194214E+03  # (SUSY scale)
  1  1     8.39078809E-06   # Y_u(Q)^DRbar
  2  2     4.26252035E-03   # Y_c(Q)^DRbar
  3  3     1.01367460E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.65194214E+03  # (SUSY scale)
  1  1     4.30912760E-04   # Y_d(Q)^DRbar
  2  2     8.18734244E-03   # Y_s(Q)^DRbar
  3  3     4.27330429E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.65194214E+03  # (SUSY scale)
  1  1     7.51974277E-05   # Y_e(Q)^DRbar
  2  2     1.55484431E-02   # Y_mu(Q)^DRbar
  3  3     2.61499235E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.65194214E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     4.55624293E+02   # A_t(Q)^DRbar
Block Ad Q=  3.65194214E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     2.37176361E+02   # A_b(Q)^DRbar
Block Ae Q=  3.65194214E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.23744502E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.65194214E+03  # soft SUSY breaking masses at Q
   1    4.33371262E+01  # M_1
   2    9.12401006E+02  # M_2
   3    2.71208922E+03  # M_3
  21    2.70857296E+06  # M^2_(H,d)
  22    1.71475013E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.60131697E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.88239702E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.93932712E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.21909147E+02  # h0
        35     1.67706749E+03  # H0
        36     1.67725628E+03  # A0
        37     1.68030736E+03  # H+
   1000001     1.01102200E+04  # ~d_L
   2000001     1.00855286E+04  # ~d_R
   1000002     1.01098515E+04  # ~u_L
   2000002     1.00888225E+04  # ~u_R
   1000003     1.01102221E+04  # ~s_L
   2000003     1.00855317E+04  # ~s_R
   1000004     1.01098536E+04  # ~c_L
   2000004     1.00888234E+04  # ~c_R
   1000005     2.71220324E+03  # ~b_1
   2000005     4.99948935E+03  # ~b_2
   1000006     2.71516872E+03  # ~t_1
   2000006     4.91191627E+03  # ~t_2
   1000011     1.00211356E+04  # ~e_L-
   2000011     1.00088229E+04  # ~e_R-
   1000012     1.00203644E+04  # ~nu_eL
   1000013     1.00211439E+04  # ~mu_L-
   2000013     1.00088388E+04  # ~mu_R-
   1000014     1.00203725E+04  # ~nu_muL
   1000015     1.00133515E+04  # ~tau_1-
   2000015     1.00234857E+04  # ~tau_2-
   1000016     1.00226945E+04  # ~nu_tauL
   1000021     3.16235937E+03  # ~g
   1000022     4.30456909E+01  # ~chi_10
   1000023     2.55752160E+02  # ~chi_20
   1000025     2.61921044E+02  # ~chi_30
   1000035     9.87044761E+02  # ~chi_40
   1000024     2.54572769E+02  # ~chi_1+
   1000037     9.86826775E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.78494163E-02   # alpha
Block Hmix Q=  3.65194214E+03  # Higgs mixing parameters
   1   -2.41778308E+02  # mu
   2    2.55567929E+01  # tan[beta](Q)
   3    2.43053243E+02  # v(Q)
   4    2.81318863E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99998347E-01   # Re[R_st(1,1)]
   1  2     1.81825562E-03   # Re[R_st(1,2)]
   2  1    -1.81825562E-03   # Re[R_st(2,1)]
   2  2    -9.99998347E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99999711E-01   # Re[R_sb(1,1)]
   1  2     7.60890001E-04   # Re[R_sb(1,2)]
   2  1    -7.60890001E-04   # Re[R_sb(2,1)]
   2  2    -9.99999711E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -4.26762648E-02   # Re[R_sta(1,1)]
   1  2     9.99088953E-01   # Re[R_sta(1,2)]
   2  1    -9.99088953E-01   # Re[R_sta(2,1)]
   2  2    -4.26762648E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.84613667E-01   # Re[N(1,1)]
   1  2     1.25599854E-03   # Re[N(1,2)]
   1  3     1.73262735E-01   # Re[N(1,3)]
   1  4     2.26798112E-02   # Re[N(1,4)]
   2  1    -1.39027828E-01   # Re[N(2,1)]
   2  2    -7.49275825E-02   # Re[N(2,2)]
   2  3    -6.98107254E-01   # Re[N(2,3)]
   2  4    -6.98357632E-01   # Re[N(2,4)]
   3  1     1.05785069E-01   # Re[N(3,1)]
   3  2    -4.82961157E-02   # Re[N(3,2)]
   3  3     6.94451538E-01   # Re[N(3,3)]
   3  4    -7.10080324E-01   # Re[N(3,4)]
   4  1     4.08761489E-03   # Re[N(4,1)]
   4  2    -9.96017954E-01   # Re[N(4,2)]
   4  3     1.90616994E-02   # Re[N(4,3)]
   4  4     8.69952755E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.70231731E-02   # Re[U(1,1)]
   1  2    -9.99634807E-01   # Re[U(1,2)]
   2  1     9.99634807E-01   # Re[U(2,1)]
   2  2    -2.70231731E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.23170124E-01   # Re[V(1,1)]
   1  2     9.92385570E-01   # Re[V(1,2)]
   2  1     9.92385570E-01   # Re[V(2,1)]
   2  2     1.23170124E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02868219E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.69502568E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.93049958E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.11760271E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.42368568E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.53073595E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     6.88767768E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.00100098E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     4.51905831E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.07232067E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03830495E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.67681114E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.97331705E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.16148285E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     9.53506919E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.42416824E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.52886839E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     7.05006389E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.83866243E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.99998793E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     4.51753086E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.07026840E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.76636580E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     6.34895941E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     9.78884640E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     8.97444505E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     5.72199472E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.75755812E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.14313267E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.55876252E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.97619928E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.88646307E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.31317811E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.73943308E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.54239146E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42373374E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.61165256E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.39827701E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02709028E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     9.38724631E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.98388790E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42421622E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.60874212E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.39712852E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02606732E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     9.72176993E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.98186815E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.56024900E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.85999643E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.10166504E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.76290003E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     9.57818879E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.46226358E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.52873735E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.36079239E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.66513193E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.91376087E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.81904907E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.77469989E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.43093398E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.56180485E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.54702604E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.11585499E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.30687251E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.52900812E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.36106491E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.76505060E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.06298801E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.91335409E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.81922205E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.77517719E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.51459789E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.64456268E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.54690512E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.11583124E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.30669120E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     9.10028933E+01   # ~b_1
#    BR                NDA      ID1      ID2
     7.35431744E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     5.21602427E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     5.18108687E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     9.81140658E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.84522841E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.06037664E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.80976290E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.79469045E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     4.89510066E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     4.83179229E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     9.95217719E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     7.84726057E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     6.70075344E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.25952808E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     6.49058656E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.75752564E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.66379357E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.81890943E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.72453515E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.64811164E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.53072393E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.72466042E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.09970758E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.30658674E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.70082680E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.25949284E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     6.51702935E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.78488334E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.66368855E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.81908211E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.72449977E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.67074689E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.53060648E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.74178367E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.09968365E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.30640538E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     9.13250932E+01   # ~t_1
#    BR                NDA      ID1      ID2
     4.23356014E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.89573250E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.98179270E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.02026699E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.09781839E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.94335172E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.85427958E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.42864421E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.10871629E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.42379139E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.46791638E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.99758155E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.86950683E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.01317511E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.84950019E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     2.60570101E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.66083437E-04    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     1.29828445E-04    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.32961356E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     9.07476956E-04    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.33680715E-04    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     1.48274305E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99971637E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     8.34328258E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     3.70083936E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.51445078E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.51909822E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.48065907E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.36835462E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.16963658E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.87644104E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     4.29634892E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.19904957E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     5.13370148E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     4.86623986E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.85381367E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.01606188E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.98358916E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     9.46671618E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.22590537E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.22590537E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.40904712E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.58579040E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     2.09000672E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.48120472E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.74674204E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.14262619E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.21040504E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     3.34951358E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.34951358E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.26366075E+01   # ~g
#    BR                NDA      ID1      ID2
     7.77646840E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     7.77646840E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.38819879E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.38819879E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.58979631E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.58979631E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     2.08726525E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
     2.17703727E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.03834342E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     4.61855117E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     4.61667807E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     5.43576531E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     5.43576531E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.10993320E-03   # Gamma(h0)
     2.63072591E-03   2        22        22   # BR(h0 -> photon photon)
     1.39813939E-03   2        22        23   # BR(h0 -> photon Z)
     2.31793037E-02   2        23        23   # BR(h0 -> Z Z)
     2.03788420E-01   2       -24        24   # BR(h0 -> W W)
     8.42140276E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.17895244E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.30367323E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.64189234E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.46133782E-07   2        -2         2   # BR(h0 -> Up up)
     2.83610445E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.02151512E-07   2        -1         1   # BR(h0 -> Down down)
     2.17783501E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.78618592E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.09419196E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     2.51067168E+01   # Gamma(HH)
     6.29478321E-08   2        22        22   # BR(HH -> photon photon)
     1.52613201E-07   2        22        23   # BR(HH -> photon Z)
     3.73799483E-06   2        23        23   # BR(HH -> Z Z)
     2.55112655E-06   2       -24        24   # BR(HH -> W W)
     1.16908012E-05   2        21        21   # BR(HH -> gluon gluon)
     6.22840719E-09   2       -11        11   # BR(HH -> Electron electron)
     2.77268482E-04   2       -13        13   # BR(HH -> Muon muon)
     8.00710036E-02   2       -15        15   # BR(HH -> Tau tau)
     2.23807344E-13   2        -2         2   # BR(HH -> Up up)
     4.34086564E-08   2        -4         4   # BR(HH -> Charm charm)
     3.13331095E-03   2        -6         6   # BR(HH -> Top top)
     4.94920333E-07   2        -1         1   # BR(HH -> Down down)
     1.79011101E-04   2        -3         3   # BR(HH -> Strange strange)
     4.75530181E-01   2        -5         5   # BR(HH -> Bottom bottom)
     8.41198387E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.18293630E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.18293630E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     4.96752237E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.80349633E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.90260935E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     3.58588660E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.44225352E-06   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.56543482E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     4.88884315E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     2.51877289E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     6.71842942E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.84000241E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     2.46990522E+01   # Gamma(A0)
     1.78864154E-08   2        22        22   # BR(A0 -> photon photon)
     1.78607808E-07   2        22        23   # BR(A0 -> photon Z)
     3.51268470E-05   2        21        21   # BR(A0 -> gluon gluon)
     6.14679267E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.73634888E-04   2       -13        13   # BR(A0 -> Muon muon)
     7.90220593E-02   2       -15        15   # BR(A0 -> Tau tau)
     2.14289809E-13   2        -2         2   # BR(A0 -> Up up)
     4.15568970E-08   2        -4         4   # BR(A0 -> Charm charm)
     3.14688245E-03   2        -6         6   # BR(A0 -> Top top)
     4.88426751E-07   2        -1         1   # BR(A0 -> Down down)
     1.76662475E-04   2        -3         3   # BR(A0 -> Strange strange)
     4.69296814E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     9.00527718E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.20001438E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.20001438E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     4.95792556E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     3.37591986E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     3.42629746E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     4.03442420E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.37515044E-06   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.24339056E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     6.87214584E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     3.27647522E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     4.87776830E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     5.02905632E-06   2        23        25   # BR(A0 -> Z h0)
     6.30395557E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.73206371E+01   # Gamma(Hp)
     7.20174668E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.07897162E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     8.70906924E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.95669600E-07   2        -1         2   # BR(Hp -> Down up)
     8.30873282E-06   2        -3         2   # BR(Hp -> Strange up)
     5.37726427E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.51671829E-08   2        -1         4   # BR(Hp -> Down charm)
     1.78678246E-04   2        -3         4   # BR(Hp -> Strange charm)
     7.53008866E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.34981500E-07   2        -1         6   # BR(Hp -> Down top)
     5.38217692E-06   2        -3         6   # BR(Hp -> Strange top)
     5.07358450E-01   2        -5         6   # BR(Hp -> Bottom top)
     6.83054618E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     7.00642982E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     3.82197893E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.09058301E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.16539115E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.06178390E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.09799760E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     4.58094109E-06   2        24        25   # BR(Hp -> W h0)
     1.77614896E-11   2        24        35   # BR(Hp -> W HH)
     1.31563276E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.36673208E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    6.53212990E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    6.53149663E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00009696E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.43408664E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.53104266E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.36673208E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    6.53212990E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    6.53149663E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99998414E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.58551614E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99998414E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.58551614E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26953220E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.74943398E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.08169642E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.58551614E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99998414E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.38510607E-04   # BR(b -> s gamma)
    2    1.58821288E-06   # BR(b -> s mu+ mu-)
    3    3.52487664E-05   # BR(b -> s nu nu)
    4    2.48176755E-15   # BR(Bd -> e+ e-)
    5    1.06018324E-10   # BR(Bd -> mu+ mu-)
    6    2.21957491E-08   # BR(Bd -> tau+ tau-)
    7    8.39193854E-14   # BR(Bs -> e+ e-)
    8    3.58503439E-09   # BR(Bs -> mu+ mu-)
    9    7.60471622E-07   # BR(Bs -> tau+ tau-)
   10    9.53246364E-05   # BR(B_u -> tau nu)
   11    9.84666999E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41943440E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93564771E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15721271E-03   # epsilon_K
   17    2.28165875E-15   # Delta(M_K)
   18    2.47960821E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28958972E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.90788212E-16   # Delta(g-2)_electron/2
   21   -1.24321452E-11   # Delta(g-2)_muon/2
   22   -3.51986019E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.16127909E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.07641632E-01   # C7
     0305 4322   00   2    -3.54095418E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.16292752E-01   # C8
     0305 6321   00   2    -4.08884352E-04   # C8'
 03051111 4133   00   0     1.62335943E+00   # C9 e+e-
 03051111 4133   00   2     1.62367276E+00   # C9 e+e-
 03051111 4233   00   2     1.82032068E-04   # C9' e+e-
 03051111 4137   00   0    -4.44605154E+00   # C10 e+e-
 03051111 4137   00   2    -4.44404769E+00   # C10 e+e-
 03051111 4237   00   2    -1.35293074E-03   # C10' e+e-
 03051313 4133   00   0     1.62335943E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62367272E+00   # C9 mu+mu-
 03051313 4233   00   2     1.82031577E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44605154E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44404773E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.35293036E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50491835E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.92773092E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50491835E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.92773192E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50491835E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.92801417E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85821975E-07   # C7
     0305 4422   00   2    -1.36504968E-05   # C7
     0305 4322   00   2    -3.85414916E-07   # C7'
     0305 6421   00   0     3.30482037E-07   # C8
     0305 6421   00   2     4.33707535E-06   # C8
     0305 6321   00   2     3.40081820E-08   # C8'
 03051111 4133   00   2     7.75119696E-07   # C9 e+e-
 03051111 4233   00   2     4.04764917E-06   # C9' e+e-
 03051111 4137   00   2    -4.51257497E-08   # C10 e+e-
 03051111 4237   00   2    -3.00899308E-05   # C10' e+e-
 03051313 4133   00   2     7.75117832E-07   # C9 mu+mu-
 03051313 4233   00   2     4.04764866E-06   # C9' mu+mu-
 03051313 4137   00   2    -4.51238374E-08   # C10 mu+mu-
 03051313 4237   00   2    -3.00899323E-05   # C10' mu+mu-
 03051212 4137   00   2     4.30887421E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     6.51143855E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     4.30888295E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     6.51143855E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     4.31135321E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     6.51143855E-06   # C11' nu_3 nu_3
