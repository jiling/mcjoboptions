# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:19
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.88143903E+00  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.95983430E+03  # scale for input parameters
    1    3.58207744E+02  # M_1
    2    7.85048551E+02  # M_2
    3    3.23837134E+03  # M_3
   11    4.37088167E+03  # A_t
   12   -1.69901832E+02  # A_b
   13   -1.47825067E+03  # A_tau
   23   -1.54756890E+03  # mu
   25    3.66960305E+00  # tan(beta)
   26    7.37148078E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.77285462E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.07656519E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.99576194E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.95983430E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.95983430E+03  # (SUSY scale)
  1  1     8.69011425E-06   # Y_u(Q)^DRbar
  2  2     4.41457804E-03   # Y_c(Q)^DRbar
  3  3     1.04983560E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.95983430E+03  # (SUSY scale)
  1  1     6.40803421E-05   # Y_d(Q)^DRbar
  2  2     1.21752650E-03   # Y_s(Q)^DRbar
  3  3     6.35476195E-02   # Y_b(Q)^DRbar
Block Ye Q=  3.95983430E+03  # (SUSY scale)
  1  1     1.11824883E-05   # Y_e(Q)^DRbar
  2  2     2.31218392E-03   # Y_mu(Q)^DRbar
  3  3     3.88871298E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.95983430E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     4.37088168E+03   # A_t(Q)^DRbar
Block Ad Q=  3.95983430E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.69901831E+02   # A_b(Q)^DRbar
Block Ae Q=  3.95983430E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.47825067E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.95983430E+03  # soft SUSY breaking masses at Q
   1    3.58207744E+02  # M_1
   2    7.85048551E+02  # M_2
   3    3.23837134E+03  # M_3
  21   -1.95492390E+06  # M^2_(H,d)
  22   -2.01874270E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.77285462E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.07656519E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.99576194E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.18696336E+02  # h0
        35     7.38647832E+02  # H0
        36     7.37148078E+02  # A0
        37     7.42395607E+02  # H+
   1000001     1.01083981E+04  # ~d_L
   2000001     1.00837699E+04  # ~d_R
   1000002     1.01080801E+04  # ~u_L
   2000002     1.00871876E+04  # ~u_R
   1000003     1.01083984E+04  # ~s_L
   2000003     1.00837700E+04  # ~s_R
   1000004     1.01080805E+04  # ~c_L
   2000004     1.00871882E+04  # ~c_R
   1000005     3.85082321E+03  # ~b_1
   2000005     5.08001192E+03  # ~b_2
   1000006     3.82835754E+03  # ~t_1
   2000006     4.09582635E+03  # ~t_2
   1000011     1.00212192E+04  # ~e_L-
   2000011     1.00086615E+04  # ~e_R-
   1000012     1.00204996E+04  # ~nu_eL
   1000013     1.00212194E+04  # ~mu_L-
   2000013     1.00086617E+04  # ~mu_R-
   1000014     1.00204998E+04  # ~nu_muL
   1000015     1.00087179E+04  # ~tau_1-
   2000015     1.00212646E+04  # ~tau_2-
   1000016     1.00205340E+04  # ~nu_tauL
   1000021     3.73408482E+03  # ~g
   1000022     3.60762568E+02  # ~chi_10
   1000023     8.48421616E+02  # ~chi_20
   1000025     1.56399125E+03  # ~chi_30
   1000035     1.56405789E+03  # ~chi_40
   1000024     8.48271156E+02  # ~chi_1+
   1000037     1.56539488E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.61893101E-01   # alpha
Block Hmix Q=  3.95983430E+03  # Higgs mixing parameters
   1   -1.54756890E+03  # mu
   2    3.66960305E+00  # tan[beta](Q)
   3    2.42966998E+02  # v(Q)
   4    5.43387289E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.57605594E-01   # Re[R_st(1,1)]
   1  2     2.88082498E-01   # Re[R_st(1,2)]
   2  1    -2.88082498E-01   # Re[R_st(2,1)]
   2  2    -9.57605594E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99999481E-01   # Re[R_sb(1,1)]
   1  2     1.01893925E-03   # Re[R_sb(1,2)]
   2  1    -1.01893925E-03   # Re[R_sb(2,1)]
   2  2    -9.99999481E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -2.98003413E-02   # Re[R_sta(1,1)]
   1  2     9.99555871E-01   # Re[R_sta(1,2)]
   2  1    -9.99555871E-01   # Re[R_sta(2,1)]
   2  2    -2.98003413E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99626002E-01   # Re[N(1,1)]
   1  2    -1.40786011E-03   # Re[N(1,2)]
   1  3     2.72879015E-02   # Re[N(1,3)]
   1  4    -1.11593554E-03   # Re[N(1,4)]
   2  1    -1.75097055E-04   # Re[N(2,1)]
   2  2    -9.98115501E-01   # Re[N(2,2)]
   2  3    -5.86480369E-02   # Re[N(2,3)]
   2  4    -1.80505867E-02   # Re[N(2,4)]
   3  1    -1.85299586E-02   # Re[N(3,1)]
   3  2     5.42085816E-02   # Re[N(3,2)]
   3  3    -7.04914901E-01   # Re[N(3,3)]
   3  4    -7.06974577E-01   # Re[N(3,4)]
   4  1     2.01113631E-02   # Re[N(4,1)]
   4  2    -2.87209712E-02   # Re[N(4,2)]
   4  3     7.06336152E-01   # Re[N(4,3)]
   4  4    -7.07007694E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.96544210E-01   # Re[U(1,1)]
   1  2    -8.30640608E-02   # Re[U(1,2)]
   2  1     8.30640608E-02   # Re[U(2,1)]
   2  2    -9.96544210E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99665054E-01   # Re[V(1,1)]
   1  2     2.58801151E-02   # Re[V(1,2)]
   2  1     2.58801151E-02   # Re[V(2,1)]
   2  2     9.99665054E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.01575587E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99286342E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.27657826E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     3.85969300E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.42968462E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.82186441E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.03024843E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     5.76579002E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     6.04032904E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     4.05212217E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.01595863E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.99245985E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.37680819E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.96030373E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
DECAY   1000013     1.42969479E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.82180235E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.03022718E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.80100726E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     6.04028617E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     4.05209342E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.08145078E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.85782877E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.02263183E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     2.94324808E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.08121009E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.04025927E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     5.12977331E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.43172609E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.83059954E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     3.02245117E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.63653246E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.13504914E-03    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     6.02463699E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.21360655E-03    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42969967E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.72860133E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.02885703E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.20683041E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     4.57347108E-04    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.07770783E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     3.93323526E-04    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42970984E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.72853938E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.02883553E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.20682185E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     4.57343863E-04    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.07766521E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     4.00366320E-04    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.43257475E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.71111368E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.02278899E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.20441293E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     4.56430967E-04    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     6.06567623E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.38149734E-03    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.98236436E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.37939158E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.90613903E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.27971686E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.90125724E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.00295275E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.92569254E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.19689425E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     8.03420852E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.17323044E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.98237010E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.37938304E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.90612967E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.27975735E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.90124703E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.00291969E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.92761421E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.19688761E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     8.08547095E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.17318536E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.06381451E+02   # ~b_1
#    BR                NDA      ID1      ID2
     4.88494966E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.43694783E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.37236850E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.12600602E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.85203709E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     5.53538769E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     1.01794142E-02    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     8.82325287E+01   # ~b_2
#    BR                NDA      ID1      ID2
     3.17990670E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.89486764E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.90304183E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.76268716E-03    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     9.60363529E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     6.15417922E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.64825600E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.63491366E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.27957505E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.96234456E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.00362562E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.50905721E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.20438703E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.17297000E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.15425399E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.64821175E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.63479703E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.27961534E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.96233371E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.00359254E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.53470091E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.20438044E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.17292487E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.07373427E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.10100973E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.25930306E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.83255441E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.72217760E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.53742162E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     5.17952792E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.04895505E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.59166224E+02   # ~t_2
#    BR                NDA      ID1      ID2
     5.19330455E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.09914753E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.96076725E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.03183854E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.22037595E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     3.73465247E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     6.30827494E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     3.21100820E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.87228930E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.82301687E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     4.83296571E-04   # chi^+_1
#    BR                NDA      ID1      ID2
     9.90204007E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     9.74201033E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     6.81819284E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.21847548E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.53657891E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.04181986E-01    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     2.50469959E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.59555939E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.27150563E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.83578697E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     3.20532278E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     3.47564536E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.56235239E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     6.85113726E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.12474694E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.09785009E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
     2.31395354E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
DECAY   1000025     6.74200464E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.50205884E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.50205884E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     6.60371000E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.66359006E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     8.92432511E-03    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     5.16458398E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     2.09914509E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     9.56475619E-02    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.87897871E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.29481484E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     1.29572198E-04    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     3.71172204E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     3.71172204E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.12558246E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.56038682E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.56038682E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.37127680E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.21970306E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.66066043E-02    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     9.21308603E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     3.76760974E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.91212411E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.80152414E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     4.01399113E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.08683146E-02    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.08683146E-02    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     7.02422495E-01   # ~g
#    BR                NDA      ID1      ID2
     2.23608069E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
     2.20559493E-03    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     2.26528867E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     2.26528811E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     2.24647348E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.40225825E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     3.46222780E-04    3     1000023         2        -2   # BR(~g -> chi^0_2 u u_bar)
     3.46222745E-04    3     1000023         4        -4   # BR(~g -> chi^0_2 c c_bar)
     5.59695121E-02    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     3.46133163E-04    3     1000023         1        -1   # BR(~g -> chi^0_2 d d_bar)
     3.46133138E-04    3     1000023         3        -3   # BR(~g -> chi^0_2 s s_bar)
     6.08356827E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.54824853E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     4.87873730E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.49526311E-01    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     4.07344716E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     6.92411724E-04    3     1000024         1        -2   # BR(~g -> chi^+_1 d u_bar)
     6.92411724E-04    3    -1000024        -1         2   # BR(~g -> chi^-_1 d_bar u)
     6.92411664E-04    3     1000024         3        -4   # BR(~g -> chi^+_1 s c_bar)
     6.92411664E-04    3    -1000024        -3         4   # BR(~g -> chi^-_1 s_bar c)
     1.16758496E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.16758496E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.54684791E-01    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.54684791E-01    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.04519077E-03   # Gamma(h0)
     2.41590523E-03   2        22        22   # BR(h0 -> photon photon)
     1.02828519E-03   2        22        23   # BR(h0 -> photon Z)
     1.52626307E-02   2        23        23   # BR(h0 -> Z Z)
     1.42972022E-01   2       -24        24   # BR(h0 -> W W)
     7.87555625E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.80384687E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.58161444E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.44272433E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.54050475E-07   2        -2         2   # BR(h0 -> Up up)
     2.98940924E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.78238766E-07   2        -1         1   # BR(h0 -> Down down)
     2.45303644E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.54739955E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.74355976E+00   # Gamma(HH)
     4.75186786E-06   2        22        22   # BR(HH -> photon photon)
     9.97860960E-07   2        22        23   # BR(HH -> photon Z)
     3.91175649E-03   2        23        23   # BR(HH -> Z Z)
     6.96035384E-03   2       -24        24   # BR(HH -> W W)
     1.42830070E-03   2        21        21   # BR(HH -> gluon gluon)
     9.39092016E-10   2       -11        11   # BR(HH -> Electron electron)
     4.17948244E-05   2       -13        13   # BR(HH -> Muon muon)
     1.20679105E-02   2       -15        15   # BR(HH -> Tau tau)
     7.81285216E-11   2        -2         2   # BR(HH -> Up up)
     1.51500899E-05   2        -4         4   # BR(HH -> Charm charm)
     8.59328535E-01   2        -6         6   # BR(HH -> Top top)
     8.31995787E-08   2        -1         1   # BR(HH -> Down down)
     3.00935693E-05   2        -3         3   # BR(HH -> Strange strange)
     8.20904428E-02   2        -5         5   # BR(HH -> Bottom bottom)
     8.95419356E-06   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.76894822E-12   2        23        36   # BR(HH -> Z A0)
     3.41108737E-02   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.94857367E+00   # Gamma(A0)
     6.25027181E-06   2        22        22   # BR(A0 -> photon photon)
     1.82135492E-06   2        22        23   # BR(A0 -> photon Z)
     1.83288940E-03   2        21        21   # BR(A0 -> gluon gluon)
     8.29413067E-10   2       -11        11   # BR(A0 -> Electron electron)
     3.69134953E-05   2       -13        13   # BR(A0 -> Muon muon)
     1.06587168E-02   2       -15        15   # BR(A0 -> Tau tau)
     6.40199881E-11   2        -2         2   # BR(A0 -> Up up)
     1.24116474E-05   2        -4         4   # BR(A0 -> Charm charm)
     9.09105962E-01   2        -6         6   # BR(A0 -> Top top)
     7.35029703E-08   2        -1         1   # BR(A0 -> Down down)
     2.65862853E-05   2        -3         3   # BR(A0 -> Strange strange)
     7.25539692E-02   2        -5         5   # BR(A0 -> Bottom bottom)
     1.38717850E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.62568745E-03   2        23        25   # BR(A0 -> Z h0)
     9.41713400E-35   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.27057393E+00   # Gamma(Hp)
     8.14602894E-10   2       -11        12   # BR(Hp -> Electron nu_e)
     3.48268166E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     9.85089854E-03   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.26943757E-08   2        -1         2   # BR(Hp -> Down up)
     1.03817331E-06   2        -3         2   # BR(Hp -> Strange up)
     6.84429060E-07   2        -5         2   # BR(Hp -> Bottom up)
     6.03673556E-07   2        -1         4   # BR(Hp -> Down charm)
     3.56473267E-05   2        -3         4   # BR(Hp -> Strange charm)
     9.58639094E-05   2        -5         4   # BR(Hp -> Bottom charm)
     5.91634175E-05   2        -1         6   # BR(Hp -> Down top)
     1.28998731E-03   2        -3         6   # BR(Hp -> Strange top)
     9.83635141E-01   2        -5         6   # BR(Hp -> Bottom top)
     4.99607933E-03   2        24        25   # BR(Hp -> W h0)
     4.40676902E-10   2        24        35   # BR(Hp -> W HH)
     2.36736870E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.69715290E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.34962713E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.34659865E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00224898E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.20121981E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.42611762E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.69715290E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.34962713E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.34659865E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99982729E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.72706697E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99982729E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.72706697E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27030956E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    7.03150315E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    2.78710216E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.72706697E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99982729E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.75408620E-04   # BR(b -> s gamma)
    2    1.59549527E-06   # BR(b -> s mu+ mu-)
    3    3.54258548E-05   # BR(b -> s nu nu)
    4    2.55729180E-15   # BR(Bd -> e+ e-)
    5    1.09244603E-10   # BR(Bd -> mu+ mu-)
    6    2.28692712E-08   # BR(Bd -> tau+ tau-)
    7    8.62499424E-14   # BR(Bs -> e+ e-)
    8    3.68459478E-09   # BR(Bs -> mu+ mu-)
    9    7.81533835E-07   # BR(Bs -> tau+ tau-)
   10    9.66419896E-05   # BR(B_u -> tau nu)
   11    9.98274754E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.48069426E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.95822018E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.18203594E-03   # epsilon_K
   17    2.28191635E-15   # Delta(M_K)
   18    2.49223584E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.31954892E-11   # BR(K^+ -> pi^+ nu nu)
   20   -4.62918863E-17   # Delta(g-2)_electron/2
   21   -1.97912294E-12   # Delta(g-2)_muon/2
   22   -5.59721513E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -5.24094404E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.43261543E-01   # C7
     0305 4322   00   2    -1.07340634E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.57079855E-01   # C8
     0305 6321   00   2    -1.21457811E-03   # C8'
 03051111 4133   00   0     1.62590412E+00   # C9 e+e-
 03051111 4133   00   2     1.62719587E+00   # C9 e+e-
 03051111 4233   00   2     1.33873983E-06   # C9' e+e-
 03051111 4137   00   0    -4.44859623E+00   # C10 e+e-
 03051111 4137   00   2    -4.46399597E+00   # C10 e+e-
 03051111 4237   00   2    -9.56165478E-06   # C10' e+e-
 03051313 4133   00   0     1.62590412E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62719587E+00   # C9 mu+mu-
 03051313 4233   00   2     1.33873638E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.44859623E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.46399598E+00   # C10 mu+mu-
 03051313 4237   00   2    -9.56165137E-06   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50868231E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.06740836E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50868231E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.06740911E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50868231E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.06761884E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85826992E-07   # C7
     0305 4422   00   2    -6.82518411E-07   # C7
     0305 4322   00   2     4.77521909E-08   # C7'
     0305 6421   00   0     3.30486335E-07   # C8
     0305 6421   00   2     1.04675809E-06   # C8
     0305 6321   00   2     4.04147363E-08   # C8'
 03051111 4133   00   2     3.23617760E-07   # C9 e+e-
 03051111 4233   00   2     7.79897378E-08   # C9' e+e-
 03051111 4137   00   2     1.52020087E-06   # C10 e+e-
 03051111 4237   00   2    -5.71672949E-07   # C10' e+e-
 03051313 4133   00   2     3.23617726E-07   # C9 mu+mu-
 03051313 4233   00   2     7.79897376E-08   # C9' mu+mu-
 03051313 4137   00   2     1.52020091E-06   # C10 mu+mu-
 03051313 4237   00   2    -5.71672950E-07   # C10' mu+mu-
 03051212 4137   00   2    -3.00158575E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.23629276E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -3.00158573E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.23629276E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -3.00158090E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.23629276E-07   # C11' nu_3 nu_3
