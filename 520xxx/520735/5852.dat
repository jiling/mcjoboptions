# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 12.05.2022,  15:46
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.81521230E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.49611336E+03  # scale for input parameters
    1   -3.85978360E+01  # M_1
    2   -1.57270811E+03  # M_2
    3    4.62631025E+03  # M_3
   11   -2.52310582E+02  # A_t
   12   -1.56623240E+03  # A_b
   13    1.63606776E+03  # A_tau
   23    2.53142019E+02  # mu
   25    2.71431535E+01  # tan(beta)
   26    1.04324714E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.35025170E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.59411350E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.28935307E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.49611336E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.49611336E+03  # (SUSY scale)
  1  1     8.39006030E-06   # Y_u(Q)^DRbar
  2  2     4.26215063E-03   # Y_c(Q)^DRbar
  3  3     1.01358668E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.49611336E+03  # (SUSY scale)
  1  1     4.57620670E-04   # Y_d(Q)^DRbar
  2  2     8.69479273E-03   # Y_s(Q)^DRbar
  3  3     4.53816307E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.49611336E+03  # (SUSY scale)
  1  1     7.98581533E-05   # Y_e(Q)^DRbar
  2  2     1.65121333E-02   # Y_mu(Q)^DRbar
  3  3     2.77706919E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.49611336E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -2.52310584E+02   # A_t(Q)^DRbar
Block Ad Q=  2.49611336E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.56623240E+03   # A_b(Q)^DRbar
Block Ae Q=  2.49611336E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.63606776E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.49611336E+03  # soft SUSY breaking masses at Q
   1   -3.85978360E+01  # M_1
   2   -1.57270811E+03  # M_2
   3    4.62631025E+03  # M_3
  21    8.36479737E+05  # M^2_(H,d)
  22    3.05246481E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.35025170E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.59411350E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.28935307E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.18872144E+02  # h0
        35     1.04325459E+03  # H0
        36     1.04324714E+03  # A0
        37     1.04848719E+03  # H+
   1000001     1.00591627E+04  # ~d_L
   2000001     1.00382750E+04  # ~d_R
   1000002     1.00589713E+04  # ~u_L
   2000002     1.00414941E+04  # ~u_R
   1000003     1.00591685E+04  # ~s_L
   2000003     1.00382839E+04  # ~s_R
   1000004     1.00589771E+04  # ~c_L
   2000004     1.00414963E+04  # ~c_R
   1000005     2.32104074E+03  # ~b_1
   2000005     2.38256942E+03  # ~b_2
   1000006     2.38533326E+03  # ~t_1
   2000006     2.61203833E+03  # ~t_2
   1000011     1.00197247E+04  # ~e_L-
   2000011     1.00086378E+04  # ~e_R-
   1000012     1.00189599E+04  # ~nu_eL
   1000013     1.00197472E+04  # ~mu_L-
   2000013     1.00086814E+04  # ~mu_R-
   1000014     1.00189823E+04  # ~nu_muL
   1000015     1.00211287E+04  # ~tau_1-
   2000015     1.00261915E+04  # ~tau_2-
   1000016     1.00253796E+04  # ~nu_tauL
   1000021     4.98421132E+03  # ~g
   1000022     3.84692652E+01  # ~chi_10
   1000023     2.60343520E+02  # ~chi_20
   1000025     2.63516767E+02  # ~chi_30
   1000035     1.66838967E+03  # ~chi_40
   1000024     2.57789269E+02  # ~chi_1+
   1000037     1.66835410E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.61917547E-02   # alpha
Block Hmix Q=  2.49611336E+03  # Higgs mixing parameters
   1    2.53142019E+02  # mu
   2    2.71431535E+01  # tan[beta](Q)
   3    2.43458765E+02  # v(Q)
   4    1.08836460E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99610154E-01   # Re[R_st(1,1)]
   1  2     2.79202479E-02   # Re[R_st(1,2)]
   2  1    -2.79202479E-02   # Re[R_st(2,1)]
   2  2     9.99610154E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     6.96325297E-02   # Re[R_sb(1,1)]
   1  2     9.97572710E-01   # Re[R_sb(1,2)]
   2  1    -9.97572710E-01   # Re[R_sb(2,1)]
   2  2     6.96325297E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.26490783E-02   # Re[R_sta(1,1)]
   1  2     9.95698824E-01   # Re[R_sta(1,2)]
   2  1    -9.95698824E-01   # Re[R_sta(2,1)]
   2  2     9.26490783E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.84677994E-01   # Re[N(1,1)]
   1  2     6.07393110E-04   # Re[N(1,2)]
   1  3    -1.73251108E-01   # Re[N(1,3)]
   1  4    -1.98225275E-02   # Re[N(1,4)]
   2  1     1.36782109E-01   # Re[N(2,1)]
   2  2     3.88117200E-02   # Re[N(2,2)]
   2  3    -6.96835905E-01   # Re[N(2,3)]
   2  4    -7.02996462E-01   # Re[N(2,4)]
   3  1    -1.08157170E-01   # Re[N(3,1)]
   3  2     3.07284459E-02   # Re[N(3,2)]
   3  3     6.95967285E-01   # Re[N(3,3)]
   3  4    -7.09215995E-01   # Re[N(3,4)]
   4  1     1.38886363E-03   # Re[N(4,1)]
   4  2    -9.98773770E-01   # Re[N(4,2)]
   4  3    -5.77171598E-03   # Re[N(4,3)]
   4  4    -4.91499163E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -8.17456376E-03   # Re[U(1,1)]
   1  2     9.99966588E-01   # Re[U(1,2)]
   2  1    -9.99966588E-01   # Re[U(2,1)]
   2  2    -8.17456376E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     6.95663485E-02   # Re[V(1,1)]
   1  2     9.97577327E-01   # Re[V(1,2)]
   2  1     9.97577327E-01   # Re[V(2,1)]
   2  2    -6.95663485E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02862173E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.69630198E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.86853602E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.16825933E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.37666547E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.84390419E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.00476345E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.34107668E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01645506E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.05633823E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03948681E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.67576234E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.91675439E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.21785248E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.07577546E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.37721164E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.84160125E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.19454583E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     4.24894537E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01526648E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.05395158E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     8.16815115E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     6.04851762E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.01353932E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     9.45733147E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     4.05091384E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.87037877E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     8.13220034E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.52503827E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.21013764E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     5.33048435E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.09724134E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.70329985E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.39022044E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.42752360E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.37670506E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.88391184E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.75470339E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.50582773E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02508747E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.09627384E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02674563E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.37725117E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.88040902E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.75322298E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.50483972E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02389548E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.48908721E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02437113E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.53129145E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.99218442E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.37782874E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.25430783E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.72164133E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.03095494E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.42226438E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.55433795E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.19308567E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.29960949E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     1.43794363E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.87695274E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.79019121E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.36728563E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.90850613E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.21759949E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.44625704E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.80613968E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.55464792E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.19310474E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.45975338E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.59774046E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.87630076E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.79038498E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.36798981E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.03447626E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.21736267E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.44620986E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.80588880E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.99195236E+01   # ~b_1
#    BR                NDA      ID1      ID2
     8.33975493E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.25605876E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.21935558E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.90823852E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.67572876E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     9.97317534E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     6.47378486E+01   # ~b_2
#    BR                NDA      ID1      ID2
     7.62795664E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     7.20264779E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     7.29700594E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     4.18256043E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     7.20818433E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.47314687E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
DECAY   2000002     4.72585653E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.60061083E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     8.86572630E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     5.54309133E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.52552923E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.79021310E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.33465532E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.04710986E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.21027406E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     7.39200758E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.43931817E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.80576832E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.72593101E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.60053946E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     8.90347801E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     5.58158234E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.52538328E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.79040653E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.33458105E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.07799943E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     7.21003864E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     7.65195716E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.43927103E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.80551738E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     6.49649501E+01   # ~t_1
#    BR                NDA      ID1      ID2
     5.02534191E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.59420496E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.62934231E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.92716611E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.47056436E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     8.43582413E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.89276160E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.09956991E+02   # ~t_2
#    BR                NDA      ID1      ID2
     5.12542874E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.33536068E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.38992361E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.19033522E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.74722882E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.47167620E-04    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.33002925E-04    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     1.50833272E-04    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.40919732E-04    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.63889367E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99948493E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.98831753E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.81164444E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.80423967E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     3.90784796E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     5.95197380E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     6.37540994E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.80290146E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     6.65730557E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.77708040E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     6.64499046E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.75087085E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     6.36589467E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     6.00052368E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.28592431E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     5.24834863E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     4.75133089E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.85215269E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.00935575E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.99055352E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     2.09290746E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.72589221E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.72589221E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.80255347E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     5.80255347E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     6.55360964E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.06065792E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.74099602E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.39555328E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     1.59695616E-02    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.04387438E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.20850260E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     2.01551884E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     4.76138514E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     4.11372925E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.25000588E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.37288049E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     6.38969850E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.38969850E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.44071288E+02   # ~g
#    BR                NDA      ID1      ID2
     9.35547793E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     9.35547793E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.26672629E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.26672629E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.13260002E-01    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.13260002E-01    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.31450688E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.31450688E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.27645464E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.27645464E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     2.85938505E-03   # Gamma(h0)
     2.58561410E-03   2        22        22   # BR(h0 -> photon photon)
     1.11688724E-03   2        22        23   # BR(h0 -> photon Z)
     1.66627622E-02   2        23        23   # BR(h0 -> Z Z)
     1.55566898E-01   2       -24        24   # BR(h0 -> W W)
     8.51633002E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.60668031E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.49391368E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.18988681E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.55245211E-07   2        -2         2   # BR(h0 -> Up up)
     3.01274865E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.54711915E-07   2        -1         1   # BR(h0 -> Down down)
     2.36793325E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.28556401E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     7.83478189E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.05218567E+01   # Gamma(HH)
     3.20060964E-08   2        22        22   # BR(HH -> photon photon)
     2.45737071E-08   2        22        23   # BR(HH -> photon Z)
     1.08484278E-05   2        23        23   # BR(HH -> Z Z)
     1.38788653E-05   2       -24        24   # BR(HH -> W W)
     4.60477327E-05   2        21        21   # BR(HH -> gluon gluon)
     1.03601747E-08   2       -11        11   # BR(HH -> Electron electron)
     4.61133950E-04   2       -13        13   # BR(HH -> Muon muon)
     1.33157649E-01   2       -15        15   # BR(HH -> Tau tau)
     3.17966912E-13   2        -2         2   # BR(HH -> Up up)
     6.16629045E-08   2        -4         4   # BR(HH -> Charm charm)
     3.71314976E-03   2        -6         6   # BR(HH -> Top top)
     8.62736839E-07   2        -1         1   # BR(HH -> Down down)
     3.12045744E-04   2        -3         3   # BR(HH -> Strange strange)
     7.56570992E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.95946438E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     6.58716196E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.97184013E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.16528793E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.68701282E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     2.55933534E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.89735834E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     6.99605747E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.03183951E+01   # Gamma(A0)
     6.75282593E-08   2        22        22   # BR(A0 -> photon photon)
     5.93629020E-08   2        22        23   # BR(A0 -> photon Z)
     9.98895111E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.03198785E-08   2       -11        11   # BR(A0 -> Electron electron)
     4.59340761E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.32641467E-01   2       -15        15   # BR(A0 -> Tau tau)
     3.06223915E-13   2        -2         2   # BR(A0 -> Up up)
     5.93679797E-08   2        -4         4   # BR(A0 -> Charm charm)
     4.04130013E-03   2        -6         6   # BR(A0 -> Top top)
     8.59383045E-07   2        -1         1   # BR(A0 -> Down down)
     3.10832635E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.53652640E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.87366127E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     6.64247272E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     4.54896359E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.76471313E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.11495707E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.84483124E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.96832155E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.59250551E-05   2        23        25   # BR(A0 -> Z h0)
     4.20983844E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.18044893E+01   # Gamma(Hp)
     1.16412307E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.97698962E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.40776860E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.40318086E-07   2        -1         2   # BR(Hp -> Down up)
     1.39985259E-05   2        -3         2   # BR(Hp -> Strange up)
     8.33554435E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.17030038E-08   2        -1         4   # BR(Hp -> Down charm)
     3.02899703E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.16727152E-03   2        -5         4   # BR(Hp -> Bottom charm)
     3.09865960E-07   2        -1         6   # BR(Hp -> Down top)
     7.17750950E-06   2        -3         6   # BR(Hp -> Strange top)
     7.64199715E-01   2        -5         6   # BR(Hp -> Bottom top)
     8.98885906E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.04617942E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     7.58535171E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.42151914E-05   2        24        25   # BR(Hp -> W h0)
     4.50322878E-10   2        24        35   # BR(Hp -> W HH)
     4.53534598E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.65915934E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    7.36784866E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    7.36750782E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00004626E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.31104840E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.35731108E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.65915934E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    7.36784866E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    7.36750782E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999599E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.01060377E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999599E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.01060377E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26424551E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    8.33333358E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.19465252E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.01060377E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999599E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.48678003E-04   # BR(b -> s gamma)
    2    1.58718258E-06   # BR(b -> s mu+ mu-)
    3    3.52402309E-05   # BR(b -> s nu nu)
    4    2.69887810E-15   # BR(Bd -> e+ e-)
    5    1.15292821E-10   # BR(Bd -> mu+ mu-)
    6    2.41244488E-08   # BR(Bd -> tau+ tau-)
    7    9.02009558E-14   # BR(Bs -> e+ e-)
    8    3.85337709E-09   # BR(Bs -> mu+ mu-)
    9    8.17046020E-07   # BR(Bs -> tau+ tau-)
   10    9.26880731E-05   # BR(B_u -> tau nu)
   11    9.57432309E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42172055E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93527452E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15832060E-03   # epsilon_K
   17    2.28167309E-15   # Delta(M_K)
   18    2.47918806E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28852999E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.30862135E-16   # Delta(g-2)_electron/2
   21   -1.41454249E-11   # Delta(g-2)_muon/2
   22   -4.00417502E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.84927835E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.16367355E-01   # C7
     0305 4322   00   2    -6.29907236E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.31956740E-01   # C8
     0305 6321   00   2    -7.59947106E-04   # C8'
 03051111 4133   00   0     1.61003285E+00   # C9 e+e-
 03051111 4133   00   2     1.61059636E+00   # C9 e+e-
 03051111 4233   00   2     7.21198457E-05   # C9' e+e-
 03051111 4137   00   0    -4.43272496E+00   # C10 e+e-
 03051111 4137   00   2    -4.42985977E+00   # C10 e+e-
 03051111 4237   00   2    -5.45542324E-04   # C10' e+e-
 03051313 4133   00   0     1.61003285E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61059631E+00   # C9 mu+mu-
 03051313 4233   00   2     7.21169426E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43272496E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.42985982E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.45539525E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50472919E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.18409188E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50472919E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.18409813E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50472919E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.18585812E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822221E-07   # C7
     0305 4422   00   2    -5.35673138E-06   # C7
     0305 4322   00   2     2.90421855E-07   # C7'
     0305 6421   00   0     3.30482248E-07   # C8
     0305 6421   00   2    -3.72802003E-06   # C8
     0305 6321   00   2     4.13650819E-08   # C8'
 03051111 4133   00   2     1.03984557E-07   # C9 e+e-
 03051111 4233   00   2     2.99806320E-06   # C9' e+e-
 03051111 4137   00   2     9.10662859E-07   # C10 e+e-
 03051111 4237   00   2    -2.27175494E-05   # C10' e+e-
 03051313 4133   00   2     1.03983734E-07   # C9 mu+mu-
 03051313 4233   00   2     2.99806274E-06   # C9' mu+mu-
 03051313 4137   00   2     9.10663807E-07   # C10 mu+mu-
 03051313 4237   00   2    -2.27175508E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.80617606E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     4.93083247E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.80617571E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     4.93083247E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.80607497E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     4.93083246E-06   # C11' nu_3 nu_3
