# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 20.12.2022,  11:55
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.49105133E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.80771351E+03  # scale for input parameters
    1   -2.38057660E+02  # M_1
    2   -1.99021837E+03  # M_2
    3    4.44722743E+03  # M_3
   11   -1.00782180E+03  # A_t
   12    9.62531287E+02  # A_b
   13   -1.39808213E+03  # A_tau
   23   -2.16159433E+02  # mu
   25    1.42556859E+01  # tan(beta)
   26    3.03885134E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.40345659E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.20816203E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.56737501E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.80771351E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.80771351E+03  # (SUSY scale)
  1  1     8.40497516E-06   # Y_u(Q)^DRbar
  2  2     4.26972738E-03   # Y_c(Q)^DRbar
  3  3     1.01538851E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.80771351E+03  # (SUSY scale)
  1  1     2.40771345E-04   # Y_d(Q)^DRbar
  2  2     4.57465556E-03   # Y_s(Q)^DRbar
  3  3     2.38769728E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.80771351E+03  # (SUSY scale)
  1  1     4.20163604E-05   # Y_e(Q)^DRbar
  2  2     8.68765076E-03   # Y_mu(Q)^DRbar
  3  3     1.46111994E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.80771351E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.00782188E+03   # A_t(Q)^DRbar
Block Ad Q=  2.80771351E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     9.62531288E+02   # A_b(Q)^DRbar
Block Ae Q=  2.80771351E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.39808209E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.80771351E+03  # soft SUSY breaking masses at Q
   1   -2.38057660E+02  # M_1
   2   -1.99021837E+03  # M_2
   3    4.44722743E+03  # M_3
  21    9.06040378E+06  # M^2_(H,d)
  22    1.04217795E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.40345659E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.20816203E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.56737501E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.18772844E+02  # h0
        35     3.03869461E+03  # H0
        36     3.03885134E+03  # A0
        37     3.04171893E+03  # H+
   1000001     1.00707089E+04  # ~d_L
   2000001     1.00500511E+04  # ~d_R
   1000002     1.00704766E+04  # ~u_L
   2000002     1.00530865E+04  # ~u_R
   1000003     1.00707110E+04  # ~s_L
   2000003     1.00500531E+04  # ~s_R
   1000004     1.00704787E+04  # ~c_L
   2000004     1.00530883E+04  # ~c_R
   1000005     2.65420214E+03  # ~b_1
   2000005     3.49131763E+03  # ~b_2
   1000006     2.25630724E+03  # ~t_1
   2000006     3.49387486E+03  # ~t_2
   1000011     1.00189336E+04  # ~e_L-
   2000011     1.00088294E+04  # ~e_R-
   1000012     1.00181743E+04  # ~nu_eL
   1000013     1.00189389E+04  # ~mu_L-
   2000013     1.00088396E+04  # ~mu_R-
   1000014     1.00181795E+04  # ~nu_muL
   1000015     1.00117497E+04  # ~tau_1-
   2000015     1.00204331E+04  # ~tau_2-
   1000016     1.00196690E+04  # ~nu_tauL
   1000021     4.83642855E+03  # ~g
   1000022     1.96805006E+02  # ~chi_10
   1000023     2.26741719E+02  # ~chi_20
   1000025     2.67358954E+02  # ~chi_30
   1000035     2.10799856E+03  # ~chi_40
   1000024     2.23222763E+02  # ~chi_1+
   1000037     2.10796712E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -6.71309977E-02   # alpha
Block Hmix Q=  2.80771351E+03  # Higgs mixing parameters
   1   -2.16159433E+02  # mu
   2    1.42556859E+01  # tan[beta](Q)
   3    2.43340098E+02  # v(Q)
   4    9.23461747E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     1.99928871E-02   # Re[R_st(1,1)]
   1  2     9.99800122E-01   # Re[R_st(1,2)]
   2  1    -9.99800122E-01   # Re[R_st(2,1)]
   2  2     1.99928871E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.82818499E-03   # Re[R_sb(1,1)]
   1  2     9.99998329E-01   # Re[R_sb(1,2)]
   2  1    -9.99998329E-01   # Re[R_sb(2,1)]
   2  2    -1.82818499E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.68863354E-02   # Re[R_sta(1,1)]
   1  2     9.99857416E-01   # Re[R_sta(1,2)]
   2  1    -9.99857416E-01   # Re[R_sta(2,1)]
   2  2    -1.68863354E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     5.94054152E-01   # Re[N(1,1)]
   1  2    -2.40926138E-02   # Re[N(1,2)]
   1  3    -5.99163754E-01   # Re[N(1,3)]
   1  4     5.36210786E-01   # Re[N(1,4)]
   2  1    -6.35948990E-02   # Re[N(2,1)]
   2  2     2.26949611E-02   # Re[N(2,2)]
   2  3    -7.00112249E-01   # Re[N(2,3)]
   2  4    -7.10832938E-01   # Re[N(2,4)]
   3  1     8.01906710E-01   # Re[N(3,1)]
   3  2     2.08825363E-02   # Re[N(3,2)]
   3  3     3.88331390E-01   # Re[N(3,3)]
   3  4    -4.53550747E-01   # Re[N(3,4)]
   4  1     9.91004144E-04   # Re[N(4,1)]
   4  2    -9.99233909E-01   # Re[N(4,2)]
   4  3     6.66084782E-03   # Re[N(4,3)]
   4  4    -3.85518694E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.42089114E-03   # Re[U(1,1)]
   1  2    -9.99955622E-01   # Re[U(1,2)]
   2  1    -9.99955622E-01   # Re[U(2,1)]
   2  2     9.42089114E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     5.45390282E-02   # Re[V(1,1)]
   1  2     9.98511640E-01   # Re[V(1,2)]
   2  1     9.98511640E-01   # Re[V(2,1)]
   2  2    -5.45390282E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02306429E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     3.53049193E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     4.04499596E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     6.42904906E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.33422419E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     2.84037919E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     6.66203256E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01121245E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.03754023E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02607206E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     3.52945542E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     4.18898817E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     6.42565847E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.98688964E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.33437529E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.84410308E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     6.66297946E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01087338E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.03686031E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.87644767E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.29365596E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     3.85769686E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.59240570E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.50646829E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     7.23655159E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.00703473E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.37674025E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.79614948E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.53108626E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     6.98076390E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.91812232E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.85086898E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.33425380E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     3.85007085E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.06021580E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.48045362E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.01726278E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.96411684E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01944145E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.33440489E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     3.84963694E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.06009631E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.47983597E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01692302E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.07649961E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01876373E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.37699918E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     3.73111281E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.02745773E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.31112166E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.92411669E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.27743837E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.83364145E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.74006334E+02   # ~d_R
#    BR                NDA      ID1      ID2
     4.17412980E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.60114630E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.88176853E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.93516305E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.25884429E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.10850986E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.82028185E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.36554887E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.92775340E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.74014878E+02   # ~s_R
#    BR                NDA      ID1      ID2
     4.17721334E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     7.60233616E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.88159475E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.93524375E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.26135463E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.10955579E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.82019081E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.36553080E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.92764923E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     7.36659867E+00   # ~b_1
#    BR                NDA      ID1      ID2
     2.15949247E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.98070432E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.86607086E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.99371708E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     9.59032282E+01   # ~b_2
#    BR                NDA      ID1      ID2
     1.73559441E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.02631114E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.55264078E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.46253583E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     7.34714572E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.30720340E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.36753299E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     4.91137776E+02   # ~u_R
#    BR                NDA      ID1      ID2
     1.61189662E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.84680350E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.93529125E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.54343400E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.93513087E+02   # ~u_L
#    BR                NDA      ID1      ID2
     4.98032618E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.99805078E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     6.81531643E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     4.43874219E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.36157810E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.92739422E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.91145217E+02   # ~c_R
#    BR                NDA      ID1      ID2
     1.61208591E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.88425150E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     2.93539937E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.54329256E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.93521125E+02   # ~c_L
#    BR                NDA      ID1      ID2
     4.99793633E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.99928892E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     6.81522608E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     4.50905240E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.36156000E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.92728999E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     9.46452863E+01   # ~t_1
#    BR                NDA      ID1      ID2
     1.54754808E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.39128794E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.28914336E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.77165879E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     9.65836778E+01   # ~t_2
#    BR                NDA      ID1      ID2
     2.14366213E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     3.70618778E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.52242015E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     6.27038431E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.29538628E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.27946861E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.17904408E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.73031271E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     3.24796091E-05   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.35326854E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.31692142E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11775775E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11769961E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.09435268E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.95261173E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.32112917E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.42945482E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.07608846E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.40843261E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.41844432E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.46400286E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     5.34361991E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.03827924E-02    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     4.23037866E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.83472774E-05   # chi^0_2
#    BR                NDA      ID1      ID2
     3.14662784E-02    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.18937739E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.16212829E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.52501686E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.52484669E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.22865301E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.44151119E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.44090138E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.27445422E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.03939311E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     6.77105080E-04   # chi^0_3
#    BR                NDA      ID1      ID2
     2.51489046E-04    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
     6.98132822E-03    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     1.73634974E-04    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     1.73277433E-04    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     2.22928566E-04    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     2.22926670E-04    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     2.17832942E-04    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     2.97723188E-04    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
     2.30004758E-02    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     2.27255025E-02    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     2.94913170E-02    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     2.94896205E-02    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     2.63944657E-02    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     6.65541853E-03    3     1000023        11       -11   # BR(chi^0_3 -> chi^0_2 e^- e^+)
     6.65480813E-03    3     1000023        13       -13   # BR(chi^0_3 -> chi^0_2 mu^- mu^+)
     6.48570858E-03    3     1000023        15       -15   # BR(chi^0_3 -> chi^0_2 tau^- tau^+)
     3.94381317E-02    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     1.33746398E-01    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     1.33746398E-01    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     1.33288146E-01    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     1.33288146E-01    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     4.45820248E-02    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     4.45820248E-02    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     4.45812849E-02    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     4.45812849E-02    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     4.42880864E-02    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     4.42880864E-02    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     2.03842223E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.31666420E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.31666420E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.24178667E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.55434330E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     3.31016133E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     8.46103053E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.19465025E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     6.90728906E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.54243441E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     6.21825437E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.79040542E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     9.97380116E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     9.97380116E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.22717372E+02   # ~g
#    BR                NDA      ID1      ID2
     1.95817909E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.95817909E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     7.34732869E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     7.34732869E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.56954296E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.56954296E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     7.37050498E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     7.37050498E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.22350599E-03   # Gamma(h0)
     2.29240243E-03   2        22        22   # BR(h0 -> photon photon)
     9.81787417E-04   2        22        23   # BR(h0 -> photon Z)
     1.45758066E-02   2        23        23   # BR(h0 -> Z Z)
     1.36340130E-01   2       -24        24   # BR(h0 -> W W)
     7.54216639E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.88492628E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.61767999E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.54671951E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.68529156E-07   2        -2         2   # BR(h0 -> Up up)
     3.26994564E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.87515995E-07   2        -1         1   # BR(h0 -> Down down)
     2.48658148E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.61710270E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     2.50312553E+01   # Gamma(HH)
     5.99548112E-08   2        22        22   # BR(HH -> photon photon)
     5.02581123E-08   2        22        23   # BR(HH -> photon Z)
     6.02018571E-06   2        23        23   # BR(HH -> Z Z)
     1.78740426E-06   2       -24        24   # BR(HH -> W W)
     1.56192555E-06   2        21        21   # BR(HH -> gluon gluon)
     4.23404287E-09   2       -11        11   # BR(HH -> Electron electron)
     1.88519502E-04   2       -13        13   # BR(HH -> Muon muon)
     5.44468618E-02   2       -15        15   # BR(HH -> Tau tau)
     1.45318184E-12   2        -2         2   # BR(HH -> Up up)
     2.81909122E-07   2        -4         4   # BR(HH -> Charm charm)
     2.16022772E-02   2        -6         6   # BR(HH -> Top top)
     3.12429749E-07   2        -1         1   # BR(HH -> Down down)
     1.13007040E-04   2        -3         3   # BR(HH -> Strange strange)
     3.02857139E-01   2        -5         5   # BR(HH -> Bottom bottom)
     3.15106326E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.59129504E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.59129504E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     3.42875073E-02   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.17606995E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     8.61398010E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     4.42827626E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.77304739E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     5.45935799E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.01375487E-01   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.88114382E-02   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.38425855E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     3.09574899E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     2.47522154E+01   # Gamma(A0)
     1.46969173E-07   2        22        22   # BR(A0 -> photon photon)
     1.23290647E-07   2        22        23   # BR(A0 -> photon Z)
     2.14556171E-05   2        21        21   # BR(A0 -> gluon gluon)
     4.16266890E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.85341676E-04   2       -13        13   # BR(A0 -> Muon muon)
     5.35291453E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.36492407E-12   2        -2         2   # BR(A0 -> Up up)
     2.64779923E-07   2        -4         4   # BR(A0 -> Charm charm)
     2.05724647E-02   2        -6         6   # BR(A0 -> Top top)
     3.07159608E-07   2        -1         1   # BR(A0 -> Down down)
     1.11101353E-04   2        -3         3   # BR(A0 -> Strange strange)
     2.97750226E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.40763202E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.60838845E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.60838845E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     4.49257464E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.49478403E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.06560525E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     7.63703884E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.39330878E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     4.26030795E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     5.56689965E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.69485670E-02   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     2.92201221E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     9.99560590E-06   2        23        25   # BR(A0 -> Z h0)
     5.95595704E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.49398014E+01   # Gamma(Hp)
     4.48406515E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.91707790E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     5.42258653E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     2.87484222E-07   2        -1         2   # BR(Hp -> Down up)
     4.85341677E-06   2        -3         2   # BR(Hp -> Strange up)
     3.17553825E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.63075046E-08   2        -1         4   # BR(Hp -> Down charm)
     1.03880665E-04   2        -3         4   # BR(Hp -> Strange charm)
     4.44689401E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.40935236E-06   2        -1         6   # BR(Hp -> Down top)
     3.08819926E-05   2        -3         6   # BR(Hp -> Strange top)
     3.21444554E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.90453017E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.20952029E-01   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.02704944E-05   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.57534787E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.02396760E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     4.31684523E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.60431109E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     9.95468421E-06   2        24        25   # BR(Hp -> W h0)
     1.38080604E-11   2        24        35   # BR(Hp -> W HH)
     1.05822977E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.18970828E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.03305610E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.03224580E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00039872E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    4.52194722E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    4.92066460E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.18970828E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.03305610E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.03224580E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99991580E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.42002773E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99991580E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.42002773E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26846630E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.79608480E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.02335448E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.42002773E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99991580E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.28968576E-04   # BR(b -> s gamma)
    2    1.58890687E-06   # BR(b -> s mu+ mu-)
    3    3.52437030E-05   # BR(b -> s nu nu)
    4    2.45965185E-15   # BR(Bd -> e+ e-)
    5    1.05073551E-10   # BR(Bd -> mu+ mu-)
    6    2.19970765E-08   # BR(Bd -> tau+ tau-)
    7    8.30151056E-14   # BR(Bs -> e+ e-)
    8    3.54640320E-09   # BR(Bs -> mu+ mu-)
    9    7.52254791E-07   # BR(Bs -> tau+ tau-)
   10    9.66636355E-05   # BR(B_u -> tau nu)
   11    9.98498347E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42296277E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93759513E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15860883E-03   # epsilon_K
   17    2.28167134E-15   # Delta(M_K)
   18    2.47942321E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28907497E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.45202075E-16   # Delta(g-2)_electron/2
   21    6.20784606E-12   # Delta(g-2)_muon/2
   22    1.75618710E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    2.39295829E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.98251877E-01   # C7
     0305 4322   00   2    -1.44132962E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.04758806E-01   # C8
     0305 6321   00   2    -1.69340923E-04   # C8'
 03051111 4133   00   0     1.61390420E+00   # C9 e+e-
 03051111 4133   00   2     1.61446116E+00   # C9 e+e-
 03051111 4233   00   2     3.88361687E-05   # C9' e+e-
 03051111 4137   00   0    -4.43659630E+00   # C10 e+e-
 03051111 4137   00   2    -4.43406120E+00   # C10 e+e-
 03051111 4237   00   2    -2.92178447E-04   # C10' e+e-
 03051313 4133   00   0     1.61390420E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61446114E+00   # C9 mu+mu-
 03051313 4233   00   2     3.88361613E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43659630E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43406122E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.92178447E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50480111E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     6.33612140E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50480111E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     6.33612152E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50480111E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     6.33615536E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85821133E-07   # C7
     0305 4422   00   2     2.26022490E-06   # C7
     0305 4322   00   2    -2.86928343E-07   # C7'
     0305 6421   00   0     3.30481316E-07   # C8
     0305 6421   00   2     2.28686877E-06   # C8
     0305 6321   00   2    -7.05774770E-08   # C8'
 03051111 4133   00   2    -2.75441133E-08   # C9 e+e-
 03051111 4233   00   2     7.91486340E-07   # C9' e+e-
 03051111 4137   00   2     1.28773849E-06   # C10 e+e-
 03051111 4237   00   2    -5.95663048E-06   # C10' e+e-
 03051313 4133   00   2    -2.75442957E-08   # C9 mu+mu-
 03051313 4233   00   2     7.91486307E-07   # C9' mu+mu-
 03051313 4137   00   2     1.28773871E-06   # C10 mu+mu-
 03051313 4237   00   2    -5.95663057E-06   # C10' mu+mu-
 03051212 4137   00   2    -2.65849484E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.29174468E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.65849477E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.29174468E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.65847411E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.29174468E-06   # C11' nu_3 nu_3
