# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  18:19
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.86828000E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.66382921E+03  # scale for input parameters
    1   -4.50539441E+01  # M_1
    2    1.96502607E+03  # M_2
    3    2.86626528E+03  # M_3
   11    6.70516136E+03  # A_t
   12    1.85600979E+03  # A_b
   13    5.44637614E+02  # A_tau
   23    2.06730985E+02  # mu
   25    5.84938281E+01  # tan(beta)
   26    2.38882299E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.36771719E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.08263500E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.30971415E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.66382921E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.66382921E+03  # (SUSY scale)
  1  1     8.38559728E-06   # Y_u(Q)^DRbar
  2  2     4.25988342E-03   # Y_c(Q)^DRbar
  3  3     1.01304751E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.66382921E+03  # (SUSY scale)
  1  1     9.85653557E-04   # Y_d(Q)^DRbar
  2  2     1.87274176E-02   # Y_s(Q)^DRbar
  3  3     9.77459470E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.66382921E+03  # (SUSY scale)
  1  1     1.72003753E-04   # Y_e(Q)^DRbar
  2  2     3.55649210E-02   # Y_mu(Q)^DRbar
  3  3     5.98143463E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.66382921E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     6.70515782E+03   # A_t(Q)^DRbar
Block Ad Q=  3.66382921E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.85601033E+03   # A_b(Q)^DRbar
Block Ae Q=  3.66382921E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     5.44637797E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.66382921E+03  # soft SUSY breaking masses at Q
   1   -4.50539441E+01  # M_1
   2    1.96502607E+03  # M_2
   3    2.86626528E+03  # M_3
  21    5.58750143E+06  # M^2_(H,d)
  22    1.45557255E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.36771719E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.08263500E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.30971415E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28300772E+02  # h0
        35     2.38828558E+03  # H0
        36     2.38882299E+03  # A0
        37     2.39572194E+03  # H+
   1000001     1.01082006E+04  # ~d_L
   2000001     1.00849299E+04  # ~d_R
   1000002     1.01078420E+04  # ~u_L
   2000002     1.00879800E+04  # ~u_R
   1000003     1.01082082E+04  # ~s_L
   2000003     1.00849439E+04  # ~s_R
   1000004     1.01078496E+04  # ~c_L
   2000004     1.00879808E+04  # ~c_R
   1000005     3.36732388E+03  # ~b_1
   2000005     4.35182992E+03  # ~b_2
   1000006     3.07669177E+03  # ~t_1
   2000006     4.36301244E+03  # ~t_2
   1000011     1.00198415E+04  # ~e_L-
   2000011     1.00090457E+04  # ~e_R-
   1000012     1.00190827E+04  # ~nu_eL
   1000013     1.00198793E+04  # ~mu_L-
   2000013     1.00091183E+04  # ~mu_R-
   1000014     1.00191201E+04  # ~nu_muL
   1000015     1.00291126E+04  # ~tau_1-
   2000015     1.00312620E+04  # ~tau_2-
   1000016     1.00297409E+04  # ~nu_tauL
   1000021     3.33307086E+03  # ~g
   1000022     4.38183860E+01  # ~chi_10
   1000023     2.22677532E+02  # ~chi_20
   1000025     2.27020774E+02  # ~chi_30
   1000035     2.08219390E+03  # ~chi_40
   1000024     2.20127400E+02  # ~chi_1+
   1000037     2.08216223E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.71172901E-02   # alpha
Block Hmix Q=  3.66382921E+03  # Higgs mixing parameters
   1    2.06730985E+02  # mu
   2    5.84938281E+01  # tan[beta](Q)
   3    2.43001875E+02  # v(Q)
   4    5.70647528E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.18357931E-02   # Re[R_st(1,1)]
   1  2     9.95774165E-01   # Re[R_st(1,2)]
   2  1    -9.95774165E-01   # Re[R_st(2,1)]
   2  2    -9.18357931E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     2.62341408E-03   # Re[R_sb(1,1)]
   1  2     9.99996559E-01   # Re[R_sb(1,2)]
   2  1    -9.99996559E-01   # Re[R_sb(2,1)]
   2  2     2.62341408E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     5.94668342E-01   # Re[R_sta(1,1)]
   1  2     8.03971121E-01   # Re[R_sta(1,2)]
   2  1    -8.03971121E-01   # Re[R_sta(2,1)]
   2  2     5.94668342E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.77802215E-01   # Re[N(1,1)]
   1  2    -1.34432889E-03   # Re[N(1,2)]
   1  3    -2.06106561E-01   # Re[N(1,3)]
   1  4    -3.76975764E-02   # Re[N(1,4)]
   2  1    -1.19931144E-01   # Re[N(2,1)]
   2  2    -3.06312757E-02   # Re[N(2,2)]
   2  3     6.98150099E-01   # Re[N(2,3)]
   2  4    -7.05169969E-01   # Re[N(2,4)]
   3  1     1.71809844E-01   # Re[N(3,1)]
   3  2    -2.40737669E-02   # Re[N(3,2)]
   3  3    -6.85627676E-01   # Re[N(3,3)]
   3  4    -7.06977031E-01   # Re[N(3,4)]
   4  1     8.52669703E-04   # Re[N(4,1)]
   4  2    -9.99239897E-01   # Re[N(4,2)]
   4  3    -4.60601337E-03   # Re[N(4,3)]
   4  4     3.86999499E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.51449601E-03   # Re[U(1,1)]
   1  2     9.99978780E-01   # Re[U(1,2)]
   2  1     9.99978780E-01   # Re[U(2,1)]
   2  2     6.51449601E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -5.47345703E-02   # Re[V(1,1)]
   1  2     9.98500940E-01   # Re[V(1,2)]
   2  1     9.98500940E-01   # Re[V(2,1)]
   2  2     5.47345703E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02877212E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.56138047E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.43704665E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.94907044E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.33722426E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.04521784E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.95489924E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.51054281E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01203705E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.03850708E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.07913183E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.46875418E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.66426498E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     3.15271495E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.95383488E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.33974900E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.03620047E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.86571119E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.39145892E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00637467E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.02715444E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.97387491E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.24018390E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.72834017E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.53478698E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     7.10991498E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.36031446E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.42538299E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     2.00860014E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.10205879E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.81873224E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.91409360E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.30922478E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.23130622E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.62458437E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.33725569E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.95169314E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.68365505E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     4.42205007E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.01719366E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.97406022E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01999227E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.33978017E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.93485925E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.67672787E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     4.41373432E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01152178E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.85019060E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.00867632E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     2.05210889E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     5.83957212E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.40301052E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.88469836E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.96862172E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.48818151E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     3.92798957E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.37262319E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.44700854E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.27106049E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     2.60682475E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.91164891E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.57607243E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.75302472E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.75456608E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.37828175E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.07692331E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.36586218E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.37403840E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.44982106E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.80684253E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     3.12323518E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     1.10283966E-04    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     9.90946875E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.57681971E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.75679619E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.19031623E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.37775625E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.07681818E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.36504643E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.28562758E+02   # ~b_1
#    BR                NDA      ID1      ID2
     3.52511230E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.40721340E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.32239977E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.90940488E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     8.40732364E-04    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     3.22703006E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.25884982E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.24304282E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.20069580E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.52534121E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.72230224E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     7.12741659E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.81906325E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.82253087E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     6.54443140E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.29109118E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.94646472E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.01510360E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.65579315E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.57592466E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.80796841E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.58565201E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.37489410E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.51791599E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.07371865E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.36558374E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.54450460E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.29105530E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.97405910E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.01787019E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.65568582E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.57667161E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.80779829E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.60942042E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.37436963E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     4.44489133E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.07361379E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.36476796E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.30221926E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.12048424E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.38105176E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.35222227E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     9.26937894E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.72644525E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.86490861E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     3.46761430E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.84735502E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.26186001E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.28985947E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.25080499E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.40071564E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     6.43073628E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.73582659E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     8.59067855E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.46531487E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.40291739E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99999718E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.87603433E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.72465162E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.41082391E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.37771862E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.39652171E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.43311335E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.42733107E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.08715283E-03    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     1.08241275E-04    3     1000024        15       -15   # BR(chi^+_2 -> chi^+_1 tau^- tau^+)
     1.12442715E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.05289201E-02    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.02355549E-02    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.49634717E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     7.02957499E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     2.97033472E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     9.34065647E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     5.41392622E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.58575108E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.96729615E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.29257984E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.29257984E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     9.04373241E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.75172600E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.40447311E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     7.11814313E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.44472602E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.34000526E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.39924562E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     5.01535752E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     6.20905332E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     4.93982712E-04    3     1000025         5        -5   # BR(chi^0_4 -> chi^0_3 b b_bar)
     9.75925956E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     9.75925956E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.57564803E+00   # ~g
#    BR                NDA      ID1      ID2
     2.92583847E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     2.92583847E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     3.54199790E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     3.54199790E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.57779661E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.56213781E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.62009916E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     8.25025702E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     5.58368042E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     6.27415266E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     5.72758572E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     6.05187496E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     2.41506316E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     3.17232491E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     6.88411342E-02    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     6.88411342E-02    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     5.57596093E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     5.57596093E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.24986074E-03   # Gamma(h0)
     2.39279555E-03   2        22        22   # BR(h0 -> photon photon)
     1.81638169E-03   2        22        23   # BR(h0 -> photon Z)
     3.76888476E-02   2        23        23   # BR(h0 -> Z Z)
     2.92564733E-01   2       -24        24   # BR(h0 -> W W)
     7.14361795E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.31187416E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.91801391E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.53066819E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.21331437E-07   2        -2         2   # BR(h0 -> Up up)
     2.35491078E-02   2        -4         4   # BR(h0 -> Charm charm)
     4.96757891E-07   2        -1         1   # BR(h0 -> Down down)
     1.79665788E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.77553561E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     3.73196220E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     9.43435062E+01   # Gamma(HH)
     7.19742423E-09   2        22        22   # BR(HH -> photon photon)
     1.26364686E-08   2        22        23   # BR(HH -> photon Z)
     2.00348031E-07   2        23        23   # BR(HH -> Z Z)
     8.25381807E-08   2       -24        24   # BR(HH -> W W)
     1.59586315E-05   2        21        21   # BR(HH -> gluon gluon)
     1.20349443E-08   2       -11        11   # BR(HH -> Electron electron)
     5.35815040E-04   2       -13        13   # BR(HH -> Muon muon)
     1.54744270E-01   2       -15        15   # BR(HH -> Tau tau)
     1.55874486E-14   2        -2         2   # BR(HH -> Up up)
     3.02361526E-09   2        -4         4   # BR(HH -> Charm charm)
     2.12018812E-04   2        -6         6   # BR(HH -> Top top)
     9.00123193E-07   2        -1         1   # BR(HH -> Down down)
     3.25560048E-04   2        -3         3   # BR(HH -> Strange strange)
     7.86673804E-01   2        -5         5   # BR(HH -> Bottom bottom)
     6.74357378E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     9.16958100E-03   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     9.16958100E-03   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.52809427E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.26029797E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.15489753E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     5.09061763E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     9.73311594E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.13269941E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     2.29326578E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.39431875E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     6.36985899E-03   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.25050074E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     8.85341755E+01   # Gamma(A0)
     4.75715700E-08   2        22        22   # BR(A0 -> photon photon)
     2.90850361E-08   2        22        23   # BR(A0 -> photon Z)
     2.34993071E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.19854095E-08   2       -11        11   # BR(A0 -> Electron electron)
     5.33608135E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.54107292E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.50919747E-14   2        -2         2   # BR(A0 -> Up up)
     2.92757841E-09   2        -4         4   # BR(A0 -> Charm charm)
     2.09762345E-04   2        -6         6   # BR(A0 -> Top top)
     8.96390147E-07   2        -1         1   # BR(A0 -> Down down)
     3.24209764E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.83420523E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.47062281E-04   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     9.78763553E-03   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     9.78763553E-03   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.66270153E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.43223814E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.14941522E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     4.41598767E-04   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.14644161E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.21726493E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     7.09175066E-03   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.43052235E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     2.28246305E-03   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     3.04745112E-07   2        23        25   # BR(A0 -> Z h0)
     4.56120530E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.05770364E+02   # Gamma(Hp)
     1.28989610E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.51470870E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.55987259E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.43920055E-07   2        -1         2   # BR(Hp -> Down up)
     1.42070719E-05   2        -3         2   # BR(Hp -> Strange up)
     8.37602342E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.94676139E-08   2        -1         4   # BR(Hp -> Down charm)
     3.04122011E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.17294272E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.73348400E-08   2        -1         6   # BR(Hp -> Down top)
     8.25364647E-07   2        -3         6   # BR(Hp -> Strange top)
     7.90114929E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.53066089E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     8.47873930E-04   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     7.42278456E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     8.26704352E-03   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     3.52009206E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     7.85682141E-03   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     8.47206096E-03   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.57572059E-07   2        24        25   # BR(Hp -> W h0)
     2.92905418E-10   2        24        35   # BR(Hp -> W HH)
     2.01228912E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.00270829E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.42152522E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.42152793E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99999208E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.93058631E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.92267087E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.00270829E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.42152522E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.42152793E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999999E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.35208620E-10        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999999E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.35208620E-10        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26173525E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.27040849E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.70444685E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.35208620E-10        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999999E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.46503405E-04   # BR(b -> s gamma)
    2    1.58773258E-06   # BR(b -> s mu+ mu-)
    3    3.52479004E-05   # BR(b -> s nu nu)
    4    2.14679713E-15   # BR(Bd -> e+ e-)
    5    9.17089805E-11   # BR(Bd -> mu+ mu-)
    6    1.92130924E-08   # BR(Bd -> tau+ tau-)
    7    7.12385058E-14   # BR(Bs -> e+ e-)
    8    3.04331262E-09   # BR(Bs -> mu+ mu-)
    9    6.45884511E-07   # BR(Bs -> tau+ tau-)
   10    9.35265545E-05   # BR(B_u -> tau nu)
   11    9.66093502E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42122737E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93544306E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15820014E-03   # epsilon_K
   17    2.28167108E-15   # Delta(M_K)
   18    2.48001135E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29038161E-11   # BR(K^+ -> pi^+ nu nu)
   20    5.51206903E-16   # Delta(g-2)_electron/2
   21    2.35659249E-11   # Delta(g-2)_muon/2
   22    6.67135034E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.10281449E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.17702801E-01   # C7
     0305 4322   00   2    -3.14885093E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.18387578E-01   # C8
     0305 6321   00   2    -3.19301170E-04   # C8'
 03051111 4133   00   0     1.62489431E+00   # C9 e+e-
 03051111 4133   00   2     1.62516641E+00   # C9 e+e-
 03051111 4233   00   2     6.65390953E-04   # C9' e+e-
 03051111 4137   00   0    -4.44758641E+00   # C10 e+e-
 03051111 4137   00   2    -4.44564224E+00   # C10 e+e-
 03051111 4237   00   2    -4.93664434E-03   # C10' e+e-
 03051313 4133   00   0     1.62489431E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62516601E+00   # C9 mu+mu-
 03051313 4233   00   2     6.65387659E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44758641E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44564264E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.93664264E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50493052E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.06791819E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50493052E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.06791882E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50493053E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.06809517E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85796986E-07   # C7
     0305 4422   00   2     1.57850096E-05   # C7
     0305 4322   00   2    -2.24942707E-06   # C7'
     0305 6421   00   0     3.30460633E-07   # C8
     0305 6421   00   2    -1.59653081E-05   # C8
     0305 6321   00   2    -1.41242579E-06   # C8'
 03051111 4133   00   2    -2.27046313E-07   # C9 e+e-
 03051111 4233   00   2     1.40483666E-05   # C9' e+e-
 03051111 4137   00   2     3.23319919E-06   # C10 e+e-
 03051111 4237   00   2    -1.04237167E-04   # C10' e+e-
 03051313 4133   00   2    -2.27049872E-07   # C9 mu+mu-
 03051313 4233   00   2     1.40483595E-05   # C9' mu+mu-
 03051313 4137   00   2     3.23320445E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.04237188E-04   # C10' mu+mu-
 03051212 4137   00   2    -6.81957515E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.25490787E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -6.81957352E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.25490787E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -6.81912544E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.25490786E-05   # C11' nu_3 nu_3
