# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:39
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.82458213E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.63599047E+03  # scale for input parameters
    1    5.75156596E+01  # M_1
    2   -9.54478052E+02  # M_2
    3    2.53196563E+03  # M_3
   11   -2.87814424E+02  # A_t
   12   -1.95894497E+03  # A_b
   13    1.01807344E+03  # A_tau
   23    8.02966663E+02  # mu
   25    3.61233169E+00  # tan(beta)
   26    1.70526211E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.40985923E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.79474488E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.50142338E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.63599047E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.63599047E+03  # (SUSY scale)
  1  1     8.69970863E-06   # Y_u(Q)^DRbar
  2  2     4.41945198E-03   # Y_c(Q)^DRbar
  3  3     1.05099468E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.63599047E+03  # (SUSY scale)
  1  1     6.31498867E-05   # Y_d(Q)^DRbar
  2  2     1.19984785E-03   # Y_s(Q)^DRbar
  3  3     6.26248993E-02   # Y_b(Q)^DRbar
Block Ye Q=  4.63599047E+03  # (SUSY scale)
  1  1     1.10201170E-05   # Y_e(Q)^DRbar
  2  2     2.27861069E-03   # Y_mu(Q)^DRbar
  3  3     3.83224833E-02   # Y_tau(Q)^DRbar
Block Au Q=  4.63599047E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -2.87814312E+02   # A_t(Q)^DRbar
Block Ad Q=  4.63599047E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.95894992E+03   # A_b(Q)^DRbar
Block Ae Q=  4.63599047E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.01807360E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.63599047E+03  # soft SUSY breaking masses at Q
   1    5.75156596E+01  # M_1
   2   -9.54478052E+02  # M_2
   3    2.53196563E+03  # M_3
  21    2.02875044E+06  # M^2_(H,d)
  22    1.03705212E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.40985923E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.79474488E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.50142338E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.16181136E+02  # h0
        35     1.70587785E+03  # H0
        36     1.70526211E+03  # A0
        37     1.71082762E+03  # H+
   1000001     1.01167951E+04  # ~d_L
   2000001     1.00920210E+04  # ~d_R
   1000002     1.01164761E+04  # ~u_L
   2000002     1.00953371E+04  # ~u_R
   1000003     1.01167952E+04  # ~s_L
   2000003     1.00920210E+04  # ~s_R
   1000004     1.01164762E+04  # ~c_L
   2000004     1.00953372E+04  # ~c_R
   1000005     4.46832950E+03  # ~b_1
   2000005     4.58119434E+03  # ~b_2
   1000006     4.46980404E+03  # ~t_1
   2000006     4.80835567E+03  # ~t_2
   1000011     1.00211914E+04  # ~e_L-
   2000011     1.00088540E+04  # ~e_R-
   1000012     1.00204680E+04  # ~nu_eL
   1000013     1.00211914E+04  # ~mu_L-
   2000013     1.00088539E+04  # ~mu_R-
   1000014     1.00204680E+04  # ~nu_muL
   1000015     1.00088552E+04  # ~tau_1-
   2000015     1.00211957E+04  # ~tau_2-
   1000016     1.00204698E+04  # ~nu_tauL
   1000021     3.01774648E+03  # ~g
   1000022     5.58677935E+01  # ~chi_10
   1000023     7.92365025E+02  # ~chi_20
   1000025     8.03105751E+02  # ~chi_30
   1000035     1.03849078E+03  # ~chi_40
   1000024     7.94374440E+02  # ~chi_1+
   1000037     1.03750521E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.57487724E-01   # alpha
Block Hmix Q=  4.63599047E+03  # Higgs mixing parameters
   1    8.02966663E+02  # mu
   2    3.61233169E+00  # tan[beta](Q)
   3    2.42816911E+02  # v(Q)
   4    2.90791886E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99465595E-01   # Re[R_st(1,1)]
   1  2     3.26883036E-02   # Re[R_st(1,2)]
   2  1    -3.26883036E-02   # Re[R_st(2,1)]
   2  2     9.99465595E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99936194E-01   # Re[R_sb(1,1)]
   1  2     1.12963719E-02   # Re[R_sb(1,2)]
   2  1    -1.12963719E-02   # Re[R_sb(2,1)]
   2  2     9.99936194E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.38572415E-02   # Re[R_sta(1,1)]
   1  2     9.99903984E-01   # Re[R_sta(1,2)]
   2  1    -9.99903984E-01   # Re[R_sta(2,1)]
   2  2     1.38572415E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.98356262E-01   # Re[N(1,1)]
   1  2    -2.40649679E-03   # Re[N(1,2)]
   1  3    -5.42823375E-02   # Re[N(1,3)]
   1  4     1.82321408E-02   # Re[N(1,4)]
   2  1     2.50771605E-02   # Re[N(2,1)]
   2  2     1.73013687E-01   # Re[N(2,2)]
   2  3    -7.01084003E-01   # Re[N(2,3)]
   2  4    -6.91316585E-01   # Re[N(2,4)]
   3  1     5.13056260E-02   # Re[N(3,1)]
   3  2    -3.81195139E-02   # Re[N(3,2)]
   3  3    -7.04522476E-01   # Re[N(3,3)]
   3  4     7.06797507E-01   # Re[N(3,4)]
   4  1    -4.86241822E-03   # Re[N(4,1)]
   4  2     9.84178528E-01   # Re[N(4,2)]
   4  3     9.58265606E-02   # Re[N(4,3)]
   4  4     1.48950500E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.33447373E-01   # Re[U(1,1)]
   1  2     9.91055901E-01   # Re[U(1,2)]
   2  1    -9.91055901E-01   # Re[U(2,1)]
   2  2    -1.33447373E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     2.08549698E-01   # Re[V(1,1)]
   1  2     9.78011771E-01   # Re[V(1,2)]
   2  1     9.78011771E-01   # Re[V(2,1)]
   2  2    -2.08549698E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02855921E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.96757037E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     6.21071081E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.59875246E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.42118738E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.90772829E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.06503576E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.92647696E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.09155620E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.96673951E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02876330E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.96716635E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.31024574E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.60872061E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     1.42119760E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.90766638E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.06538156E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.92645658E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.09154835E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.96669660E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.08808607E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.85027610E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     3.62512203E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.29502751E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     5.93991566E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.42390025E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.89441034E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.15486872E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.07469935E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.92117535E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.07638875E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.95551088E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42121122E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.74762070E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     7.80804649E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.31549600E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.95726448E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.66566610E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.81017141E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42122143E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.74755780E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     7.80799035E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.31548654E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.95724322E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.66635340E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.81013089E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.42410208E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.72986497E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     7.79219793E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.31282584E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.95126192E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.85968706E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.79873264E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.66144341E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.42978041E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.91542785E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.94996002E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.72260941E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.57175365E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.23509448E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.32098268E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.97042274E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.07726880E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.33674997E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.66144910E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.42977448E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.91541938E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.95000178E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.72260145E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.57192175E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.23686970E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.32095512E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.97507961E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.07726531E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.33670628E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.49838442E+02   # ~b_1
#    BR                NDA      ID1      ID2
     2.43227148E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.76508308E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     8.03191727E-04    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     6.83382240E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.53155416E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.54886996E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     4.17618817E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     1.19118059E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.13866916E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.34774331E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.47468998E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.78586870E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.76629356E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     9.72737536E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     6.83355189E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.28806805E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.67012318E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.94984612E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.81842653E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.74270557E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.30219190E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     4.81227853E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.04907827E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.33650384E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.83362913E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.28803098E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.67001357E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.94988756E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.81841865E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.74503346E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.30217441E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     4.81260638E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.04907287E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.33646011E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     2.49861211E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.83057306E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.77231239E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.82809854E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     7.62351333E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.96934229E-03    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.37696966E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     4.16522002E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     1.70488972E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.48278398E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.06228455E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.37866338E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.43491906E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.98004319E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.76704281E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     9.76896651E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.95571701E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     5.40428366E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.59582963E-04    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.93887467E-04    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     5.40948385E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.87584937E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.24150278E-02    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     2.90480028E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.46395860E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.34281757E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.18720504E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.23455311E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.08067070E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.61445027E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     5.00592944E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     5.56607097E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     7.72681848E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     2.23199933E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.08360570E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
DECAY   1000025     5.16431713E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.17276252E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     7.78738815E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.98169615E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     5.10577122E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     1.75639986E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.75639986E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.64803739E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.22399224E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.72786344E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.32639978E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.09868350E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     3.48150263E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.00529912E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.00529912E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     6.26884666E-02   # ~g
#    BR                NDA      ID1      ID2
     4.00824544E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     4.09828518E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
     1.70757749E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     7.94527295E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     7.94527388E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     1.95024971E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     2.32033032E-04    3     1000022         1        -1   # BR(~g -> chi^0_1 d d_bar)
     2.32033112E-04    3     1000022         3        -3   # BR(~g -> chi^0_1 s s_bar)
     7.88822137E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.03462015E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.13102709E-03    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     2.04089195E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     9.64530138E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     8.23367362E-04    3     1000035         2        -2   # BR(~g -> chi^0_4 u u_bar)
     8.23370947E-04    3     1000035         4        -4   # BR(~g -> chi^0_4 c c_bar)
     2.91692130E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     8.26153295E-04    3     1000035         1        -1   # BR(~g -> chi^0_4 d d_bar)
     8.26153371E-04    3     1000035         3        -3   # BR(~g -> chi^0_4 s s_bar)
     3.36309077E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.06875836E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.06875836E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     7.29037345E-04    3     1000037         1        -2   # BR(~g -> chi^+_2 d u_bar)
     7.29037345E-04    3    -1000037        -1         2   # BR(~g -> chi^-_2 d_bar u)
     7.29040975E-04    3     1000037         3        -4   # BR(~g -> chi^+_2 s c_bar)
     7.29040975E-04    3    -1000037        -3         4   # BR(~g -> chi^-_2 s_bar c)
     3.42759402E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     3.42759402E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     2.79434520E-03   # Gamma(h0)
     2.41435087E-03   2        22        22   # BR(h0 -> photon photon)
     8.44270954E-04   2        22        23   # BR(h0 -> photon Z)
     1.15703100E-02   2        23        23   # BR(h0 -> Z Z)
     1.13383228E-01   2       -24        24   # BR(h0 -> W W)
     8.12809476E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.01144084E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.67393712E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.70842770E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.71227550E-07   2        -2         2   # BR(h0 -> Up up)
     3.32246768E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.05028412E-07   2        -1         1   # BR(h0 -> Down down)
     2.54991582E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.78289969E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.38470209E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     5.65345639E+00   # Gamma(HH)
     2.37377964E-06   2        22        22   # BR(HH -> photon photon)
     1.35142319E-06   2        22        23   # BR(HH -> photon Z)
     6.48212191E-04   2        23        23   # BR(HH -> Z Z)
     4.31763330E-04   2       -24        24   # BR(HH -> W W)
     4.26952146E-04   2        21        21   # BR(HH -> gluon gluon)
     6.46590204E-10   2       -11        11   # BR(HH -> Electron electron)
     2.87841821E-05   2       -13        13   # BR(HH -> Muon muon)
     8.31247250E-03   2       -15        15   # BR(HH -> Tau tau)
     5.09757858E-11   2        -2         2   # BR(HH -> Up up)
     9.88728209E-06   2        -4         4   # BR(HH -> Charm charm)
     7.02198068E-01   2        -6         6   # BR(HH -> Top top)
     5.08791916E-08   2        -1         1   # BR(HH -> Down down)
     1.84030662E-05   2        -3         3   # BR(HH -> Strange strange)
     4.79094682E-02   2        -5         5   # BR(HH -> Bottom bottom)
     1.16266581E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.51324374E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.65786324E-01   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.94677934E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.80605063E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     4.48564766E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.45832098E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     8.97020975E-05   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.77847197E-03   2        25        25   # BR(HH -> h0 h0)
DECAY        36     5.84946941E+00   # Gamma(A0)
     2.76645402E-06   2        22        22   # BR(A0 -> photon photon)
     3.00654293E-06   2        22        23   # BR(A0 -> photon Z)
     5.73376984E-04   2        21        21   # BR(A0 -> gluon gluon)
     6.16036077E-10   2       -11        11   # BR(A0 -> Electron electron)
     2.74239939E-05   2       -13        13   # BR(A0 -> Muon muon)
     7.91970451E-03   2       -15        15   # BR(A0 -> Tau tau)
     4.72162954E-11   2        -2         2   # BR(A0 -> Up up)
     9.15711036E-06   2        -4         4   # BR(A0 -> Charm charm)
     6.79626899E-01   2        -6         6   # BR(A0 -> Top top)
     4.84771825E-08   2        -1         1   # BR(A0 -> Down down)
     1.75342638E-05   2        -3         3   # BR(A0 -> Strange strange)
     4.56529250E-02   2        -5         5   # BR(A0 -> Bottom bottom)
     4.03706499E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.07029712E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.42326692E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.53655995E-01   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.33766292E-05   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.12369394E-02   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.29122225E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.45643731E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     8.29570232E-04   2        23        25   # BR(A0 -> Z h0)
     1.86642359E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     6.55084838E+00   # Gamma(Hp)
     6.31738513E-10   2       -11        12   # BR(Hp -> Electron nu_e)
     2.70087944E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     7.63961181E-03   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.31744445E-08   2        -1         2   # BR(Hp -> Down up)
     7.23044936E-07   2        -3         2   # BR(Hp -> Strange up)
     4.59971229E-07   2        -5         2   # BR(Hp -> Bottom up)
     4.49034887E-07   2        -1         4   # BR(Hp -> Down charm)
     2.51618581E-05   2        -3         4   # BR(Hp -> Strange charm)
     6.44272064E-05   2        -5         4   # BR(Hp -> Bottom charm)
     4.82307768E-05   2        -1         6   # BR(Hp -> Down top)
     1.05161322E-03   2        -3         6   # BR(Hp -> Strange top)
     7.89101807E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.93508261E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.68249507E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     5.83070876E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.51705014E-03   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     7.49584432E-04   2        24        25   # BR(Hp -> W h0)
     6.16403910E-10   2        24        35   # BR(Hp -> W HH)
     1.10785166E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.11039953E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.31379003E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.30489402E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00681742E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.98171604E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.66345758E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.11039953E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.31379003E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.30489402E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99841788E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.58212027E-04        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99841788E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.58212027E-04        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.28381088E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    6.55229321E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    2.84419401E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.58212027E-04        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99841788E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.36666747E-04   # BR(b -> s gamma)
    2    1.59101765E-06   # BR(b -> s mu+ mu-)
    3    3.53055694E-05   # BR(b -> s nu nu)
    4    2.56944105E-15   # BR(Bd -> e+ e-)
    5    1.09763605E-10   # BR(Bd -> mu+ mu-)
    6    2.29779389E-08   # BR(Bd -> tau+ tau-)
    7    8.65530263E-14   # BR(Bs -> e+ e-)
    8    3.69754252E-09   # BR(Bs -> mu+ mu-)
    9    7.84280876E-07   # BR(Bs -> tau+ tau-)
   10    9.67774676E-05   # BR(B_u -> tau nu)
   11    9.99674189E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43945973E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94354697E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16532113E-03   # epsilon_K
   17    2.28174142E-15   # Delta(M_K)
   18    2.48368063E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29923976E-11   # BR(K^+ -> pi^+ nu nu)
   20   -4.93740332E-17   # Delta(g-2)_electron/2
   21   -2.11089434E-12   # Delta(g-2)_muon/2
   22   -5.97110076E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -5.86240096E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.05265031E-01   # C7
     0305 4322   00   2    -3.28341648E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.15930333E-01   # C8
     0305 6321   00   2    -4.08887849E-04   # C8'
 03051111 4133   00   0     1.63139057E+00   # C9 e+e-
 03051111 4133   00   2     1.63190518E+00   # C9 e+e-
 03051111 4233   00   2     3.43998272E-06   # C9' e+e-
 03051111 4137   00   0    -4.45408267E+00   # C10 e+e-
 03051111 4137   00   2    -4.45763533E+00   # C10 e+e-
 03051111 4237   00   2    -2.47418120E-05   # C10' e+e-
 03051313 4133   00   0     1.63139057E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63190518E+00   # C9 mu+mu-
 03051313 4233   00   2     3.43998252E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.45408267E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45763533E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.47418118E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50611897E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     5.34371772E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50611897E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     5.34371777E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50611897E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     5.34372947E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822555E-07   # C7
     0305 4422   00   2    -2.10156432E-07   # C7
     0305 4322   00   2     1.96752427E-08   # C7'
     0305 6421   00   0     3.30482535E-07   # C8
     0305 6421   00   2    -1.17482410E-06   # C8
     0305 6321   00   2    -2.05337311E-08   # C8'
 03051111 4133   00   2     3.19166788E-07   # C9 e+e-
 03051111 4233   00   2     7.79645844E-08   # C9' e+e-
 03051111 4137   00   2     6.36460127E-07   # C10 e+e-
 03051111 4237   00   2    -5.63447577E-07   # C10' e+e-
 03051313 4133   00   2     3.19166757E-07   # C9 mu+mu-
 03051313 4233   00   2     7.79645842E-08   # C9' mu+mu-
 03051313 4137   00   2     6.36460160E-07   # C10 mu+mu-
 03051313 4237   00   2    -5.63447577E-07   # C10' mu+mu-
 03051212 4137   00   2    -1.11751243E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.21696655E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.11751241E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.21696655E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.11750805E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.21696655E-07   # C11' nu_3 nu_3
