# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:05
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.47879402E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.92341713E+03  # scale for input parameters
    1    4.27813067E+01  # M_1
    2   -1.85369699E+03  # M_2
    3    2.76949045E+03  # M_3
   11    5.40434125E+03  # A_t
   12    4.85461205E+02  # A_b
   13    7.65690821E+02  # A_tau
   23   -2.77161686E+02  # mu
   25    5.46368751E+01  # tan(beta)
   26    4.68853117E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.40557371E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.48877761E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.83435719E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.92341713E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.92341713E+03  # (SUSY scale)
  1  1     8.38577635E-06   # Y_u(Q)^DRbar
  2  2     4.25997438E-03   # Y_c(Q)^DRbar
  3  3     1.01306914E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.92341713E+03  # (SUSY scale)
  1  1     9.20681412E-04   # Y_d(Q)^DRbar
  2  2     1.74929468E-02   # Y_s(Q)^DRbar
  3  3     9.13027461E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.92341713E+03  # (SUSY scale)
  1  1     1.60665639E-04   # Y_e(Q)^DRbar
  2  2     3.32205585E-02   # Y_mu(Q)^DRbar
  3  3     5.58715143E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.92341713E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     5.40434126E+03   # A_t(Q)^DRbar
Block Ad Q=  3.92341713E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     4.85461197E+02   # A_b(Q)^DRbar
Block Ae Q=  3.92341713E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     7.65690819E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.92341713E+03  # soft SUSY breaking masses at Q
   1    4.27813067E+01  # M_1
   2   -1.85369699E+03  # M_2
   3    2.76949045E+03  # M_3
  21    2.22273936E+07  # M^2_(H,d)
  22    2.05053624E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.40557371E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.48877761E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.83435719E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.26117225E+02  # h0
        35     4.68520571E+03  # H0
        36     4.68853117E+03  # A0
        37     4.68517387E+03  # H+
   1000001     1.01109678E+04  # ~d_L
   2000001     1.00873683E+04  # ~d_R
   1000002     1.01106071E+04  # ~u_L
   2000002     1.00902249E+04  # ~u_R
   1000003     1.01109724E+04  # ~s_L
   2000003     1.00873767E+04  # ~s_R
   1000004     1.01106117E+04  # ~c_L
   2000004     1.00902255E+04  # ~c_R
   1000005     4.40797377E+03  # ~b_1
   2000005     4.85911322E+03  # ~b_2
   1000006     3.48463881E+03  # ~t_1
   2000006     4.41744548E+03  # ~t_2
   1000011     1.00200547E+04  # ~e_L-
   2000011     1.00092451E+04  # ~e_R-
   1000012     1.00192931E+04  # ~nu_eL
   1000013     1.00200772E+04  # ~mu_L-
   2000013     1.00092873E+04  # ~mu_R-
   1000014     1.00193151E+04  # ~nu_muL
   1000015     1.00210734E+04  # ~tau_1-
   2000015     1.00267009E+04  # ~tau_2-
   1000016     1.00255627E+04  # ~nu_tauL
   1000021     3.25174927E+03  # ~g
   1000022     4.23408221E+01  # ~chi_10
   1000023     2.93486870E+02  # ~chi_20
   1000025     2.97651379E+02  # ~chi_30
   1000035     1.97092031E+03  # ~chi_40
   1000024     2.91963321E+02  # ~chi_1+
   1000037     1.97088991E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.82719061E-02   # alpha
Block Hmix Q=  3.92341713E+03  # Higgs mixing parameters
   1   -2.77161686E+02  # mu
   2    5.46368751E+01  # tan[beta](Q)
   3    2.42940998E+02  # v(Q)
   4    2.19823245E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.58835146E-02   # Re[R_st(1,1)]
   1  2     9.95392562E-01   # Re[R_st(1,2)]
   2  1    -9.95392562E-01   # Re[R_st(2,1)]
   2  2    -9.58835146E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99968677E-01   # Re[R_sb(1,1)]
   1  2     7.91481452E-03   # Re[R_sb(1,2)]
   2  1    -7.91481452E-03   # Re[R_sb(2,1)]
   2  2    -9.99968677E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -2.63667796E-01   # Re[R_sta(1,1)]
   1  2     9.64613546E-01   # Re[R_sta(1,2)]
   2  1    -9.64613546E-01   # Re[R_sta(2,1)]
   2  2    -2.63667796E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.87906212E-01   # Re[N(1,1)]
   1  2     7.02720347E-04   # Re[N(1,2)]
   1  3    -1.53821035E-01   # Re[N(1,3)]
   1  4    -1.94913510E-02   # Re[N(1,4)]
   2  1     9.54110770E-02   # Re[N(2,1)]
   2  2     3.42368184E-02   # Re[N(2,2)]
   2  3     7.02224241E-01   # Re[N(2,3)]
   2  4    -7.04702549E-01   # Re[N(2,4)]
   3  1     1.22217613E-01   # Re[N(3,1)]
   3  2    -2.46582324E-02   # Re[N(3,2)]
   3  3     6.95107163E-01   # Re[N(3,3)]
   3  4     7.08011906E-01   # Re[N(3,4)]
   4  1     9.47967598E-04   # Re[N(4,1)]
   4  2    -9.99109262E-01   # Re[N(4,2)]
   4  3     6.79977354E-03   # Re[N(4,3)]
   4  4    -4.16358789E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.62008066E-03   # Re[U(1,1)]
   1  2    -9.99953726E-01   # Re[U(1,2)]
   2  1    -9.99953726E-01   # Re[U(2,1)]
   2  2     9.62008066E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     5.89077033E-02   # Re[V(1,1)]
   1  2     9.98263433E-01   # Re[V(1,2)]
   2  1     9.98263433E-01   # Re[V(2,1)]
   2  2    -5.89077033E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02889357E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.75998759E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     9.08837588E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.49119332E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.34866264E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.13235882E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.37018567E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     5.38982241E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01357845E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.04348974E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.07279052E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.67659616E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.11422905E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.68723667E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.32434851E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.35086227E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.12136195E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.16801134E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.32357839E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00867990E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.03366473E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.76205571E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.84968682E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.74633639E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.64903802E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.51025492E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.30103196E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.02881322E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.95527873E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.26110338E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.60055913E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.62726969E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.94431247E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.02868964E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.89887941E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.34869876E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.08291472E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.64091825E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.01934734E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.26552515E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02238272E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.35089815E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.06814682E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.63662440E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01443925E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.88728029E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01259447E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.97122089E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.21837057E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.80803678E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.06732204E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.16838465E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.12375011E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.44978734E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.52139928E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.30325551E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.91268523E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.66510499E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.78245071E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.25009921E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.36541204E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.07453663E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.36956677E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.45101790E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.52202697E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.26198657E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.76088240E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.91080642E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.66575914E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.78419399E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.63621558E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.36495733E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.07444566E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.36885812E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.99456935E+02   # ~b_1
#    BR                NDA      ID1      ID2
     7.92563554E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.19311333E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.16892941E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.14084761E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.95950093E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     8.38345701E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     2.41375790E-01    2     1000021         5   # BR(~b_1 -> ~g b)
     9.33011608E-02    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     2.79522625E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.60998853E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.41242505E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.38541808E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.85451844E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.18357452E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     6.62143635E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.32113670E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.09266642E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     5.07436907E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.65971901E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.66496037E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.81112415E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.52102481E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.36166680E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     4.02494270E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.07088294E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.36928832E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.62150933E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.32110038E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.11990256E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     5.10184974E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.65961293E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.66561417E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.81097225E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.54450642E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.36121264E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     4.82390995E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.07079224E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.36857967E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.50622961E+02   # ~t_1
#    BR                NDA      ID1      ID2
     4.94664519E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.30276756E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.36142673E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.66768579E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.41602006E-04    2     1000021         4   # BR(~t_1 -> ~g c)
     1.71696495E-02    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     3.11522286E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.58021411E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.42441408E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.41621824E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.00861229E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.31897137E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     8.04577498E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.37062946E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     4.55479570E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.83046422E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.84543115E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99805991E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.94005009E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.78912087E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     8.92947096E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.42671857E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.40930411E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.41638829E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.41893025E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.31050392E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     9.30201827E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     9.45897438E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.07266831E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     6.06129810E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.93837935E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.54338262E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     4.96914237E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.03080222E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.91499921E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.26944836E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.26944836E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.68748459E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     7.68930345E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.48790363E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     3.50558979E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.49521777E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.39039321E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.87944056E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     5.78518092E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     9.62897234E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     9.62897234E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     4.94397031E-01   # ~g
#    BR                NDA      ID1      ID2
     2.05487956E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     2.53199773E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.43941623E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     1.43941598E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     2.53282718E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.75019254E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.87780118E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     4.86247632E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.95306925E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     4.75757533E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.10814746E-03    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.50257982E-03    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.40885033E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.40885033E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.03021559E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.03021559E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.79153213E-03   # Gamma(h0)
     2.49098567E-03   2        22        22   # BR(h0 -> photon photon)
     1.69108386E-03   2        22        23   # BR(h0 -> photon Z)
     3.24956306E-02   2        23        23   # BR(h0 -> Z Z)
     2.62638253E-01   2       -24        24   # BR(h0 -> W W)
     7.62131919E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.73985077E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.10837614E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.07934619E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.34677163E-07   2        -2         2   # BR(h0 -> Up up)
     2.61379913E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.47771796E-07   2        -1         1   # BR(h0 -> Down down)
     1.98116304E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.26885207E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.02445545E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     2.14764390E+02   # Gamma(HH)
     1.11617878E-09   2        22        22   # BR(HH -> photon photon)
     3.77677612E-09   2        22        23   # BR(HH -> photon Z)
     2.91122206E-08   2        23        23   # BR(HH -> Z Z)
     5.15492162E-09   2       -24        24   # BR(HH -> W W)
     4.82402087E-06   2        21        21   # BR(HH -> gluon gluon)
     9.01136654E-09   2       -11        11   # BR(HH -> Electron electron)
     4.01288830E-04   2       -13        13   # BR(HH -> Muon muon)
     1.15905166E-01   2       -15        15   # BR(HH -> Tau tau)
     1.82553984E-14   2        -2         2   # BR(HH -> Up up)
     3.54176767E-09   2        -4         4   # BR(HH -> Charm charm)
     2.81473031E-04   2        -6         6   # BR(HH -> Top top)
     6.37400551E-07   2        -1         1   # BR(HH -> Down down)
     2.30566891E-04   2        -3         3   # BR(HH -> Strange strange)
     6.68754028E-01   2        -5         5   # BR(HH -> Bottom bottom)
     6.84130838E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     6.26825681E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     6.26825681E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     3.50428721E-06   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.22153022E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.16066167E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.14050633E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.49882563E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.85728693E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.95002810E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     2.81449316E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     7.56959287E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.32197844E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.75760641E-06   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.48027532E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.97549509E+02   # Gamma(A0)
     2.15391307E-08   2        22        22   # BR(A0 -> photon photon)
     2.58496889E-08   2        22        23   # BR(A0 -> photon Z)
     7.60770043E-06   2        21        21   # BR(A0 -> gluon gluon)
     8.79398751E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.91610890E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.13109950E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.67994489E-14   2        -2         2   # BR(A0 -> Up up)
     3.25926326E-09   2        -4         4   # BR(A0 -> Charm charm)
     2.60835472E-04   2        -6         6   # BR(A0 -> Top top)
     6.21976812E-07   2        -1         1   # BR(A0 -> Down down)
     2.24987629E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.52622248E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.64045315E-04   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     6.82136521E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     6.82136521E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.59577828E-05   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.31643357E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.35654162E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.15113349E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.57046971E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.20528815E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     5.59735602E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     3.68794426E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     7.75671638E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     2.99661376E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     7.99622011E-06   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     8.06537861E-08   2        23        25   # BR(A0 -> Z h0)
     2.26208181E-12   2        23        35   # BR(A0 -> Z HH)
     1.38474422E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.39243121E+02   # Gamma(Hp)
     9.72113256E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     4.15608779E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.17557845E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.00946123E-07   2        -1         2   # BR(Hp -> Down up)
     1.01945233E-05   2        -3         2   # BR(Hp -> Strange up)
     7.29595309E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.83488257E-08   2        -1         4   # BR(Hp -> Down charm)
     2.16596248E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.02169469E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.61089663E-08   2        -1         6   # BR(Hp -> Down top)
     6.74772653E-07   2        -3         6   # BR(Hp -> Strange top)
     6.88413116E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.25125573E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.64241893E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     4.77087403E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.58153649E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     7.34161679E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     5.45140471E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     5.63213613E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     5.75552701E-12   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     6.64716094E-08   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.96865442E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.98519126E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.98518812E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000105E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.33937227E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.34987264E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.96865442E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.98519126E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.98518812E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999999E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.24142099E-10        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999999E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.24142099E-10        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26426541E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.26768186E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.40093072E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.24142099E-10        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999999E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.10482980E-04   # BR(b -> s gamma)
    2    1.59187475E-06   # BR(b -> s mu+ mu-)
    3    3.52543866E-05   # BR(b -> s nu nu)
    4    4.11273859E-15   # BR(Bd -> e+ e-)
    5    1.75683646E-10   # BR(Bd -> mu+ mu-)
    6    3.63279020E-08   # BR(Bd -> tau+ tau-)
    7    1.41383072E-13   # BR(Bs -> e+ e-)
    8    6.03960142E-09   # BR(Bs -> mu+ mu-)
    9    1.26491732E-06   # BR(Bs -> tau+ tau-)
   10    9.61330122E-05   # BR(B_u -> tau nu)
   11    9.93017212E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41964572E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93683949E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15740017E-03   # epsilon_K
   17    2.28165982E-15   # Delta(M_K)
   18    2.47996302E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29049628E-11   # BR(K^+ -> pi^+ nu nu)
   20    5.28972866E-16   # Delta(g-2)_electron/2
   21    2.26154995E-11   # Delta(g-2)_muon/2
   22    6.41514191E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.02190412E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.76671638E-01   # C7
     0305 4322   00   2     4.21413195E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.13014475E-02   # C8
     0305 6321   00   2    -2.77862871E-05   # C8'
 03051111 4133   00   0     1.62710897E+00   # C9 e+e-
 03051111 4133   00   2     1.62749163E+00   # C9 e+e-
 03051111 4233   00   2     6.68238900E-04   # C9' e+e-
 03051111 4137   00   0    -4.44980108E+00   # C10 e+e-
 03051111 4137   00   2    -4.44849629E+00   # C10 e+e-
 03051111 4237   00   2    -4.94245454E-03   # C10' e+e-
 03051313 4133   00   0     1.62710897E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62749133E+00   # C9 mu+mu-
 03051313 4233   00   2     6.68238369E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44980108E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44849659E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.94245532E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50506901E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.06864328E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50506901E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.06864332E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50506901E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.06865564E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85833115E-07   # C7
     0305 4422   00   2     7.01546018E-06   # C7
     0305 4322   00   2     1.88395122E-06   # C7'
     0305 6421   00   0     3.30491579E-07   # C8
     0305 6421   00   2     1.58345726E-05   # C8
     0305 6321   00   2     1.00533946E-06   # C8'
 03051111 4133   00   2     3.04506421E-07   # C9 e+e-
 03051111 4233   00   2     1.25847654E-05   # C9' e+e-
 03051111 4137   00   2    -6.25027472E-07   # C10 e+e-
 03051111 4237   00   2    -9.30823165E-05   # C10' e+e-
 03051313 4133   00   2     3.04501928E-07   # C9 mu+mu-
 03051313 4233   00   2     1.25847595E-05   # C9' mu+mu-
 03051313 4137   00   2    -6.25023321E-07   # C10 mu+mu-
 03051313 4237   00   2    -9.30823340E-05   # C10' mu+mu-
 03051212 4137   00   2     1.52896310E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.01259908E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     1.52896475E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.01259908E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     1.52941838E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.01259907E-05   # C11' nu_3 nu_3
