# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:39
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.50168014E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.56363613E+03  # scale for input parameters
    1    4.53060914E+01  # M_1
    2   -1.85299843E+03  # M_2
    3    2.00221407E+03  # M_3
   11   -5.54599103E+02  # A_t
   12    1.38552318E+03  # A_b
   13    1.95518614E+03  # A_tau
   23    2.14063582E+02  # mu
   25    2.39441089E+01  # tan(beta)
   26    2.16189564E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.57762072E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.50106203E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.27242361E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.56363613E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.56363613E+03  # (SUSY scale)
  1  1     8.39168107E-06   # Y_u(Q)^DRbar
  2  2     4.26297398E-03   # Y_c(Q)^DRbar
  3  3     1.01378248E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.56363613E+03  # (SUSY scale)
  1  1     4.03764281E-04   # Y_d(Q)^DRbar
  2  2     7.67152134E-03   # Y_s(Q)^DRbar
  3  3     4.00407645E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.56363613E+03  # (SUSY scale)
  1  1     7.04598196E-05   # Y_e(Q)^DRbar
  2  2     1.45688560E-02   # Y_mu(Q)^DRbar
  3  3     2.45024191E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.56363613E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -5.54599112E+02   # A_t(Q)^DRbar
Block Ad Q=  4.56363613E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.38552319E+03   # A_b(Q)^DRbar
Block Ae Q=  4.56363613E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.95518613E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.56363613E+03  # soft SUSY breaking masses at Q
   1    4.53060914E+01  # M_1
   2   -1.85299843E+03  # M_2
   3    2.00221407E+03  # M_3
  21    4.59094637E+06  # M^2_(H,d)
  22    3.94737614E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.57762072E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.50106203E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.27242361E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22371034E+02  # h0
        35     2.16184367E+03  # H0
        36     2.16189564E+03  # A0
        37     2.16470764E+03  # H+
   1000001     1.01185652E+04  # ~d_L
   2000001     1.00943446E+04  # ~d_R
   1000002     1.01182032E+04  # ~u_L
   2000002     1.00971982E+04  # ~u_R
   1000003     1.01185653E+04  # ~s_L
   2000003     1.00943447E+04  # ~s_R
   1000004     1.01182033E+04  # ~c_L
   2000004     1.00971983E+04  # ~c_R
   1000005     3.33863509E+03  # ~b_1
   2000005     4.62075840E+03  # ~b_2
   1000006     4.50457369E+03  # ~t_1
   2000006     4.62347297E+03  # ~t_2
   1000011     1.00204923E+04  # ~e_L-
   2000011     1.00093024E+04  # ~e_R-
   1000012     1.00197260E+04  # ~nu_eL
   1000013     1.00204925E+04  # ~mu_L-
   2000013     1.00093026E+04  # ~mu_R-
   1000014     1.00197262E+04  # ~nu_muL
   1000015     1.00094029E+04  # ~tau_1-
   2000015     1.00205559E+04  # ~tau_2-
   1000016     1.00197841E+04  # ~nu_tauL
   1000021     2.43798225E+03  # ~g
   1000022     4.30613530E+01  # ~chi_10
   1000023     2.38144695E+02  # ~chi_20
   1000025     2.43601636E+02  # ~chi_30
   1000035     1.96598053E+03  # ~chi_40
   1000024     2.36605880E+02  # ~chi_1+
   1000037     1.96594872E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.01388224E-02   # alpha
Block Hmix Q=  4.56363613E+03  # Higgs mixing parameters
   1    2.14063582E+02  # mu
   2    2.39441089E+01  # tan[beta](Q)
   3    2.42808697E+02  # v(Q)
   4    4.67379276E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.45915763E-02   # Re[R_st(1,1)]
   1  2     9.95516164E-01   # Re[R_st(1,2)]
   2  1    -9.95516164E-01   # Re[R_st(2,1)]
   2  2     9.45915763E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     7.73540003E-04   # Re[R_sb(1,1)]
   1  2     9.99999701E-01   # Re[R_sb(1,2)]
   2  1    -9.99999701E-01   # Re[R_sb(2,1)]
   2  2     7.73540003E-04   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     2.40590608E-02   # Re[R_sta(1,1)]
   1  2     9.99710539E-01   # Re[R_sta(1,2)]
   2  1    -9.99710539E-01   # Re[R_sta(2,1)]
   2  2     2.40590608E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.80614937E-01   # Re[N(1,1)]
   1  2    -2.06403657E-03   # Re[N(1,2)]
   1  3    -1.91291525E-01   # Re[N(1,3)]
   1  4     4.23985462E-02   # Re[N(1,4)]
   2  1     1.06044215E-01   # Re[N(2,1)]
   2  2     3.11521536E-02   # Re[N(2,2)]
   2  3    -7.00268097E-01   # Re[N(2,3)]
   2  4    -7.05272117E-01   # Re[N(2,4)]
   3  1     1.64766850E-01   # Re[N(3,1)]
   3  2    -2.67290724E-02   # Re[N(3,2)]
   3  3    -6.87766608E-01   # Re[N(3,3)]
   3  4     7.06480385E-01   # Re[N(3,4)]
   4  1     9.24246615E-04   # Re[N(4,1)]
   4  2    -9.99155063E-01   # Re[N(4,2)]
   4  3    -3.03923114E-03   # Re[N(4,3)]
   4  4    -4.09764453E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.30472908E-03   # Re[U(1,1)]
   1  2     9.99990735E-01   # Re[U(1,2)]
   2  1    -9.99990735E-01   # Re[U(2,1)]
   2  2    -4.30472908E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     5.79629489E-02   # Re[V(1,1)]
   1  2     9.98318735E-01   # Re[V(1,2)]
   2  1     9.98318735E-01   # Re[V(2,1)]
   2  2    -5.79629489E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02890398E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.61647473E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.12335616E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.71181562E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.34921462E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.04167777E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.51733721E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.22664131E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01406524E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.04420618E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03734753E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.60066288E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.16257040E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.74691110E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     8.38087599E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.34963729E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.03999347E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.67010924E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.37437601E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01312140E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.04231342E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.42160906E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     6.58718395E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     8.62485439E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     9.33013768E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.76897623E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.60900518E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     5.54268220E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.46834667E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.54555300E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.23734971E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.02356678E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.76815980E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.55106855E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.34924869E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.89939113E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.12284159E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     4.29898411E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.01968478E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.19389032E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02332452E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.34967133E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.89660450E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.12217688E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     4.29763799E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01873925E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.50632086E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02143854E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.46879993E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.17508305E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.95006673E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.94909630E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.77391974E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     8.34018292E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.53311263E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.12171466E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.60916218E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.14601716E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.92087273E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.33782072E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.59045768E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.67753054E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     4.93916973E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.89163652E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.49923699E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.12195020E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.60951611E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.22413577E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.92054473E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.33797536E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.59094666E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.74444629E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     4.93907819E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.89145455E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.49907945E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.03027794E+01   # ~b_1
#    BR                NDA      ID1      ID2
     2.71868654E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     6.46235772E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     6.26677786E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.30562076E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     7.14958930E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     3.39888801E+02   # ~b_2
#    BR                NDA      ID1      ID2
     3.35997103E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.11611851E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.05910712E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     4.00809580E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.74801175E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.11085688E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     5.58797271E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.29332449E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.97288789E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.47286577E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     8.38361444E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.69085449E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.33769233E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.66742907E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.33580028E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.93578813E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.58561923E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.85833000E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.49898884E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.29339718E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.97285907E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.49769600E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     8.40848961E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.69075760E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.33784663E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.66740606E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.35757670E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.93569711E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.72708646E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.85814768E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.49883129E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     3.70635755E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.67192815E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.23095284E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.21899086E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.15931895E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.42675094E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.33197134E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     4.82084226E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     3.37982091E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.08192957E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.37848081E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.40511683E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.91694554E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.78874294E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     7.84846191E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.53973442E-01    2     1000021         6   # BR(~t_2 -> ~g t)
DECAY   1000024     1.41947256E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99996028E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.72816336E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.12316233E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.42435012E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.38668861E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.40966300E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.42754095E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.32165026E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.25005849E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     1.00465568E-02    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     9.42764969E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.70518069E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     7.04131978E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     2.95832116E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.09411780E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     4.52620862E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.47372938E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.84585203E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.26567180E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.26567180E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     7.52083676E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     9.61717659E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.29018454E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.23518291E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.32265381E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.53651982E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.15811897E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.03192586E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     6.31971077E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.04786270E-02    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.04786270E-02    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.21631374E-02   # ~g
#    BR                NDA      ID1      ID2
     4.63421584E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     3.87387754E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     4.99891547E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     4.99891937E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     1.36877198E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.46094888E-04    3     1000022         1        -1   # BR(~g -> chi^0_1 d d_bar)
     1.46121904E-04    3     1000022         3        -3   # BR(~g -> chi^0_1 s s_bar)
     2.22602160E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.63417435E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     7.05144476E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.68563621E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     6.82541960E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.29843807E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.41541965E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.41541965E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.65210737E-03   # Gamma(h0)
     2.27730885E-03   2        22        22   # BR(h0 -> photon photon)
     1.24612652E-03   2        22        23   # BR(h0 -> photon Z)
     2.09777550E-02   2        23        23   # BR(h0 -> Z Z)
     1.82715164E-01   2       -24        24   # BR(h0 -> W W)
     7.24446104E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.85495258E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.15955590E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.22644572E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.37710592E-07   2        -2         2   # BR(h0 -> Up up)
     2.67243493E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.64097128E-07   2        -1         1   # BR(h0 -> Down down)
     2.04020478E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.42434881E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     8.84946649E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     2.01860882E+01   # Gamma(HH)
     1.57223189E-08   2        22        22   # BR(HH -> photon photon)
     1.83097714E-08   2        22        23   # BR(HH -> photon Z)
     4.10773119E-06   2        23        23   # BR(HH -> Z Z)
     1.94833091E-06   2       -24        24   # BR(HH -> W W)
     8.88083665E-06   2        21        21   # BR(HH -> gluon gluon)
     9.64629339E-09   2       -11        11   # BR(HH -> Electron electron)
     4.29455490E-04   2       -13        13   # BR(HH -> Muon muon)
     1.24025516E-01   2       -15        15   # BR(HH -> Tau tau)
     4.39777049E-13   2        -2         2   # BR(HH -> Up up)
     8.53038949E-08   2        -4         4   # BR(HH -> Charm charm)
     6.10026060E-03   2        -6         6   # BR(HH -> Top top)
     7.36156934E-07   2        -1         1   # BR(HH -> Down down)
     2.66269138E-04   2        -3         3   # BR(HH -> Strange strange)
     6.84060205E-01   2        -5         5   # BR(HH -> Bottom bottom)
     3.15917774E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.95351838E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.95351838E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     9.80078770E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     6.01699721E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.27109602E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.35549359E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.08489587E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.99315978E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     3.50291663E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     5.45638504E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.45530641E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.17180482E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.97810249E+01   # Gamma(A0)
     1.53772518E-08   2        22        22   # BR(A0 -> photon photon)
     3.63152219E-08   2        22        23   # BR(A0 -> photon Z)
     3.59954730E-05   2        21        21   # BR(A0 -> gluon gluon)
     9.59735352E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.27276762E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.23396668E-01   2       -15        15   # BR(A0 -> Tau tau)
     4.22026370E-13   2        -2         2   # BR(A0 -> Up up)
     8.18528584E-08   2        -4         4   # BR(A0 -> Charm charm)
     6.01875855E-03   2        -6         6   # BR(A0 -> Top top)
     7.32419502E-07   2        -1         1   # BR(A0 -> Down down)
     2.64917329E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.80591331E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.31502478E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.99355477E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.99355477E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.04070625E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.16735705E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     5.23699537E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.12782715E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.78952774E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     4.40054913E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.58436751E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     6.85884852E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.11184570E-03   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     5.77162549E-06   2        23        25   # BR(A0 -> Z h0)
     2.08736051E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.13877569E+01   # Gamma(Hp)
     1.04751015E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.47843303E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.26675471E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.92206043E-07   2        -1         2   # BR(Hp -> Down up)
     1.16396389E-05   2        -3         2   # BR(Hp -> Strange up)
     7.33206277E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.61455942E-08   2        -1         4   # BR(Hp -> Down charm)
     2.49546682E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.02675091E-03   2        -5         4   # BR(Hp -> Bottom charm)
     4.28698634E-07   2        -1         6   # BR(Hp -> Down top)
     9.71271779E-06   2        -3         6   # BR(Hp -> Strange top)
     6.97185799E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.12855526E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.36213462E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     3.10140959E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.84544847E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.59019238E-03   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.70800969E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.89355281E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     5.36553981E-06   2        24        25   # BR(Hp -> W h0)
     1.22551090E-11   2        24        35   # BR(Hp -> W HH)
     1.11828903E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.24805093E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.73395546E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.73320351E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00013116E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.61306866E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.74422554E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.24805093E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.73395546E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.73320351E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99997437E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.56271022E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99997437E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.56271022E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27252228E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.22961485E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.02423265E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.56271022E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99997437E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.30303191E-04   # BR(b -> s gamma)
    2    1.58901624E-06   # BR(b -> s mu+ mu-)
    3    3.52507168E-05   # BR(b -> s nu nu)
    4    2.59313204E-15   # BR(Bd -> e+ e-)
    5    1.10775633E-10   # BR(Bd -> mu+ mu-)
    6    2.31883630E-08   # BR(Bd -> tau+ tau-)
    7    8.69803497E-14   # BR(Bs -> e+ e-)
    8    3.71579724E-09   # BR(Bs -> mu+ mu-)
    9    7.88121589E-07   # BR(Bs -> tau+ tau-)
   10    9.60022928E-05   # BR(B_u -> tau nu)
   11    9.91666930E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42010607E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93621440E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15740613E-03   # epsilon_K
   17    2.28165994E-15   # Delta(M_K)
   18    2.47982637E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29007512E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.66856373E-16   # Delta(g-2)_electron/2
   21   -1.14089656E-11   # Delta(g-2)_muon/2
   22   -3.22915678E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    5.84303385E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.98566591E-01   # C7
     0305 4322   00   2    -2.19564088E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.09633321E-01   # C8
     0305 6321   00   2    -2.97361083E-04   # C8'
 03051111 4133   00   0     1.63109437E+00   # C9 e+e-
 03051111 4133   00   2     1.63138271E+00   # C9 e+e-
 03051111 4233   00   2     1.29671957E-04   # C9' e+e-
 03051111 4137   00   0    -4.45378647E+00   # C10 e+e-
 03051111 4137   00   2    -4.45195997E+00   # C10 e+e-
 03051111 4237   00   2    -9.52428249E-04   # C10' e+e-
 03051313 4133   00   0     1.63109437E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63138266E+00   # C9 mu+mu-
 03051313 4233   00   2     1.29671791E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45378647E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45196002E+00   # C10 mu+mu-
 03051313 4237   00   2    -9.52428146E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50495652E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.05746046E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50495652E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.05746078E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50495652E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.05755212E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85821820E-07   # C7
     0305 4422   00   2    -1.92442763E-06   # C7
     0305 4322   00   2     1.60373004E-07   # C7'
     0305 6421   00   0     3.30481905E-07   # C8
     0305 6421   00   2    -1.20103725E-05   # C8
     0305 6321   00   2    -1.63146083E-07   # C8'
 03051111 4133   00   2     1.59276171E-07   # C9 e+e-
 03051111 4233   00   2     2.80338331E-06   # C9' e+e-
 03051111 4137   00   2     4.16464798E-07   # C10 e+e-
 03051111 4237   00   2    -2.05969738E-05   # C10' e+e-
 03051313 4133   00   2     1.59275380E-07   # C9 mu+mu-
 03051313 4233   00   2     2.80338302E-06   # C9' mu+mu-
 03051313 4137   00   2     4.16465679E-07   # C10 mu+mu-
 03051313 4237   00   2    -2.05969746E-05   # C10' mu+mu-
 03051212 4137   00   2    -7.27076487E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     4.44941634E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -7.27076160E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     4.44941634E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -7.26982749E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     4.44941633E-06   # C11' nu_3 nu_3
