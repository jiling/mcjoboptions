evgenConfig.description = "aMC@NLO W+/W- -> enu +charm forced D+,D0 decays"
evgenConfig.keywords = ["SM", "W", "electron", "jets", "NLO" ]
evgenConfig.contact  = [ "mdshapiro@lbl.gov" ]
evgenConfig.nEventsPerJob = 5000

isPythia = True
useCKM = True
We = False
Wmu = True

include("MadGraphControl_Wc.py")
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
genSeq.EvtInclusiveDecay.userDecayFile = "DplusAndMinusToKpipiAndD0ToKpi.dec"
evgenConfig.auxfiles += [ 'DplusAndMinusToKpipiAndD0ToKpi.dec']

include("GeneratorFilters/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 15000.
filtSeq.LeptonFilter.Etacut = 2.7

from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
DplusHadronFilter = HeavyFlavorHadronFilter("DplusHadronFilter")
filtSeq += DplusHadronFilter
DplusHadronFilter.RequestSpecificPDGID=True
DplusHadronFilter.RequestCharm = False
DplusHadronFilter.RequestBottom = False
DplusHadronFilter.Request_cQuark = False
DplusHadronFilter.Request_bQuark = False
DplusHadronFilter.PDGPtMin=7.0*GeV
DplusHadronFilter.PDGEtaMax=2.3
DplusHadronFilter.PDGID=411
DplusHadronFilter.PDGAntiParticleToo=True

DstarHadronFilter = HeavyFlavorHadronFilter("DstarHadronFilter")
filtSeq += DstarHadronFilter
DstarHadronFilter.RequestCharm = False
DstarHadronFilter.RequestBottom = False
DstarHadronFilter.Request_cQuark = False
DstarHadronFilter.Request_bQuark = False
DstarHadronFilter.RequestSpecificPDGID = True
DstarHadronFilter.RequireTruthJet = False
DstarHadronFilter.RequestCharm=False
DstarHadronFilter.PDGPtMin=7.0*GeV
DstarHadronFilter.PDGEtaMax=2.3
DstarHadronFilter.PDGID=413
DstarHadronFilter.PDGAntiParticleToo=True

filtSeq.Expression="LeptonFilter and ((DplusHadronFilter) or (DstarHadronFilter))"


