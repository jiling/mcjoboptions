### logger
from AthenaCommon import Logging
log = Logging.logging.getLogger('MGC_HC_topHiggs')
log.info("Starting generation top-Higgs process with MadGraph5_aMC@NLO at NLO in QCD")
log.info("Using the Higgs Caracterisation model, for top+Higgs production")

############
### reading the actual joboption name to deduce the parameters
### syntax:
### mc.aMC[Py8EG]_tWH[_ytm1][_CPalpha0][_lep][_bb].py
### mc.aMC_tWH[_ytm1][_CPalpha0][_bb][_lhePy8].py
### examples:
### mc.aMCH7EG_tHjb_CPalpha0.py
### mc.aMCH7EG_tWH_yt1_CPalpha0.py
############

### first retrieve the physics_short
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
if phys_short is "":
    log.error("Impossible to assess the phys_short of the joboption")
    raise RuntimeError
else:
    log.info("phys_short: {}".format(phys_short))

### to handle regular expressions
import re
expression = re.compile(r"""
aMC(?P<shower>Py8EG|Py8|H7EG|H7)?   # optional: shower is anything after 'aMC'
(?:_(?P<process>ttH|tHjb|tWH))      # then comes the process
(?:(?P<mH>[0-9]*))?                 # optional: Higgs mass value
(?:_DR(?P<DR>1|2))?                 # optional: DR1 or DR2 for tWH (DR2 is default)
(?:_yt(?P<yukawa>[mp0-9]*))?        # optional: yukawa coupling, possibly with minus (m) sign, and decimal (separated by p)
(?:_CPalpha(?P<alpha>[mp0-9]*))?    # optional: CP alpha angle in degree, possibly with minus (m) sign, and decimal (separated by p)
(?:_(?P<Hdecay>bb|ML|gg))?          # optional: Higgs decay (inclusive is default)
(?:_(?P<topdecay>had|lep|nonallhad|allhad|lplus|lminus|sglep|dil))? # optional: top decay (inclusive is default)
(?:_filter(?P<filtering>[0-9]L))?   # optional: additional lepton (e, mu and tau) filter, 1L, 2L, 3L, 4L, ...
(?:_(?P<genmode>LHE|PS))?           # optional: generation mode (none, LHE (lhe-only), PS (shower-only))
""", re.VERBOSE)
matched = expression.match(phys_short)
if matched is None:
    log.error("Impossible to interpret the physics_short in joboption name")
    raise RuntimeError

### finding the parton shower, handling lhe-only case
shower = None
doEvtGen = None
lheOnly = None
showerOnly = None
if matched.group("genmode") is None: 
    lheOnly = False
    showerOnly = False
    shower = matched.group("shower")
    log.info("On-the-fly run - will produce lhe files and shower them  with Pythia8(+EvtGen) or Herwig7(+EvtGen)")
elif matched.group("genmode") == 'LHE':
    lheOnly = True
    showerOnly = False
    shower = matched.group("shower")
    log.info("No parton shower will be run - will produce lhe files only, to be later showered with Pythia8(+EvtGen) or Herwig7(+EvtGen)")
    log.info("Please make sure to provide the outputTXTFile")
elif matched.group("genmode") == 'PS':
    lheOnly = False
    showerOnly = True
    shower = matched.group("shower")
    log.info("No event generation will be run - will shower the input lhe files with Pythia8(+EvtGen) or Herwig7(+EvtGen)")
    log.info("Please make sure to provide the inputGeneratorFile")
else:
    log.error("Impossible to assess it")
    log.info("It should be on-the-flly (outputTXTFile:None and inputGeneratorFile:None) or ME-only (_LHE in shortname, outputTXTFile and inputGeneratorFile:None)")
    log.info("or ME-only (_PS in shortname, outputTXTFile:None and inputGeneratorFile)")
    raise RuntimeError

if shower == "Py8EG":
    log.info("Will use Pythia8+EvtGen")
    shower = "Py8"
    doEvtGen = True
elif shower == "H7EG":
    log.info("Will use Herwig7+EvtGen")
    shower = "H7"
    doEvtGen = True
elif shower == "Py8":
    log.warning("Will use Pythia8, without EvtGen - this is not standard for ATLAS MC")
    doEvtGen = False
elif shower == "H7":
    log.warning("Will use Herwig7, without EvtGen - this is not standard for ATLAS MC")
    doEvtGen = False

### finding the process name
process = matched.group("process")
process_MG = "" # MadGraph syntax
if process == "tHjb":
    process_MG = "generate p p > x0 t b~ j $$ w+ w- [QCD] @0\nadd process p p > x0 t~ b j $$ w+ w- [QCD] @1\n"
elif process == "tWH":
    process_MG = "generate p p > t w- x0 [QCD] @0\nadd process p p > t~ w+ x0 [QCD] @1\n"
elif process == "ttH":
    process_MG = "generate p p > t t~ x0 [QCD] @0"
else:
    log.error("Unknown process {}".format(process))
    log.info("Should be one in tHjb, tWH, ttH")
    raise RuntimeError
log.info("Will generate {} events".format(process))

### finding the diagram removal scheme for tWH
DR = None
if matched.group("DR") is None:
    if process == "tWH":
        DR = 2 # default is DR2
else:
    if process != 'tWH':
        log.error("Diagram removal scheme only make sense for tWH")
        log.info("Need to be removed from joboption name")
        raise RuntimeError
    else:
        DR = int(matched.group("DR"))
        if DR == 1 or DR == 2:
            log.info("Using DR{} scheme for tWH".format(DR))
        else:
            log.error("Unknown scheme DR{} for tWH".format(DR))
            raise RuntimeError

# defining the flavour scheme
flavour_scheme = 4 if process == "tHjb" else 5
log.info("Using {} flavour scheme for process {}".format(flavour_scheme, process))

### finding the top Yukawa coupling
yt = None
if matched.group("yukawa") is None:
    yt = 1.
else: # 125 GeV is the default
    yt = float (matched.group("yukawa").replace("m","-").replace("p","."))
log.info("Using yt={}".format(yt))

### finding the alpha CP-angle
alpha = None
if matched.group("alpha") is None:
    alpha = 0. # 0 is the default
else:
    alpha = float (matched.group("alpha").replace("m","-").replace("p","."))
log.info("Using alpha={}".format(alpha))

### finding the Higgs decay mode
Hdecay   = "inc" if matched.group("Hdecay") is None else matched.group("Hdecay")
if lheOnly and Hdecay != "inc":
    log.warning("Higgs decay does not have any effect if lhe-only production")
    log.info("Higgs decay is handled by the parton shower")
if Hdecay == "bb":
    log.info("Including H->bb decays only (done by the PS generator)")
elif Hdecay == "gg":
    log.info("Including H->gamma gamma decays only (done by the PS generator)")
elif Hdecay == "ML":
    log.info("Including H->tau tau/ZZ/WW decays only (done by the PS generator)")
elif Hdecay == "inc":
    log.info("Including all Higgs decays (done by the PS generator)")
else:
    log.error("Impossible to recognise Higgs decay more {}".format(Hdecay))
    log.info("Should be one in bb, ML, gg, ZZ, or nothing for inclusive")
    raise RuntimeError

### finding the top(s) decay mode
topdecay = "inc" if matched.group("topdecay") is None else matched.group("topdecay")
topdecay_MS = "" # MadSpin syntax
if process == "ttH":
    if topdecay == "inc":
        topdecay_MS = "decay t > w+ b, w+ > all all\ndecay t~ > w- b~, w- > all all"
    elif topdecay == "dil":
        topdecay_MS = "decay t > w+ b, w+ > l+ vl\ndecay t~ > w- b~, w- > l- vl~"
    elif topdecay == "sglep":
        if lheOnly:
            log.error("Impossible to do single-lepton ttbar decay in lhe-only production")
            log.info("This can only be achieved with a filter, after parton shower")
            raise RuntimeError
        else:# inclusive decays with MadSpin, will use a filter afterwards
            topdecay_MS = "decay t > w+ b, w+ > all all\ndecay t~ > w- b~, w- > all all"
    elif topdecay == "lminus":
        topdecay_MS = "decay t > w+ b, w+ > j j\ndecay t~ > w- b~, w- > l- vl~"
    elif topdecay == "lplus":
        topdecay_MS = "decay t > w+ b, w+ > l+ vl\ndecay t~ > w- b~, w- > j j"
    elif topdecay == "allhad":
        topdecay_MS = "decay t > w+ b, w+ > j j\ndecay t~ > w- b~, w- > j j"
    elif topdecay == "nonallhad":
        if lheOnly:
            log.error("Impossible to do non-allhad ttbar decay in lhe-only production")
            log.info("This can only be achieved with a filter, after parton shower")
            raise RuntimeError
        else:# inclusive decays with MadSpin, will use a filter afterwards
            topdecay_MS = "decay t > w+ b, w+ > all all\ndecay t~ > w- b~, w- > all all"
    else:
        log.error("Unknown ttbar decay mode {}".format(topdecay))
        log.info("Should be one in nonallhad, allhad, lplus, lminus, sglep, dil, or nothing for inclusive")
        raise RuntimeError
    log.info("ttbar decay: {}".format(topdecay))
else:# tHjb or tWH
    if topdecay == "inc":
        topdecay_MS = "decay t > w+ b, w+ > all all\ndecay t~ > w- b~, w- > all all"
    elif topdecay == "lep":
        topdecay_MS = "decay t > w+ b, w+ > l+ vl\ndecay t~ > w- b~, w- > l- vl~"
    elif topdecay == "had":
        topdecay_MS = "decay t > w+ b, w+ > j j\ndecay t~ > w- b~, w- > j j"
    else:
        log.error("Unknown top decay mode {}".format(topdecay))
        log.info("Should be one in had, lep, or nothing for inclusive")
        raise RuntimeError
    log.info("top decay: {}".format(topdecay))

### finding additional filters
filtering = None
if lheOnly and matched.group("filtering") is not None:
    filtering = int (matched.group("filtering").replace("L",""))
    log.warning("Lepton filter does not have any effect if lhe-only production (apart from the number of generated events)")
    log.info("Lepton filter is applied at the parton shower level")
elif matched.group("filtering") is not None:
    filtering = int (matched.group("filtering").replace("L",""))
    log.info("Using lepton filter={}".format(filtering))

############
### MadGraphControl
############
if not showerOnly:
    from MadGraphControl.MadGraphUtils import *
    
    ### setting PDF and scale variations
    if process == "tHjb": # 4fs scheme for tHjb
        import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
        # adding NNPDF31_nlo_as_0118_nf_4 to the standard 4fs list
        MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING['alternative_pdfs'].append(320500)
    else: # 5fs scheme for tWH and ttH
        import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
        # adding NNPDF31_nlo_as_0118 to the standard 5fs list
        MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING['alternative_pdfs'].append(303400)
        
    ### initialise MadGraph process directory
    gridpack_mode=True # set to True to create gridpack
    if not is_gen_from_gridpack():
        full_proc = ""
        if process == "tHjb":
            full_proc = """
            import model HC_NLO_X0_UFO-4Fnoyb
            define p = g u c d s u~ c~ d~ s~
            define j = g u c d s u~ c~ d~ s~
            """+process_MG+"""
            output -f
            """
        else:
            full_proc = """
            import model HC_NLO_X0_UFO-no_b_mass
            define p = g u c d s b u~ c~ d~ s~ b~
            define j = g u c d s b u~ c~ d~ s~ b~
            """+process_MG+"""
            output -f
            """
        proc_dir = new_process(process=full_proc)
    else:
        proc_dir = MADGRAPH_GRIDPACK_LOCATION
        
    ### compute the number of events we need
    nevents = runArgs.maxEvents
    # number of events requested in --maxEvents (taken from evgenConfig.nEventsPerJob in official production)
    # need safety margin if parton shower is used
    # otherwise for lhe-only, the number of events should be exactly what is requested
    if not lheOnly:
        if topdecay=="nonallhad" or topdecay=="sglep":
            nevents *= 3 # need ca. 15k inclusive ttbar events to get 5k nonallhad ttbar events
        elif filtering is not None:
            nevents *= filtering # x2 for 2L-filter, x3 for 3L-filter, ...
        else:
            nevents *= 1.1 # 10% is sufficient in most cases, if no filter is used
            
    ### modify the few run_card parameters we need
    BW_cut = 50.0
    run_card_settings = {
        'parton_shower' : "PYTHIA8" if shower == "Py8" else "HERWIGPP",
        'bwcutoff'      : BW_cut,
        'nevents'       : nevents,
    }
    modify_run_card(process_dir=proc_dir,settings=run_card_settings)

    ### modify the few param_card parameters we need
    # first compute the relevant parameters
    from math import cos,radians
    topMass = 172.5 # GeV
    topYukawa = topMass*yt # MadGraph multiplies the yukawa by the fermion mass
    cosalpha = cos(radians(alpha))
    # cos alpha cannot be 0 in this UFO model - setting it to a very small value when it's 0 in the inputs
    cosalpha = 0.000001 if cosalpha == 0 else cosalpha
    # then apply the standard top physics parameters (using 4fs for tHjb)
    from MadGraphControl.MadGraphParamHelpers import *
    set_top_params(process_dir=proc_dir, mTop=topMass, FourFS=(flavour_scheme==4))
    # finally modify the parameters specific to the Higgs Caracterisation model
    param_card_settings={
        'frblock':{
            'Lambda' : '1.000000e+03',
            'cosa'   : str(cosalpha),
            'kSM'    : str(1/cosalpha),
            'kHtt'   : '1.000000e+00',
            'kAtt'   : '1.000000e+00',
        },
        'yukawa':{
            '6'      : str(topYukawa)
        },
        'sminputs':{# these parameters need to be set to the recommended SM values, otherwith the Wmass is inconsistent when using the Higgs caracterisation model
            '1'      : '1.323489e+02',# aEWM1
            '2'      : '1.166370e-05',# Gf
            '3'      : '1.180000e-01',# aS
        }
    }
    modify_param_card(process_dir=proc_dir, params=param_card_settings)
    
    ### special treatment for tWH: DR1 or DR2
    if process == "tWH":
        from MadGraphControl.DiagramRemoval import do_DRX
        do_DRX(DRmode=DR, process_dir=proc_dir)

    # Set up batch jobs
    #modify_config_card(process_dir=proc_dir,settings={'cluster_type':'condor','cluster_queue':'workday'})
    
    ### write madspin card 
    Nevents_for_max_weigth = 2000
    max_weight_ps_point = 500
    madspin_dir = 'MadSpin'
    madspin_card_loc = proc_dir+'/Cards/madspin_card.dat'
    fMadSpinCard = open(madspin_card_loc,'w')
    #fMadSpinCard.write('import '+proc_dir+'/Events/'+MADGRAPH_RUN_NAME+'/events.lhe.gz\n')
    fMadSpinCard.write('set ms_dir '+proc_dir+'/'+madspin_dir+'\n')
    fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
    fMadSpinCard.write('#set use_old_dir True\n')
    # for these numbers I get negligible(<1) amount of events above weights / 10k decayed events
    fMadSpinCard.write('set BW_cut {}\n'.format(BW_cut))
    fMadSpinCard.write('set Nevents_for_max_weight {} # number of events for the estimate of the max. weight (default: 75)\n'.format(Nevents_for_max_weigth))
    fMadSpinCard.write('set max_weight_ps_point {}  # number of PS to estimate the maximum for each event (default: 400)\n'.format(max_weight_ps_point))
    fMadSpinCard.write(topdecay_MS+"\nlaunch\n")
    fMadSpinCard.close()
    
    ### Hack: removing MadSpinCard for the generation of tWH events
    if process == "tWH":
        subprocess.call(["mv","-v",proc_dir+'/Cards/madspin_card.dat','madspin_card.dat'])
        
    ### calling event generation
    generate(process_dir=proc_dir,
             grid_pack=gridpack_mode,
             required_accuracy=0.001,
    runArgs=runArgs)
    
    outputDS=arrange_output(process_dir=proc_dir,
                            lhe_version=3,
                            runArgs=runArgs,
                            saveProcDir=True) # saveProcDir to be removed for official production
    
    ### special treatment for tWH: DR1 or DR2
    ### MadSpin hack: needed to properly handle diagram removal in madspin 
    #FIXME: need to make sure it works as expected!
    if process == "tWH":
        log.info("----> Redecaying tWH events with MadSpin <------")
        subprocess.call(["ls","-lahtr"])
        subprocess.call(["mv","-v",'madspin_card.dat',proc_dir+'/Cards/madspin_card.dat'])
        # run MadSpin to create MadSpin files
        lheFile = runArgs.outputTXTFile+".events"
        if not os.path.isfile(lheFile):
            lheFiles = glob.glob("*.events")
            if len(lheFiles) > 0:
                lheFile = lheFiles[0]
            else:
                raise RuntimeError("Could not find LHE events file!")
                
        log.info("Run madspin on "+lheFile)
        madspin_on_lhe(lheFile,madspin_card_loc,runArgs=runArgs,keep_original=True)
        log.info("----> madspin_on_lhe done. <------")
    
        # apply hacks on MadSpin files
        from MadGraphControl.DiagramRemoval import do_MadSpin_DRX
        #runName = MADGRAPH_RUN_NAME if not is_gen_from_gridpack() else 'GridRun_'+str(runArgs.randomSeed)
        full_madspin_dir = proc_dir+'/'+madspin_dir
        do_MadSpin_DRX(DR, full_madspin_dir)
        
        # Make sure the code is taken from the madspin directory and not overwritten when you redo the decays
        msfile=proc_dir+'/Cards/madspin_card.dat'
        mstilde=proc_dir+'/Cards/madspin_card.dat~'
        shutil.copyfile(msfile,mstilde)
        with open(msfile,"w") as myfile, open(mstilde,'r') as f:
            for line in f:
                if '#set use_old_dir True' in line:
                    line = line.replace('#','')
                myfile.write(line)
        os.remove(mstilde)

        # hack to be able to redecay the events
        if os.path.isfile(lheFile):
            os.remove(lheFile)
        else:
            raise RuntimeError(lheFile+" not found!")
        if os.path.isfile("madspin_makefile.txt"):
            makefile_hack = "madspin_makefile.txt"
        elif os.path.isfile(runArgs.jobConfig[0]+"/madspin_makefile.txt"):
            makefile_hack = runArgs.jobConfig[0]+"/madspin_makefile.txt"
        else:
            raise RuntimeError("madspin_makefile.txt not found!")
        log.info("------> "+makefile_hack)
        do_hack_MS=subprocess.Popen(["cp", lheFile+".original", lheFile])
        do_hack_MS.communicate()
        do_hack_MS=subprocess.Popen(["rm", lheFile+".original"])
        do_hack_MS.communicate()
        do_hack_MS=subprocess.Popen(["cp", makefile_hack, full_madspin_dir+"/makefile"])
        do_hack_MS.communicate()
        do_hack_MS=subprocess.Popen(["make","clean"], stdout=subprocess.PIPE, cwd=proc_dir)
        do_hack_MS.communicate()
        log.info("----> First Compilation <------")
        do_hack_MS=subprocess.Popen("make", stdout=subprocess.PIPE, cwd=proc_dir) #The first compilation attempt fails, with errors written to the log
        do_hack_MS.communicate()
        log.info("----> Second Compilation <------")
        do_hack_MS=subprocess.Popen("make", stdout=subprocess.PIPE, cwd=proc_dir) #The second compilation attempt works, producing a new tarfile in the process directory
        do_hack_MS.communicate()
        log.info("----> Second Compilation completed <------")
        
        # re-run MadSpin on undecayed lhe files with hack applied
        if os.path.isfile(lheFile):
            madspin_on_lhe(lheFile,madspin_card_loc,runArgs=runArgs,keep_original=False)
        else:
            raise RuntimeError(lheFile+" not found!")
            
        # remove process dir and madspin dir
        shutil.rmtree(proc_dir,ignore_errors=True)
        if os.path.isdir('MGC_LHAPDF/'):
            shutil.rmtree('MGC_LHAPDF/',ignore_errors=True)
        shutil.rmtree(madspin_dir,ignore_errors=True)


############
### Parton Shower generator
############
if not lheOnly:
    if shower == "Py8EG" or shower == "Py8":
        if doEvtGen:
            include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
        else: # Py8, without EvtGen
            include("Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py")
        include("Pythia8_i/Pythia8_aMcAtNlo.py") 
        genSeq.Pythia8.Commands += [
          '25:onMode = off',
          '25:oneChannel = 1 0.5770   100 5 -5', # H -> bbbar
          '25:addChannel = 1 0.0291   100 4 -4', # H -> ccbar
          '25:addChannel = 1 0.000246 100 3 -3', # H -> ssbar
          '25:addChannel = 1 0.00000  100 6 -6', # H -> ttbar
          '25:addChannel = 1 0.000219 100 13 -13', # H -> mumu
          '25:addChannel = 1 0.0632   100 15 -15', # H -> tautau
          '25:addChannel = 1 0.0857   100 21 21', # H -> gluon gluon
          '25:addChannel = 1 0.00228  100 22 22', # H -> gamma gamma
          '25:addChannel = 1 0.00154  100 22 23', # H -> gamma Z
          '25:addChannel = 1 0.0264   100 23 23', # H -> Z Z
          '25:addChannel = 1 0.2150   100 24 -24'] # H -> W W
        if Hdecay == "bb":
            genSeq.Pythia8.Commands += [
                '25:onMode = off', # switch OFF all Higgs decay channels
                '25:onIfMatch = 5 -5' # H -> bbbar
            ]
        elif Hdecay == "gg":
            genSeq.Pythia8.Commands += [
                '25:onMode = off', # switch OFF all Higgs decay channels
                '25:onIfMatch = 22 22' # H -> gamma gamma
            ]
        elif Hdecay == "ML":
            genSeq.Pythia8.Commands += [
                '25:onMode = off', # switch OFF all Higgs decay channels
                '25:onIfMatch = 15 -15', # H -> tautau
                '25:onIfMatch = 23 23', # H -> Z Z
                '25:onIfMatch = 24 -24', # H -> W W
            ]

    else: # H7EG or H7
        include("Herwig7_i/Herwig7_LHEF.py")
        Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118", max_flav=flavour_scheme)
        Herwig7Config.tune_commands()
        if not runArgs.inputGeneratorFile.endswith(".events"):
            Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile+".events", me_pdf_order="NLO")
        else:
            Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
        include("Herwig7_i/Herwig71_EvtGen.py")
        #HW7 settings and Higgs BRs - inclusive Higgs decay
#set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
#set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED 
        Herwig7Config.add_commands("""
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio      0.5770
set /Herwig/Particles/h0/h0->c,cbar;:BranchingRatio      0.0291
set /Herwig/Particles/h0/h0->t,tbar;:BranchingRatio      0.00000
set /Herwig/Particles/h0/h0->mu-,mu+;:BranchingRatio     0.000219
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio   0.0632
set /Herwig/Particles/h0/h0->g,g;:BranchingRatio         0.0857
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio 0.00228
set /Herwig/Particles/h0/h0->Z0,Z0;:BranchingRatio       0.0264
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio       0.2150
decaymode h0->Z0,gamma; 0.00154 1 /Herwig/Decays/Mambo
set /Herwig/Particles/h0/h0->Z0,gamma;:OnOff On
set /Herwig/Particles/h0/h0->Z0,gamma;:BranchingRatio    0.00154
decaymode h0->s,sbar; 0.000246 1 /Herwig/Decays/Hff
set /Herwig/Particles/h0/h0->s,sbar;:OnOff On
set /Herwig/Particles/h0/h0->s,sbar;:BranchingRatio      0.000246
""")
        if Hdecay == "bb": # switch off all decay modes, except H-> bbbar
            Herwig7Config.add_commands("""
set /Herwig/Particles/h0/h0->b,bbar;:OnOff On
set /Herwig/Particles/h0/h0->c,cbar;:OnOff Off
set /Herwig/Particles/h0/h0->t,tbar;:OnOff Off
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff Off
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff Off
set /Herwig/Particles/h0/h0->g,g;:OnOff Off
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff Off
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff Off
set /Herwig/Particles/h0/h0->W+,W-;:OnOff Off
set /Herwig/Particles/h0/h0->Z0,gamma;:OnOff Off
set /Herwig/Particles/h0/h0->s,sbar;:OnOff Off
""")
        elif Hdecay == "gg": # switch off all decay modes, except H-> gamma gamma
            Herwig7Config.add_commands("""
set /Herwig/Particles/h0/h0->b,bbar;:OnOff Off
set /Herwig/Particles/h0/h0->c,cbar;:OnOff Off
set /Herwig/Particles/h0/h0->t,tbar;:OnOff Off
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff Off
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff Off
set /Herwig/Particles/h0/h0->g,g;:OnOff Off
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff On
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff Off
set /Herwig/Particles/h0/h0->W+,W-;:OnOff Off
set /Herwig/Particles/h0/h0->Z0,gamma;:OnOff Off
set /Herwig/Particles/h0/h0->s,sbar;:OnOff Off
""")
        elif Hdecay == "ML": # switch off all decay modes, except H-> tau tau / Z Z / W W
            Herwig7Config.add_commands("""
set /Herwig/Particles/h0/h0->b,bbar;:OnOff Off
set /Herwig/Particles/h0/h0->c,cbar;:OnOff Off
set /Herwig/Particles/h0/h0->t,tbar;:OnOff Off
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff Off
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff On
set /Herwig/Particles/h0/h0->g,g;:OnOff Off
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff Off
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff On
set /Herwig/Particles/h0/h0->W+,W-;:OnOff On
set /Herwig/Particles/h0/h0->Z0,gamma;:OnOff Off
set /Herwig/Particles/h0/h0->s,sbar;:OnOff Off
""")
        # common to all tWH_H7
        Herwig7Config.add_commands("""
# print out Higgs decays modes and branching ratios to log.generate
do /Herwig/Particles/h0:PrintDecayModes""")
        Herwig7Config.run()

    if filtering is not None:
        include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
        filtSeq.MultiElecMuTauFilter.MinPt  = 5000.
        filtSeq.MultiElecMuTauFilter.MaxEta = 10.
        filtSeq.MultiElecMuTauFilter.NLeptons = filtering

# Metadata
evgenConfig.generators += ["aMcAtNlo", "EvtGen"]
evgenConfig.description = "MG5aMCatNLO+MadSpin+Pythia8/Herwig7+EvtGen ttH/tHjb/tHW"
evgenConfig.keywords    = ["SM","top", 'Higgs', 'tHiggs']
evgenConfig.contact = ["tpelzer@cern.ch", 'nello.bruscino@cern.ch']
