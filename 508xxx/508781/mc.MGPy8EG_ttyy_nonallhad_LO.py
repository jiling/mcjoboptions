
import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[260000,90900], # list of pdfs ids for which all variations (error sets) will be included as weights
    'alternative_pdfs':None, # list of pdfs ids for which only the central set will be included as weights
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

from MadGraphControl.MadGraphUtils import *

# General settings
nevents=int(1.1*runArgs.maxEvents)

evgenConfig.inputconfcheck="madevent/"

mode = 0 
gridpack_dir='madevent/'
gridpack_mode=True

defs = """
define l+ = e+ mu+ ta+ 
define vl = ve vm vt
define uc~ = u~ c~
define ds = d s
define l- = e- mu- ta- 
define vl~ = ve~ vm~ vt~
define uc = u c
define ds~ = d~ s~
"""

mcprod = defs+"""
generate p p > t t~ > l+ vl b ds uc~ b~ a a QCD=2 QED=6 \n
add process p p > t t~ > uc ds~ b l- vl~ b~ a a  QCD=2 QED=6 \n
add process p p > t t~ > l+ vl b l- vl~ b~ a a QCD=2 QED=6 """




process = """
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
"""+mcprod+"""
output -f
"""

settings = {'lhe_version'   :'3.0',
			'maxjetflavor'  :5,
			'ptl'           :4.,
			'xptl'          :10.,
			'ptgmin'        :15.,
			'R0gamma'       :0.1,
			'xn'            :2,
			'epsgamma'      :0.1,
			'ptj'           :1.,
			'ptb'           :1.,
			'etal'          :5.0,
			'etaa'          :5.0,
			'dynamical_scale_choice':'3',
			'nevents'       :nevents
}

process_dir = new_process(process)
# Run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


# Print cards
print_cards()
# set up
generate(runArgs=runArgs, process_dir=process_dir, grid_pack=gridpack_mode)
# run
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print ('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print ('Did not see option!')
    else: opts.nprocs = 0
    print (opts)

## pythia shower
keyword=['SM','top', 'ttgammagamma', 'photon']
evgenConfig.keywords += keyword
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_ttgammagamma_nonallhad_GamFromProdAtLO'
evgenConfig.nEventsPerJob = 5000
evgenConfig.contact = ["arpan.ghosal@cern.ch"]

runArgs.inputGeneratorFile=outputDS+".events"
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
