# --------------------------------------------------
# SUSY_SimplifiedModel_PQMSSM.py
#
# Author: Jan Tuzlic Offermann & Benjamin Rosser (UChicago)
#
# This is the contol file (CO) for MC production
# of an axino model. This file gets included by
# joboption (JO) files -- together these configure
# MC generation.
# --------------------------------------------------

include('MadGraphControl/SUSY_SimplifiedModel_PreInclude.py') # "C-style" includes in Python...? Linters hate this!
include('GeneratorFilters/MissingEtFilter.py')

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

# We are using the 4FS (see further below), so to make sure we get the right PDFs we run the following import,
# this should override a similar one called in the PreInclude (which defaults to 5FS sometime since release 21.6.101)
from MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment import *

import numpy as np # Not sure how often this is done in a CO, but we need to actually compute some stuff below!
import ROOT as rt # used for the EvtMultiplierMap class below, used for determining event multiplier

# Some functions/classes to help with setting the event multiplier nicely.
class EvtMultiplierMap:
    """
    This is a handy class for setting the event multiplier.
    It is based on some studies that were performed to determine
    what the event multiplier should be set to for a range of
    points in parameter space for a 100 GeV MET filter.
    This class then allows us to interpolate from those results,
    so if we define new grid points within the domain of the grid points
    that were tested to derive the map, this hopefully allows us to
    interpolate appropriate choices for the event multiplier.
    """

    def __init__(self,default_value=1.2):
        self.reset()
        self.default_value = 1.2
        self.hist = None

    def reset(self):
        self.maxino_values = np.array([])
        self.maxion_values = np.array([])
        self.mn1_values = np.array([])

    def SetMaxinoValues(self,values):
        self.maxino_values = np.sort(np.array(values))

    def SetMaxionValues(self,values):
        self.maxion_values = np.sort(np.array(values))

    def SetMn1Values(self,values):
        self.mn1_values = np.sort(np.array(values))

    def _centers_to_edges(self,bin_centers,allow_negative=True):
        nbins = len(bin_centers)
        edges = []

        for i,center in enumerate(bin_centers):
            if(i != nbins - 1):
                width_up = (bin_centers[i+1] - center) / 2.
            else:
                width_up = (center - bin_centers[i-1]) / 2.

            if (i == 0):
                width_low = (bin_centers[i+1] - center) / 2.
                low = center - width_low
                if(low < 0 and not allow_negative): low = 0
                edges.append(low)
                edges.append(center + width_up)
            else:
                edges.append(center + width_up)
        return np.array(edges)

    def InitializeHistogram(self,default_value = None):
        if(default_value is None): default_value = self.default_value
        n1 = len(self.maxion_values)
        n2 = len(self.mn1_values)
        n3 = len(self.maxino_values)

        e1 = self._centers_to_edges(self.maxion_values)
        e2 = self._centers_to_edges(self.mn1_values)
        e3 = self._centers_to_edges(self.maxino_values)

        self.hist = rt.TH3D("evt_multiplier_map","",n1,e1,n2,e2,n3,e3)
        for i in range(n1):
            for j in range(n2):
                for k in range(n3):
                    self.hist.SetBinContent(i+1,j+1,k+1,default_value)

    def SetHistogramValue(self,key1,key2,key3,val):
        if(self.hist is None): self.InitializeHistogram()

        if(key3 is None):
            for k in self.maxino_values:
                self.SetHistogramValue(key1,key2,k,val)
            return

        if(key2 is None):
            for k in self.mn1_values:
                self.SetHistogramValue(key1,k,key3,val)
            return

        bin = self.hist.FindBin(key1,key2,key3)
        self.hist.SetBinContent(bin,val)

    def SetHistogramValueByIndex(self,i,j,k,val):
        if(self.hist is None): self.InitializeHistogram()
        self.hist.SetBinContent(i+1,j+1,k+1,val) # 1-indexing in histograms

    def GetEventMultiplier(self,maxion,mn1,maxino):
        if(self.hist is None): self.InitializeHistogram()
        result = self.hist.Interpolate(maxion,mn1,maxino)

        # Sometimes interpolation fails and claims to be outside domain, even
        # when it is clearly within the domain. Not sure why the method is
        # failing but in such cases we should just pick the nearest point.
        if(result == 0):
            evgenLog.info('Event multiplier interpolation from map (TH3D) failed. Falling back on simpler method.')
            evgenLog.info('\t(Failure for maxion = {:.2e}, mn1 = {:.2e}, maxino = {:.2e})'.format(maxion,mn1,maxino))
            # print('Event multiplier interpolation from map failed. Falling back on simpler method.')
            b = self.hist.FindBin(maxion,mn1,maxino)
            result = self.hist.GetBinContent(b)
        return result

# --------------------------------------------------
# Fetching parameters via "physics_short"
# --------------------------------------------------
#
# We will get some of the variable parameters (those that change from one
# grid point to another) by parsing the joboption (JO) filtename.
# Jan: This seems like a slightly clunky process (string parsing should be
#      avoided when possible!) but apparently the actual *contents* of the JO
#      files will not be visible via AMI -- where folks will typically browse
#      -- so we want "useful" parameters to be in the JO filename or configured
#      within this CO file. This will make it easy to later figure out what
#      went into the samples' configs. (AMI should probably be modified/updated
#      to optionally just print out the JO contents in another column, but that's
#      an issue for the future)

phys_short = get_physics_short()
evgenLog.info(" >>> Physics short name: " + str(phys_short))

# We fetch the following parameters (and in this order):
# 1) channel (hh vs zz)
# 2) n1 mass    (in GeV)
# 3) axino mass (in GeV)
# 4) axion mass (in mueV) (have to scale to get GeV)
#
# NOTE: We set m_{n2} = m_{n1} + dm, with dm = 2.5 GeV
# NOTE: We read in the n1 and n5 masses in units of 100's of GeV,
#       this is done to keep the physicsShort below 50 characters.
#       (If it is above, it breaks things...)
#
hbar = 6.582119514e-16 # eV * s, which is equivalent to GeV * ns
dm_n1_n2 = 2.5 # GeV
channel = phys_short.split('_')[4]
masses_1000022 = float(phys_short.split('_')[5]) * 1e2 # converting to GeV
masses_1000023 = masses_1000022 + dm_n1_n2
masses_1000045 = float(phys_short.split('_')[6]) * 1e2 # converting to GeV
masses_51 = float(phys_short.split('_')[7]) * 1e-15 # note the scaling from mueV to GeV

# NOTE: The lifetimes are contained within the JO file, not in the physicsShort.
#       This is due (in part) to the fact that the physicsShort apparently has a 50-char limit,
#       which limits its usefulness in this case where we have many parameters to vary per JO.
#       (As a side-note, storing this value in the file lets us keep it with its original precision too!)
#
#       The variables defined in the JO are named "lifetime_1000022" and "lifetime_1000023"
# lifetime_1000022 = float(phys_short.split('_')[8]) # ns
# lifetime_1000023 = float(phys_short.split('_')[9]) # ns

# The widths are actually what we originally calculated (numerical integration w/ MadGraph)
# to get the lifetimes. This might seem roundabout, but the lifetimes are a little easier
# to interpret (i.e. human-readable) so having them present in the JO seems more helpful.
width_1000022 = hbar / lifetime_1000022
width_1000023 = hbar / lifetime_1000023

masses['1000022'] = masses_1000022 # assign n1 mass
masses['1000023'] = masses_1000023 # assign n2 mass
masses['51'] = masses_51 # assign axion mass
masses['1000051'] = masses_1000045 # assign axino mass (to n5, which is almost pure axino + perturbative higgsino mixing)
masses['1000045'] = masses_1000045 # assign axino mass

# Calculate decay constant fa from the axion mass.
decay_constant = 0.00569151 / (masses['51'])

# --------------------------------------------------
# Declaring the MadGraph process.
# --------------------------------------------------

model_process = '''
# exclude a bunch of BSM particles -- we typically set these to a very high mass scale anyway
# so we don't want MG to waste time creating diagrams with these
define exclude = go dl sl b1 dr sr b2 ul cl t1 ur cr t2 dl~ sl~ b1~ dr~ sr~ b2~ ul~ cl~ t1~ ur~ cr~ t2~ sve svm svt h2 h3 h+ el- mul- ta1- ta2- sve~ svm~ svt~ h- el+ mul+ ta1+ er+ mur+ ta2+ n3 n4 x2+ x2-
generate p p > n1 n1 vl vl~ / exclude QED <=4  QCD = 0 @1 # avoid particles in "exclude", and then apply the QED/QCD constraints
add process p p > n1 n1 vl vl~ j / exclude QED <=4 @2
add process p p > n1 n1 vl vl~ j j / exclude QED <=4 @3
'''
njets = 2 # max number of jets in final state -- dictated by the above string and how many "add process" lines we want to add

# Produce the full process string -- this includes the model import.
# TODO: The model path may need to be adjusted in the "import model" line.
process = '''
import model PQMSSM_SARAH
'''
process += model_process
process +='''
output -f
'''

# --------------------------------------------------
# Modified the decay tables.
# --------------------------------------------------
decay_axion = "DECAY  51 " + str(decay_constant) + " # axion"

# Set the n1 and n2 decays.
decay_to_pdg = 25 # Higgs
if(channel.lower() == 'zz'): decay_to_pdg = 23 # Z boson

decay_n1 = """DECAY  1000022   {w:.6e}
#  BR             NDA  ID1    ID2   ...
   1.000000e+00   2    {p}  1000045  # {w:.13e}#
""".format(w=width_1000022,p=decay_to_pdg)
decay_n2 = """DECAY  1000023   {w:.6e}
#  BR             NDA  ID1    ID2   ...
   1.000000e+00   2    {p}  1000045  # {w:.13e}#
""".format(w=width_1000023,p=decay_to_pdg)

# All the modified decays (widths and optionally branching ratios).
# The n1 and n2 decays were fetched above, others may be channel-specific.

decays_modified = {
    '51':decay_axion,
    '1000022': decay_n1, # 100% n1 -> h n5
    '1000023': decay_n2 # 100% n2 -> h n5 (possibly not relevant)
}

if(channel.lower() == 'hh'):
    evgenLog.info(" >>> Setting decay widths and branching ratios for channel: hh.")
    decays_modified['25'] = """DECAY  25   5.745263e-03
#  BR             NDA  ID1  ID2   ...
   1.000000e+00   2    5    -5  # 5.7452630000000e-03#
""" # 100% h -> b bbar

elif(channel.lower() == 'zz'):
    evgenLog.info(" >>> Setting decay widths and branching ratios for channel: zz.")
    decays_modified['23'] = """DECAY  23   2.470751e+00
#  BR             NDA  ID1  ID2   ...
   2.192249e-01   2     -1    1 # 0.5416500582658226
   2.192249e-01   2     -3    3 # 0.5416500582658226
   2.174009e-01   2     -5    5 # 0.5371435947923272
   1.720747e-01   2     -2    2 # 0.42515364433801367
   1.720747e-01   2     -4    4 # 0.42515364433801367
""" # 100% z -> q qbar

else:
    evgenLog.info(" >>> WARNING: Decay channel not recognized: {}".format(channel))

for key,val in decays_modified.items():
    decays[str(key)] = val # str just in case someone makes key an int (which they really are...)

# --------------------------------------------------
# Modify the "HMIX" parameters (related to higgsinos).
# --------------------------------------------------

# HMIX parameters -- mu, alpha and beta.
mu = masses_1000022
alpha = -0.09966865
beta = 1.471128

param_blocks["HMIX"] = {}
param_blocks["HMIX"]['1'] = '{:.6e} # rmu'.format(mu)
param_blocks["HMIX"]['10'] = '{:.16e} # betah'.format(beta)
param_blocks["HMIX"]['11'] = '{:.6e} # alphah'.format(alpha)

# --------------------------------------------------
# Modify the neutralino mixing matrix.
# --------------------------------------------------
#
# This is a 5x5 matrix, some of its terms are dependent
# on the variables from the JO files. Its construction assumes
# that n1 and n2 are purely (up- and down-type) Higgsino, and
# n3 and n4 are purely Wino/Bino. n5 is "purely" axino, with
# some perturbative Higgsino mixing.

neutralino_mixing_matrix = """      1 1 0.000000e+00 # rnn1x1
      1 2 0.000000e+00 # rnn1x2
      1 3 7.071068e-01 # rnn1x3
      1 4 -7.071068e-01 # rnn1x4
      1 5 {:.6e} # rnn1x5
      2 1 0.000000e+00 # rnn2x1
      2 2 0.000000e+00 # rnn2x2
      2 3 7.071068e-01 # rnn2x3
      2 4 7.071068e-01 # rnn2x4
      2 5 {:.6e} # rnn2x5
      3 1 1.000000e+00 # rnn3x1
      3 2 0.000000e+00 # rnn3x2
      3 3 0.000000e+00 # rnn3x3
      3 4 0.000000e+00 # rnn3x4
      3 5 0.000000e+00 # rnn3x5
      4 1 0.000000e+00 # rnn4x1
      4 2 1.000000e+00 # rnn4x2
      4 3 0.000000e+00 # rnn4x3
      4 4 0.000000e+00 # rnn4x4
      4 5 0.000000e+00 # rnn4x5
      5 1 0.000000e+00 # rnn5x1
      5 2 0.000000e+00 # rnn5x2
      5 3 {:.6e} # rnn5x3
      5 4 {:.6e} # rnn5x4
      5 5 1.000000e+00 # rnn5x5
"""

# Here we define a bunch of temporary variables
# for computing the matrix elements.

# Some shorthand to make below more readable.
maxino = masses_1000045
fa = decay_constant

mw = 80.379
mz = 91.1876
aEWM1 = 1.370360e+02
Gf = 1.166390e-05
MWm = np.sqrt(mz**2/2. + np.sqrt(mz**4/4. - (mz**2*np.pi)/(np.sqrt(2)*aEWM1*Gf)))
tw = np.arcsin(np.sqrt(1 - MWm**2/mz**2))
sw = np.sin(tw)
# cw = np.cos(tw)
vev = sw * MWm / np.sqrt(2. * np.pi / aEWM1)
ncolor = 3.
ndw = 3. * ncolor
yaxion = mu * ncolor * np.sqrt(2.) / (fa * ndw)

vy = vev * yaxion
sinb = np.sin(beta)
cosb = np.cos(beta)
ma = maxino
sq2 = np.sqrt(2.)

# These terms are the most important ones.
rnn_5_3 =  vy * (ma * sinb - mu * cosb) / (mu**2 - ma**2)
rnn_5_4 =  vy * (ma * cosb - mu * sinb) / (mu**2 - ma**2)

# These terms are subleading and *probably* could also be set to zero.
rnn_1_5 =  vy * (     sinb -      cosb) / (sq2 * (ma - mu))
rnn_2_5 =  vy * (     sinb +      cosb) / (sq2 * (ma + mu))

nmix_str = neutralino_mixing_matrix.format(rnn_1_5,rnn_2_5,rnn_5_3,rnn_5_4)

evgenLog.info(" >>> Computed neutralino mixing matrix:")
evgenLog.info("------")
evgenLog.info(nmix_str)
evgenLog.info("------")

# There's probably a better way to do this, but convert the NMIX string block into something
# the athena packages can deal with (in order to turn it *back* into the above format).
param_blocks["NMIX"] = {}
for line in nmix_str.split("\n"):
    line = line.split("#")[0].lstrip().rstrip()
    if line != "":
        terms = line.split("  ")
        if len(terms) == 3:
            param_blocks["NMIX"][terms[0].lstrip().rstrip() + " " + terms[1].lstrip().rstrip()] = terms[2]

# --------------------------------------------------
# Enable lifetimes.
# --------------------------------------------------
# Important for studies involving long-lived particles!

add_lifetimes_lhe = True # might be unnecessary
run_settings['time_of_flight'] = 0.1 # run_settings gets passed directly to the run card.

# --------------------------------------------------
# Additional MadGraph/MadGraphControl configs.
# --------------------------------------------------
# Use the 4FS, since we have H->bb~ decay so we do not want massless b-quark approximation. (that would eliminate the Yukawa coupling)
flavourScheme = 4
finalStateB = True # Used with 4FS

# --------------------------------------------------
# Configure MadSpin.
# --------------------------------------------------

# Madspin configuration. This could be made configurable later on,
# to decide which decays to use (e.g. have one joboptions for ZZ vs hh, as is often done
# in centrally produced samples).
evgenLog.info(" >>> Setting MadSpin settings.")

ms_string="""
#
# set seed 1
set Nevents_for_max_weight 200 # number of events for the estimate of the max. weight
# set BW_cut 15                 # cut on how far the particle can be off-shell
# set spinmode onshell          # Use one of the madspin special mode
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event

# specify the decay for the final state particles
# These are the decays contained by a default madspin card. We don't need them for now.
#decay t > w+ b, w+ > all all
#decay t~ > w- b~, w- > all all
#decay w+ > all all
#decay w- > all all
#decay z > all all

{}

# running the actual code
launch
"""

ms_string_insert = """
decay n1 > n5 {a}, {a} > {b} {c}
decay n2 > n5 {a}, {a} > {b} {c}
"""

if(channel.lower() == 'hh'):
    ms_string_insert = ms_string_insert.format(a='h1',b='b',c='b~')
else:
    ms_string_insert = ms_string_insert.format(a='z', b='j',c='j' )
ms_string = ms_string.format(ms_string_insert)

evgenLog.info(" >>> Wrote the following lines for MadSpin config:")
evgenLog.info("-----")
evgenLog.info(ms_string_insert)
evgenLog.info("-----")

madspin_card='madspin_card.dat'
mscard = open(madspin_card,'w')
mscard.write(ms_string)
mscard.close()

# --------------------------------------------------
# Configure the event filter.
# --------------------------------------------------

# Here we can specify filter settings.
# We will also define the event multiplier here, which is used to compensate
# for the filter efficiency -- we have to generate extra events, so that when
# the filter cuts some out, we end up with at least as many as we requested
# in the generation job. Otherwise things crash! (bit of a strange design...)

met_cut_value = 100 # in GeV. This will be our default met filter cut.
filtSeq.MissingEtFilter.METCut = met_cut_value * GeV
filtSeq.MissingEtFilterUpperCut.METCut = 100000 * GeV

evgenLog.info(" >>> Setting MissingEtFilter.METCut = {} (GeV).".format(met_cut_value))

evt_multiplier_map = {} # keys are (m_axion, m_n1, m_axino)

evt_multiplier_map[1.13e-13] = {}


# Special config for certain point(s) that we know have low efficiency.
# Based on producing samples without any MET filter, and looking at the
# efficiency for the above MET cut value. The multipliers are given by
# 1/efficiency, plus some extra just in case.
m_n1    = masses_1000022 # Just a shorthand for use here.
m_axino = masses_1000045 # Just a shorthand for use here.
m_axion = masses_51 # Just a shorthand for use here.

# TODO: The interpolation method we're using with a TH3 seems to
#       sometimes fail, complaining about the interpolation being
#       outside of the domain even though it clearly is not. I don't
#       know if this has something to do with interpolating at the
#       edges of the histogram. We can try to avoid this by adding
#       extra bins in some dimensions, with the understanding that
#       these additional bins are *not* based on any study of
#       efficiencies and might need to be adjusted if we actually
#       want to make more samples in those regions of parameter space.
#
#       Extra Maxion values: 8.0e-14, 1.2e-12
#       Extra Mn1 values: 150, 1050
#       Extra Maxino values: 550

evt_multiplier_maxion_values_orig = [1.13e-13,1.14e-12]
evt_multiplier_maxion_values_extra = [8.0e-14, 1.2e-12]

evt_multiplier_mn1_values_orig = [200.,400.,600.,800.,1000.]
evt_multiplier_mn1_values_extra = [150.,1050.]

evt_multiplier_maxino_values_orig = [0.,100.,200.,300.,400.,500.]
evt_multiplier_maxino_values_extra = [-100.,550.]


evt_multiplier_default = 1.2
evt_multiplier_map = EvtMultiplierMap(evt_multiplier_default)
evt_multiplier_map.SetMaxionValues(np.sort(np.array(evt_multiplier_maxion_values_orig + evt_multiplier_maxion_values_extra)))
evt_multiplier_map.SetMn1Values(np.sort(np.array(evt_multiplier_mn1_values_orig + evt_multiplier_mn1_values_extra)))
evt_multiplier_map.SetMaxinoValues(np.sort(np.array(evt_multiplier_maxino_values_orig + evt_multiplier_maxino_values_extra)))

if(channel.lower() == 'hh'):
    for maxion_tmp in [evt_multiplier_maxion_values_orig[0], evt_multiplier_maxion_values_extra[0]]:
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,200,1.3)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,300,1.3)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,400,1.5)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,800,500,1.3)

    for maxion_tmp in [evt_multiplier_maxion_values_orig[1], evt_multiplier_maxion_values_extra[1]]:
        evt_multiplier_map.SetHistogramValue(maxion_tmp,200,None,5.0)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,400,None,1.6)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,400,0,1.4)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,400,100,1.4)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,300,1.3)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,400,1.5)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,500,1.5)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,800,500,1.3)

        evt_multiplier_map.SetHistogramValue(maxion_tmp,150,None,5.0) # extra
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,550,1.5) # extra
        evt_multiplier_map.SetHistogramValue(maxion_tmp,800,550,1.3) # extra

else: # based on zz channel studies
    for maxion_tmp in [evt_multiplier_maxion_values_orig[0], evt_multiplier_maxion_values_extra[0]]:
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,300,1.3)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,400,1.4)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,800,500,1.3)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,800,500,1.3)

        evt_multiplier_map.SetHistogramValue(maxion_tmp,800,550,1.3) # extra

    for maxion_tmp in [evt_multiplier_maxion_values_orig[1], evt_multiplier_maxion_values_extra[1]]:
        evt_multiplier_map.SetHistogramValue(maxion_tmp,200,None,23.3)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,200,0,3.4)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,400,None,1.3)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,400,200,1.5)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,400,300,9.1)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,300,1.3)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,400,1.4)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,500,5.8)
        evt_multiplier_map.SetHistogramValue(maxion_tmp,800,500,1.4)

        evt_multiplier_map.SetHistogramValue(maxion_tmp,150,0,3.4) # extra
        evt_multiplier_map.SetHistogramValue(maxion_tmp,150,100,23.3) # extra
        evt_multiplier_map.SetHistogramValue(maxion_tmp,600,550,5.8) # extra
        evt_multiplier_map.SetHistogramValue(maxion_tmp,800,550,1.4) # extra
        evt_multiplier_map.SetHistogramValue(maxion_tmp,400,400,9.1) # extra

evt_multiplier = evt_multiplier_map.GetEventMultiplier(m_axion,m_n1,m_axino)

# An extra factor (additive), just in case the above evt_multiplier mapping
# is underestimating the required # of events. This can be an issue since
# the points in the histogram above are set following some study, but I am
# not 100% sure *exactly* how the interpolation is happening so it might
# yield results slightly lower than we want. There's probably a more elegant
# method but this is at more flexible/elegant than endless "if" statements!
evt_multiplier += np.around(np.minimum(0.25, evt_multiplier * 0.2),2)
evgenLog.info(" >>> Set evt_multiplier = {}.".format(evt_multiplier))

# --------------------------------------------------
# Configure "TestHepMC".
# --------------------------------------------------
#
# Allowing for highly-displaced vertices.
testSeq.TestHepMC.MaxVtxDisp = 10000*10000 #in mm
testSeq.TestHepMC.MaxTransVtxDisp = 10000*10000 #in mm
testSeq.TestHepMC.DumpEvent = True

# --------------------------------------------------
# Metadata.
# --------------------------------------------------
evgenConfig.contact  = [ "jan.offermann@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel'] # Axino is not a valid keyword. :(
evgenConfig.description = 'n1n2 pair production with n1/n2 -> axino h decay in simplified PQMSSM model, f_a = %s GeV, m_axino = %s GeV, m_N1 = %s GeV'%(decay_constant,masses['1000045'],masses['1000022'])

include('MadGraphControl/SUSY_SimplifiedModel_PostInclude.py')

# --------------------------------------------------
# Config for simulation (GEANT4 handling of n5).
# --------------------------------------------------
evgenConfig.specialConfig = "NonInteractingPDGCodes=[1000045]"
evgenConfig.specialConfig = "InteractingNonPDGCodes=[1000045]" # dealing with a typo here

# --------------------------------------------------
# Pythia8 ME/PS merging configuration.
# --------------------------------------------------
# Here is where we could tell Pythia e.g. how to do ME/PS merging. For now we just use the defaults.
#genSeq.Pythia8.Commands += ["Merging:Process = pp>{go,1000021}{go,1000021}"]

if(njets > 0): # keeping this conditional just in case we ever change njets above
    genSeq.Pythia8.Commands += ["Merging:Process = guess",
                                "1000024:spinType = 1",
                                "1000022:spinType = 1",
                                "1000023:spinType = 1" ]
    genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
