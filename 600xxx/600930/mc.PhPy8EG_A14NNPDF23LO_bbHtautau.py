#--------------------------------------------------------------
# PowHeg+Pythia bbH with H->tautau. 
# Showered in Pythia from centrally produced evgen files
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15']

evgenConfig.description    = "Powheg+Pythia8 for bbH, H->tautau"
evgenConfig.keywords       = [ "SM", "bbHiggs", "SMHiggs", "mH125" , "2tau" ]
evgenConfig.contact        = ["antonio.de.maria@cern.ch"]
evgenConfig.inputFilesPerJob = 2
evgenConfig.nEventsPerJob    = 90000 #  less than 100k to avoid failures and still multiple of 10k'


