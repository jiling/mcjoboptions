#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125" ]

evgenConfig.description = "POWHEG+Pythia8 H+2j -> aa+2j -> bbtautau+2j production"
evgenConfig.process     = "hSM->aa->bbtautau"
evgenConfig.contact     = [ 'lars.rickard.stroem@cern.ch' ]

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

#--------------------------------------------------------------
# Defining the function to extract parameters
#--------------------------------------------------------------
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
evgenLog.info("phys_short = {:s}".format(phys_short))
data = phys_short.split("_")
tune = data[1]
mass = float(data[5].replace('p', '.'))
mh1 = mass
wh1 = 0.00407
mh2 = mass
wh2 = 0.00407
mh3 = 125.
wh3 = 0.00407
filt = data[6]

# Printing some settings
evgenLog.info("mh1 = %.2f"%mh1)
evgenLog.info("wh1 = %.6f"%wh1)
evgenLog.info("mh2 = %.2f"%mh2)
evgenLog.info("wh2 = %.6f"%wh2)
evgenLog.info("mh3 = %.2f"%mh3)
evgenLog.info("wh3 = %.6f"%wh3)

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
if tune == 'A14NNPDF23':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var1Down':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Down_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var1Up':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Up_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var2Down':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Down_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var2Up':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Up_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3aDown':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aDown_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3aUp':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aUp_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3bDown':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bDown_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3bUp':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bUp_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3cDown':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py')
elif tune == 'A14NNPDF23Var3cUp':
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py')
else:
    evgenLog.error('I do not know this tune. Sorry...')

include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += ["POWHEG:nFinal = 3"]

genSeq.Pythia8.Commands += [
    'Higgs:useBSM = on',
    '35:m0 = %.1f' % mh3,
    '35:mWidth = %.6f' % wh3,
    '35:doForceWidth = on',
]


genSeq.Pythia8.Commands += [
    'HiggsH2:coup2A3H1 = 1',
    '35:oneChannel = 1 1.0 0 25 36',
]
    
genSeq.Pythia8.Commands += [
    '25:oneChannel = 1 1.0 0 5 -5',
    '25:m0 = %.1f' % mh1,
    '25:mWidth = %.6f' % wh1,
    '25:doForceWidth = on',
    '25:mMin = 0',
    '25:tau0 = 0',
]
    
genSeq.Pythia8.Commands += [
    '36:oneChannel = 1 1.0 0 15 -15',
    '36:m0 = %.1f' % mh2,
    '36:mWidth = %.6f' % wh2,
    '36:doForceWidth = on',
    '36:mMin = 0',
    '36:tau0 = 0',
]


if not hasattr(filtSeq, "XtoVVDecayFilter"):
    from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilter
    filtSeq += XtoVVDecayFilter("tauscalarFilter")
    
filtSeq.tauscalarFilter.PDGGrandParent = 36
filtSeq.tauscalarFilter.PDGParent = 15
filtSeq.tauscalarFilter.StatusParent = 2
filtSeq.tauscalarFilter.PDGChild1 = [11,13]

if filt=="LL":
    filtSeq.tauscalarFilter.PDGChild2 = [11,13]
elif filt=="LH":
    filtSeq.tauscalarFilter.PDGChild2 = [111,130,211,221,223,310,311,321,323]

from GeneratorFilters.GeneratorFiltersConf import TauFilter
filtSeq += TauFilter("leptonTriggerRegion")
filtSeq.leptonTriggerRegion.Ntaus    = 1
filtSeq.leptonTriggerRegion.EtaMaxe  = 3
filtSeq.leptonTriggerRegion.EtaMaxmu = 3
filtSeq.leptonTriggerRegion.Ptcute   = 12000
filtSeq.leptonTriggerRegion.Ptcutmu  = 12000
filtSeq.leptonTriggerRegion.Ptcuthad = 13000000
