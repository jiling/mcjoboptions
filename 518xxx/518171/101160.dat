# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 22.11.2021,  12:48
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.52988287E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.61554139E+03  # scale for input parameters
    1   -1.51455084E+02  # M_1
    2    6.08722918E+02  # M_2
    3    3.66255411E+03  # M_3
   11    3.09990779E+02  # A_t
   12    1.86585425E+02  # A_b
   13   -1.48090831E+03  # A_tau
   23    2.30306683E+02  # mu
   25    2.42672396E+01  # tan(beta)
   26    4.63398667E+02  # m_A, pole mass
   31    1.27079780E+03  # M_L11
   32    1.27079780E+03  # M_L22
   33    1.78548993E+03  # M_L33
   34    1.08626299E+03  # M_E11
   35    1.08626299E+03  # M_E22
   36    4.79478253E+02  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.13226843E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.99063902E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.17307169E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.61554139E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.61554139E+03  # (SUSY scale)
  1  1     8.39148780E-06   # Y_u(Q)^DRbar
  2  2     4.26287580E-03   # Y_c(Q)^DRbar
  3  3     1.01375913E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.61554139E+03  # (SUSY scale)
  1  1     4.09203739E-04   # Y_d(Q)^DRbar
  2  2     7.77487104E-03   # Y_s(Q)^DRbar
  3  3     4.05801883E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.61554139E+03  # (SUSY scale)
  1  1     7.14090447E-05   # Y_e(Q)^DRbar
  2  2     1.47651257E-02   # Y_mu(Q)^DRbar
  3  3     2.48325124E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.61554139E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     3.09990781E+02   # A_t(Q)^DRbar
Block Ad Q=  3.61554139E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.86585424E+02   # A_b(Q)^DRbar
Block Ae Q=  3.61554139E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.48090831E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.61554139E+03  # soft SUSY breaking masses at Q
   1   -1.51455084E+02  # M_1
   2    6.08722918E+02  # M_2
   3    3.66255411E+03  # M_3
  21    1.98891885E+05  # M^2_(H,d)
  22    2.23813992E+05  # M^2_(H,u)
  31    1.27079780E+03  # M_(L,11)
  32    1.27079780E+03  # M_(L,22)
  33    1.78548993E+03  # M_(L,33)
  34    1.08626299E+03  # M_(E,11)
  35    1.08626299E+03  # M_(E,22)
  36    4.79478253E+02  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.13226843E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.99063902E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.17307169E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.21575437E+02  # h0
        35     4.63538449E+02  # H0
        36     4.63398667E+02  # A0
        37     4.73598269E+02  # H+
   1000001     1.01008137E+04  # ~d_L
   2000001     1.00759924E+04  # ~d_R
   1000002     1.01004484E+04  # ~u_L
   2000002     1.00788250E+04  # ~u_R
   1000003     1.01008157E+04  # ~s_L
   2000003     1.00759955E+04  # ~s_R
   1000004     1.01004505E+04  # ~c_L
   2000004     1.00788260E+04  # ~c_R
   1000005     2.33149268E+03  # ~b_1
   2000005     4.22797916E+03  # ~b_2
   1000006     3.09058655E+03  # ~t_1
   2000006     4.22966299E+03  # ~t_2
   1000011     1.27519917E+03  # ~e_L-
   2000011     1.09375063E+03  # ~e_R-
   1000012     1.27231192E+03  # ~nu_eL
   1000013     1.27519214E+03  # ~mu_L-
   2000013     1.09373433E+03  # ~mu_R-
   1000014     1.27230503E+03  # ~nu_muL
   1000015     4.83201589E+02  # ~tau_1-
   2000015     1.78698359E+03  # ~tau_2-
   1000016     1.78488654E+03  # ~nu_tauL
   1000021     4.12097935E+03  # ~g
   1000022     1.43711775E+02  # ~chi_10
   1000023     2.39699215E+02  # ~chi_20
   1000025     2.57069166E+02  # ~chi_30
   1000035     6.61177152E+02  # ~chi_40
   1000024     2.40341075E+02  # ~chi_1+
   1000037     6.61427776E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.41687280E-02   # alpha
Block Hmix Q=  3.61554139E+03  # Higgs mixing parameters
   1    2.30306683E+02  # mu
   2    2.42672396E+01  # tan[beta](Q)
   3    2.43064607E+02  # v(Q)
   4    2.14738325E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -1.91525072E-03   # Re[R_st(1,1)]
   1  2     9.99998166E-01   # Re[R_st(1,2)]
   2  1    -9.99998166E-01   # Re[R_st(2,1)]
   2  2    -1.91525072E-03   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.64678770E-04   # Re[R_sb(1,1)]
   1  2     9.99999535E-01   # Re[R_sb(1,2)]
   2  1    -9.99999535E-01   # Re[R_sb(2,1)]
   2  2     9.64678770E-04   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     4.43458125E-03   # Re[R_sta(1,1)]
   1  2     9.99990167E-01   # Re[R_sta(1,2)]
   2  1    -9.99990167E-01   # Re[R_sta(2,1)]
   2  2     4.43458125E-03   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.57352593E-01   # Re[N(1,1)]
   1  2    -1.32881824E-02   # Re[N(1,2)]
   1  3    -2.51901928E-01   # Re[N(1,3)]
   1  4    -1.40871770E-01   # Re[N(1,4)]
   2  1    -8.19930952E-02   # Re[N(2,1)]
   2  2    -1.39113012E-01   # Re[N(2,2)]
   2  3     7.05093810E-01   # Re[N(2,3)]
   2  4    -6.90483470E-01   # Re[N(2,4)]
   3  1     2.76931226E-01   # Re[N(3,1)]
   3  2    -5.89667538E-02   # Re[N(3,2)]
   3  3    -6.60455673E-01   # Re[N(3,3)]
   3  4    -6.95435348E-01   # Re[N(3,4)]
   4  1     7.88932625E-03   # Re[N(4,1)]
   4  2    -9.88430026E-01   # Re[N(4,2)]
   4  3    -5.64485866E-02   # Re[N(4,3)]
   4  4     1.40561017E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -7.99168854E-02   # Re[U(1,1)]
   1  2     9.96801531E-01   # Re[U(1,2)]
   2  1     9.96801531E-01   # Re[U(2,1)]
   2  2     7.99168854E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.99324452E-01   # Re[V(1,1)]
   1  2     9.79933550E-01   # Re[V(1,2)]
   2  1     9.79933550E-01   # Re[V(2,1)]
   2  2     1.99324452E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.27433384E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.22300683E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     6.34831614E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     7.13248663E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.06550979E+01   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.41379374E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.64013499E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.83822501E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.72796859E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.25914906E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.59325044E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.28281211E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.20860457E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.74271540E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     7.15591651E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     8.08128277E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.06601148E+01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.41344188E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.66334242E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     4.04461712E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.72665754E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.25616384E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.59054198E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     2.62097310E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     7.17309655E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     6.77369274E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     8.74265632E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.27526855E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     2.32893016E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.75024283E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     5.59113857E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.21445367E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.44006200E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.98921015E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.99967727E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     1.29261153E-03    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     2.70182574E-02    2     1000015        36   # BR(~tau^-_2 -> ~tau^-_1 A^0)
     1.18489362E-03    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
     2.69827491E-02    2     1000015        35   # BR(~tau^-_2 -> ~tau^-_1 H^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.07037045E+01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.26540232E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     4.42135759E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.06071917E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.74683969E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.86592138E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.35088035E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
DECAY   1000014     1.07086927E+01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.26480569E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     4.41927065E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.05974622E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.74552334E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.91167940E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.34833570E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
DECAY   1000016     2.33308016E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.24810167E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.94957034E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.38240884E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.46986277E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.15672304E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.81756389E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
     2.57570797E-03    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
     5.37546468E-02    2     1000015        37   # BR(~nu_tau -> ~tau^-_1 H^+)
DECAY   2000001     5.57133261E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.25034717E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.73363672E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.89907772E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.87615663E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.59696012E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.00128775E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     7.57123565E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.28072286E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     8.24952535E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.27388039E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.05624408E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.57157707E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.25131965E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     7.82778252E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.89864887E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.87631633E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.59804067E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.01001266E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     7.64780575E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.28058386E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     8.30026374E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.27385317E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.05605984E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.61465645E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.03224608E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.30844552E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.07280118E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.22942504E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.54999839E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.42145795E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.54082799E+02   # ~b_2
#    BR                NDA      ID1      ID2
     8.64271886E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     4.62500528E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     4.03577476E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.12903065E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     5.34684249E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.50585165E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     5.40432689E-03    2     1000021         5   # BR(~b_2 -> ~g b)
     1.22667639E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     7.81650714E-04    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
DECAY   2000002     5.74287077E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.59062508E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.63187950E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.00179792E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.60826345E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.87598939E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.18257662E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.52596685E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.24503849E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.13182618E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.23111636E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.05591593E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.74294423E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.59059201E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.66206487E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.00482282E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.60814172E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.87614888E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.18263159E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.52845988E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.24490541E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.14919125E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.23108918E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.05573164E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.30933282E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.75582046E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.26340346E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.31998498E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     8.74277400E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.57604334E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.76740314E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.53412250E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.50907608E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.68769929E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.69826670E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.23454043E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     9.90186930E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.22094658E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.05058595E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.30751262E-04    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     3.94889869E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     6.55051134E-04    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     3.97785614E-04    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     1.19899552E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99998265E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     5.62735934E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.44848402E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.51190764E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.39781934E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     4.49031717E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     2.44504028E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.30826766E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.62295824E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     9.93516928E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     1.25517382E-04    3     1000024        15       -15   # BR(chi^+_2 -> chi^+_1 tau^- tau^+)
     1.69573905E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.70774708E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.28909967E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     7.46817865E-02   # chi^0_2
#    BR                NDA      ID1      ID2
     9.99954275E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.10728395E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     9.98565067E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
#    BR                NDA      ID1      ID2       ID3
     1.43912241E-04    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     1.43912241E-04    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     1.39942646E-04    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     1.39942646E-04    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
DECAY   1000035     6.43293949E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.22228377E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.22228377E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.56896088E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     3.45179280E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.80275231E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.32002418E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.19771239E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.67432992E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.36993268E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     4.13801592E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     4.42092528E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     3.17129064E-04    3     1000025         5        -5   # BR(chi^0_4 -> chi^0_3 b b_bar)
     1.48329623E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.48329623E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     8.03849521E+01   # ~g
#    BR                NDA      ID1      ID2
     1.43836560E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.43836560E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.50745188E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.50745188E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     1.30987904E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.56632444E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.32968854E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.23954536E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     6.70928809E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     6.67099789E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     1.69770344E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.69770344E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.34517154E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.34517154E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.71044677E-03   # Gamma(h0)
     2.19036708E-03   2        22        22   # BR(h0 -> photon photon)
     1.13756538E-03   2        22        23   # BR(h0 -> photon Z)
     1.85849319E-02   2        23        23   # BR(h0 -> Z Z)
     1.64501223E-01   2       -24        24   # BR(h0 -> W W)
     6.92111734E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.77282253E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.56783307E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.39011356E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.31599683E-07   2        -2         2   # BR(h0 -> Up up)
     2.55385468E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.68710900E-07   2        -1         1   # BR(h0 -> Down down)
     2.41858722E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.44435608E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.64508891E+00   # Gamma(HH)
     5.12835057E-07   2        22        22   # BR(HH -> photon photon)
     3.68040186E-07   2        22        23   # BR(HH -> photon Z)
     6.23403852E-05   2        23        23   # BR(HH -> Z Z)
     1.33842164E-04   2       -24        24   # BR(HH -> W W)
     2.02526827E-04   2        21        21   # BR(HH -> gluon gluon)
     9.70924957E-09   2       -11        11   # BR(HH -> Electron electron)
     4.32054961E-04   2       -13        13   # BR(HH -> Muon muon)
     1.22753148E-01   2       -15        15   # BR(HH -> Tau tau)
     5.15089328E-13   2        -2         2   # BR(HH -> Up up)
     9.98639189E-08   2        -4         4   # BR(HH -> Charm charm)
     2.57430550E-03   2        -6         6   # BR(HH -> Top top)
     8.81544418E-07   2        -1         1   # BR(HH -> Down down)
     3.18853670E-04   2        -3         3   # BR(HH -> Strange strange)
     8.29176322E-01   2        -5         5   # BR(HH -> Bottom bottom)
     5.12235785E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     7.48335023E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.08370357E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.73635834E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     7.01996773E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     7.05553921E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.62976531E+00   # Gamma(A0)
     6.75387577E-07   2        22        22   # BR(A0 -> photon photon)
     5.66446043E-07   2        22        23   # BR(A0 -> photon Z)
     2.32268364E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.65572025E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.29673056E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.22080162E-01   2       -15        15   # BR(A0 -> Tau tau)
     3.93996116E-13   2        -2         2   # BR(A0 -> Up up)
     7.64186933E-08   2        -4         4   # BR(A0 -> Charm charm)
     4.51751495E-03   2        -6         6   # BR(A0 -> Top top)
     8.76696102E-07   2        -1         1   # BR(A0 -> Down down)
     3.17100064E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.24731925E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     9.28067767E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.10962479E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.35288826E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.24256995E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.25087248E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.06772052E-04   2        23        25   # BR(A0 -> Z h0)
     7.83424711E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     3.95761270E+00   # Gamma(Hp)
     1.26659658E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.41509543E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.53165501E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.03071397E-06   2        -1         2   # BR(Hp -> Down up)
     1.69683575E-05   2        -3         2   # BR(Hp -> Strange up)
     1.06057247E-05   2        -5         2   # BR(Hp -> Bottom up)
     5.24305837E-08   2        -1         4   # BR(Hp -> Down charm)
     3.71573588E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.48516022E-03   2        -5         4   # BR(Hp -> Bottom charm)
     4.53402232E-07   2        -1         6   # BR(Hp -> Down top)
     1.02902099E-05   2        -3         6   # BR(Hp -> Strange top)
     7.81571847E-01   2        -5         6   # BR(Hp -> Bottom top)
     6.17188079E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     9.60138993E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.92670785E-06   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.40050329E-04   2        24        25   # BR(Hp -> W h0)
     3.46470695E-08   2        24        35   # BR(Hp -> W HH)
     3.71155723E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.15007178E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.88748846E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.88898918E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99745165E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.95291882E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.69808429E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.15007178E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.88748846E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.88898918E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99991095E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.90548737E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99991095E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.90548737E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25517553E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.22637288E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.20140958E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.90548737E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99991095E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    4.15147100E-04   # BR(b -> s gamma)
    2    1.58906904E-06   # BR(b -> s mu+ mu-)
    3    3.52444649E-05   # BR(b -> s nu nu)
    4    2.61581879E-15   # BR(Bd -> e+ e-)
    5    1.11744801E-10   # BR(Bd -> mu+ mu-)
    6    2.33917740E-08   # BR(Bd -> tau+ tau-)
    7    8.77235859E-14   # BR(Bs -> e+ e-)
    8    3.74754900E-09   # BR(Bs -> mu+ mu-)
    9    7.94885484E-07   # BR(Bs -> tau+ tau-)
   10    8.04861544E-05   # BR(B_u -> tau nu)
   11    8.31391161E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42226078E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93111684E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15895963E-03   # epsilon_K
   17    2.28169491E-15   # Delta(M_K)
   18    2.47925069E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28874209E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.85245759E-15   # Delta(g-2)_electron/2
   21    1.64700495E-10   # Delta(g-2)_muon/2
   22    2.69596127E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.58127424E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.80769416E-01   # C7
     0305 4322   00   2    -1.83745897E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.94392731E-01   # C8
     0305 6321   00   2    -1.95946748E-03   # C8'
 03051111 4133   00   0     1.62297264E+00   # C9 e+e-
 03051111 4133   00   2     1.62332997E+00   # C9 e+e-
 03051111 4233   00   2    -8.41518772E-05   # C9' e+e-
 03051111 4137   00   0    -4.44566474E+00   # C10 e+e-
 03051111 4137   00   2    -4.44312465E+00   # C10 e+e-
 03051111 4237   00   2     6.27588406E-04   # C10' e+e-
 03051313 4133   00   0     1.62297264E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62332925E+00   # C9 mu+mu-
 03051313 4233   00   2    -8.41775799E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44566474E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44312537E+00   # C10 mu+mu-
 03051313 4237   00   2     6.27613822E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50480946E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -1.35834660E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50480946E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -1.35829109E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50480936E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -1.34264480E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85821038E-07   # C7
     0305 4422   00   2     6.13687644E-06   # C7
     0305 4322   00   2     3.60038364E-07   # C7'
     0305 6421   00   0     3.30481235E-07   # C8
     0305 6421   00   2    -2.76147769E-06   # C8
     0305 6321   00   2     1.76008761E-08   # C8'
 03051111 4133   00   2    -9.65779015E-08   # C9 e+e-
 03051111 4233   00   2     2.38772677E-06   # C9' e+e-
 03051111 4137   00   2     5.35997354E-07   # C10 e+e-
 03051111 4237   00   2    -1.77555922E-05   # C10' e+e-
 03051313 4133   00   2    -9.65802395E-08   # C9 mu+mu-
 03051313 4233   00   2     2.38772534E-06   # C9' mu+mu-
 03051313 4137   00   2     5.35999282E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.77555964E-05   # C10' mu+mu-
 03051212 4137   00   2     4.49834098E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     3.84240549E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     4.49834871E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     3.84240549E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     4.07832236E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     3.84241403E-06   # C11' nu_3 nu_3
