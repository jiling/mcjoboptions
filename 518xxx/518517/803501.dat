# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  11:38
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    7.58602145E+00  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.86251483E+03  # scale for input parameters
    1   -4.82426411E+02  # M_1
    2   -1.98375406E+03  # M_2
    3    2.53749881E+03  # M_3
   11    3.21535284E+03  # A_t
   12    5.42901799E+02  # A_b
   13   -2.54514989E+03  # A_tau
   23   -2.51354958E+03  # mu
   25    7.21890599E+00  # tan(beta)
   26    1.33200227E+03  # m_A, pole mass
   31    7.30681530E+02  # M_L11
   32    7.30681530E+02  # M_L22
   33    1.72347854E+03  # M_L33
   34    5.96284990E+02  # M_E11
   35    5.96284990E+02  # M_E22
   36    6.53221444E+02  # M_E33
   41    2.50846307E+03  # M_Q11
   42    2.50846307E+03  # M_Q22
   43    3.67428820E+03  # M_Q33
   44    3.18223177E+03  # M_U11
   45    3.18223177E+03  # M_U22
   46    2.09485208E+03  # M_U33
   47    3.43934111E+03  # M_D11
   48    3.43934111E+03  # M_D22
   49    4.32447156E+02  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.86251483E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.86251483E+03  # (SUSY scale)
  1  1     8.46443464E-06   # Y_u(Q)^DRbar
  2  2     4.29993280E-03   # Y_c(Q)^DRbar
  3  3     1.02257169E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.86251483E+03  # (SUSY scale)
  1  1     1.22786209E-04   # Y_d(Q)^DRbar
  2  2     2.33293796E-03   # Y_s(Q)^DRbar
  3  3     1.21765443E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.86251483E+03  # (SUSY scale)
  1  1     2.14270913E-05   # Y_e(Q)^DRbar
  2  2     4.43044290E-03   # Y_mu(Q)^DRbar
  3  3     7.45127611E-02   # Y_tau(Q)^DRbar
Block Au Q=  2.86251483E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     3.21535284E+03   # A_t(Q)^DRbar
Block Ad Q=  2.86251483E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     5.42901793E+02   # A_b(Q)^DRbar
Block Ae Q=  2.86251483E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -2.54514988E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.86251483E+03  # soft SUSY breaking masses at Q
   1   -4.82426411E+02  # M_1
   2   -1.98375406E+03  # M_2
   3    2.53749881E+03  # M_3
  21   -4.68063975E+06  # M^2_(H,d)
  22   -6.21626526E+06  # M^2_(H,u)
  31    7.30681530E+02  # M_(L,11)
  32    7.30681530E+02  # M_(L,22)
  33    1.72347854E+03  # M_(L,33)
  34    5.96284990E+02  # M_(E,11)
  35    5.96284990E+02  # M_(E,22)
  36    6.53221444E+02  # M_(E,33)
  41    2.50846307E+03  # M_(Q,11)
  42    2.50846307E+03  # M_(Q,22)
  43    3.67428820E+03  # M_(Q,33)
  44    3.18223177E+03  # M_(U,11)
  45    3.18223177E+03  # M_(U,22)
  46    2.09485208E+03  # M_(U,33)
  47    3.43934111E+03  # M_(D,11)
  48    3.43934111E+03  # M_(D,22)
  49    4.32447156E+02  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.19913521E+02  # h0
        35     1.33218393E+03  # H0
        36     1.33200227E+03  # A0
        37     1.33656442E+03  # H+
   1000001     2.63031761E+03  # ~d_L
   2000001     3.51227267E+03  # ~d_R
   1000002     2.62918297E+03  # ~u_L
   2000002     3.26054816E+03  # ~u_R
   1000003     2.63031745E+03  # ~s_L
   2000003     3.51227245E+03  # ~s_R
   1000004     2.62918291E+03  # ~c_L
   2000004     3.26054773E+03  # ~c_R
   1000005     7.06149135E+02  # ~b_1
   2000005     3.71987891E+03  # ~b_2
   1000006     2.19939943E+03  # ~t_1
   2000006     3.72555847E+03  # ~t_2
   1000011     7.68044585E+02  # ~e_L-
   2000011     6.13022823E+02  # ~e_R-
   1000012     7.63779456E+02  # ~nu_eL
   1000013     7.68053428E+02  # ~mu_L-
   2000013     6.13006341E+02  # ~mu_R-
   1000014     7.63777478E+02  # ~nu_muL
   1000015     6.67527747E+02  # ~tau_1-
   2000015     1.74471940E+03  # ~tau_2-
   1000016     1.74251284E+03  # ~nu_tauL
   1000021     2.74722969E+03  # ~g
   1000022     4.73369156E+02  # ~chi_10
   1000023     2.00044950E+03  # ~chi_20
   1000025     2.51001497E+03  # ~chi_30
   1000035     2.51700639E+03  # ~chi_40
   1000024     2.00066406E+03  # ~chi_1+
   1000037     2.51747639E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.32677617E-01   # alpha
Block Hmix Q=  2.86251483E+03  # Higgs mixing parameters
   1   -2.51354958E+03  # mu
   2    7.21890599E+00  # tan[beta](Q)
   3    2.43321776E+02  # v(Q)
   4    1.77423005E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -5.30968489E-02   # Re[R_st(1,1)]
   1  2     9.98589367E-01   # Re[R_st(1,2)]
   2  1    -9.98589367E-01   # Re[R_st(2,1)]
   2  2    -5.30968489E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -3.46353418E-03   # Re[R_sb(1,1)]
   1  2     9.99994002E-01   # Re[R_sb(1,2)]
   2  1    -9.99994002E-01   # Re[R_sb(2,1)]
   2  2    -3.46353418E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.06202418E-02   # Re[R_sta(1,1)]
   1  2     9.99943604E-01   # Re[R_sta(1,2)]
   2  1    -9.99943604E-01   # Re[R_sta(2,1)]
   2  2    -1.06202418E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99803353E-01   # Re[N(1,1)]
   1  2    -4.45121893E-04   # Re[N(1,2)]
   1  3    -1.88795288E-02   # Re[N(1,3)]
   1  4     6.05150179E-03   # Re[N(1,4)]
   2  1    -2.56337880E-03   # Re[N(2,1)]
   2  2    -9.93307388E-01   # Re[N(2,2)]
   2  3    -8.85889607E-02   # Re[N(2,3)]
   2  4     7.40665764E-02   # Re[N(2,4)]
   3  1     9.06340660E-03   # Re[N(3,1)]
   3  2    -1.03361593E-02   # Re[N(3,2)]
   3  3     7.06891441E-01   # Re[N(3,3)]
   3  4     7.07188454E-01   # Re[N(3,4)]
   4  1    -1.74510506E-02   # Re[N(4,1)]
   4  2     1.15036508E-01   # Re[N(4,2)]
   4  3    -7.01498432E-01   # Re[N(4,3)]
   4  4     7.03108820E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.91815058E-01   # Re[U(1,1)]
   1  2    -1.27682775E-01   # Re[U(1,2)]
   2  1    -1.27682775E-01   # Re[U(2,1)]
   2  2     9.91815058E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.94235320E-01   # Re[V(1,1)]
   1  2     1.07220002E-01   # Re[V(1,2)]
   2  1     1.07220002E-01   # Re[V(2,1)]
   2  2    -9.94235320E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.01858784E-01   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     3.70263734E-01   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99999971E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.01765520E-01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     3.70640109E-01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.99024015E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     9.75979350E-04    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     8.28456785E-01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     2.12698401E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.82699513E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     5.88225105E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     5.84779764E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     3.64385514E-01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
DECAY   1000014     3.64382216E-01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
DECAY   1000016     2.13154480E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.82042808E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.17141535E-01    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     4.07826829E-04    3     1000012        15       -11   # BR(~nu_tau -> ~nu_e tau^- e^+)
     4.07829536E-04    3     1000014        15       -13   # BR(~nu_tau -> ~nu_mu tau^- mu^+)
DECAY   1000001     6.42346506E+00   # ~d_L
#    BR                NDA      ID1      ID2
     5.37577911E-02    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     3.15778393E-01    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.78789070E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     6.29868702E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     4.14268060E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
DECAY   2000001     4.36676718E+01   # ~d_R
#    BR                NDA      ID1      ID2
     4.32709878E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.56724744E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000003     6.42345343E+00   # ~s_L
#    BR                NDA      ID1      ID2
     5.37579007E-02    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.15778971E-01    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.78922628E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     6.29866181E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     4.15793292E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
DECAY   2000003     4.36678746E+01   # ~s_R
#    BR                NDA      ID1      ID2
     4.32707871E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.56719830E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000005     1.19546011E-01   # ~b_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
DECAY   2000005     1.43213391E+02   # ~b_2
#    BR                NDA      ID1      ID2
     3.52584427E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     5.69641748E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.14095750E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.57834440E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.13088001E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.58700779E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     4.23671143E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.88856362E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     4.93949533E-02    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     1.31124232E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.75003143E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   1000002     6.42533407E+00   # ~u_L
#    BR                NDA      ID1      ID2
     5.31813531E-02    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.15382498E-01    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.57363936E-04    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     6.30991267E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     2.86442702E-04    2     1000037         1   # BR(~u_L -> chi^+_2 d)
DECAY   2000002     2.86319770E+01   # ~u_R
#    BR                NDA      ID1      ID2
     2.43605492E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     7.56377745E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000004     6.42534008E+00   # ~c_L
#    BR                NDA      ID1      ID2
     5.31812682E-02    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.15382122E-01    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.57647515E-04    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     6.30990498E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     2.86779373E-04    2     1000037         3   # BR(~c_L -> chi^+_2 s)
DECAY   2000004     2.86322469E+01   # ~c_R
#    BR                NDA      ID1      ID2
     2.43603073E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     7.56366212E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000006     4.44039710E+00   # ~t_1
#    BR                NDA      ID1      ID2
     9.91476994E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.47988916E-03    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     6.13203373E-03    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     5.37654397E-04    2     1000005        37   # BR(~t_1 -> ~b_1 H^+)
#    BR                NDA      ID1      ID2       ID3
     2.07136783E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.52241255E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.36438624E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     5.49578635E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     7.39278996E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.21436588E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.05695355E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.35662311E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.00003071E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     2.47329360E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     8.93126525E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.38468131E-02    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     1.45933727E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     2.39667062E-02    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     2.63585599E+01   # chi^+_1
#    BR                NDA      ID1      ID2
     2.38909230E-01    2    -1000011        12   # BR(chi^+_1 -> ~e^+_L nu_e)
     2.38907329E-01    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     1.62730988E-04    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     1.88418291E-02    2    -2000015        16   # BR(chi^+_1 -> ~tau^+_2 nu_tau)
     2.40996732E-01    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     2.40997385E-01    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
     1.92548114E-02    2     1000016       -15   # BR(chi^+_1 -> ~nu_tau tau^+)
     5.55419183E-04    2    -1000005         6   # BR(chi^+_1 -> ~b^*_1 t)
     9.43904102E-04    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
     3.98476790E-04    2          37   1000022   # BR(chi^+_1 -> H^+ chi^0_1)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     2.02456418E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     7.33909555E-03    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     7.33906086E-03    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     5.69663009E-03    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     2.45433476E-03    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     5.18700095E-03    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     5.20663558E-03    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     3.53074356E-03    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     2.12776067E-01    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     4.61093933E-02    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     9.22414907E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.98567291E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     4.23324740E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     1.75424064E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.89760505E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.19343274E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.25453495E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.56239623E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.63605545E+01   # chi^0_2
#    BR                NDA      ID1      ID2
     1.20114065E-01    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     1.20114065E-01    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     1.20113168E-01    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     1.20113168E-01    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     9.46049244E-03    2     2000015       -15   # BR(chi^0_2 -> ~tau^-_2 tau^+)
     9.46049244E-03    2    -2000015        15   # BR(chi^0_2 -> ~tau^+_2 tau^-)
     1.19911432E-01    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     1.19911432E-01    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     1.19911644E-01    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     1.19911644E-01    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     9.56449271E-03    2     1000016       -16   # BR(chi^0_2 -> ~nu_tau nu_bar_tau)
     9.56449271E-03    2    -1000016        16   # BR(chi^0_2 -> ~nu^*_tau nu_tau)
     2.67376920E-04    2     1000005        -5   # BR(chi^0_2 -> ~b_1 b_bar)
     2.67376920E-04    2    -1000005         5   # BR(chi^0_2 -> ~b^*_1 b)
     1.30704988E-04    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     6.96790155E-04    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     2.49383834E-04    2     1000022        35   # BR(chi^0_2 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.06701491E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.90789406E-03    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     2.90789406E-03    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     9.00177532E-04    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     9.00177532E-04    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     8.08123898E-02    2     1000006        -6   # BR(chi^0_3 -> ~t_1 t_bar)
     8.08123898E-02    2    -1000006         6   # BR(chi^0_3 -> ~t^*_1 t)
     2.27623556E-02    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     2.27623556E-02    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     1.69159089E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     1.69159089E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     7.26450795E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.74849607E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     6.82093919E-03    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     3.82657065E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.11454363E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     3.55187477E-02    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     2.37599566E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.71305334E-04    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     2.19065904E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     2.19065904E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.85743706E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.74103360E-03    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     2.74103360E-03    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     1.03271044E-04    2     2000013       -13   # BR(chi^0_4 -> ~mu^-_R mu^+)
     1.03271044E-04    2    -2000013        13   # BR(chi^0_4 -> ~mu^+_R mu^-)
     2.75176048E-03    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     2.75176048E-03    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     3.19878276E-03    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     3.19878276E-03    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     1.92219934E-03    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     1.92219934E-03    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     3.80184242E-03    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     3.80184242E-03    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     3.80184642E-03    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     3.80184642E-03    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     1.25060124E-03    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     1.25060124E-03    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     9.39026748E-02    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     9.39026748E-02    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     2.49592212E-02    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     2.49592212E-02    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     1.90229434E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.90229434E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.12166952E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.54604974E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.31022980E-02    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     7.17074508E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.05316869E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     2.25917829E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.25017770E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     3.61020045E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.69552649E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.69552649E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     8.36418387E+01   # ~g
#    BR                NDA      ID1      ID2
     3.43752820E-03    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     3.43752820E-03    2    -1000002         2   # BR(~g -> ~u^*_L u)
     3.43734131E-03    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     3.43734131E-03    2    -1000004         4   # BR(~g -> ~c^*_L c)
     6.22901194E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     6.22901194E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.37318750E-03    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     3.37318750E-03    2    -1000001         1   # BR(~g -> ~d^*_L d)
     3.37319547E-03    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     3.37319547E-03    2    -1000003         3   # BR(~g -> ~s^*_L s)
     4.24005621E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.24005621E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.37629476E-03   # Gamma(h0)
     2.26682187E-03   2        22        22   # BR(h0 -> photon photon)
     1.05667970E-03   2        22        23   # BR(h0 -> photon Z)
     1.63088491E-02   2        23        23   # BR(h0 -> Z Z)
     1.49221906E-01   2       -24        24   # BR(h0 -> W W)
     7.38099657E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.77722834E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.56978246E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.40925900E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.61827956E-07   2        -2         2   # BR(h0 -> Up up)
     3.13999311E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.74779028E-07   2        -1         1   # BR(h0 -> Down down)
     2.44052359E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.51341383E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     2.49285985E+00   # Gamma(HH)
     1.01552005E-06   2        22        22   # BR(HH -> photon photon)
     2.30035246E-07   2        22        23   # BR(HH -> photon Z)
     5.75816177E-04   2        23        23   # BR(HH -> Z Z)
     5.41542341E-04   2       -24        24   # BR(HH -> W W)
     1.93352221E-04   2        21        21   # BR(HH -> gluon gluon)
     5.17076794E-09   2       -11        11   # BR(HH -> Electron electron)
     2.30169493E-04   2       -13        13   # BR(HH -> Muon muon)
     6.63566230E-02   2       -15        15   # BR(HH -> Tau tau)
     3.21425809E-11   2        -2         2   # BR(HH -> Up up)
     6.23359551E-06   2        -4         4   # BR(HH -> Charm charm)
     4.27994690E-01   2        -6         6   # BR(HH -> Top top)
     4.69006434E-07   2        -1         1   # BR(HH -> Down down)
     1.69651450E-04   2        -3         3   # BR(HH -> Strange strange)
     5.01474672E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.21718645E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.25496835E-03   2        25        25   # BR(HH -> h0 h0)
     3.92793018E-05   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     9.02490805E-12   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     9.02490805E-12   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     3.87822581E-05   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     3.85950203E-07   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     3.85950203E-07   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
     9.14179099E-09   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
DECAY        36     2.46687783E+00   # Gamma(A0)
     1.55064510E-06   2        22        22   # BR(A0 -> photon photon)
     4.24891608E-07   2        22        23   # BR(A0 -> photon Z)
     4.93673889E-04   2        21        21   # BR(A0 -> gluon gluon)
     5.12928727E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.28322766E-04   2       -13        13   # BR(A0 -> Muon muon)
     6.58234937E-02   2       -15        15   # BR(A0 -> Tau tau)
     3.03708620E-11   2        -2         2   # BR(A0 -> Up up)
     5.88971729E-06   2        -4         4   # BR(A0 -> Charm charm)
     4.34745392E-01   2        -6         6   # BR(A0 -> Top top)
     4.65220051E-07   2        -1         1   # BR(A0 -> Down down)
     1.68281911E-04   2        -3         3   # BR(A0 -> Strange strange)
     4.97440588E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.05475471E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     7.85692160E-04   2        23        25   # BR(A0 -> Z h0)
     5.96936550E-36   2        25        25   # BR(A0 -> h0 h0)
     8.69758148E-12   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     8.69758148E-12   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     3.72167469E-07   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     3.72167469E-07   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
DECAY        37     2.30480718E+00   # Gamma(Hp)
     5.51879248E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     2.35945613E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     6.67386477E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.48172861E-07   2        -1         2   # BR(Hp -> Down up)
     7.49044223E-06   2        -3         2   # BR(Hp -> Strange up)
     5.41300223E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.80393229E-07   2        -1         4   # BR(Hp -> Down charm)
     1.67120685E-04   2        -3         4   # BR(Hp -> Strange charm)
     7.58021468E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.76468413E-05   2        -1         6   # BR(Hp -> Down top)
     6.03022766E-04   2        -3         6   # BR(Hp -> Strange top)
     9.30601805E-01   2        -5         6   # BR(Hp -> Bottom top)
     8.52565243E-04   2        24        25   # BR(Hp -> W h0)
     9.50074971E-10   2        24        35   # BR(Hp -> W HH)
     1.16404242E-09   2        24        36   # BR(Hp -> W A0)
     3.70432547E-11   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     1.58493305E-06   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.29486474E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.21831172E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.21126037E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00135310E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.78361166E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.91892158E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.29486474E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.21831172E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.21126037E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99975284E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.47158980E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99975284E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.47158980E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26842215E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.45240878E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.55176212E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.47158980E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99975284E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.42104575E-04   # BR(b -> s gamma)
    2    1.58927671E-06   # BR(b -> s mu+ mu-)
    3    3.52778296E-05   # BR(b -> s nu nu)
    4    2.49395933E-15   # BR(Bd -> e+ e-)
    5    1.06539113E-10   # BR(Bd -> mu+ mu-)
    6    2.23029064E-08   # BR(Bd -> tau+ tau-)
    7    8.41932584E-14   # BR(Bs -> e+ e-)
    8    3.59673337E-09   # BR(Bs -> mu+ mu-)
    9    7.62898288E-07   # BR(Bs -> tau+ tau-)
   10    9.66242363E-05   # BR(B_u -> tau nu)
   11    9.98091369E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42779367E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93930576E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16046774E-03   # epsilon_K
   17    2.28169008E-15   # Delta(M_K)
   18    2.48163316E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29447378E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.42612851E-15   # Delta(g-2)_electron/2
   21    1.03723974E-10   # Delta(g-2)_muon/2
   22    8.03537438E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -7.28697482E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.10542451E-01   # C7
     0305 4322   00   2    -4.14604946E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.22509757E-01   # C8
     0305 6321   00   2    -5.23433168E-04   # C8'
 03051111 4133   00   0     1.61451813E+00   # C9 e+e-
 03051111 4133   00   2     1.61490160E+00   # C9 e+e-
 03051111 4233   00   2    -1.28433098E-06   # C9' e+e-
 03051111 4137   00   0    -4.43721023E+00   # C10 e+e-
 03051111 4137   00   2    -4.43801844E+00   # C10 e+e-
 03051111 4237   00   2     1.11810592E-05   # C10' e+e-
 03051313 4133   00   0     1.61451813E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61490149E+00   # C9 mu+mu-
 03051313 4233   00   2    -1.28433790E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.43721023E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43801855E+00   # C10 mu+mu-
 03051313 4237   00   2     1.11810654E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50552678E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -2.43466672E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50552678E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -2.43466526E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50552701E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -2.43370639E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85835728E-07   # C7
     0305 4422   00   2     1.59169099E-07   # C7
     0305 4322   00   2     7.08286799E-07   # C7'
     0305 6421   00   0     3.30493818E-07   # C8
     0305 6421   00   2     2.94388963E-06   # C8
     0305 6321   00   2     5.37951752E-07   # C8'
 03051111 4133   00   2     3.25893967E-07   # C9 e+e-
 03051111 4233   00   2     6.66649424E-08   # C9' e+e-
 03051111 4137   00   2    -2.14149142E-06   # C10 e+e-
 03051111 4237   00   2    -4.75662501E-07   # C10' e+e-
 03051313 4133   00   2     3.25893129E-07   # C9 mu+mu-
 03051313 4233   00   2     6.66649397E-08   # C9' mu+mu-
 03051313 4137   00   2    -2.14149250E-06   # C10 mu+mu-
 03051313 4237   00   2    -4.75662511E-07   # C10' mu+mu-
 03051212 4137   00   2     4.88700185E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.02954987E-07   # C11' nu_1 nu_1
 03051414 4137   00   2     4.88700198E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.02954987E-07   # C11' nu_2 nu_2
 03051616 4137   00   2     4.92895119E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.02964735E-07   # C11' nu_3 nu_3
