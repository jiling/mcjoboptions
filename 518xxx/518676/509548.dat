# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  13:42
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.35404003E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.27783180E+03  # scale for input parameters
    1   -4.78213184E+02  # M_1
    2   -1.66955908E+03  # M_2
    3    5.20117962E+02  # M_3
   11   -6.99666411E+03  # A_t
   12    3.00772623E+03  # A_b
   13    3.63745413E+03  # A_tau
   23    1.95388753E+03  # mu
   25    4.27746215E+01  # tan(beta)
   26    2.36084328E+02  # m_A, pole mass
   31    1.95304253E+03  # M_L11
   32    1.95304253E+03  # M_L22
   33    5.05905641E+02  # M_L33
   34    1.59500375E+03  # M_E11
   35    1.59500375E+03  # M_E22
   36    1.25252752E+03  # M_E33
   41    3.70401313E+02  # M_Q11
   42    3.70401313E+02  # M_Q22
   43    2.54310387E+03  # M_Q33
   44    3.27829801E+03  # M_U11
   45    3.27829801E+03  # M_U22
   46    2.27751100E+03  # M_U33
   47    3.40885307E+03  # M_D11
   48    3.40885307E+03  # M_D22
   49    1.38549469E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.27783180E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.27783180E+03  # (SUSY scale)
  1  1     8.38666305E-06   # Y_u(Q)^DRbar
  2  2     4.26042483E-03   # Y_c(Q)^DRbar
  3  3     1.01317626E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.27783180E+03  # (SUSY scale)
  1  1     7.20867785E-04   # Y_d(Q)^DRbar
  2  2     1.36964879E-02   # Y_s(Q)^DRbar
  3  3     7.14874955E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.27783180E+03  # (SUSY scale)
  1  1     1.25796700E-04   # Y_e(Q)^DRbar
  2  2     2.60107678E-02   # Y_mu(Q)^DRbar
  3  3     4.37458324E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.27783180E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.99666387E+03   # A_t(Q)^DRbar
Block Ad Q=  2.27783180E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     3.00772677E+03   # A_b(Q)^DRbar
Block Ae Q=  2.27783180E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     3.63745402E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.27783180E+03  # soft SUSY breaking masses at Q
   1   -4.78213184E+02  # M_1
   2   -1.66955908E+03  # M_2
   3    5.20117962E+02  # M_3
  21   -3.69031409E+06  # M^2_(H,d)
  22   -3.75546456E+06  # M^2_(H,u)
  31    1.95304253E+03  # M_(L,11)
  32    1.95304253E+03  # M_(L,22)
  33    5.05905641E+02  # M_(L,33)
  34    1.59500375E+03  # M_(E,11)
  35    1.59500375E+03  # M_(E,22)
  36    1.25252752E+03  # M_(E,33)
  41    3.70401313E+02  # M_(Q,11)
  42    3.70401313E+02  # M_(Q,22)
  43    2.54310387E+03  # M_(Q,33)
  44    3.27829801E+03  # M_(U,11)
  45    3.27829801E+03  # M_(U,22)
  46    2.27751100E+03  # M_(U,33)
  47    3.40885307E+03  # M_(D,11)
  48    3.40885307E+03  # M_(D,22)
  49    1.38549469E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.21058109E+02  # h0
        35     2.34456819E+02  # H0
        36     2.36084328E+02  # A0
        37     2.71128999E+02  # H+
   1000001     4.85809918E+02  # ~d_L
   2000001     3.44742938E+03  # ~d_R
   1000002     4.79808594E+02  # ~u_L
   2000002     3.31521872E+03  # ~u_R
   1000003     4.85812145E+02  # ~s_L
   2000003     3.44742708E+03  # ~s_R
   1000004     4.79812053E+02  # ~c_L
   2000004     3.31521839E+03  # ~c_R
   1000005     1.39000926E+03  # ~b_1
   2000005     2.44674602E+03  # ~b_2
   1000006     2.04141589E+03  # ~t_1
   2000006     2.54162698E+03  # ~t_2
   1000011     1.96729973E+03  # ~e_L-
   2000011     1.59976936E+03  # ~e_R-
   1000012     1.96540224E+03  # ~nu_eL
   1000013     1.96730212E+03  # ~mu_L-
   2000013     1.59973872E+03  # ~mu_R-
   1000014     1.96538981E+03  # ~nu_muL
   1000015     4.88939608E+02  # ~tau_1-
   2000015     1.20316729E+03  # ~tau_2-
   1000016     5.03492373E+02  # ~nu_tauL
   1000021     6.40834411E+02  # ~g
   1000022     4.73039230E+02  # ~chi_10
   1000023     1.65429107E+03  # ~chi_20
   1000025     1.93990752E+03  # ~chi_30
   1000035     1.94988939E+03  # ~chi_40
   1000024     1.65442959E+03  # ~chi_1+
   1000037     1.94990517E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.78418331E-02   # alpha
Block Hmix Q=  2.27783180E+03  # Higgs mixing parameters
   1    1.95388753E+03  # mu
   2    4.27746215E+01  # tan[beta](Q)
   3    2.43479580E+02  # v(Q)
   4    5.57358099E+04  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     4.49264851E-01   # Re[R_st(1,1)]
   1  2     8.93398620E-01   # Re[R_st(1,2)]
   2  1    -8.93398620E-01   # Re[R_st(2,1)]
   2  2     4.49264851E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     4.28538486E-02   # Re[R_sb(1,1)]
   1  2     9.99081352E-01   # Re[R_sb(1,2)]
   2  1    -9.99081352E-01   # Re[R_sb(2,1)]
   2  2     4.28538486E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.92953458E-01   # Re[R_sta(1,1)]
   1  2     1.18504978E-01   # Re[R_sta(1,2)]
   2  1    -1.18504978E-01   # Re[R_sta(2,1)]
   2  2     9.92953458E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99692921E-01   # Re[N(1,1)]
   1  2     1.90763089E-04   # Re[N(1,2)]
   1  3    -2.42185615E-02   # Re[N(1,3)]
   1  4    -5.24289633E-03   # Re[N(1,4)]
   2  1    -3.60794669E-03   # Re[N(2,1)]
   2  2    -9.87920010E-01   # Re[N(2,2)]
   2  3     1.19911712E-01   # Re[N(2,3)]
   2  4     9.80929040E-02   # Re[N(2,4)]
   3  1     1.34110758E-02   # Re[N(3,1)]
   3  2    -1.56248096E-02   # Re[N(3,2)]
   3  3    -7.06785835E-01   # Re[N(3,3)]
   3  4     7.07127847E-01   # Re[N(3,4)]
   4  1    -2.05228857E-02   # Re[N(4,1)]
   4  2     1.54174845E-01   # Re[N(4,2)]
   4  3     6.96769995E-01   # Re[N(4,3)]
   4  4     7.00228893E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.81515519E-01   # Re[U(1,1)]
   1  2     1.91382563E-01   # Re[U(1,2)]
   2  1    -1.91382563E-01   # Re[U(2,1)]
   2  2    -9.81515519E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.87016855E-01   # Re[V(1,1)]
   1  2     1.60616710E-01   # Re[V(1,2)]
   2  1     1.60616710E-01   # Re[V(2,1)]
   2  2    -9.87016855E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     6.68996461E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     4.34906084E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     5.03747942E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.67357076E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.28836963E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     6.68979710E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     4.35332507E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     5.03258498E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.67202110E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.28519111E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     9.56828650E-04    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     2.78037611E-03   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
DECAY   2000015     8.66190661E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     4.92159467E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.05357208E-01    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     3.66684347E-01    2     1000016       -37   # BR(~tau^-_2 -> ~nu_tau H^-)
     5.35388522E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.93219378E-01    2     1000015        36   # BR(~tau^-_2 -> ~tau^-_1 A^0)
     4.48235092E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
     1.87160759E-01    2     1000015        35   # BR(~tau^-_2 -> ~tau^-_1 H^0)
DECAY   1000012     4.33763693E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     5.05189162E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.64784996E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.29986319E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     4.33757189E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     5.05192756E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.64776294E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.29989381E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     8.70570814E-03   # ~nu_tau
#    BR                NDA      ID1      ID2
     9.99866449E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000001     1.82795196E-04   # ~d_L
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> chi^0_1 d)
DECAY   2000001     2.55488753E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.24784889E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92749840E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000003     1.82815050E-04   # ~s_L
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> chi^0_1 s)
DECAY   2000003     2.55502563E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.24747971E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92695418E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000005     6.86728336E+01   # ~b_1
#    BR                NDA      ID1      ID2
     9.02306900E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     9.90976931E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     2.77408005E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.13671416E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.13167735E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     6.19677185E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.01956438E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.03161080E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.41991685E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     6.03898946E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.91135760E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.63692152E-02    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     1.01062144E-02    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     4.59828112E-02    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     1.63021219E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     4.70198308E-02    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   1000002     5.24137328E-05   # ~u_L
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> chi^0_1 u)
DECAY   2000002     2.49593725E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.84490047E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.71543126E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000004     5.13069928E-05   # ~c_L
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> chi^0_1 c)
DECAY   2000004     2.49595441E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.84487960E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.71535958E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000006     1.45245634E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.24106067E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     6.05616336E-04    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     8.67347721E-04    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.05583006E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     9.27964829E-01    2     1000021         6   # BR(~t_1 -> ~g t)
     2.22570896E-03    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
     4.38596325E-02    2     1000005        37   # BR(~t_1 -> ~b_1 H^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.88310904E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.64942908E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.31409425E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.80895894E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.53120363E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.62527886E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.54648832E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.85836429E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.94011634E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.81187861E-03    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     6.36551381E-02    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     1.32520403E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.39477921E-02    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     8.58657339E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     4.05179289E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     8.30275515E+01   # chi^+_1
#    BR                NDA      ID1      ID2
     7.15304960E-02    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     7.14507267E-02    2     1000016       -15   # BR(chi^+_1 -> ~nu_tau tau^+)
     2.15024410E-01    2     1000002        -1   # BR(chi^+_1 -> ~u_L d_bar)
     2.15027043E-01    2     1000004        -3   # BR(chi^+_1 -> ~c_L s_bar)
     2.11652205E-01    2    -1000001         2   # BR(chi^+_1 -> ~d^*_L u)
     2.11652791E-01    2    -1000003         4   # BR(chi^+_1 -> ~s^*_L c)
     1.15674994E-03    2    -1000005         6   # BR(chi^+_1 -> ~b^*_1 t)
     3.63291317E-04    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
     7.42784325E-04    2          37   1000022   # BR(chi^+_1 -> H^+ chi^0_1)
#    BR                NDA      ID1      ID2       ID3
     5.15331758E-04    3     1000021        -1         2   # BR(chi^+_1 -> ~g d_bar u)
     8.31680110E-04    3     1000021        -3         4   # BR(chi^+_1 -> ~g s_bar c)
DECAY   1000037     2.56114526E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     3.79389860E-03    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     5.65925873E-02    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     1.29271189E-01    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     2.28903388E-02    2     1000002        -1   # BR(chi^+_2 -> ~u_L d_bar)
     2.32522652E-02    2     1000004        -3   # BR(chi^+_2 -> ~c_L s_bar)
     3.23926220E-02    2    -1000001         2   # BR(chi^+_2 -> ~d^*_L u)
     3.24249507E-02    2    -1000003         4   # BR(chi^+_2 -> ~s^*_L c)
     2.68771362E-01    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     4.84038179E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     9.24300121E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     4.19238216E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     3.04956663E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     5.64442980E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     5.01926948E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     5.85580241E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     4.68493459E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     2.00687175E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.86760238E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.05094353E-04    3     1000021        -1         2   # BR(chi^+_2 -> ~g d_bar u)
     1.62768411E-04    3     1000021        -3         4   # BR(chi^+_2 -> ~g s_bar c)
     2.79258725E-03    3     1000021        -5         6   # BR(chi^+_2 -> ~g b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     8.36621663E+01   # chi^0_2
#    BR                NDA      ID1      ID2
     3.64225304E-02    2     1000015       -15   # BR(chi^0_2 -> ~tau^-_1 tau^+)
     3.64225304E-02    2    -1000015        15   # BR(chi^0_2 -> ~tau^+_1 tau^-)
     3.48313256E-02    2     1000016       -16   # BR(chi^0_2 -> ~nu_tau nu_bar_tau)
     3.48313256E-02    2    -1000016        16   # BR(chi^0_2 -> ~nu^*_tau nu_tau)
     1.07018847E-01    2     1000002        -2   # BR(chi^0_2 -> ~u_L u_bar)
     1.07018847E-01    2    -1000002         2   # BR(chi^0_2 -> ~u^*_L u)
     1.07018315E-01    2     1000004        -4   # BR(chi^0_2 -> ~c_L c_bar)
     1.07018315E-01    2    -1000004         4   # BR(chi^0_2 -> ~c^*_L c)
     1.06247817E-01    2     1000001        -1   # BR(chi^0_2 -> ~d_L d_bar)
     1.06247817E-01    2    -1000001         1   # BR(chi^0_2 -> ~d^*_L d)
     1.06249053E-01    2     1000003        -3   # BR(chi^0_2 -> ~s_L s_bar)
     1.06249053E-01    2    -1000003         3   # BR(chi^0_2 -> ~s^*_L s)
     5.69096989E-04    2     1000005        -5   # BR(chi^0_2 -> ~b_1 b_bar)
     5.69096989E-04    2    -1000005         5   # BR(chi^0_2 -> ~b^*_1 b)
     1.42751707E-04    2     1000022        36   # BR(chi^0_2 -> chi^0_1 A^0)
     1.85851283E-04    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     5.21881669E-04    2     1000022        35   # BR(chi^0_2 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     5.69515447E-04    3     1000021         2        -2   # BR(chi^0_2 -> ~g u u_bar)
     5.69499240E-04    3     1000021         4        -4   # BR(chi^0_2 -> ~g c c_bar)
     5.77926428E-04    3     1000021         1        -1   # BR(chi^0_2 -> ~g d d_bar)
     5.77935036E-04    3     1000021         3        -3   # BR(chi^0_2 -> ~g s s_bar)
DECAY   1000025     2.30707682E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.95914330E-02    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     6.95914330E-02    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     3.05109576E-02    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     3.05109576E-02    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     1.05688254E-04    2     1000004        -4   # BR(chi^0_3 -> ~c_L c_bar)
     1.05688254E-04    2    -1000004         4   # BR(chi^0_3 -> ~c^*_L c)
     1.58816162E-04    2     1000001        -1   # BR(chi^0_3 -> ~d_L d_bar)
     1.58816162E-04    2    -1000001         1   # BR(chi^0_3 -> ~d^*_L d)
     3.64780876E-04    2     1000003        -3   # BR(chi^0_3 -> ~s_L s_bar)
     3.64780876E-04    2    -1000003         3   # BR(chi^0_3 -> ~s^*_L s)
     1.51491890E-01    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     1.51491890E-01    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     6.03626522E-02    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     6.03626522E-02    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     2.71916169E-02    2          37  -1000024   # BR(chi^0_3 -> H^+ chi^-_1)
     2.71916169E-02    2         -37   1000024   # BR(chi^0_3 -> H^- chi^+_1)
     3.89833696E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.91556105E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.41701247E-02    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     4.18186024E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     8.39205130E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     3.36887233E-02    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
     4.28212251E-02    2     1000023        35   # BR(chi^0_3 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.12705469E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.17814699E-04    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     1.17814699E-04    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
     3.69886133E-03    3     1000021         6        -6   # BR(chi^0_3 -> ~g t t_bar)
     2.76181552E-04    3     1000021         5        -5   # BR(chi^0_3 -> ~g b b_bar)
DECAY   1000035     2.27302373E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     6.87740611E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     6.87740611E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     3.22525159E-02    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     3.22525159E-02    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     4.48478609E-03    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     4.48478609E-03    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     1.13249802E-02    2     1000002        -2   # BR(chi^0_4 -> ~u_L u_bar)
     1.13249802E-02    2    -1000002         2   # BR(chi^0_4 -> ~u^*_L u)
     1.13463893E-02    2     1000004        -4   # BR(chi^0_4 -> ~c_L c_bar)
     1.13463893E-02    2    -1000004         4   # BR(chi^0_4 -> ~c^*_L c)
     1.24117490E-02    2     1000001        -1   # BR(chi^0_4 -> ~d_L d_bar)
     1.24117490E-02    2    -1000001         1   # BR(chi^0_4 -> ~d^*_L d)
     1.26159329E-02    2     1000003        -3   # BR(chi^0_4 -> ~s_L s_bar)
     1.26159329E-02    2    -1000003         3   # BR(chi^0_4 -> ~s^*_L s)
     1.51687314E-01    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     1.51687314E-01    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     4.57968939E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     4.57968939E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.35947963E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     3.35947963E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     1.61830784E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.10096800E-04    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.26057718E-02    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.12134364E-04    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     3.43661437E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     6.77501495E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     3.97256623E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     5.33939156E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.20254927E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.17354867E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.17354867E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
     4.25252296E-03    3     1000021         6        -6   # BR(chi^0_4 -> ~g t t_bar)
     1.00182245E-04    3     1000021         3        -3   # BR(chi^0_4 -> ~g s s_bar)
     2.97697965E-04    3     1000021         5        -5   # BR(chi^0_4 -> ~g b b_bar)
DECAY   1000021     1.41868635E+01   # ~g
#    BR                NDA      ID1      ID2
     1.29081334E-01    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     1.29081334E-01    2    -1000002         2   # BR(~g -> ~u^*_L u)
     1.29073642E-01    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     1.29073642E-01    2    -1000004         4   # BR(~g -> ~c^*_L c)
     1.20923905E-01    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     1.20923905E-01    2    -1000001         1   # BR(~g -> ~d^*_L d)
     1.20920892E-01    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     1.20920892E-01    2    -1000003         3   # BR(~g -> ~s^*_L s)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     1.29022188E-02   # Gamma(h0)
     6.33410131E-04   2        22        22   # BR(h0 -> photon photon)
     3.11522481E-04   2        22        23   # BR(h0 -> photon Z)
     4.98393252E-03   2        23        23   # BR(h0 -> Z Z)
     4.45761471E-02   2       -24        24   # BR(h0 -> W W)
     1.75231370E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.44880911E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.86851734E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.86646337E-02   2       -15        15   # BR(h0 -> Tau tau)
     4.23549658E-08   2        -2         2   # BR(h0 -> Up up)
     8.21756710E-03   2        -4         4   # BR(h0 -> Charm charm)
     5.91364666E-07   2        -1         1   # BR(h0 -> Down down)
     2.14070099E-04   2        -3         3   # BR(h0 -> Strange strange)
     8.44588088E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     8.37690048E+00   # Gamma(HH)
     2.39225982E-07   2        22        22   # BR(HH -> photon photon)
     4.66227776E-08   2        22        23   # BR(HH -> photon Z)
     6.55915532E-05   2        23        23   # BR(HH -> Z Z)
     1.60293055E-04   2       -24        24   # BR(HH -> W W)
     4.60757123E-04   2        21        21   # BR(HH -> gluon gluon)
     7.24992634E-09   2       -11        11   # BR(HH -> Electron electron)
     3.22550685E-04   2       -13        13   # BR(HH -> Muon muon)
     8.44502526E-02   2       -15        15   # BR(HH -> Tau tau)
     2.09981715E-13   2        -2         2   # BR(HH -> Up up)
     4.07307054E-08   2        -4         4   # BR(HH -> Charm charm)
     4.64768798E-07   2        -1         1   # BR(HH -> Down down)
     1.68376288E-04   2        -3         3   # BR(HH -> Strange strange)
     9.14371380E-01   2        -5         5   # BR(HH -> Bottom bottom)
DECAY        36     8.44940375E+00   # Gamma(A0)
     2.22777693E-07   2        22        22   # BR(A0 -> photon photon)
     3.50968787E-08   2        22        23   # BR(A0 -> photon Z)
     4.57435675E-04   2        21        21   # BR(A0 -> gluon gluon)
     7.25864440E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.22939481E-04   2       -13        13   # BR(A0 -> Muon muon)
     8.45771814E-02   2       -15        15   # BR(A0 -> Tau tau)
     2.35351817E-14   2        -2         2   # BR(A0 -> Up up)
     4.50942159E-09   2        -4         4   # BR(A0 -> Charm charm)
     4.64989622E-07   2        -1         1   # BR(A0 -> Down down)
     1.68455752E-04   2        -3         3   # BR(A0 -> Strange strange)
     9.14447333E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.59196812E-05   2        23        25   # BR(A0 -> Z h0)
     1.46946037E-12   2        23        35   # BR(A0 -> Z HH)
DECAY        37     3.98647657E+00   # Gamma(Hp)
     2.13222104E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     9.11590815E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     2.57827826E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     9.93884082E-07   2        -1         2   # BR(Hp -> Down up)
     1.62184538E-05   2        -3         2   # BR(Hp -> Strange up)
     2.14375377E-05   2        -5         2   # BR(Hp -> Bottom up)
     4.59727558E-08   2        -1         4   # BR(Hp -> Down charm)
     3.58455680E-04   2        -3         4   # BR(Hp -> Strange charm)
     3.00189149E-03   2        -5         4   # BR(Hp -> Bottom charm)
     4.38682179E-08   2        -1         6   # BR(Hp -> Down top)
     1.13668341E-06   2        -3         6   # BR(Hp -> Strange top)
     7.37528704E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.94206915E-04   2        24        25   # BR(Hp -> W h0)
     2.08437651E-05   2        24        35   # BR(Hp -> W HH)
     1.65831500E-05   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    4.18691267E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.82648133E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.82966824E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.98258202E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.28834527E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    5.46547169E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    4.18691267E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.82648133E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.82966824E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99401449E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.98550965E-04        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99401449E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.98550965E-04        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.11722155E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.85093562E+00        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    8.28308819E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.98550965E-04        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99401449E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    4.67645048E-04   # BR(b -> s gamma)
    2    1.58214652E-06   # BR(b -> s mu+ mu-)
    3    3.51697020E-05   # BR(b -> s nu nu)
    4    3.60474761E-15   # BR(Bd -> e+ e-)
    5    1.53971839E-10   # BR(Bd -> mu+ mu-)
    6    3.11467890E-08   # BR(Bd -> tau+ tau-)
    7    8.99831945E-14   # BR(Bs -> e+ e-)
    8    3.84380442E-09   # BR(Bs -> mu+ mu-)
    9    7.99412892E-07   # BR(Bs -> tau+ tau-)
   10    3.61126835E-05   # BR(B_u -> tau nu)
   11    3.73030195E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41735965E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.91683491E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15669423E-03   # epsilon_K
   17    2.28231974E-15   # Delta(M_K)
   18    2.47983641E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.23608905E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.16968652E-15   # Delta(g-2)_electron/2
   21   -1.35511067E-10   # Delta(g-2)_muon/2
   22   -1.52461872E-07   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    2.44654496E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.89096884E-01   # C7
     0305 4322   00   2     4.04725756E-04   # C7'
     0305 6421   00   0    -9.52278349E-02   # C8
     0305 6421   00   2    -6.51055321E-01   # C8
     0305 6321   00   2    -2.02671665E-03   # C8'
 03051111 4133   00   0     1.60729573E+00   # C9 e+e-
 03051111 4133   00   2     1.60777233E+00   # C9 e+e-
 03051111 4233   00   2     1.41476064E-05   # C9' e+e-
 03051111 4137   00   0    -4.42998783E+00   # C10 e+e-
 03051111 4137   00   2    -4.42020061E+00   # C10 e+e-
 03051111 4237   00   2    -1.08675718E-04   # C10' e+e-
 03051313 4133   00   0     1.60729573E+00   # C9 mu+mu-
 03051313 4133   00   2     1.60776927E+00   # C9 mu+mu-
 03051313 4233   00   2     1.29830773E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.42998783E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.42020367E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.07515211E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50323439E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.35985323E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50323439E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.38516957E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50319074E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     9.51986573E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85935164E-07   # C7
     0305 4422   00   2     1.79957789E-03   # C7
     0305 4322   00   2     6.08334635E-05   # C7'
     0305 6421   00   0     3.30578991E-07   # C8
     0305 6421   00   2    -7.97533376E-03   # C8
     0305 6321   00   2     1.43769912E-05   # C8'
 03051111 4133   00   2    -7.12963528E-06   # C9 e+e-
 03051111 4233   00   2     1.96746049E-05   # C9' e+e-
 03051111 4137   00   2     2.46793450E-04   # C10 e+e-
 03051111 4237   00   2    -1.49907903E-04   # C10' e+e-
 03051313 4133   00   2    -7.12908140E-06   # C9 mu+mu-
 03051313 4233   00   2     1.96745764E-05   # C9' mu+mu-
 03051313 4137   00   2     2.46794000E-04   # C10 mu+mu-
 03051313 4237   00   2    -1.49907996E-04   # C10' mu+mu-
 03051212 4137   00   2    -5.34071640E-05   # C11 nu_1 nu_1
 03051212 4237   00   2     3.25575661E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.34071659E-05   # C11 nu_2 nu_2
 03051414 4237   00   2     3.25575661E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -5.41872317E-05   # C11 nu_3 nu_3
 03051616 4237   00   2     3.25576356E-05   # C11' nu_3 nu_3
