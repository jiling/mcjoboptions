#### Shower 
evgenConfig.description = 'MadGraph_Wenu from LHE files'
evgenConfig.keywords+=['W','jets']
evgenConfig.contact  = [ "jens.roggel@cern.ch" ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

